/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

package BeSpaceDCore

/**
 * @author keith 
 */


class CoreDefinitionsSpec extends UnitSpec {

  val core = standardDefinitions; import core._

  
  
  //--------------------------------------- Simplify

  // These are just some arbitrary complex invariants that mean nothing for testing purposes
  val anything = BIGOR(List(AND(OccupyNode(3), ComponentState("any")), IMPLIES(TimePoint(2345), OccupyPoint(1,2))))
  val something = BIGAND(List(IMPLIES(TimePoint(2345), ComponentState("any")), OR(OccupyNode(44), OccupyPoint(1,2))))
  
  
  // AND
  "SimplifyInvariant" should "reduce a BIGAND of TRUEs to TRUE" in
    {
      val Bigand = BIGAND(List(TRUE(), TRUE(), TRUE(), TRUE(), TRUE(), TRUE(), TRUE()))

      val result = simplifyInvariant(Bigand)

      assertResult(expected = TRUE())(actual = result )
    }
  
  it should "reduce a BIGAND with one FALSE to FALSE" in
    {
      val Bigand = BIGAND(List(TRUE(), TRUE(), TRUE(), TRUE(), FALSE(), TRUE(), TRUE()))

      val result = simplifyInvariant(Bigand)

      assertResult(expected = FALSE())(actual = result )
    }

  it should "reduce an AND with two TRUEs to TRUE" in
    {
      val And = AND(TRUE(), TRUE())

      val result = simplifyInvariant(And)

      assertResult(expected = TRUE())(actual = result )
    }

  it should "reduce an AND with one FALSE to FALSE" in
    {
      val And = AND(anything, FALSE())

      val result = simplifyInvariant(And)

      assertResult(expected = FALSE())(actual = result )
    }

  // OR
  it should "reduce a BIGOR of FALSEs to FALSE" in
    {
      val Bigor = BIGOR(List(FALSE(), FALSE(), FALSE(), FALSE(), FALSE(), FALSE(), FALSE()))

      val result = simplifyInvariant(Bigor)

      assertResult(expected = FALSE())(actual = result )
    }
  
  it should "reduce a BIGOR with one TRUE to TRUE" in
    {
      val Bigor = BIGOR(List(anything, FALSE(), TRUE(), anything, anything, FALSE(), FALSE()))

      val result = simplifyInvariant(Bigor)

      assertResult(expected = TRUE())(actual = result )
    }

  it should "reduce an OR with two FALSEs to FALSE" in
    {
      val Or = OR(FALSE(), FALSE())

      val result = simplifyInvariant(Or)

      assertResult(expected = FALSE())(actual = result )
    }

  it should "reduce an OR with one TRUE to TRUE" in
    {
      val Or = OR(anything, TRUE())

      val result = simplifyInvariant(Or)

      assertResult(expected = TRUE())(actual = result )
    }

  // IMPLIES
  it should "reduce an IMPLIES with a TRUE premise to the conclusion" in
    {
      val implication = IMPLIES(TRUE(), something)

      val result = simplifyInvariant(implication)

      assertResult(expected = simplifyInvariant(something))(actual = result )
    }

  it should "reduce an IMPLIES with a FALSE premise to TRUE" in
    {
      val implication = IMPLIES(FALSE(), anything)

      val result = simplifyInvariant(implication)

      assertResult(expected = TRUE())(actual = result )
    }

  
  
  //--------------------------------------- unfoldInvariant

  "unfoldInvariant" should "expand an OccupyBox into multiple points" in
  {
    val box = OccupyBox(100, 200, 104, 204)
    
    // Expecting 16 points : (100..104, 200..204)
    val xRange = (100 to 104).toList
    val yRange = (200 to 204).toList
    
    val expectedPoints: List[(Int,Int)] = for { x <- xRange; y <- yRange } yield (x, y)
    val expectedInvariants = expectedPoints map { t => OccupyPoint(t._1, t._2) }
    val expectedInvariant = BIGAND(expectedInvariants)
    
    val actualResult = unfoldInvariant(box)
    
    assertResult(expected = order(expectedInvariant))(actual = order(simplifyInvariant(actualResult)))
  }

  //collisionTestsBig
  "CollisionTestsBig" should "not detect a collision between OccupyBox(13,55,27,26) and OccupyPoint(24,53), because of descending definitions" in
  {
    val boxA : List[Invariant] = unfoldInvariant(OccupyPoint(24,53))::Nil
    val boxB : List[Invariant] = unfoldInvariant(OccupyBox(13,55,27,26))::Nil 
    
    val result : Boolean = collisionTestsBig(boxA, boxB)
    
    assertResult(expected = false)(actual = result)
  }
  
  it should "detect a collision between OccupyBox(15,12,80,90) and OccupyBox(10,11,18,20)" in
  {
    val boxA : List[Invariant] = unfoldInvariant(OccupyBox(15,12,80,90))::Nil
    val boxB : List[Invariant] = unfoldInvariant(OccupyBox(10,11,18,20))::Nil
    
    val result : Boolean = collisionTestsBig(boxA, boxB)
    
    assertResult(expected = true)(actual = result)
  }
  it should "not detect a collision between OccupyBox(30,12,80,90) and OccupyBox(10,11,18,20)" in
  {
    val boxA : List[Invariant] = unfoldInvariant(OccupyBox(30,12,80,90))::Nil
    val boxB : List[Invariant] = unfoldInvariant(OccupyBox(10,11,18,20))::Nil
    
    val result : Boolean = collisionTestsBig(boxA, boxB)
    
    assertResult(expected = false)(actual = result)
  }
  it should "not detect a collision between List(OccupyBox(18,5,32,34), OccupyBox(13,55,27,26), OccupyBox(54,20,17,29), OccupyBox(3,46,17,50), OccupyBox(34,14,6,41), OccupyBox(55,0,50,34)) and OccupyPoint(24,53), because of descending definitions" in
  {
    val boxA : List[Invariant] = unfoldInvariant(OccupyPoint(24,53))::Nil
    val boxB : List[Invariant] = List(OccupyBox(18,5,32,34), OccupyBox(13,55,27,26), OccupyBox(54,20,17,29), OccupyBox(3,46,17,50), OccupyBox(34,14,6,41), OccupyBox(55,0,50,34)).map(x => unfoldInvariant(x))
    
    val result : Boolean = collisionTestsBig(boxA, boxB)
    
    assertResult(expected = false)(actual = result)
  }
  
  it should "detect a collision between OccupyBox(13,26,27,55) and OccupyPoint(24,53)" in
  {
    val boxA : List[Invariant] = unfoldInvariant(OccupyPoint(24,53))::Nil
    val boxB : List[Invariant] = unfoldInvariant(OccupyBox(13,26,27,55))::Nil 
    
    val result : Boolean = collisionTestsBig(boxA, boxB)
    
    assertResult(expected = true)(actual = result)
  }
}