package BeSpaceDCore.SpatioTemporal

/**
 * Created by keith on 11/01/16.
 */

import BeSpaceDCore.Log._
import BeSpaceDCore._

object FoldSpace {

  val core = standardDefinitions; import core._

  // ----------------------------------------- Within Area
   def withinArea(x: Int, y: Int, area: OccupyBox): Boolean =
   {
     x >= area.x1 && x <= area.x2 &&
       y >= area.y1 && y <= area.y2
   }
   def withinArea(point: OccupyPoint, area: OccupyBox): Boolean = withinArea(point.x, point.y, area)



   // ----------------------------------------- Filter Area

   def filterByArea(area: OccupyBox, defaultReturn: Invariant)(invariant: Invariant): Invariant =
   {
     val filterFalse = filterByArea(area, FALSE()) _
     val filterTrue  = filterByArea(area, TRUE()) _
     val filter      = filterByArea(area, defaultReturn) _

     val result = invariant match
     {
       // OccupyPoint
       case point: OccupyPoint => if (withinArea(point, area)) point else defaultReturn

       // OccupyBox
       case box: OccupyBox => filter(unfoldInvariant(box))

       // IMPLIES with POINT
       case IMPLIES(point: OccupyPoint, conclusion: Invariant) =>
         if (withinArea(point, area))
         {
           IMPLIES(point, filter(conclusion))
         }
         else
         {
           defaultReturn
         }

       case IMPLIES(premise: Invariant, point: OccupyPoint) =>
         if (withinArea(point, area))
         {
           IMPLIES(filterFalse(premise), point)
         }
         else
         {
           defaultReturn
         }

         // IMPLIES with BOX
       case IMPLIES(premise: Invariant, box: OccupyBox) =>
         BIGAND(
           unfoldBoxGeneric(box) map { p: OccupyPoint => if (withinArea(p, area)) IMPLIES( filter(premise), p ) else TRUE() }
         )

       case IMPLIES(premise: Invariant, box: OwnBox[Any]) =>
         BIGAND(
           unfoldBox(box) map { p => IMPLIES( filterFalse(premise), p ) }
         )


       case IMPLIES(box: OccupyBox, conclusion: Invariant) =>
         BIGAND(
           unfoldBox(box) map { p => IMPLIES( filterFalse(conclusion), p ) }
         )

       case IMPLIES(box: OwnBox[Any], conclusion: Invariant) =>
         BIGAND(
           unfoldBox(box) map { p => IMPLIES( filterFalse(conclusion), p ) }
         )

       // IMPLIES
       case IMPLIES(premise: Invariant, conclusion: Invariant) =>
         IMPLIES(filter(premise), filter(conclusion))   // KF: Not sure if we should be filtering the premise.

       // ANDs
       case AND(TRUE(), t2) => filterTrue(t2)
       case AND(t1, TRUE()) => filterTrue(t1)
       case AND(t1, t2) => AND(filter(t1), filter(t2))

       case BIGAND(list) =>
       {
         debugOff(s"filterByArea: list = $list")

         val dList = list.
           map    { t: Invariant => filterTrue(t) }.
           filter { t: Invariant => t != TRUE() }

         debugOff(s"filterByArea: dList = $dList")

         BIGAND(dList)
       }

       // ORs
       case OR(FALSE(), t2) => filter(t2)
       case OR(t1, FALSE()) => filter(t1)
       case OR(t1, t2) => OR( filterFalse(t1), filterFalse(t2) )

       case BIGOR(list) =>
       {
         val dList = list.
           map    { t: Invariant => filterFalse(t) }.
           filter { t: Invariant => t != FALSE() }

         BIGOR(dList)
       }

       // TODO: All the other BeSpaceD Invariant subtypes.

       case other => other
     }

     debugOff(s"result = $result")

     normalize(result)
   }



  // ----------------------------------------- Spacial Stepping

  // Translation
  type Translation = (Int, Int)


  // Stepping Forth and Back
  def stepForth(startArea: OccupyBox, translation: Translation): OccupyBox =
  {
    OccupyBox(startArea.x1 + translation._1, startArea.y1 + translation._2, startArea.x2 + translation._1, startArea.y2 + translation._2)
  }

  def stepBack(startArea: OccupyBox, translation: Translation): OccupyBox =
  {
    OccupyBox(startArea.x1 - translation._1, startArea.y1 - translation._2, startArea.x2 - translation._1, startArea.y2 - translation._2)
  }



  // ----------------------------------------- Folding Space

   def foldSpace[A](
                     invariant: Invariant,
                     accumulator: A,
                     startArea: OccupyBox,
                     stopArea: OccupyBox,
                     translation: Translation,
                     f: (A, Invariant) => A
                     ): A =
   {
     val nextStepArea     = stepForth(startArea, translation)
     val previousStepArea = stepBack(startArea, translation)

     if ( previousStepArea == stopArea)
     {
       accumulator
     }
     else
     {
       val Empty = TRUE()
       val unfoldedInvariant: Invariant = unfoldInvariant(invariant)
       val simpleInvariant:   Invariant = simplifyInvariant(unfoldedInvariant)
       val filteredInvariant: Invariant = filterByArea(startArea, defaultReturn = Empty)(simpleInvariant)

       debugOff {
         println(s"space invariants...")
         println(s"          invariant = $invariant")
         println(s"  unfoldedInvariant = $unfoldedInvariant")
         println(s"    simpleInvariant = $simpleInvariant")
         println(s"          startArea = $startArea")
         println(s"  filteredInvariant = $filteredInvariant")
       }

       val accumulation: A = f(accumulator, filteredInvariant)

       debugOn(s"f(): $accumulator -> $accumulation")

       debugOff {
         println(s"foldSpace()...")
         println(s"     startArea = $startArea")
         println(s"  nestStepArea = $nextStepArea")
         println(s"      stopArea = $stopArea")
         println(s"   translation = $translation")
       }

       val result = foldSpace[A](
         invariant,
         accumulation,
         startArea = nextStepArea,
         stopArea,
         translation,
         f
       )

       result
     }
   }

 }
