package BeSpaceDCore.SpatioTemporal

/**
 * Created by keith on 11/01/16.
 */

import BeSpaceDCore.Log._
import BeSpaceDCore._


object FilterTime {

  val core = standardDefinitions; import core._

  // ---------------------------------------------------------------- Folding Time

  def filterTime(startTime: IntegerTime, stopTime: IntegerTime, defaultReturn: Invariant)(invariant: Invariant): Invariant =
  {
    val filterFalse = filterTime(startTime, stopTime, FALSE()) _
    val filterTrue  = filterTime(startTime, stopTime, TRUE()) _
    val filter      = filterTime(startTime, stopTime, defaultReturn) _

    def withinTimeWindow(timePoint: TimePoint[IntegerTime], startTimeInclusive: IntegerTime, stopTimeExclusive: IntegerTime): Boolean =
    {
      val time = timePoint.timepoint

      time >= startTimeInclusive && time < stopTimeExclusive
    }

    invariant match
    {
      case IMPLIES(tp: TimePoint[IntegerTime], i: Invariant) =>
        if (withinTimeWindow(tp, startTime, stopTime))
        {
          IMPLIES(tp, i)
        }
        else
        {
          defaultReturn
        }

      case AND(FALSE(), t2) => filterTrue(t2)
      case AND(t1, FALSE()) => filterTrue(t1)
      case AND(t1, t2) => AND(filterTrue(t1), filterTrue(t2))

      case BIGAND(list) =>
      {
        debugOff(s"filterTime BEFORE: list = $list")

        val dList = list.
          map    { t: Invariant => filterTrue(t) }//.
          //filter { t: Invariant => t != NULL }

        debugOff(s"filterTime AFTER: list = $dList")

        BIGAND(dList)
      }

      case OR(FALSE(), t2) => filterFalse(t2)
      case OR(t1, FALSE()) => filterFalse(t1)
      case OR(t1, t2) => OR( filterFalse(t1), filterFalse(t2) )

      case BIGOR(list) =>
      {
        val dList = list.
          map    { t: Invariant => filterFalse(t) }//.
          //filter { t: Invariant => t != NULL }

        BIGOR(dList)
      }

      // TODO: All the other BeSpaceD Invariant subtypes.

      case other => other
    }
  }

  /**
   * Filter Time by re-writing all time points to either TRUE or FALSE then simplifying the whole invariant.
   *
   * @param startTime The inclusive start time of the period to filter in.
   * @param stopTime The exclusive stop time of the period to filter in.
   * @param invariant The data to be filtered.
   *
   * @return The filtered and simplified invariant.
   *
   */
  def filterTime2(startTime: IntegerTime, stopTime: IntegerTime, successReturn: BOOL)(invariant: Invariant): Invariant =
  {
    val filter = filterTime2(startTime, stopTime, successReturn) _

    def withinTimeWindow(timePoint: TimePoint[IntegerTime], startTimeInclusive: IntegerTime, stopTimeExclusive: IntegerTime): Boolean =
    {
      val time = timePoint.timepoint

      time >= startTimeInclusive && time < stopTimeExclusive
    }

    val filteredInvariant: Invariant = invariant match
    {
      case tp: TimePoint[IntegerTime] => if (withinTimeWindow(tp, startTime, stopTime)) tp else successReturn.not

      case IMPLIES(premise, conclusion) => IMPLIES(filter(premise), filter(conclusion))

      case AND(t1, t2) => AND(filter(t1), filter(t2))
      case BIGAND(sublist) => BIGAND(sublist map filter)

      case OR(t1, t2) => OR( filter(t1), filter(t2) )
      case BIGOR(sublist) => BIGOR(sublist map filter)

      // TODO: All the other BeSpaceD Invariant subtypes.

      case other => other
    }

    simplifyInvariant(filteredInvariant)
  }

}
