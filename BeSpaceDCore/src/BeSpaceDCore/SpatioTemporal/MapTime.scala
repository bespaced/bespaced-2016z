package BeSpaceDCore.SpatioTemporal

import BeSpaceDCore._

/**
 * Created by keith on 17/02/16.
 */
object MapTime {

  val core = standardDefinitions; import core._

  /**
   *
   * @param f A mapping function to apply to each conclusion of the IMPLIES filtered on time.
   * @param inv The data to be filtered.
   * @tparam T The result type of the mapping.
   *
   * @return The filtered and simplified invariant.
   */
  def mapTime[T <: Invariant](f: (Invariant) => T)( inv: Invariant): Invariant =
  {
    val normalizedInvariant = normalize(inv)

    val recurse = mapTime(f) _

    inv match
    {
      case IMPLIES(TimePoint(tp: IntegerTime), c) => IMPLIES[TimePoint[IntegerTime],T](TimePoint(tp), f(recurse(c)))

      case AND(t1, t2)   => AND(recurse(t1), recurse(t2))
      case BIGAND(terms) => BIGAND(terms map recurse)

      case OR(t1, t2)    => OR(recurse(t1), recurse(t2))
      case BIGOR(terms)  => BIGOR(terms map recurse)

      case other => other
    }
  }

  /**
   *
   * @param startTime The inclusive start time of the period to filter in.
   * @param stopTime The exclusive stop time of the period to filter in.
   * @param step The integer delta to step through a time sequence.
   * @param successReturn Either TRUE or FALSE depending which one is neutral in the calling context.
   *
   * @param f A mapping function to apply to each conclusion of the IMPLIES filtered on time.
   * @param inv The data to be filtered.
   * @tparam T The result type of the mapping.
   *
   * @return The filtered and simplified invariant.
   */
  def filterMapTime[T <: Invariant]
     (startTime: IntegerTime, stopTime: IntegerTime, step: IntegerTime.Step, successReturn: BOOL)
     (f:         (Invariant) => T)
     (inv:       Invariant):
     Invariant =
  {
    val func = filterTime2(startTime, stopTime, successReturn) _ compose mapTime(f) _

    func(inv)
  }
}
