import org.scalatest._

import scala.util.Either

/**
 * @author Keith Foster
 */



package object BeSpaceDCore {


  //-------------------------------- Standard Package includes

  import HazyTypes._



  //-------------------------------- Global Definitions
  private
  val CoreDefinitions = new CoreDefinitions()

  private
  val GraphOperations = new GraphOperations()



  //-------------------------------- Inversion of Control

  //TODO: Add some factory functions for core definitions here
  //      A factory function should be passed as a parameter to Apps and test suites
  //      so they can instantiate a core definition object to use.

  def standardDefinitions = CoreDefinitions
  val core  = standardDefinitions; import core._
  
  
  // ------------------------------- Implicits
  
  // Time Orderings
  implicit val intTimeOrdering = { _: Int => Ordering[Int] } 

  
  
  // ------------------------------- Implicit Operators

  implicit class InvariantOps[N <: Invariant, L <: N](private val left: L) extends AnyVal
  {
    // IMPLIES Construction
    @inline def implies[R <: Invariant] (right : R) = IMPLIES[L,R](left, right)
    @inline def ==>[R <: Invariant] (right : R) = implies(right)
    @inline def →[R <: Invariant] (right : R) = ==>(right)
    
    // AND
    @inline def and[R <: Invariant] (right : R) = AND(left, right)
    @inline def ^[R <: Invariant] (right : R) = and(right)

    // OR
    @inline def or[R <: Invariant] (right : R) = OR(left, right)
    @inline def v[R <: Invariant] (right : R) = or(right)
    
    // Edges
    @inline def edge[R <: N] (right : R) = Edge[N](left, right)
    @inline def -->[R <: N] (right : R) = edge(right)
    

    // MORE TODO...
  }

  implicit class AndOps[L <: AND](private val left: L) extends AnyVal
  {
    // AND
    @inline def ^[R <: Invariant] (right : R) = standardDefinitions.flatten(BIGAND(List(left.t1, left.t2, right))).asInstanceOf[BIGAND[R]]

    // OR
    @inline def v[R <: Invariant] (right : R) = standardDefinitions.flatten(OR(left, right)).asInstanceOf[OR]
  }

  implicit class BeGraphOps[N, E <: Edge[N], LN <: N, L <: BeGraph[LN]](private val left: L) extends AnyVal
  {
    // AND
    @inline def ++[RN <: N, R <: BeGraph[RN]] (right : R) = BeGraph[N](standardDefinitions.flattenAndList(left.terms ::: right.terms).asInstanceOf[List[E]])
    //@inline def ^[RN <: N, R <: BeGraph[RN]] (right : BIGAND[R]) = BeGraph(standardDefinitions.flattenAndList(left.terms ::: right.terms.flatMap { bm => bm.terms }).asInstanceOf[List[E]])

    // OR
    @inline def v[R <: Invariant] (right : R) = standardDefinitions.flatten(OR(left, right)).asInstanceOf[OR]
  }


  implicit class BigandOps[L <: BIGAND[Invariant]](private val left: L) extends AnyVal
  {
    // AND
    @inline def ^[R <: Invariant] (right : BIGAND[R]) = standardDefinitions.flatten(BIGAND(left.terms ::: right.terms)).asInstanceOf[BIGAND[R]]
    @inline def ^[R <: Invariant] (right : R) = standardDefinitions.flatten(BIGAND(left.terms ::: List(right))).asInstanceOf[BIGAND[R]]

    // OR
    @inline def v[R <: Invariant] (right : R) = standardDefinitions.flatten(OR(left, right)).asInstanceOf[OR]
  }

  implicit class OrOps[L <: OR](private val left: L) extends AnyVal
  {
    // AND
    @inline def ^[R <: Invariant] (right : R) = standardDefinitions.flatten(AND(left, right)).asInstanceOf[AND]

    // OR
    @inline def v[R <: Invariant] (right : R) = standardDefinitions.flatten(BIGOR(List(left.t1, left.t2, right))).asInstanceOf[BIGOR[R]]
  }

  implicit class BigorOps[L <: BIGOR[Invariant]](private val left: L) extends AnyVal
  {
    // AND
    @inline def ^[R <: Invariant] (right : R) = standardDefinitions.flatten(AND(left, right)).asInstanceOf[AND]

    // OR
    @inline def v[R <: Invariant] (right : R) = standardDefinitions.flatten(BIGOR(left.terms ::: List(right))).asInstanceOf[BIGOR[R]]
  }

  
  // ------------------------------- Obsolete

  // Flatten and Unwrap the terms of conjunctions

  @Deprecated
  def flattenAnds(presumedAnd: Invariant): Invariant = CoreDefinitions.simplifyInvariant(BIGAND(unwrapAnds(presumedAnd)))

  @Deprecated
  def unwrapAnds(presumedAnd: Invariant): List[Invariant] =
  {
    presumedAnd match
    {
      case AND(t1, t2)                   => unwrapAnds(t1) ++ unwrapAnds(t2)
      case BIGAND(list: List[Invariant]) => list flatMap unwrapAnds
      case _                             => List(presumedAnd)
    }
  }



  //-------------------------------- Scala Language Improvements

  // Union Types
  type or[L, R] = Either[L, R] // Move this to a common module
  implicit def l2Or[L, R](l: L): L or R = Left(l)
  implicit def r2Or[L, R](r: R): L or R = Right(r)



  //-------------------------------- ScalaTest Base classes
  
  // Unit Testing
  abstract class UnitSpec extends FlatSpec with Matchers with OptionValues with Inside with Inspectors

}