package z3test

import com.microsoft.z3._

object test1 {
  def main(args: Array[String]): Unit = {
    val context: Context = new Context();

    val x = context.mkSymbol("x");
    println("It works")
    println(linesegmentTriangleTest2D(4, 0, 5, 3, 5, 1, 2, 3, 3, 5).toString())
    /*
(define-fun w () Real
  0.0)
(define-fun v () Real
  (/ 2.0 11.0))
(define-fun u () Real
  (/ 9.0 11.0))
(define-fun l () Real
  (/ 5.0 11.0))
true
     * And true is expected. I didn't check the values, but they look ok. They are fractions.
     */
    println(linesegmentTriangleTest2D(1, 5, 4, 5, 5, 1, 2, 3, 3, 5).toString())
    /*
(define-fun w () Real
  1.0)
(define-fun v () Real
  0.0)
(define-fun u () Real
  0.0)
(define-fun l () Real
  (/ 2.0 3.0))
true
     * That's definitely correct.
     */
    println(linesegmentTriangleTest2D(3, 1, 2, 2, 5, 1, 2, 3, 3, 5).toString())
    /*
     * false
     * That is correct.
     */
  }
  def linesegmentTriangleTest2D(lineEnd1X: Int, lineEnd1Y: Int, lineEnd2X: Int, lineEnd2Y: Int, triangle1X: Int, triangle1Y: Int, triangle2X: Int, triangle2Y: Int, triangle3X: Int, triangle3Y: Int): Boolean = {
    val context: Context = new Context();
    //Point Coordinates
    val x = context.mkSymbol("x")
    val xconst = context.mkRealConst(x)
    val y = context.mkSymbol("y")
    val yconst = context.mkRealConst(y)
    //Barycentric Coordinates:
    val u = context.mkSymbol("u")
    val uconst = context.mkRealConst(u)
    val v = context.mkSymbol("v")
    val vconst = context.mkRealConst(v)
    val w = context.mkSymbol("w")
    val wconst = context.mkRealConst(w)
    //Line Segment Coordinate
    val l = context.mkSymbol("l")
    val lconst = context.mkRealConst(l)
    var s: Solver = null;
    try {
      val overlapCondition = (
        context.mkAnd(context.mkEq(context.mkReal(1), context.mkAdd(uconst, vconst, wconst)), //Barycentric coordinates sum up to 1
          //Barycentric coordinates are all between 0 and 1 iff the point is in the triangle
          context.mkLe(context.mkReal(0), uconst),
          context.mkLe(context.mkReal(0), vconst),
          context.mkLe(context.mkReal(0), wconst),
          context.mkGe(context.mkReal(1), uconst),
          context.mkGe(context.mkReal(1), vconst),
          context.mkGe(context.mkReal(1), wconst),
          //The line segment coordinate is between 0 and 1
          context.mkLe(context.mkReal(0), lconst),
          context.mkGe(context.mkReal(1), lconst),
          //And the points are equal
          context.mkAnd(
            //X-Coordinate
            context.mkEq(
              context.mkAdd(
                context.mkMul(lconst, context.mkSub(context.mkReal(lineEnd2X), context.mkReal(lineEnd1X))), //Interpolation
                context.mkReal(lineEnd1X) //Line Segment Start Point
                ),
              context.mkAdd(
                context.mkMul(uconst, context.mkReal(triangle1X)),
                context.mkMul(vconst, context.mkReal(triangle2X)),
                context.mkMul(wconst, context.mkReal(triangle3X))))),
          //Y-Coordinate
          context.mkEq(
            context.mkAdd(
              context.mkMul(lconst, context.mkSub(context.mkReal(lineEnd2Y), context.mkReal(lineEnd1Y))), //Interpolation
              context.mkReal(lineEnd1Y) //Line Segment Start Point
              ),
            context.mkAdd(
              context.mkMul(uconst, context.mkReal(triangle1Y)),
              context.mkMul(vconst, context.mkReal(triangle2Y)),
              context.mkMul(wconst, context.mkReal(triangle3Y))))))
    s = context.mkSolver();

        s.add(overlapCondition);
        if (s.check() == Status.SATISFIABLE) println (s.getModel());

    } catch {
      case e : Z3Exception => println (e); return false;
    }
    return ((s.check() == Status.SATISFIABLE));
  }
  
  //assumes points are consecutive
  def linesegmentRectangleTest2D(lineEnd1X: Int, lineEnd1Y: Int, lineEnd2X: Int, lineEnd2Y: Int, rect1X: Int, rect1Y: Int, rect2X: Int, rect2Y: Int, rect3X: Int, rect3Y: Int, rect4X: Int, rect4Y: Int): Boolean = {
    val a: Boolean = linesegmentTriangleTest2D(lineEnd1X, lineEnd1Y, lineEnd2X, lineEnd2Y, rect1X, rect1Y, rect2X, rect2Y, rect3X, rect3Y)
    val b: Boolean = linesegmentTriangleTest2D(lineEnd1X, lineEnd1Y, lineEnd2X, lineEnd2Y, rect1X, rect1Y, rect4X, rect4Y, rect3X, rect3Y)
    return a || b
  }
}