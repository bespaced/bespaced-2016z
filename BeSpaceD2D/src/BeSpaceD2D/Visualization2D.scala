/*
* Jan Olaf Blech 
* RMIT University
* 2013
*/

package BeSpaceD2D

import BeSpaceDCore._
import BeSpaceDCore.CoreDefinitions
import swing.{Panel, MainFrame, SimpleSwingApplication}
import java.awt.{Color, Graphics2D, Dimension}
import java.awt.Stroke
import java.awt.BasicStroke



class Visualization2D extends SimpleSwingApplication {

//	var inv : Invariant = FALSE();
 //     var x : Int = 10

    var inv : Invariant = FALSE()
    
    //added 21/01/2015: in addition to the invariant we optionally draw this list with invariants using the specified colors
    var invlist : List[(Color,Invariant)] = Nil
    
    var color : Color = new Color (0,0,0)
    
    //added 21/01/2015
    def addInvariant (inv : Invariant) {
      invlist ::= (new Color(0,0,0),inv)
    }

    //added 21/01/2015    
    def addInvariant (c : Color, inv : Invariant) {
      invlist ::= (c,inv)
    }
    
    def setInvariant (inv : Invariant){
        this.inv = inv
    }
    
    def setColor(c : Color) {
    	color = c
    }
    
  	
    
    val  panel : Panel = new Panel {
      preferredSize = new Dimension(800, 600)

      
//    def run() {
//        repaint();
//    }
      
    

    def drawInv (g: Graphics2D,inv : Invariant) {
        //g.setColor(color) // added 20/01/2015
        if (g.getColor() == null) { // added 20/01/2015
          g.setColor(new Color(0,0,0))
        }
          
    	  inv match {
        	 case (AND(inv1,inv2)) => {
        		 drawInv(g, inv1)
        		 drawInv(g,inv2)
        	 }
        	 case (BIGAND(inv1::l)) => {
        		 drawInv(g, inv1)
        		 drawInv(g,BIGAND(l))
        	 }
        	 case (OccupySegment(x1,y1,x2,y2,r)) => {
        		 g.setStroke(new BasicStroke(r.toFloat))
        		 g.drawLine(x1,y1,x2,y2);
        	 }  
          	 case (OccupyBox(x1,y1,x2,y2)) => {
        		 g.setStroke(new BasicStroke(1))
        		 g.drawRect(Math.min(x1,x2),Math.min(y1,y2),Math.abs(x2 - x1),Math.abs(y2 - y1));
          	 }	  
          	 //added Jan 2015, tested 09/01/2015 seems correct
          	 case (OccupyCircle(x,y,r)) => {
          	   g.setStroke(new BasicStroke(1))
          	   g.drawOval(x-r,y-r, r*2, r*2)
          	 }
          	 //added Jan 2015
          	 case (OccupyPoint(x,y)) => {
          	   g.setStroke(new BasicStroke(1))
          	   g.drawLine(x,y,x,y)
          	 }
          	 //added Jan 2015
          	 //draws conditional invariants in another color
          	 case (IMPLIES(cond,inv2)) => {
          	   drawInv(g,inv2)
          	 }
        	 case _ => 
    	  } 

    }
      

      
    override def paintComponent(g: Graphics2D) {
 
    	  
      //  g.drawLine(x, 10, 20, 20);
       // x +=1;
       
      val scalefactor : Int = 4;
      val offsetx: Int = -300;
      val offsety : Int = -500;
      var c :Int = 0;
      g.clearRect(0, 0, this.size.getWidth().toInt, this.size.getHeight().toInt)
      drawInv(g,inv);
      
      //added 21/01/2015: drawing of invlist
      for ((color,invariant) <- invlist) {
        g.setColor(color)
        drawInv(g,invariant)
      }
      return;
   //   for (sinv <- paintlist) {
    	  c +=10;
    	  g.setColor(new Color(0+c,0,0));
    	  inv match {
    	    case (OccupyBox(x1,y1,x2,y2)) => g.drawRect((x1*scalefactor) + offsetx, (y1*scalefactor) + offsety, ((x2 - x1) * scalefactor), ((y2 - y1) * scalefactor)); //g.fillRect(x1, y1, x2, y2)
    	    case (IMPLIES(_,OccupyBox(x1,y1,x2,y2))) => {
    	      g.setColor(new Color(255,0,255));
    	      g.drawRect((x1*scalefactor) + offsetx, (y1*scalefactor) + offsety, ((x2 - x1) * scalefactor), ((y2 - y1) * scalefactor));
    	    }
    	    case OccupySegment(x1,y1,x2,y2,r) => {
    	      g.setStroke(new BasicStroke(r.toFloat))
    	      g.drawLine(x1,y1,x2,y2);
    	    }
    	    case (IMPLIES(_,OccupySegment(x1,y1,x2,y2,r))) => {
    	      g.setStroke(new BasicStroke(r.toFloat))
    	      g.drawLine(x1,y1,x2,y2);
    	    }
    	    case _ => ;
    	  }
    	}
    }
    
    def top = new MainFrame() {
      	  contents = panel;
    }


  
}