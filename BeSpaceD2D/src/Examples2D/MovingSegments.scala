
/*
* Jan Olaf Blech 
* RMIT University
* 2013
*/

/* needs runtime setting -Xss515m or similar, Sat4j has problems with number of variables, encodeAsSat uses to low range for encoding of x,y variables */

package Examples2D
import BeSpaceDCore._;
import java.awt.Rectangle
import BeSpaceD2D._;


//import java.util.Timer
object MovingSegments extends CoreDefinitions{

  


  
  def invariant2bBIGANDgeneration() : Invariant ={
	var inv2b : List[Invariant] = Nil;
	  
	for (i <- 500 to 600) {
	  inv2b ::= (IMPLIES(TimeStamp (i), 
	      AND(OccupySegment(300,200,300 + (150.0 * Math.sin(i*0.1)).toInt,200 + (150.0 * Math.cos(i * 0.1)).toInt,10),
	      (AND(OccupySegment(300 + (150.0 * Math.sin(i*0.1)).toInt,200 + (150.0 * Math.cos(i * 0.1)).toInt,
	          300 + (150.0 * Math.sin(i*0.1)).toInt+ (50.0 * Math.sin(i*0.5)).toInt,200 + (150.0 * Math.cos(i * 0.1)).toInt + (50.0 * Math.cos(i*0.5)).toInt ,4),
	      OccupySegment(500,400,500 + (120.0 * - Math.sin(i*0.2)).toInt,400 + (120.0 * - Math.cos(i * 0.2)).toInt,12))  ))));
		
	}
	
	for (i <- 601 to 700) {
		inv2b ::= (IMPLIES(TimeStamp (i), 
	      AND(this.OccupySegment2OccupyBoxes(OccupySegment(300,200,300 + (150.0 * Math.sin(i*0.1)).toInt,200 + (150.0 * Math.cos(i * 0.1)).toInt,10)),
	      (AND(this.OccupySegment2OccupyBoxes(OccupySegment(300 + (150.0 * Math.sin(i*0.1)).toInt,200 + (150.0 * Math.cos(i * 0.1)).toInt,
	          300 + (150.0 * Math.sin(i*0.1)).toInt+ (50.0 * Math.sin(i*0.5)).toInt,200 + (150.0 * Math.cos(i * 0.1)).toInt + (50.0 * Math.cos(i*0.5)).toInt ,4)),
	      this.OccupySegment2OccupyBoxes(OccupySegment(500,400,500 + (120.0 * - Math.sin(i*0.2)).toInt,400 + (120.0 * - Math.cos(i * 0.2)).toInt,12)))  ))));
	}
	for (i <- 701 to 800) {
	  inv2b ::= (IMPLIES(TimeStamp (i), 
	      AND(this.OccupySegment2OccupyBoxes_Alt1(OccupySegment(300,200,300 + (150.0 * Math.sin(i*0.1)).toInt,200 + (150.0 * Math.cos(i * 0.1)).toInt,10)),
	      (AND(this.OccupySegment2OccupyBoxes_Alt1(OccupySegment(300 + (150.0 * Math.sin(i*0.1)).toInt,200 + (150.0 * Math.cos(i * 0.1)).toInt,
	          300 + (150.0 * Math.sin(i*0.1)).toInt+ (50.0 * Math.sin(i*0.5)).toInt,200 + (150.0 * Math.cos(i * 0.1)).toInt + (50.0 * Math.cos(i*0.5)).toInt ,4)),
	      this.OccupySegment2OccupyBoxes_Alt1(OccupySegment(500,400,500 + (120.0 * - Math.sin(i*0.2)).toInt,400 + (120.0 * - Math.cos(i * 0.2)).toInt,12)))  ))));
		
	}
	for (i <- 801 to 1009) {
	  inv2b ::= (IMPLIES(TimeStamp (i), 
	      AND(this.OccupySegment2OccupyBoxes_Alt2(OccupySegment(300,200,300 + (150.0 * Math.sin(i*0.1)).toInt,200 + (150.0 * Math.cos(i * 0.1)).toInt,10)),
	      (AND(this.OccupySegment2OccupyBoxes_Alt2(OccupySegment(300 + (150.0 * Math.sin(i*0.1)).toInt,200 + (150.0 * Math.cos(i * 0.1)).toInt,
	          300 + (150.0 * Math.sin(i*0.1)).toInt+ (50.0 * Math.sin(i*0.5)).toInt,200 + (150.0 * Math.cos(i * 0.1)).toInt + (50.0 * Math.cos(i*0.5)).toInt ,4)),
	      this.OccupySegment2OccupyBoxes_Alt2(OccupySegment(500,400,500 + (120.0 * - Math.sin(i*0.2)).toInt,400 + (120.0 * - Math.cos(i * 0.2)).toInt,12)))  ))));
		
	}
	//for (i <- 251 to 500) {
	//  inv2b ::= (IMPLIES(TimeStamp (i), OccupySegment(10,500+250+(2*(i -250)),4000+250,600+250+(i-250),4150+250)));
	//}
	//for (i <- 501 to 1000) {
	//  inv2b ::= (IMPLIES(TimeStamp (i), OccupySegment(10,500+250+i,4000+ i - 250,600+250+i,4150+ i - 250)));
	//}
	return (BIGAND(inv2b));
  }
  
  
  
  
  def main(args: Array[String]) {
    //new Visualization2D();
	val vis2d = new Visualization2D();
	vis2d.startup(null);
	println("x")
	for (i <- 500 to 1009) {
	  println("y")
	  Thread.sleep(100)
	  //vis2d.top.pa
	  //vis2d.top.repaint
	  //vis2d.top.
	  vis2d.setInvariant(simplifyInvariant(this.unfoldInvariant(getSubInvariantForTime(i,invariant2bBIGANDgeneration()))))
	  //println (vis2d.top.panel.x);
	  //vis2d.top.visible = true;
	  //vis2d.panel.repaint(new Rectangle(0,0,300,300))
	  vis2d.panel.repaint
	 // vis2d.top;
	  //vis2d.top.repaint(new Rectangle(0,0,800,600))
	  //vis2d.top.showing
	}
	//vis2d.top
	
	//while (true) {
	  
	//}
	  
   // for (i <- 0 to 100) {
//
//        println (this.collisionTestsAlt1( 
 //           this.simplifyInvariant(this.unfoldInvariant(getSubInvariantForTime(i,invariant1ageneration()))):: this.simplifyInvariant(this.unfoldInvariant(getSubInvariantForTime(i,invariant1bgeneration())))::Nil, 
   //         this.simplifyInvariant(this.unfoldInvariant(getSubInvariantForTime(i,invariant2bgeneration()))):: this.simplifyInvariant(this.unfoldInvariant(getSubInvariantForTime(i,invariant2ageneration())))::Nil));
     //   }
  }
}