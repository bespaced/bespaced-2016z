
/*
* Jan Olaf Blech 
* RMIT University
* 2013
*/

/* needs runtime setting -Xss515m or similar, Sat4j has problems with number of variables, encodeAsSat uses to low range for encoding of x,y variables */

package Examples2D

import BeSpaceDCore._;
import java.awt.Rectangle
import BeSpaceD2D._;

//import java.util.Timer
object MovingSegmentsAttachmentpoints extends CoreDefinitions{

  


  
//  def invariant2bBIGANDgeneration() : Invariant ={
//	var inv2b : List[Invariant] = Nil;
//	  
//	for (i <- 0 to 100) {
//	  inv2b ::= (IMPLIES(TimeStamp (i), 
//	      AND(OccupySegment(300,200,300 + (150.0 * Math.sin(i*0.1)).toInt,200 + (150.0 * Math.cos(i * 0.1)).toInt,10),
//	      (AND(OccupySegment(300 + (150.0 * Math.sin(i*0.1)).toInt,200 + (150.0 * Math.cos(i * 0.1)).toInt,
//	          300 + (150.0 * Math.sin(i*0.1)).toInt+ (50.0 * Math.sin(i*0.5)).toInt,200 + (150.0 * Math.cos(i * 0.1)).toInt + (50.0 * Math.cos(i*0.5)).toInt ,4),
//	      OccupySegment(500,400,500 + (120.0 * - Math.sin(i*0.2)).toInt,400 + (120.0 * - Math.cos(i * 0.2)).toInt,12))  ))));
//		
//	}
//	
//	for (i <- 601 to 700) {
//		inv2b ::= (IMPLIES(TimeStamp (i), 
//	      AND(this.OccupySegment2OccupyBoxes(OccupySegment(300,200,300 + (150.0 * Math.sin(i*0.1)).toInt,200 + (150.0 * Math.cos(i * 0.1)).toInt,10)),
//	      (AND(this.OccupySegment2OccupyBoxes(OccupySegment(300 + (150.0 * Math.sin(i*0.1)).toInt,200 + (150.0 * Math.cos(i * 0.1)).toInt,
//	          300 + (150.0 * Math.sin(i*0.1)).toInt+ (50.0 * Math.sin(i*0.5)).toInt,200 + (150.0 * Math.cos(i * 0.1)).toInt + (50.0 * Math.cos(i*0.5)).toInt ,4)),
//	      this.OccupySegment2OccupyBoxes(OccupySegment(500,400,500 + (120.0 * - Math.sin(i*0.2)).toInt,400 + (120.0 * - Math.cos(i * 0.2)).toInt,12)))  ))));
//	}
//	for (i <- 701 to 800) {
//	  inv2b ::= (IMPLIES(TimeStamp (i), 
//	      AND(this.OccupySegment2OccupyBoxes_Alt1(OccupySegment(300,200,300 + (150.0 * Math.sin(i*0.1)).toInt,200 + (150.0 * Math.cos(i * 0.1)).toInt,10)),
//	      (AND(this.OccupySegment2OccupyBoxes_Alt1(OccupySegment(300 + (150.0 * Math.sin(i*0.1)).toInt,200 + (150.0 * Math.cos(i * 0.1)).toInt,
//	          300 + (150.0 * Math.sin(i*0.1)).toInt+ (50.0 * Math.sin(i*0.5)).toInt,200 + (150.0 * Math.cos(i * 0.1)).toInt + (50.0 * Math.cos(i*0.5)).toInt ,4)),
//	      this.OccupySegment2OccupyBoxes_Alt1(OccupySegment(500,400,500 + (120.0 * - Math.sin(i*0.2)).toInt,400 + (120.0 * - Math.cos(i * 0.2)).toInt,12)))  ))));
//		
//	}
//	for (i <- 801 to 1009) {
//	  inv2b ::= (IMPLIES(TimeStamp (i), 
//	      AND(this.OccupySegment2OccupyBoxes_Alt2(OccupySegment(300,200,300 + (150.0 * Math.sin(i*0.1)).toInt,200 + (150.0 * Math.cos(i * 0.1)).toInt,10)),
//	      (AND(this.OccupySegment2OccupyBoxes_Alt2(OccupySegment(300 + (150.0 * Math.sin(i*0.1)).toInt,200 + (150.0 * Math.cos(i * 0.1)).toInt,
//	          300 + (150.0 * Math.sin(i*0.1)).toInt+ (50.0 * Math.sin(i*0.5)).toInt,200 + (150.0 * Math.cos(i * 0.1)).toInt + (50.0 * Math.cos(i*0.5)).toInt ,4)),
//	      this.OccupySegment2OccupyBoxes_Alt2(OccupySegment(500,400,500 + (120.0 * - Math.sin(i*0.2)).toInt,400 + (120.0 * - Math.cos(i * 0.2)).toInt,12)))  ))));
//		
//	}
//
//	return (BIGAND(inv2b));
//  }
  
  
     //C1

   def invariantR1() : Invariant ={
	var inv : List[Invariant] = Nil;
	  
	for (i <- 0 to 100) {
	  inv ::= (IMPLIES(TimeStamp (i), 
	      OccupySegment(300,200,300 + (150.0 * Math.sin(i*0.1)).toInt,200 + (150.0 * Math.cos(i * 0.1)).toInt,10)))	
	}
	return (BIGAND(inv));
   }
  
   //attachment point A of C1
  def invariantR1A() : Invariant ={
	var inv : List[Invariant] = Nil;
	  
	for (i <- 0 to 100) {
	  inv ::= (IMPLIES(TimeStamp (i), 
	      OccupyPoint(300 + (150.0 * Math.sin(i*0.1)).toInt,200 + (150.0 * Math.cos(i * 0.1)).toInt)));
		
	}
	return (BIGAND(inv));
  }
  
  //C2 is attached to C1 using A, coordinates are relative
   def invariantR2() : Invariant ={
	var inv : List[Invariant] = Nil;
	  
	for (i <- 0 to 100) {
	  inv ::= (IMPLIES(TimeStamp (i), 
	      OccupySegment(0,0,
	          (50.0 * Math.sin(i*0.5)).toInt,(50.0 * Math.cos(i*0.5)).toInt ,4)))
	}
	return (BIGAND(inv));
   }
  
   //change this with C3
   def invariantR3() : Invariant ={
	var inv : List[Invariant] = Nil;
	  
	for (i <- 0 to 100) {
	  inv ::= (IMPLIES(TimeStamp (i), 
	      OccupySegment(500,400,500 + (120.0 * - Math.sin(i*0.2)).toInt,400 + (120.0 * - Math.cos(i * 0.2)).toInt,12)))
	}
	return (BIGAND(inv));
   }
  
  def behtcomp1def =
		  SimpleSpatioTemporalBehavioralType ("RobotBasis", invariantR1, "A"::Nil , Map()("A" -> invariantR1A)) 
  
  
  def main(args: Array[String]) {
	val vis2d = new Visualization2D();
	vis2d.startup(null);
	println("x")
	for (i <- 0 to 100) {
	  Thread.sleep(100)
	  vis2d.setInvariant(BIGAND(
	      simplifyInvariant(this.unfoldInvariant(getSubInvariantForTime(i,invariantR1())))::
	      this.OccupySegment2OccupyBoxes_Alt2(simplifyInvariant(this.unfoldInvariant(getSubInvariantForTime(i,invariantR1()))))::Nil))

	  
	  vis2d.panel.repaint
	}
	for (i <- 0 to 100) {
	  Thread.sleep(100)
	  vis2d.setInvariant(simplifyInvariant(this.unfoldInvariant(getSubInvariantForTime(i,invariantR2()))))
	  vis2d.panel.repaint
	}
	// Adding the components
	for (i <- 0 to 100) {
	  Thread.sleep(100)
	  vis2d.setInvariant(simplifyInvariant(this.unfoldInvariant(addPointSegment(getSubInvariantForTime(i,invariantR1A()), getSubInvariantForTime(i,invariantR2())))))
	  vis2d.panel.repaint
	}

  }
}