/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

package SMTOperations

import org.scalatest._

import BeSpaceDCore._

/**
 * @author keith
 * 
 * To run these scala tests on MacOS run the following commands (replacing <<username>> with your own username):
 * 
 *     cd /Users/<<username>>/workspace/code/SMTOperations/lib-nojar/z3-4.4.1-x64-osx-10.11/bin/
 *     scala -cp ../../../../BeSpaceDCore/lib/scalatest_2.11-2.2.6.jar:../../../lib-nojar/z3-4.4.1-x64-osx-10.11/bin/com.microsoft.z3.jar:../../../../BeSpaceDCore/bin:../../../bin org.scalatest.run SMTOperations.CoreOperationsSpec
 *     
 */
class CoreOperationsSpec extends UnitSpec {
  
  
    println( "java.library.path = " + System.getProperty("java.library.path") )
  
    val core = new CoreOperations(); import core._

    
    // Check2Box
    
    "Check2BoxSMT" should "return true for overlapping boxes with one point in common." in
    {
      val b1 = OccupyBox(1,1,10,10)
      val b2 = OccupyBox(10,10,20,20) //  One point overlap: (10, 10)
      
      val overlapping  = check2BoxSMT(b1.x1, b1.y1, b1.x2, b1.y2, b2.x1, b2.y1, b2.x2, b2.y2)
      
      assert(overlapping == true, "because they should have a single point overlap")
    }
  
  
    it should "return false for non-overlapping boxes where x overlaps but y does not." in
    {
      val b1 = OccupyBox(1,1,10,10)
      val b2 = OccupyBox(1,20,10,30) //  on top
      
      val overlapping  = check2BoxSMT(b1.x1, b1.y1, b1.x2, b1.y2, b2.x1, b2.y1, b2.x2, b2.y2)
      
      assert(overlapping == false, "because they the two boxes on top of each other.")
    }
  
  
    it should "return false for adjacent boxes." in
    {
      val b1 = OccupyBox(1,1,5,5)
      val b2 = OccupyBox(6,1,10,5) //  adjacent on vertical boundary between: x=5/x=6
      
      val overlapping  = check2BoxSMT(b1.x1, b1.y1, b1.x2, b1.y2, b2.x1, b2.y1, b2.x2, b2.y2)
      
      assert(overlapping == false, "because they are adjacent (touching but no overlap)")
    }
  
  
    it should "return false for non-overlapping boxes mirroed in the x and y axies." in
    {
      val b1 = OccupyBox(1,1,10,10)
      val b2 = OccupyBox(-1,-1,-10,-10) //  reflected in x/y
      
      val overlapping  = check2BoxSMT(b1.x1, b1.y1, b1.x2, b1.y2, b2.x1, b2.y1, b2.x2, b2.y2)
      
      assert(overlapping == false, "because the two boxes are on opposite sides of the origin.")
    }
  
    
    // Check2BoxList
    
    "check2BoxListSMT" should "return false for two crossing paths that miss each other." in
    {
      val a1 = OccupyBox(0,0,9,9)
      val b1 = OccupyBox(30,0,39,9)
      
      val a2 = OccupyBox(10,5,19,14)
      val b2 = OccupyBox(20,5,29,14)
      
      val a3 = OccupyBox(20,10,29,19)
      val b3 = OccupyBox(10,10,19,19)
      
      val a4 = OccupyBox(30,15,39,24)
      val b4 = OccupyBox(0,15,9,24)
      
      val a: List[OccupyBox] = List(a1, a2, a3, a4)
      val b: List[OccupyBox] = List(b1, b2, b3, b4)
      
      val overlapping  = check2BoxListSMT(a, b)
      
      assert(overlapping == false, "because none of the pairs overlap.")
    }
  
    
    it should "return true for two crossing paths that hit each other." in
    {
      val a1 = OccupyBox(0,0,9,9)
      val b1 = OccupyBox(40,0,49,9)
      
      val a2 = OccupyBox(10,5,19,14)
      val b2 = OccupyBox(30,4,39,13)
      
      val a3 = OccupyBox(20,10,29,19)
      val b3 = OccupyBox(20,8,29,17)   // Overlaps on (20..29, 10..17)
      
      val a4 = OccupyBox(30,15,39,24)
      val b4 = OccupyBox(10,12,19,21)

      val a5 = OccupyBox(40,20,49,29)
      val b5 = OccupyBox(0,16,9,25)

      val a: List[OccupyBox] = List(a1, a2, a3, a4, a5)
      val b: List[OccupyBox] = List(b1, b2, b3, b4, b5)
      
      val overlapping  = check2BoxListSMT(a, b)
      
      assert(overlapping == true, "because the third of the pair of boxes overlaps.")
    }
  
    
}