/* 
 * Created 15/05/2014
 * modified: 16/05/2014
 * working example for KIVA paper
 * !!!! may need runtime setting -Xss515m or similar,  */

package BeSpaceDExamples
import BeSpaceDCore._;
object MobileRangesExample extends CoreDefinitions{

 
  def main(args: Array[String]) {
    
    def coverage = IMPLIES (AND(Owner ("WLAN_RANGE"),Prob(1.0)), BIGAND(
                OccupyCircle(15,15,40)::
                OccupyCircle(35,15,43)::
                OccupyCircle(65,16,42)::
                
                OccupyCircle(15,35,40)::
                OccupyCircle(35,35,43)::
                OccupyCircle(65,36,42)::
                
                
                OccupyCircle(15,65,40)::
                OccupyCircle(35,65,43)::
                OccupyCircle(65,66,42)::

                OccupyCircle(15,95,40)::
                OccupyCircle(35,95,43)::
                OccupyCircle(65,96,42)::
                
                OccupyCircle(15,115,40)::
                OccupyCircle(35,115,43)::
                OccupyCircle(65,116,42)::
                
                
                OccupyCircle(15,130,40)::
                OccupyCircle(35,130,43)::
                OccupyCircle(65,130,42)::
                
                Nil ));

     def factoryhallarea = IMPLIES (AND(Owner ("FactoryHall"),Event("Operation")), 
         OccupyBox(10,10,100,150)
     );   
    
    println(
        inclusionTestsBig(
        unfoldInvariant(
            BIGAND(
                OccupyCircle(15,15,40)::
                OccupyCircle(35,15,43)::
                OccupyCircle(65,16,42)::
                
                OccupyCircle(15,35,40)::
                OccupyCircle(35,35,43)::
                OccupyCircle(65,36,42)::
                
                
                OccupyCircle(15,65,40)::
                OccupyCircle(35,65,43)::
                OccupyCircle(65,66,42)::

                OccupyCircle(15,95,40)::
                OccupyCircle(35,95,43)::
                OccupyCircle(65,96,42)::
                
                OccupyCircle(15,115,40)::
                OccupyCircle(35,115,43)::
                OccupyCircle(65,116,42)::
                
                
                OccupyCircle(15,130,40)::
                OccupyCircle(35,130,43)::
                OccupyCircle(65,130,42)::
                
                Nil ))::Nil,
          unfoldInvariant(
            OccupyBox(10,10,100,150))::Nil))  
  
  }
}