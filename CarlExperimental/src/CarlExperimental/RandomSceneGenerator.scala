package CarlExperimental
import BeSpaceDCore._
import scala.util._

class RandomSceneGenerator extends CoreDefinitions{
//  val core = standardDefinitions; import core._
  def Generator() : (OccupyPoint, OccupyPoint, List[OccupyBox]) = {
    val numObstacles : Int = Random.nextInt(10) + 1
    println("numObstacles: " + numObstacles)
    val obstacles : List[OccupyBox] = GenerateObstacles(numObstacles)
    
    //println(obstacles)
    return(GeneratePoints(obstacles), GeneratePoints(obstacles), obstacles)
  }
  def GenerateObstacles(n : Int) : List[OccupyBox] = {
    if(n < 1) return Nil else return OccupyBox(Random.nextInt(60), Random.nextInt(60), Random.nextInt(60), Random.nextInt(60))::GenerateObstacles(n - 1)
  }
  def GeneratePoints(obstacles : List[OccupyBox]) : OccupyPoint = {
    val point : OccupyPoint = OccupyPoint(Random.nextInt(60), Random.nextInt(60))
    if(collisionTestsBig(obstacles.map(x => unfoldInvariant(x)), unfoldInvariant(point)::Nil)) return GeneratePoints(obstacles) else return point
  }
}