package CarlExperimental

import BeSpaceDCore._
import BeSpaceDCore.Log._

class CircularListSpec extends UnitSpec
{
  
  "add" should "add a CircularListElement of OccupyPoint(40,30) to the CircularList Nil to get OccupyPoint(40,30)::Nil" in
  {
    var result : CircularList = new CircularList()
    result.add(OccupyPoint(40,30))
    
    val length : Int = result.size
    assertResult(expected = 1)(actual = length)
    val first = result.get(0).x
    
    assertResult(expected = 40)(actual = first )
  }
}