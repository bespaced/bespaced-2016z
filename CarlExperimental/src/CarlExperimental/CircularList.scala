package CarlExperimental
import BeSpaceDCore._

class CircularList{
  var entries : List[CircularListElement[OccupyPoint]] = Nil
  def add(point : OccupyPoint) : CircularList = {
    var entry : CircularListElement[OccupyPoint] = new CircularListElement[OccupyPoint]
    entry.setData(point)
    entries = entry::entries
    return this
  }
  def add(list : List[OccupyPoint]) : CircularList = {
    list match{
      case Nil => return this
      case h::Nil => add(h); return this
      case h::t => add(h); add(t); return this
    }
//    if(list != Nil) add(list.head)
//    if(list.tail != Nil) add(list.tail)
  }
  def get(i : Int) : OccupyPoint = {
    return entries(i).getData()
  }
  def link(i : Int, pred : Int, succ : Int) : Unit = {
    entries(i).setPred(entries(pred))
    entries(i).setSucc(entries(succ))
  }
  def link(i : OccupyPoint, pred : OccupyPoint, succ : OccupyPoint) : Unit = {
    val one : Int = entries.indexOf(entries.filter { _.getData() == i }.head)
    val two : Int = entries.indexOf(entries.filter { _.getData() == pred }.head)
    val three : Int = entries.indexOf(entries.filter { _.getData() == succ }.head)
    link(one, two, three)
  }
  def pred(i : Int) : (OccupyPoint, Int) = {
    return (entries(i).getPred().getData(), entries.indexOf(entries(i).getPred()))
  }
  def succ(i : Int) : (OccupyPoint, Int) = {
    return (entries(i).getSucc().getData(), entries.indexOf(entries(i).getSucc()))
  }
  def pred(p : OccupyPoint) : OccupyPoint = {
    val current = entries.filter { _.getData() == p }
    return current.head.getPred().getData()
  }
  def succ(s : OccupyPoint) : OccupyPoint = {
    val current = entries.filter { _.getData() == s }
    return current.head.getSucc().getData()
  }
  def deleteFromHull(x : Int, y : Int) : Unit = {
    val start : CircularListElement[OccupyPoint] = entries(x)
    val end : CircularListElement[OccupyPoint] = entries(y)
    while(start.getSucc() != end && start != end){
      start.getSucc().setPred(null)
      start.setSucc(start.getSucc().getSucc())
      start.getSucc().getPred().setSucc(null)
      start.getSucc().setPred(start)
    }
  }
  def deleteFromHull(x : OccupyPoint, y : OccupyPoint) : Unit = {
    val one : Int = entries.indexOf(entries.filter { _.getData() == x }.head)
    val two : Int = entries.indexOf(entries.filter { _.getData() == y }.head)
    deleteFromHull(one, two)
  }
  def lexicographicSort = entries.sortWith(_.getData.y < _.getData.y).sortWith(_.getData.x < _.getData.x)
  def sortSelf = {
    entries = lexicographicSort
  }
  
  def rightmost : OccupyPoint = lexicographicSort.last.getData()
  def leftmost : OccupyPoint = lexicographicSort.head.getData()
  def start : OccupyPoint = lexicographicSort.filter { _.getPred != null }.head.getData()
 /* def read() : Seq[OccupyPoint] = {
    //var output : List[OccupyPoint] = Nil
    for(i : Int <- (entries.length - 1) to 0) yield {
      entries(i).getData
    }
  }*/
  def size : Int = entries.length
  def read = entries map {_.getData()}
}