package CarlExperimental

import BeSpaceDCore._
import BeSpaceDCore.Log._
import ExampleSpecification1._

class ExampleSpecification1Spec extends UnitSpec{
  "Turn" should "say right turn for (170,120), (100,250), (20,190)" in
  {
    var a = OccupyPoint(170,120)
    var b = OccupyPoint(100,250)
    var c = OccupyPoint(20,190)
    var result : String = Turn(a,b,c)
    
    assertResult(expected = "right turn")(actual = result )
  }
  it should "say left turn for (170,120), (20,190), (100,250)" in
  {
    var a = OccupyPoint(170,120)
    var b = OccupyPoint(100,250)
    var c = OccupyPoint(20,190)
    var result : String = Turn(a,c,b)
    
    assertResult(expected = "left turn")(actual = result )
  }
  it should "say no turn for (170,120), (190,130), (230,150)" in
  {
    var a = OccupyPoint(170,120)
    var b = OccupyPoint(190,130)
    var c = OccupyPoint(230,150)
    
    var result : String = Turn(a,b,c)
    
    assertResult(expected = "no turn")(actual = result)
  }
  
  "UnfoldToPoints" should "take an OccupyBox(20,120,22,122) and return List(OccupyPoint(20,120), OccupyPoint(20,121), OccupyPoint(20,122), OccupyPoint(21,120), OccupyPoint(21,121), OccupyPoint(21,122), OccupyPoint(22,120), OccupyPoint(22,121), OccupyPoint(22,122))" in{
    val result : List[OccupyPoint] = UnfoldToPoints(OccupyBox(20,120,22,122))
    
    assertResult(expected = List(OccupyPoint(20,120), OccupyPoint(20,121), OccupyPoint(20,122), OccupyPoint(21,120), OccupyPoint(21,121), OccupyPoint(21,122), OccupyPoint(22,120), OccupyPoint(22,121), OccupyPoint(22,122)))(actual = result)
  }
}