package CarlExperimental
import BeSpaceDCore._

object Test1 extends CoreDefinitions{
  val something = BIGAND (List (TRUE(), FALSE()))
  val obstacle = OccupyBox(40,120,170,190)
  
  def main(args: Array[String]):Unit = {
    println(simplifyInvariant(unfoldInvariant(obstacle)))
    println(something)
  }
}