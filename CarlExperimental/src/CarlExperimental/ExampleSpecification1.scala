package CarlExperimental
import BeSpaceDCore._
import math._
import scala.collection.mutable._
import DoubleLinkedList._
import scala.swing._
import scala.swing.event._
import java.awt.Rectangle
import BeSpaceD2D._;
import scalafx.Includes._
import template.RandomSceneGenerator

//use scalafx


object ExampleSpecification1 extends  CoreDefinitions {

  def test() : Unit = {
    var top = new MainFrame {
          title = "Reactive Swing App"
    val button = new Button {
      text = "Click me"
    }
    val label = new Label {
      text = "No button clicks registered"
    }	
    contents = new BoxPanel(Orientation.Vertical) {
      contents += button
      contents += label
      border = Swing.EmptyBorder(30, 30, 10, 30)
    }
    listenTo(button)
    var nClicks = 0
    reactions += {
      case ButtonClicked(b) =>
        nClicks += 1
        label.text = "Number of button clicks: "+ nClicks
    }
    }
    top.visible = true
  }
  
  
def main (args: Array[String]):Unit = {
    test()
    return
    val scene = new RandomSceneGenerator().RandomSceneGenerator()
    val robotStartValue = scene._1
    val targetArea = scene._2
    val obstacle = scene._3
//    val robotStartValue = OccupyPoint(0,50)
//    val targetArea = OccupyPoint(250,50)
//    val obstacle = OccupyBox(20,30,80,70)::Nil
    println("Start: " + robotStartValue)
    println("Target: " + targetArea)
    println("Obstacle: " + obstacle)
//    val robotStartValue = OccupyPoint(100,0)
//    val obstacle = OccupyBox(20,10,181,150)
//    val targetArea = OccupyPoint(100,250)
    
    //println(collisionTestsBig(OccupyBox(34,49,15,0)::Nil,OccupyPoint(15,21)::Nil))
    
    val path : List[OccupyPoint] = RobotPathSuper(robotStartValue, obstacle, targetArea)
    path map (println _)

    //Visualisation:
    val vis2d = new Visualization2D();
	  vis2d.startup(null);
	  println("x")

//	  for (i <- 0 to 200) {
//	    Thread.sleep(100)
	  val obstacleInvariant : List[Invariant] = obstacle.map(x => simplifyInvariant(this.unfoldInvariant(x)))
	    vis2d.setInvariant(BIGAND(
	      robotStartValue:: targetArea ::
	        obstacleInvariant
	    ))
	    vis2d.panel.repaint
//	  }
    println("y")
	  
    return
//    println(simplifyInvariant(unfoldInvariant(obstacle)))
//    var hull = new CircularList()
//    hull.add(robotStartValue)
//    var area : List[OccupyPoint] = simplify(unfoldInvariant(obstacle)) match {
//      case b : BIGAND[OccupyPoint] => b.terms
//      case x => { println(x); List() }
////      case _ => throw new IllegalStateException
//    }
//    while(area != Nil){
//      hull.add(area.head)
//      area.drop(0)
//    }
//    println(simplifyInvariant(unfoldInvariant(obstacle)))
//    hull.add(OccupyPoint(40,120))
//    hull.add(OccupyPoint(170,120))
//    hull.add(OccupyPoint(40,190))
//    hull.add(OccupyPoint(170,190))
//    hull.add(UnfoldToPoints(obstacle))
//    hull.add(targetArea)
//    hull.sortSelf
//    hull = ConvexHullDC(hull, 0, hull.size - 1)
////    println (robotpath(hull, robotStartValue, targetArea))
//    robotpath(hull, robotStartValue, targetArea) map (println _)
    
    

//    var robotpath : List[OccupyPoint] = Nil
//    var current : OccupyPoint = robotStartValue
//    while(current != targetArea){
//      robotpath = current::robotpath
//      
//    }
    
    //robotpath = robotpath++(OccupyPoint(170,120)::Nil)
//    robotpath = OccupyPoint(170,120)::robotpath
  
//    println (collisionTestsBig(unfoldInvariant(obstacle)::Nil,OccupyPoint(25,12)::Nil))
     //println
  //  println (unfoldInvariant(obstacle))
    //println
    //  println(robotpath.reverse)
  }
  def RobotPathSuper(start : OccupyPoint, obstacles : List[OccupyBox], target : OccupyPoint) : List[OccupyPoint] = {
    if(obstacles == Nil){
      return List(start, target)
    }
    var c : ConvexHull = new ConvexHull()
    var path : List[OccupyPoint] = robotpath(c.convexHull((start::BoxExtremePoints(obstacles) ++ (target::Nil))), start, target)
    if(path == Nil){
      val leftHalf : Int = obstacles.length / 2
      val rightHalf : Int = if(obstacles.length % 2 == 0) obstacles.length / 2 else obstacles.length / 2 + 1
      val pathLeft : List[OccupyPoint] = RobotPathSuper(start, obstacles.take(leftHalf), target)
      val pathRight : List[OccupyPoint] = RobotPathSuper(start, obstacles.takeRight(rightHalf), target)
      path = if(pathLength(pathLeft) < pathLength(pathRight)) pathLeft else pathRight
    }
    return path
  }
  def BoxExtremePoints(boxes : List[OccupyBox]) : List[OccupyPoint] = {
    if(boxes == Nil) return Nil else return BoxExtremePoints(boxes.head) ++ BoxExtremePoints(boxes.tail)
  }
  def BoxExtremePoints(box : OccupyBox) : List[OccupyPoint] = {
    box match{
      case OccupyBox(a,b,c,d) => return OccupyPoint(a,b)::OccupyPoint(c,d)::OccupyPoint(a,d)::OccupyPoint(c,b)::Nil
      case _ => throw new IllegalArgumentException
    }
  }
  def UnfoldToPoints(inv : Invariant) : List[OccupyPoint] = {
    var outputList : List[OccupyPoint] = Nil
    UnfoldToPointsSimple(simplifyInvariant(unfoldInvariant(inv))) map {_ match{
      case OccupyPoint(x,y) => outputList = OccupyPoint(x,y)::outputList
      case _ => throw new IllegalArgumentException
    }}
    return outputList.reverse
  }
  def UnfoldToPointsSimple(inv : Invariant) : List[Invariant] = {
    return inv match{
      case BIGAND(l) => l
      case _ => Nil
    }
    return Nil
  }
  def robotpath (hull : CircularList, start : OccupyPoint, target : OccupyPoint) : List[OccupyPoint] = {
    val r = robotpathRight(hull, start, target)
    val l = robotpathLeft(hull, start, target)
    if(pathLength(r) < pathLength(l)) {println(pathLength(r)); return r} else {println(pathLength(l)); return l}
  }
  def robotpathRight (hull : CircularList, start : OccupyPoint, target : OccupyPoint) : List[OccupyPoint] = {
    if(start == target) return target::Nil else return start::robotpathRight(hull, hull.succ(start), target)
  }
  def robotpathLeft (hull : CircularList, start : OccupyPoint, target : OccupyPoint) : List[OccupyPoint] = {
    if(start == target) return target::Nil else return start::robotpathLeft(hull, hull.pred(start), target)
  }
  def robotpath (hull : List[OccupyPoint], start : OccupyPoint, target : OccupyPoint) : List[OccupyPoint] = {
    val s : Int = hull.indexOf(start)
    val t : Int = hull.indexOf(target)
    if(s < 0 || t < 0){
      return Nil
    }
    val l : List[OccupyPoint] = robotpathRight(hull,s,t)
    val r : List[OccupyPoint] = robotpathLeft(hull,s,t)
    if(pathLength(r) < pathLength(l)) {println(pathLength(r)); return r} else {println(pathLength(l)); return l}
  }
  def robotpathRight (hull : List[OccupyPoint], start : Int, target : Int) : List[OccupyPoint] = {
    if(start == target) return hull(target)::Nil else return hull(start)::robotpathRight(hull, (start + 1) % hull.length, target)
  }
  def robotpathLeft (hull : List[OccupyPoint], start : Int, target : Int) : List[OccupyPoint] = {
    if(start == target) return hull(target)::Nil else return hull(start)::robotpathLeft(hull, (hull.length + start - 1) % hull.length, target)
  }
  def pathLength (path : List[OccupyPoint]) : Double ={
    if(path == Nil || path.tail == Nil) return 0 else return pathLength(path.tail) + distance(path.head, path.tail.head)
  }
  def distance(point1 : OccupyPoint, point2 : OccupyPoint):Double = {
    val distanceX = abs(point1.x - point2.x)
    val distanceY = abs(point1.y - point2.y)
    return sqrt(pow(distanceX,2) + pow(abs(point1.y - point2.y),2)).toDouble
  }
//  def followWall(position: OccupyPoint, orientation : OccupyPoint): Array[OccupyPoint] = {
//    val idealNextPoint = {OccupyPoint(if (targetArea.x > position.x) position.x + 1 else if(targetArea.x == position.x) position.x else position.x - 1, 
//        if (targetArea.y > position.y) position.y + 1 else if(targetArea.y == position.y) position.y else position.y - 1)};
//    
//  }
  def ConvexHullDC(points : CircularList, lower : Int, upper: Int) : CircularList = {
    if(upper - lower <= 2) return ConvexHullTriangle(points, lower, upper)
    else{
      val k = (lower + upper) / 2
      val C1 = ConvexHullDC(points, lower, k)
      val C2 = ConvexHullDC(points, k + 1, upper)
      val C0 = Merge(points, k)
      return C0
    }
  }
  def ConvexHullTriangle(points : CircularList, lower : Int, upper: Int) : CircularList = {
    if(upper - lower < 1) return points
    if(upper - lower < 2){
      points.link(upper, lower, lower)
      points.link(lower, upper, upper)
      return points
    }
    if(Turn(points.get(upper - 2), points.get(upper - 1), points.get(upper)).equals("left turn")){
      points.link(upper - 2, upper, upper - 1)
      points.link(upper - 1, upper - 2, upper)
      points.link(upper, upper - 1, upper - 2)
    }
    else{
      points.link(upper - 2, upper - 1, upper)
      points.link(upper - 1, upper, upper - 2)
      points.link(upper, upper - 2, upper - 1)
    }
    return points
/*    var Hull : CircularList = new CircularList()
    Hull.add(points.get(upper))
    Hull.add(points.get(upper - 1))
    Hull.add(points.get(upper - 2))
    Hull.sortSelf
    if(Turn(Hull.get(0), Hull.get(1), Hull.get(2)).equals("left turn")){
      Hull.link(0, 2, 1)
      Hull.link(1, 0, 2)
      Hull.link(2, 1, 0)
    }
    else{
      Hull.link(0, 1, 2)
      Hull.link(1, 2, 0)
      Hull.link(2, 0, 1)
    }*/
//    Hull = Array(points.get(upper), points.get(upper - 1), points.get(upper - 2))
//    if(Turn(Hull(0), Hull(1), Hull(2)) != "left turn") Hull = Hull.reverse
  }
  def Turn(a : OccupyPoint, b : OccupyPoint, c : OccupyPoint) : String = {
    val ab : Array[Int] = Array(b.x - a.x, b.y - a.y, 0)
    val bc : Array[Int] = Array(c.x - b.x, c.y - b.y, 0)
    val crossProduct = Array(ab(1)*bc(2) - ab(2)*bc(1),ab(2)*bc(0) - ab(0)*bc(2),ab(0)*bc(1) - ab(1)*bc(0))
    if(crossProduct(2) == 0) {return "no turn"}
//    crossProduct(2) = crossProduct(2) / crossProduct(2)
    if(crossProduct(2) < 0) {return "left turn"}
    else {if(crossProduct(2) > 0) {return "right turn"} else "undef"}
  }
  def Merge(points : CircularList, k: Int) : CircularList = {
    var (v,w) = UpperBridge(points, k)
    var (x,y) = LowerBridge(points, k)
    points.deleteFromHull(x, v)
    points.deleteFromHull(w, y)
    points.link(v, w, points.succ(v))
    points.link(w, points.pred(w), v)
    points.link(x, points.pred(x), y)
    points.link(y, x, points.succ(y))
    return points
  }
/*  def UpperBridge2(points : CircularList, left : CircularList, right : CircularList, i : Int, k : Int, j : Int) : (OccupyPoint, OccupyPoint) = {
    var v : OccupyPoint = left.rightmost
    var w : OccupyPoint = right.leftmost
    while(!(Turn(w,v,left.succ(v)).equals("left turn")) || !(Turn(v,w,right.pred(w)).equals("right turn"))){
      while(!(Turn(w,v,left.succ(v)).equals("left turn"))){
        v = left.succ(v)
      }
      while(!(Turn(v,w,right.pred(w)).equals("right turn"))){
        w = right.pred(w)
      }
    }
    return (v,w)
  }*/
  def UpperBridge(points : CircularList, k : Int) : (OccupyPoint, OccupyPoint) = {
    //In order to deal with collinear points, the shifting stops before it closes the cycle. Otherwise it would run into an infinite loop.
    var v : OccupyPoint = points.get(k)
    val vStart : OccupyPoint = v
    var w : OccupyPoint = points.get(k + 1)
    val wStart : OccupyPoint = w
    //I assume that the extreme points in the partial lists are corner points of the convex hull.
    while((points.succ(v) != vStart && !(Turn(w,v,points.succ(v)).equals("left turn"))) || (points.pred(w) != wStart && !(Turn(v,w,points.pred(w)).equals("right turn")))){
      while(points.succ(v) != vStart && !(Turn(w,v,points.succ(v)).equals("left turn"))){
        v = points.succ(v)
      }
      while(points.pred(w) != wStart && !(Turn(v,w,points.pred(w)).equals("right turn"))){
        w = points.pred(w)
      }
    }
    return (v,w)
  }
  def LowerBridge(points : CircularList, k : Int) : (OccupyPoint, OccupyPoint) = {
    //In order to deal with collinear points, the shifting stops before it closes the cycle. Otherwise it would run into an infinite loop.
    var v : OccupyPoint = points.get(k)
    val vStart = v
    var w : OccupyPoint = points.get(k + 1)
    val wStart = w
    while((points.pred(v) != vStart && !(Turn(w,v,points.pred(v)).equals("right turn"))) || (points.succ(w) != wStart && !(Turn(v,w,points.succ(w)).equals("left turn")))){
      while(points.pred(v) != vStart && !(Turn(w,v,points.pred(v)).equals("right turn"))){
        v = points.pred(v)
      }
      while(points.succ(w) != wStart && !(Turn(v,w,points.succ(w)).equals("left turn"))){
        w = points.succ(w)
      }
    }
    return (v,w)
  }
}
//    var hashSet : HashSet[OccupyPoint] = new HashSet
//    hashSet.add(v)
//    hashSet.contains(v)