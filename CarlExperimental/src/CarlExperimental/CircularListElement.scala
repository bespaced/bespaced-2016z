package CarlExperimental
import BeSpaceDCore._

class CircularListElement[T] {
  override def toString() : String = {
    require (data.isDefined, "")
    val p : String = if(pred == null) "null" else pred.getData().toString()
    val s : String = if(succ == null) "null" else succ.getData().toString()
    return data match {
      case None => ???
      case Some(d) => p + d.toString() + s
    }
  }
  var data : Option[T] = None
  var pred : CircularListElement[T] = null
  var succ : CircularListElement[T] = null
  def setData(d : T) = {
    data = Some(d)
  }
  def getData() : T = {
    require (data.isDefined, "")
    return data match {
      case None => ???
      case Some(d) => d
    }
  }
  def setPred(p : CircularListElement[T]) = {
    pred = p
  }
  def getPred() : CircularListElement[T] = {
    return pred
  }
   def setSucc(s : CircularListElement[T]) = {
    succ = s
  }
  def getSucc() : CircularListElement[T] = {
    return succ
  }
}