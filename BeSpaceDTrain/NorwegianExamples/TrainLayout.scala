// Copyright by NTNU students

package BeSpaceDExternalExample_DO_NOT_SHARE

import BeSpaceDCore._
import scala.io.Source
object TrainLayout extends GraphOperations{

				
	def getTrack(n : Int, j : Int): List[Invariant] = {
			var positions : List[Invariant] = Nil
					for (i <- n to j-1) {
						positions ::= Edge(i,i+1)
					}
	return positions
	}

  def trackGraph = BIGAND(
		  // Track 1
		  getTrack(1,47):::
			  List(Edge(47,48))::: //connect to track2        
				  List(Edge(47,90))::: //connect to track3

					  // Track 2
					  getTrack(48,89):::
						  List(Edge(89,518))::: //connect to track18

							  // Track 3
							  getTrack(90,111):::
								  List(Edge(111,112)):::   //connect to track4
                  List(Edge(111,129)):::      //connect to track5
                  
                  // Track 4
                getTrack(112,128):::
                  List(Edge(128,148)):::   //connect to track6
                  
                   // Track 5
                getTrack(129,147):::
                  List(Edge(147,148)):::   //connect to track6
                  
                    // Track 6
                getTrack(148,238):::
                  List(Edge(238,239)):::   //connect to track7
                  List(Edge(238,258)):::   //connect to track8
                  
                             // Track 7
                getTrack(239,257):::
                  List(Edge(257,275)):::   //connect to track9
                  
                                // Track 8
                getTrack(258,274):::
                  List(Edge(274,275)):::   //connect to track9
                  
                   // Track 9
                getTrack(275,286):::
                  List(Edge(286,287)):::   //connect to track10
                  List(Edge(286,304)):::   //connect to track11
                  
                  // Track 10
                getTrack(287,303):::
                  List(Edge(303,673)):::   //connect to track23
                  
                // Track 11
                getTrack(304,344):::
                  List(Edge(344,381)):::   //connect to track14
                  List(Edge(344,402)):::   //connect to track15
                  
                      // Track 12
                getTrack(345,361):::
                  List(Edge(361,425)):::   //connect to track16
                  
                 // Track 13
                getTrack(362,380):::
                  List(Edge(380,425)):::   //connect to track16
                  
                // Track 14
                getTrack(381,401):::
                List(Edge(401,444)):::   //connect to track17
                
                  // Track 15
                getTrack(402,424):::
                List(Edge(424,444)):::   //connect to track17
                
                   // Track 16
                getTrack(425,443):::
                List(Edge(443,518)):::   //connect to track18
                
                  // Track 17
                getTrack(444,517):::
                List(Edge(517,546)):::   //connect to track19
                
                  // Track 18
                getTrack(518,545):::
                List(Edge(545,546)):::   //connect to track19
                
                  // Track 19
                getTrack(546,573):::
                List(Edge(573,574)):::   //connect to track20
                List(Edge(573,591)):::   //connect to track21

               // Track 20
                getTrack(574,590):::
                List(Edge(590,610)):::   //connect to track22
                
                // Track 21
                getTrack(591,609):::
                List(Edge(609,610)):::   //connect to track22
                
                // Track 22
                getTrack(610,672):::
                List(Edge(672,1)):::   //connect to track1
                
                // Track 23
                getTrack(673,705):::
                List(Edge(705,345)):::   //connect to track12
                List(Edge(705,362))   //connect to track13
		  )
      
      // provided by Kristoffer and SImon (May 2015)
      
       def mapNodesToGeometricPosition() =
    // Track 1 [1,47]
    List(
      IMPLIES(OccupyNode(1),OccupyBox(210,460,220,470)),
      IMPLIES(OccupyNode(2),OccupyBox(210,450,220,460)),
      IMPLIES(OccupyNode(3),OccupyBox(210,440,220,450)),
      IMPLIES(OccupyNode(4),OccupyBox(210,430,220,440)),
      IMPLIES(OccupyNode(5),OccupyBox(210,420,220,430)),
      IMPLIES(OccupyNode(6),OccupyBox(210,410,220,420)),
      IMPLIES(OccupyNode(7),OccupyBox(210,400,220,410)),
      IMPLIES(OccupyNode(8),OccupyBox(210,390,220,400)),
      IMPLIES(OccupyNode(9),OccupyBox(210,380,220,390)),
      IMPLIES(OccupyNode(10),OccupyBox(210,370,220,380)),
      IMPLIES(OccupyNode(11),OccupyBox(210,360,220,370)),
      IMPLIES(OccupyNode(12),OccupyBox(210,350,220,360)),
      IMPLIES(OccupyNode(13),OccupyBox(216,340,226,350)),
      IMPLIES(OccupyNode(14),OccupyBox(222,334,232,344)),
      IMPLIES(OccupyNode(15),OccupyBox(228,328,238,338)),
      IMPLIES(OccupyNode(16),OccupyBox(234,322,244,332)),
      IMPLIES(OccupyNode(17),OccupyBox(240,316,250,326)),
      IMPLIES(OccupyNode(18),OccupyBox(246,310,256,320)),
      IMPLIES(OccupyNode(19),OccupyBox(252,304,262,214)),
      IMPLIES(OccupyNode(20),OccupyBox(258,298,268,308)),
      IMPLIES(OccupyNode(21),OccupyBox(264,292,274,302)),
      IMPLIES(OccupyNode(22),OccupyBox(270,286,280,296)),
      IMPLIES(OccupyNode(23),OccupyBox(276,280,786,290)),
      IMPLIES(OccupyNode(24),OccupyBox(282,274,292,284)),
      IMPLIES(OccupyNode(25),OccupyBox(288,268,298,278)),
      IMPLIES(OccupyNode(26),OccupyBox(294,262,304,272)),
      IMPLIES(OccupyNode(27),OccupyBox(297,256,307,266)),
      IMPLIES(OccupyNode(28),OccupyBox(300,250,310,260)),
      IMPLIES(OccupyNode(29),OccupyBox(310,250,320,260)),
      IMPLIES(OccupyNode(30),OccupyBox(320,250,330,260)),
      IMPLIES(OccupyNode(31),OccupyBox(330,250,340,260)),
      IMPLIES(OccupyNode(32),OccupyBox(340,250,350,260)),
      IMPLIES(OccupyNode(33),OccupyBox(350,250,360,260)),
      IMPLIES(OccupyNode(34),OccupyBox(360,250,370,260)),
      IMPLIES(OccupyNode(35),OccupyBox(370,250,380,260)),
      IMPLIES(OccupyNode(36),OccupyBox(380,250,390,260)),
      IMPLIES(OccupyNode(37),OccupyBox(390,250,400,260)),
      IMPLIES(OccupyNode(38),OccupyBox(400,250,410,260)),
      IMPLIES(OccupyNode(39),OccupyBox(410,250,420,260)),
      IMPLIES(OccupyNode(40),OccupyBox(420,250,430,260)),
      IMPLIES(OccupyNode(41),OccupyBox(430,250,440,260)),
      IMPLIES(OccupyNode(42),OccupyBox(440,250,450,260)),
      IMPLIES(OccupyNode(43),OccupyBox(450,250,460,260)),
      IMPLIES(OccupyNode(44),OccupyBox(460,250,470,260)),
      IMPLIES(OccupyNode(45),OccupyBox(470,250,480,260)),
      IMPLIES(OccupyNode(46),OccupyBox(480,250,490,260)),
      IMPLIES(OccupyNode(47),OccupyBox(490,250,500,260)),
      
    // Track 2 [48,89]
      IMPLIES(OccupyNode(48),OccupyBox(490,247,500,257)),
      IMPLIES(OccupyNode(49),OccupyBox(497,241,507,251)),
      IMPLIES(OccupyNode(50),OccupyBox(504,238,514,248)),
      IMPLIES(OccupyNode(51),OccupyBox(507,236,517,246)),
      IMPLIES(OccupyNode(52),OccupyBox(511,234,521,244)),
      IMPLIES(OccupyNode(53),OccupyBox(514,232,524,242)),
      IMPLIES(OccupyNode(54),OccupyBox(518,230,528,240)),
      IMPLIES(OccupyNode(55),OccupyBox(521,228,531,238)),
      IMPLIES(OccupyNode(56),OccupyBox(524,226,534,236)),
      IMPLIES(OccupyNode(57),OccupyBox(530,222,540,232)),
      IMPLIES(OccupyNode(58),OccupyBox(534,220,544,230)),
      IMPLIES(OccupyNode(59),OccupyBox(538,218,548,228)),
      IMPLIES(OccupyNode(60),OccupyBox(542,216,552,226)),
      IMPLIES(OccupyNode(61),OccupyBox(548,210,558,220)),
      IMPLIES(OccupyNode(62),OccupyBox(551,207,561,217)),
      IMPLIES(OccupyNode(63),OccupyBox(554,204,564,214)),
      IMPLIES(OccupyNode(64),OccupyBox(557,200,567,210)),
      IMPLIES(OccupyNode(65),OccupyBox(560,196,570,206)),
      IMPLIES(OccupyNode(66),OccupyBox(562,192,572,202)),
      IMPLIES(OccupyNode(67),OccupyBox(566,189,576,299)),
      IMPLIES(OccupyNode(68),OccupyBox(570,185,580,295)),
      IMPLIES(OccupyNode(69),OccupyBox(567,181,577,191)),
      IMPLIES(OccupyNode(70),OccupyBox(564,177,574,187)),
      IMPLIES(OccupyNode(71),OccupyBox(560,173,570,183)),
      IMPLIES(OccupyNode(72),OccupyBox(557,170,567,180)),
      IMPLIES(OccupyNode(73),OccupyBox(554,166,564,176)),
      IMPLIES(OccupyNode(74),OccupyBox(551,163,561,173)),
      IMPLIES(OccupyNode(75),OccupyBox(548,160,558,170)),
      IMPLIES(OccupyNode(76),OccupyBox(545,156,555,166)),
      IMPLIES(OccupyNode(77),OccupyBox(541,154,551,164)),
      IMPLIES(OccupyNode(78),OccupyBox(538,152,548,162)),
      IMPLIES(OccupyNode(79),OccupyBox(531,148,541,158)),
      IMPLIES(OccupyNode(80),OccupyBox(527,145,537,155)),
      IMPLIES(OccupyNode(81),OccupyBox(524,142,534,152)),
      IMPLIES(OccupyNode(82),OccupyBox(520,141,530,151)),
      IMPLIES(OccupyNode(83),OccupyBox(517,139,527,149)) , 
      IMPLIES(OccupyNode(84),OccupyBox(513,137,523,147)) ,
      IMPLIES(OccupyNode(85),OccupyBox(510,135,520,145)) ,
      IMPLIES(OccupyNode(86),OccupyBox(503,131,513,141)) ,
      IMPLIES(OccupyNode(87),OccupyBox(497,127,507,137)) ,
      IMPLIES(OccupyNode(88),OccupyBox(490,123,500,133))  ,
      IMPLIES(OccupyNode(89),OccupyBox(490,123,500,133)),      
    
    // Track 3 [90,111]
    IMPLIES(OccupyNode(90),OccupyBox(490,250,500,260)),
    IMPLIES(OccupyNode(91),OccupyBox(500,250,510,260)),
    IMPLIES(OccupyNode(92),OccupyBox(510,250,520,260)),
    IMPLIES(OccupyNode(93),OccupyBox(520,250,530,260)),
    IMPLIES(OccupyNode(94),OccupyBox(530,250,540,260)),
    IMPLIES(OccupyNode(95),OccupyBox(540,250,550,260)),
    IMPLIES(OccupyNode(96),OccupyBox(550,250,560,260)),
    IMPLIES(OccupyNode(97),OccupyBox(560,250,570,260)),
    IMPLIES(OccupyNode(98),OccupyBox(570,250,580,260)),
    IMPLIES(OccupyNode(99),OccupyBox(580,250,590,260)),
    IMPLIES(OccupyNode(100),OccupyBox(590,250,600,260)),
    IMPLIES(OccupyNode(101),OccupyBox(600,250,610,260)),
    IMPLIES(OccupyNode(102),OccupyBox(610,250,620,260)),
    IMPLIES(OccupyNode(103),OccupyBox(620,250,630,260)),
    IMPLIES(OccupyNode(104),OccupyBox(630,250,640,260)),
    IMPLIES(OccupyNode(105),OccupyBox(640,250,650,260)),
    IMPLIES(OccupyNode(106),OccupyBox(650,250,660,260)),
    IMPLIES(OccupyNode(107),OccupyBox(660,250,670,260)),
    IMPLIES(OccupyNode(109),OccupyBox(670,250,680,260)),
    IMPLIES(OccupyNode(110),OccupyBox(680,250,690,260)),
    IMPLIES(OccupyNode(111),OccupyBox(690,250,700,260)),
    
    // Track 4 [112,128]
    IMPLIES(OccupyNode(112),OccupyBox(700,250,710,260)),
    IMPLIES(OccupyNode(113),OccupyBox(710,250,720,260)),
    IMPLIES(OccupyNode(114),OccupyBox(720,250,730,260)),
    IMPLIES(OccupyNode(115),OccupyBox(730,250,740,260)),
    IMPLIES(OccupyNode(116),OccupyBox(740,250,750,260)),
    IMPLIES(OccupyNode(117),OccupyBox(750,250,760,260)),
    IMPLIES(OccupyNode(118),OccupyBox(760,250,770,260)),
    IMPLIES(OccupyNode(119),OccupyBox(770,250,780,260)),
    IMPLIES(OccupyNode(120),OccupyBox(780,250,790,260)),
    IMPLIES(OccupyNode(121),OccupyBox(790,250,800,260)),
    IMPLIES(OccupyNode(122),OccupyBox(800,250,810,260)),
    IMPLIES(OccupyNode(123),OccupyBox(810,250,820,260)),
    IMPLIES(OccupyNode(124),OccupyBox(820,250,830,260)),
    IMPLIES(OccupyNode(125),OccupyBox(830,250,840,260)),
    IMPLIES(OccupyNode(126),OccupyBox(840,250,850,260)),
    IMPLIES(OccupyNode(127),OccupyBox(850,250,860,260)),
    IMPLIES(OccupyNode(128),OccupyBox(850,250,860,260)),
    
    // Track 5 [129,147]
    IMPLIES(OccupyNode(129),OccupyBox(700,245,710,255)),
    IMPLIES(OccupyNode(130),OccupyBox(707,239,717,249)),
    IMPLIES(OccupyNode(131),OccupyBox(715,233,725,243)),
    IMPLIES(OccupyNode(132),OccupyBox(722,226,732,236)),
    IMPLIES(OccupyNode(133),OccupyBox(731,223,741,233)),
    IMPLIES(OccupyNode(134),OccupyBox(740,220,750,230)),
    IMPLIES(OccupyNode(135),OccupyBox(750,220,760,230)),
    IMPLIES(OccupyNode(136),OccupyBox(760,220,770,230)),
    IMPLIES(OccupyNode(137),OccupyBox(770,220,780,230)),
    IMPLIES(OccupyNode(138),OccupyBox(780,220,790,230)),
    IMPLIES(OccupyNode(139),OccupyBox(790,220,800,230)),
    IMPLIES(OccupyNode(140),OccupyBox(800,220,810,230)),
    IMPLIES(OccupyNode(141),OccupyBox(809,223,819,233)),
    IMPLIES(OccupyNode(142),OccupyBox(816,229,826,239)),
    IMPLIES(OccupyNode(143),OccupyBox(826,232,836,242)),
    IMPLIES(OccupyNode(144),OccupyBox(832,235,842,245)),
    IMPLIES(OccupyNode(145),OccupyBox(841,240,851,250)),
    IMPLIES(OccupyNode(146),OccupyBox(850,245,860,255)),
    IMPLIES(OccupyNode(147),OccupyBox(850,245,860,255)),
    
    // Track 6 [148,238]
    IMPLIES(OccupyNode(148),OccupyBox(860,250,870,260)),
    IMPLIES(OccupyNode(149),OccupyBox(870,250,880,260)),
    IMPLIES(OccupyNode(150),OccupyBox(880,250,890,260)),
      IMPLIES(OccupyNode(151),OccupyBox(890,250,900,260)),
    IMPLIES(OccupyNode(152),OccupyBox(900,250,910,260)),
      IMPLIES(OccupyNode(153),OccupyBox(910,250,920,260)),
    IMPLIES(OccupyNode(154),OccupyBox(920,250,930,260)),
    IMPLIES(OccupyNode(155),OccupyBox(930,250,940,260)),
    IMPLIES(OccupyNode(156),OccupyBox(939,253,950,263)),
    IMPLIES(OccupyNode(157),OccupyBox(948,255,960,265)),
    IMPLIES(OccupyNode(158),OccupyBox(957,257,967,267)),
    IMPLIES(OccupyNode(159),OccupyBox(965,260,975,270)),
    IMPLIES(OccupyNode(160),OccupyBox(974,265,984,275)),
    IMPLIES(OccupyNode(161),OccupyBox(983,270,993,280)),
    IMPLIES(OccupyNode(162),OccupyBox(992,275,1002,285)),
    IMPLIES(OccupyNode(163),OccupyBox(1000,280,1010,290)),
    IMPLIES(OccupyNode(164),OccupyBox(1007,287,1017,297)),
    IMPLIES(OccupyNode(165),OccupyBox(1013,293,1023,303)),
    IMPLIES(OccupyNode(166),OccupyBox(1021,301,1031,311)),
    IMPLIES(OccupyNode(167),OccupyBox(1031,311,1041,321)),
    IMPLIES(OccupyNode(168),OccupyBox(1040,320,1041,330)),
    IMPLIES(OccupyNode(169),OccupyBox(1049,329,1041,339)),
    IMPLIES(OccupyNode(170),OccupyBox(1059,340,1041,350)),
    IMPLIES(OccupyNode(171),OccupyBox(1060,350,1070,360)),
    IMPLIES(OccupyNode(172),OccupyBox(1060,360,1070,370)),
    IMPLIES(OccupyNode(173),OccupyBox(1060,370,1070,380)),
    IMPLIES(OccupyNode(174),OccupyBox(1060,380,1070,390)),
    IMPLIES(OccupyNode(175),OccupyBox(1060,390,1070,400)),
    IMPLIES(OccupyNode(176),OccupyBox(1060,400,1070,410)),
    IMPLIES(OccupyNode(177),OccupyBox(1060,410,1070,420)),
    IMPLIES(OccupyNode(178),OccupyBox(1060,420,1070,430)),
    IMPLIES(OccupyNode(179),OccupyBox(1060,430,1070,440)),
    IMPLIES(OccupyNode(180),OccupyBox(1060,440,1070,450)),
    IMPLIES(OccupyNode(181),OccupyBox(1060,450,1070,460)),
    IMPLIES(OccupyNode(182),OccupyBox(1060,460,1070,470)),
    IMPLIES(OccupyNode(183),OccupyBox(1060,470,1070,480)),
    IMPLIES(OccupyNode(184),OccupyBox(1060,480,1070,490)),
    IMPLIES(OccupyNode(185),OccupyBox(1060,490,1070,500)),
    IMPLIES(OccupyNode(186),OccupyBox(1060,500,1070,510)),
    IMPLIES(OccupyNode(187),OccupyBox(1060,510,1070,520)),
    IMPLIES(OccupyNode(188),OccupyBox(1060,520,1070,530)),
    IMPLIES(OccupyNode(189),OccupyBox(1060,530,1070,540)),
    IMPLIES(OccupyNode(190),OccupyBox(1060,540,1070,550)),
    IMPLIES(OccupyNode(191),OccupyBox(1060,550,1070,560)),
    IMPLIES(OccupyNode(192),OccupyBox(1060,560,1070,570)),
    IMPLIES(OccupyNode(193),OccupyBox(1060,570,1070,580)),
    IMPLIES(OccupyNode(194),OccupyBox(1060,580,1070,590)),
    IMPLIES(OccupyNode(195),OccupyBox(1060,590,1070,600)),
    IMPLIES(OccupyNode(196),OccupyBox(1060,600,1070,610)),
    IMPLIES(OccupyNode(197),OccupyBox(1060,610,1070,620)),
    IMPLIES(OccupyNode(198),OccupyBox(1060,620,1070,630)),
    IMPLIES(OccupyNode(199),OccupyBox(1060,630,1070,640)),
    IMPLIES(OccupyNode(200),OccupyBox(1060,640,1070,650)),
    IMPLIES(OccupyNode(201),OccupyBox(1060,650,1070,660)),
    IMPLIES(OccupyNode(202),OccupyBox(1068,657,1078,667)),
    IMPLIES(OccupyNode(203),OccupyBox(1076,664,1086,674)),
    IMPLIES(OccupyNode(204),OccupyBox(1084,671,1094,681)),
    IMPLIES(OccupyNode(205),OccupyBox(1093,678,1103,688)),
    IMPLIES(OccupyNode(206),OccupyBox(1101,685,1111,695)),
    IMPLIES(OccupyNode(207),OccupyBox(1109,692,1119,702)),
    IMPLIES(OccupyNode(208),OccupyBox(1117,698,1127,708)),
    IMPLIES(OccupyNode(209),OccupyBox(1125,705,1135,715)),
    IMPLIES(OccupyNode(210),OccupyBox(1133,709,1143,719)),
    IMPLIES(OccupyNode(211),OccupyBox(1141,713,1151,723)),
    IMPLIES(OccupyNode(212),OccupyBox(1149,716,1159,726)),
    IMPLIES(OccupyNode(213),OccupyBox(1157,720,1167,730)),
    IMPLIES(OccupyNode(214),OccupyBox(1165,722,1175,732)),
    IMPLIES(OccupyNode(215),OccupyBox(1173,725,1183,735)),
    IMPLIES(OccupyNode(216),OccupyBox(1182,727,1192,737)),
    IMPLIES(OccupyNode(217),OccupyBox(1190,730,1200,740)),
    IMPLIES(OccupyNode(218),OccupyBox(1195,729,1205,739)),
    IMPLIES(OccupyNode(219),OccupyBox(1200,728,1210,738)),
    IMPLIES(OccupyNode(220),OccupyBox(1208,726,1218,736)),
    IMPLIES(OccupyNode(221),OccupyBox(1217,724,1227,734)),
    IMPLIES(OccupyNode(222),OccupyBox(1225,722,1235,732)),
    IMPLIES(OccupyNode(223),OccupyBox(1233,718,1243,728)),
    IMPLIES(OccupyNode(224),OccupyBox(1242,714,1252,724)),
    IMPLIES(OccupyNode(225),OccupyBox(1251,710,1261,720)),
    IMPLIES(OccupyNode(226),OccupyBox(1260,705,1270,715)),
    IMPLIES(OccupyNode(227),OccupyBox(1267,698,1277,708)),
    IMPLIES(OccupyNode(228),OccupyBox(1273,691,1283,701)),
    IMPLIES(OccupyNode(229),OccupyBox(1279,684,1289,694)),
    IMPLIES(OccupyNode(230),OccupyBox(1285,677,1295,687)),
    IMPLIES(OccupyNode(231),OccupyBox(1292,670,1302,680)),
    IMPLIES(OccupyNode(232),OccupyBox(1298,663,1308,673)),
    IMPLIES(OccupyNode(233),OccupyBox(1304,657,1314,667)),
    IMPLIES(OccupyNode(234),OccupyBox(1310,650,1320,660)),
    IMPLIES(OccupyNode(235),OccupyBox(1310,640,1320,650)),
    IMPLIES(OccupyNode(236),OccupyBox(1310,630,1320,640)),
    IMPLIES(OccupyNode(237),OccupyBox(1310,620,1320,630)),
    IMPLIES(OccupyNode(238),OccupyBox(1310,620,1320,630)),
   
    // Track 7 [239,257]
    IMPLIES(OccupyNode(239),OccupyBox(1302,605,1312,615)),
    IMPLIES(OccupyNode(240),OccupyBox(1298,600,1308,610)),
    IMPLIES(OccupyNode(241),OccupyBox(1293,595,1303,605)),
    IMPLIES(OccupyNode(242),OccupyBox(1289,586,1299,596)),
    IMPLIES(OccupyNode(243),OccupyBox(1284,577,1294,587)),
    IMPLIES(OccupyNode(244),OccupyBox(1279,566,1289,576)),
    IMPLIES(OccupyNode(245),OccupyBox(1274,557,1284,567)),
    IMPLIES(OccupyNode(246),OccupyBox(1269,548,1279,558)),
    IMPLIES(OccupyNode(247),OccupyBox(1265,539,1275,549)),
    IMPLIES(OccupyNode(248),OccupyBox(1263,530,1273,540)),
    IMPLIES(OccupyNode(249),OccupyBox(1266,525,1276,535)),
    IMPLIES(OccupyNode(250),OccupyBox(1272,519,1282,529)),
    IMPLIES(OccupyNode(251),OccupyBox(1278,510,1288,520)),
    IMPLIES(OccupyNode(252),OccupyBox(1284,501,1294,511)),
    IMPLIES(OccupyNode(253),OccupyBox(1290,492,1300,502)),
    IMPLIES(OccupyNode(254),OccupyBox(1294,483,1304,493)),
    IMPLIES(OccupyNode(255),OccupyBox(1296,474,1306,484)),
    IMPLIES(OccupyNode(256),OccupyBox(1302,465,1312,475)),
    IMPLIES(OccupyNode(257),OccupyBox(1302,465,1312,475)),
    
    // Track 8 [258,274]
    IMPLIES(OccupyNode(258),OccupyBox(1310,610,1320,620)),
    IMPLIES(OccupyNode(259),OccupyBox(1310,600,1320,610)),
    IMPLIES(OccupyNode(260),OccupyBox(1310,590,1320,600)),
    IMPLIES(OccupyNode(261),OccupyBox(1310,580,1320,590)),
    IMPLIES(OccupyNode(262),OccupyBox(1310,570,1320,580)),
    IMPLIES(OccupyNode(263),OccupyBox(1310,560,1320,570)),
    IMPLIES(OccupyNode(264),OccupyBox(1310,550,1320,560)),
    IMPLIES(OccupyNode(265),OccupyBox(1310,540,1320,550)),
    IMPLIES(OccupyNode(266),OccupyBox(1310,530,1320,540)),
    IMPLIES(OccupyNode(267),OccupyBox(1310,520,1320,530)),
    IMPLIES(OccupyNode(268),OccupyBox(1310,510,1320,520)),
    IMPLIES(OccupyNode(269),OccupyBox(1310,500,1320,510)),
    IMPLIES(OccupyNode(270),OccupyBox(1310,490,1320,500)),
    IMPLIES(OccupyNode(271),OccupyBox(1310,480,1320,490)),
    IMPLIES(OccupyNode(272),OccupyBox(1310,470,1320,480)),
    IMPLIES(OccupyNode(273),OccupyBox(1310,460,1320,470)),
    IMPLIES(OccupyNode(274),OccupyBox(1310,460,1320,470)),
    
    // Track 9 [275,286]
    IMPLIES(OccupyNode(275),OccupyBox(1310,450,1320,460)),
    IMPLIES(OccupyNode(276),OccupyBox(1310,440,1320,450)),
    IMPLIES(OccupyNode(277),OccupyBox(1310,430,1320,440)),
    IMPLIES(OccupyNode(278),OccupyBox(1310,420,1320,430)),
    IMPLIES(OccupyNode(279),OccupyBox(1310,410,1320,420)),
    IMPLIES(OccupyNode(280),OccupyBox(1310,400,1320,410)),
    IMPLIES(OccupyNode(281),OccupyBox(1310,390,1320,400)),
    IMPLIES(OccupyNode(282),OccupyBox(1310,380,1320,390)),
    IMPLIES(OccupyNode(283),OccupyBox(1310,370,1320,380)),
    IMPLIES(OccupyNode(284),OccupyBox(1310,360,1320,370)),
    IMPLIES(OccupyNode(285),OccupyBox(1310,350,1320,360)),
    IMPLIES(OccupyNode(286),OccupyBox(1310,350,1320,360)),
    
    // Track 10 [287,303]
    IMPLIES(OccupyNode(287),OccupyBox(1300,348,1310,358)),
    IMPLIES(OccupyNode(288),OccupyBox(1289,340,1299,350)),
    IMPLIES(OccupyNode(289),OccupyBox(1278,332,1288,342)),
    IMPLIES(OccupyNode(290),OccupyBox(1265,320,1275,330)),
    IMPLIES(OccupyNode(291),OccupyBox(1255,308,1265,318)),
    IMPLIES(OccupyNode(292),OccupyBox(1245,296,1255,306)),
    IMPLIES(OccupyNode(293),OccupyBox(1235,284,1245,294)),
    IMPLIES(OccupyNode(294),OccupyBox(1225,272,1235,282)),
    IMPLIES(OccupyNode(295),OccupyBox(1215,260,1225,270)),
    IMPLIES(OccupyNode(296),OccupyBox(1205,248,1215,258)),
    IMPLIES(OccupyNode(297),OccupyBox(1195,236,1205,246)),
    IMPLIES(OccupyNode(298),OccupyBox(1185,224,1195,234)),
    IMPLIES(OccupyNode(299),OccupyBox(1175,212,1185,222)),
    IMPLIES(OccupyNode(300),OccupyBox(1165,200,1175,210)),
    IMPLIES(OccupyNode(301),OccupyBox(1155,188,1165,198)),
    IMPLIES(OccupyNode(302),OccupyBox(1140,176,1150,186)),
    IMPLIES(OccupyNode(303),OccupyBox(1140,176,1150,186)),
    
    // Track 11 [304,344]
    IMPLIES(OccupyNode(304),OccupyBox(1310,340,1320,350)),
    IMPLIES(OccupyNode(305),OccupyBox(1310,330,1320,340)),
    IMPLIES(OccupyNode(306),OccupyBox(1310,320,1320,330)),
    IMPLIES(OccupyNode(307),OccupyBox(1310,310,1320,320)),
    IMPLIES(OccupyNode(308),OccupyBox(1310,300,1320,310)),
    IMPLIES(OccupyNode(309),OccupyBox(1310,290,1320,300)),
    IMPLIES(OccupyNode(310),OccupyBox(1310,280,1320,290)),
    IMPLIES(OccupyNode(311),OccupyBox(1310,270,1320,280)),
    IMPLIES(OccupyNode(312),OccupyBox(1310,260,1320,270)),
    IMPLIES(OccupyNode(313),OccupyBox(1310,250,1320,260)),
    IMPLIES(OccupyNode(314),OccupyBox(1310,240,1320,250)),
    IMPLIES(OccupyNode(315),OccupyBox(1310,230,1320,240)),
    IMPLIES(OccupyNode(316),OccupyBox(1310,220,1320,230)),
    IMPLIES(OccupyNode(317),OccupyBox(1310,210,1320,220)),
    IMPLIES(OccupyNode(318),OccupyBox(1310,200,1320,210)),
    IMPLIES(OccupyNode(319),OccupyBox(1310,190,1320,200)),
    IMPLIES(OccupyNode(320),OccupyBox(1310,180,1320,190)),
    IMPLIES(OccupyNode(321),OccupyBox(1310,170,1320,180)),
    IMPLIES(OccupyNode(322),OccupyBox(1310,160,1320,170)),
    IMPLIES(OccupyNode(323),OccupyBox(1310,150,1320,160)),
    IMPLIES(OccupyNode(324),OccupyBox(1310,140,1320,150)),
    IMPLIES(OccupyNode(325),OccupyBox(1310,130,1320,140)),
    IMPLIES(OccupyNode(326),OccupyBox(1310,120,1320,130)),
    IMPLIES(OccupyNode(327),OccupyBox(1310,110,1320,120)),
    IMPLIES(OccupyNode(328),OccupyBox(1310,100,1320,110)),
    IMPLIES(OccupyNode(329),OccupyBox(1308,90,1318,100)),
    IMPLIES(OccupyNode(330),OccupyBox(1300,82,1310,92)),
    IMPLIES(OccupyNode(331),OccupyBox(1293,74,1303,84)),
    IMPLIES(OccupyNode(332),OccupyBox(1286,69,1296,79)),
    IMPLIES(OccupyNode(333),OccupyBox(1279,63,1289,73)),
    IMPLIES(OccupyNode(334),OccupyBox(1269,56,1279,66)),
    IMPLIES(OccupyNode(335),OccupyBox(1260,48,1270,58)),
    IMPLIES(OccupyNode(336),OccupyBox(1251,40,1261,50)),
    IMPLIES(OccupyNode(337),OccupyBox(1242,32,1252,42)),
    IMPLIES(OccupyNode(338),OccupyBox(1234,28,1244,38)),
    IMPLIES(OccupyNode(339),OccupyBox(1224,23,1234,33)),
    IMPLIES(OccupyNode(340),OccupyBox(1215,20,1225,30)),
    IMPLIES(OccupyNode(341),OccupyBox(1206,17,1216,27)),
    IMPLIES(OccupyNode(342),OccupyBox(1197,14,1207,24)),
    IMPLIES(OccupyNode(343),OccupyBox(1190,12,1200,22)),
    IMPLIES(OccupyNode(344),OccupyBox(1190,12,1200,22)),
    
    // Track 12 [345,361]
    IMPLIES(OccupyNode(345),OccupyBox(810,120,820,130)),
    IMPLIES(OccupyNode(346),OccupyBox(800,120,810,130)),
    IMPLIES(OccupyNode(347),OccupyBox(790,120,800,130)),
    IMPLIES(OccupyNode(348),OccupyBox(780,120,790,130)),
    IMPLIES(OccupyNode(349),OccupyBox(770,120,780,130)),
    IMPLIES(OccupyNode(350),OccupyBox(760,120,770,130)),
    IMPLIES(OccupyNode(351),OccupyBox(750,120,760,130)),
    IMPLIES(OccupyNode(352),OccupyBox(740,120,750,130)),
    IMPLIES(OccupyNode(353),OccupyBox(730,120,740,130)),
    IMPLIES(OccupyNode(354),OccupyBox(720,120,730,130)),
    IMPLIES(OccupyNode(355),OccupyBox(710,120,720,130)),
    IMPLIES(OccupyNode(356),OccupyBox(700,120,710,130)),
    IMPLIES(OccupyNode(357),OccupyBox(690,120,700,130)),
    IMPLIES(OccupyNode(358),OccupyBox(680,120,690,130)),
    IMPLIES(OccupyNode(359),OccupyBox(670,120,680,130)),
    IMPLIES(OccupyNode(360),OccupyBox(660,120,670,130)),
    IMPLIES(OccupyNode(361),OccupyBox(660,120,670,130)),
    
    // Track 13 [362,380]
    IMPLIES(OccupyNode(362),OccupyBox(810,115,820,125)),
    IMPLIES(OccupyNode(363),OccupyBox(800,110,810,120)) ,
    IMPLIES(OccupyNode(364),OccupyBox(792,105,802,115)) ,
    IMPLIES(OccupyNode(365),OccupyBox(786,102,796,112)) ,
    IMPLIES(OccupyNode(366),OccupyBox(776,99,786,109)) ,
    IMPLIES(OccupyNode(367),OccupyBox(769,93,779,103)) ,
    IMPLIES(OccupyNode(368),OccupyBox(760,90,770,100)) ,
    IMPLIES(OccupyNode(369),OccupyBox(750,90,760,100)) ,
    IMPLIES(OccupyNode(370),OccupyBox(740,90,750,100)) ,
    IMPLIES(OccupyNode(371),OccupyBox(730,90,720,100)) ,
    IMPLIES(OccupyNode(372),OccupyBox(720,90,730,100)) ,
    IMPLIES(OccupyNode(373),OccupyBox(710,90,720,100)) ,
    IMPLIES(OccupyNode(374),OccupyBox(700,90,700,100)) ,
    IMPLIES(OccupyNode(375),OccupyBox(691,93,701,103)) ,
    IMPLIES(OccupyNode(376),OccupyBox(682,96,692,106)) ,
    IMPLIES(OccupyNode(377),OccupyBox(675,103,685,113)) ,
    IMPLIES(OccupyNode(378),OccupyBox(667,109,677,119)) ,
    IMPLIES(OccupyNode(379),OccupyBox(660,115,670,125)) ,
    IMPLIES(OccupyNode(380),OccupyBox(660,115,670,125)) ,
    
    // Track 14 [381,401]
    IMPLIES(OccupyNode(381),OccupyBox(1180,10,1190,20)),
    IMPLIES(OccupyNode(382),OccupyBox(1170,10,1180,20)),
    IMPLIES(OccupyNode(383),OccupyBox(1160,10,1170,20)),
    IMPLIES(OccupyNode(384),OccupyBox(1150,10,1160,20)),
    IMPLIES(OccupyNode(385),OccupyBox(1140,10,1150,20)),
    IMPLIES(OccupyNode(386),OccupyBox(1130,10,1140,20)),
    IMPLIES(OccupyNode(387),OccupyBox(1120,10,1130,20)),
    IMPLIES(OccupyNode(388),OccupyBox(1110,10,1120,20)),
    IMPLIES(OccupyNode(389),OccupyBox(1100,10,1110,20)),
    IMPLIES(OccupyNode(390),OccupyBox(1090,10,1100,20)),
    IMPLIES(OccupyNode(391),OccupyBox(1080,10,1090,20)),
    IMPLIES(OccupyNode(392),OccupyBox(1070,10,1080,20)),
    IMPLIES(OccupyNode(393),OccupyBox(1060,10,1070,20)),
    IMPLIES(OccupyNode(394),OccupyBox(1050,10,1060,20)),
    IMPLIES(OccupyNode(395),OccupyBox(1040,10,1050,20)),
    IMPLIES(OccupyNode(396),OccupyBox(1030,10,1040,20)),
    IMPLIES(OccupyNode(397),OccupyBox(1020,10,1030,20)),
    IMPLIES(OccupyNode(398),OccupyBox(1010,10,1020,20)),
    IMPLIES(OccupyNode(399),OccupyBox(1000,10,1010,20)),
    IMPLIES(OccupyNode(400),OccupyBox(990,10,1000,20)),
    IMPLIES(OccupyNode(401),OccupyBox(990,10,1000,20)),
     
    // Track 15 [402,424]
    IMPLIES(OccupyNode(402),OccupyBox(1175,15,1185,25)),
    IMPLIES(OccupyNode(403),OccupyBox(1170,19,1180,29)),
    IMPLIES(OccupyNode(404),OccupyBox(1165,23,1175,33)),
    IMPLIES(OccupyNode(405),OccupyBox(1155,27,1165,37)),
    IMPLIES(OccupyNode(406),OccupyBox(1145,31,1155,41)),
    IMPLIES(OccupyNode(407),OccupyBox(1135,35,1145,45)),
    IMPLIES(OccupyNode(408),OccupyBox(1125,39,1135,49)),
    IMPLIES(OccupyNode(409),OccupyBox(1115,44,1135,54)),
    IMPLIES(OccupyNode(410),OccupyBox(1105,49,1135,59)),
    IMPLIES(OccupyNode(411),OccupyBox(1095,54,1135,64)),
    IMPLIES(OccupyNode(412),OccupyBox(1085,50,1135,60)),
    IMPLIES(OccupyNode(413),OccupyBox(1080,47,1135,57)),
    IMPLIES(OccupyNode(414),OccupyBox(1076,45,1086,55)),
    IMPLIES(OccupyNode(415),OccupyBox(1067,43,1077,53)),
    IMPLIES(OccupyNode(416),OccupyBox(1058,40,1068,50)),
    IMPLIES(OccupyNode(417),OccupyBox(1049,37,1059,47)),
    IMPLIES(OccupyNode(418),OccupyBox(1040,33,1050,43)),
    IMPLIES(OccupyNode(419),OccupyBox(1031,29,1041,39)),
    IMPLIES(OccupyNode(420),OccupyBox(1022,25,1032,35)),
    IMPLIES(OccupyNode(421),OccupyBox(1013,22,1023,32)),
    IMPLIES(OccupyNode(422),OccupyBox(1004,18,1014,28)),
    IMPLIES(OccupyNode(423),OccupyBox(995,15,1005,25)),
    IMPLIES(OccupyNode(424),OccupyBox(995,15,1005,25)),
    
    // Track 16 [425, 443]
    IMPLIES(OccupyNode(425),OccupyBox(650,120,660,130)),
    IMPLIES(OccupyNode(426),OccupyBox(640,120,650,130)),
    IMPLIES(OccupyNode(427),OccupyBox(630,120,640,130)),
    IMPLIES(OccupyNode(428),OccupyBox(620,120,630,130)),
    IMPLIES(OccupyNode(429),OccupyBox(610,120,620,130)),
    IMPLIES(OccupyNode(430),OccupyBox(600,120,610,130)),
    IMPLIES(OccupyNode(431),OccupyBox(590,120,600,130)),
    IMPLIES(OccupyNode(432),OccupyBox(580,120,590,130)),
    IMPLIES(OccupyNode(433),OccupyBox(570,120,580,130)),
    IMPLIES(OccupyNode(434),OccupyBox(560,120,570,130)),
    IMPLIES(OccupyNode(435),OccupyBox(550,120,560,130)),
    IMPLIES(OccupyNode(436),OccupyBox(540,120,550,130)),
    IMPLIES(OccupyNode(437),OccupyBox(530,120,540,130)),
    IMPLIES(OccupyNode(438),OccupyBox(520,120,530,130)),
    IMPLIES(OccupyNode(439),OccupyBox(510,120,520,130)),
    IMPLIES(OccupyNode(440),OccupyBox(500,120,510,130)),
    IMPLIES(OccupyNode(441),OccupyBox(490,120,500,130)),
    IMPLIES(OccupyNode(442),OccupyBox(490,120,500,130)),
    IMPLIES(OccupyNode(443),OccupyBox(490,120,500,130)),
    
    // Track 17 [444, 517]
    IMPLIES(OccupyNode(444),OccupyBox(980,10,990,20)),
    IMPLIES(OccupyNode(445),OccupyBox(970,10,980,20)),
    IMPLIES(OccupyNode(446),OccupyBox(960,10,970,20)),
    IMPLIES(OccupyNode(447),OccupyBox(950,10,960,20)),
    IMPLIES(OccupyNode(448),OccupyBox(940,10,950,20)),
    IMPLIES(OccupyNode(449),OccupyBox(930,10,940,20)),
    IMPLIES(OccupyNode(450),OccupyBox(920,10,930,20)),
    IMPLIES(OccupyNode(451),OccupyBox(910,10,920,20)),
    IMPLIES(OccupyNode(452),OccupyBox(900,10,910,20)),
    IMPLIES(OccupyNode(453),OccupyBox(890,10,900,20)),
    IMPLIES(OccupyNode(454),OccupyBox(880,10,890,20)),
    IMPLIES(OccupyNode(455),OccupyBox(870,10,880,20)),
    IMPLIES(OccupyNode(456),OccupyBox(860,10,870,20)),
    IMPLIES(OccupyNode(457),OccupyBox(850,10,860,20)),
    IMPLIES(OccupyNode(458),OccupyBox(840,10,850,20)),
    IMPLIES(OccupyNode(459),OccupyBox(830,10,840,20)),
    IMPLIES(OccupyNode(460),OccupyBox(820,10,830,20)),
    IMPLIES(OccupyNode(461),OccupyBox(810,10,820,20)),
    IMPLIES(OccupyNode(462),OccupyBox(800,10,810,20)),
    IMPLIES(OccupyNode(463),OccupyBox(790,10,800,20)),
    IMPLIES(OccupyNode(464),OccupyBox(780,10,790,20)),
    IMPLIES(OccupyNode(465),OccupyBox(770,10,780,20)),
    IMPLIES(OccupyNode(466),OccupyBox(760,10,770,20)),
    IMPLIES(OccupyNode(467),OccupyBox(750,10,760,20)),
    IMPLIES(OccupyNode(468),OccupyBox(740,10,750,20)),
    IMPLIES(OccupyNode(469),OccupyBox(730,10,740,20)),
    IMPLIES(OccupyNode(470),OccupyBox(720,10,730,20)),
    IMPLIES(OccupyNode(471),OccupyBox(710,10,720,20)),
    IMPLIES(OccupyNode(472),OccupyBox(700,10,710,20)),
    IMPLIES(OccupyNode(473),OccupyBox(690,10,700,20)),
    IMPLIES(OccupyNode(474),OccupyBox(680,10,690,20)),
    IMPLIES(OccupyNode(475),OccupyBox(670,10,680,20)),
    IMPLIES(OccupyNode(476),OccupyBox(660,10,670,20)),
    IMPLIES(OccupyNode(477),OccupyBox(650,10,660,20)),
    IMPLIES(OccupyNode(478),OccupyBox(640,10,650,20)),
    IMPLIES(OccupyNode(479),OccupyBox(630,10,640,20)),
    IMPLIES(OccupyNode(480),OccupyBox(620,10,630,20)),
    IMPLIES(OccupyNode(481),OccupyBox(610,10,620,20)),
    IMPLIES(OccupyNode(482),OccupyBox(600,10,610,20)),
    IMPLIES(OccupyNode(483),OccupyBox(590,10,600,20)),
    IMPLIES(OccupyNode(484),OccupyBox(580,10,590,20)),
    IMPLIES(OccupyNode(485),OccupyBox(570,10,580,20)),
    IMPLIES(OccupyNode(486),OccupyBox(560,10,570,20)),
    IMPLIES(OccupyNode(487),OccupyBox(550,10,560,20)),
    IMPLIES(OccupyNode(488),OccupyBox(540,10,550,20)),
    IMPLIES(OccupyNode(489),OccupyBox(530,10,540,20)),
    IMPLIES(OccupyNode(490),OccupyBox(520,10,530,20)),
    IMPLIES(OccupyNode(491),OccupyBox(510,10,520,20)),
    IMPLIES(OccupyNode(492),OccupyBox(500,10,510,20)),
    IMPLIES(OccupyNode(493),OccupyBox(490,10,500,20)),
    IMPLIES(OccupyNode(494),OccupyBox(480,10,490,20)),
    IMPLIES(OccupyNode(495),OccupyBox(470,10,480,20)),
    IMPLIES(OccupyNode(496),OccupyBox(460,10,470,20)),
    IMPLIES(OccupyNode(497),OccupyBox(450,10,460,20)),
    IMPLIES(OccupyNode(498),OccupyBox(440,10,450,20)),
    IMPLIES(OccupyNode(499),OccupyBox(430,10,440,20)),
    IMPLIES(OccupyNode(500),OccupyBox(420,10,430,20)),
    IMPLIES(OccupyNode(501),OccupyBox(410,10,420,20)),
    IMPLIES(OccupyNode(502),OccupyBox(400,10,410,20)),
    IMPLIES(OccupyNode(503),OccupyBox(390,10,400,20)),
    IMPLIES(OccupyNode(504),OccupyBox(380,10,390,20)),
    IMPLIES(OccupyNode(505),OccupyBox(370,10,380,20)),
    IMPLIES(OccupyNode(506),OccupyBox(360,10,370,20)),
    IMPLIES(OccupyNode(507),OccupyBox(350,10,360,20)),
    IMPLIES(OccupyNode(508),OccupyBox(340,10,350,20)),
    IMPLIES(OccupyNode(509),OccupyBox(330,10,340,20)),
    IMPLIES(OccupyNode(510),OccupyBox(320,10,330,20)),
    IMPLIES(OccupyNode(511),OccupyBox(310,10,320,20)),
    IMPLIES(OccupyNode(512),OccupyBox(300,10,310,20)),
    IMPLIES(OccupyNode(513),OccupyBox(290,10,300,20)),
    IMPLIES(OccupyNode(514),OccupyBox(280,10,290,20)),
    IMPLIES(OccupyNode(515),OccupyBox(270,10,280,20)),
    IMPLIES(OccupyNode(516),OccupyBox(260,10,270,20)),
    IMPLIES(OccupyNode(517),OccupyBox(260,10,270,20)),
    
    //TRACK 18 ->  518 to 545  
    IMPLIES(OccupyNode(518),OccupyBox(480,115,490,125)),
    IMPLIES(OccupyNode(519),OccupyBox(469,108,479,118)),
    IMPLIES(OccupyNode(520),OccupyBox(459,102,469,112)),
    IMPLIES(OccupyNode(521),OccupyBox(449,99,459,109)),
    IMPLIES(OccupyNode(522),OccupyBox(439,95,449,105)),
    IMPLIES(OccupyNode(523),OccupyBox(430,92,440,102)),
    IMPLIES(OccupyNode(524),OccupyBox(421,88,431,98)),
    IMPLIES(OccupyNode(525),OccupyBox(412,84,422,94)),
    IMPLIES(OccupyNode(526),OccupyBox(403,81,413,91)),
    IMPLIES(OccupyNode(527),OccupyBox(394,78,104,88)),
    IMPLIES(OccupyNode(528),OccupyBox(385,74,395,84)),
    IMPLIES(OccupyNode(529),OccupyBox(376,70,386,80)),
    IMPLIES(OccupyNode(530),OccupyBox(367,66,377,76)),
    IMPLIES(OccupyNode(531),OccupyBox(358,63,368,73)),
    IMPLIES(OccupyNode(532),OccupyBox(349,59,359,69)),
    IMPLIES(OccupyNode(533),OccupyBox(340,55,350,65)),
    IMPLIES(OccupyNode(534),OccupyBox(331,51,341,61)),
    IMPLIES(OccupyNode(535),OccupyBox(322,48,332,58)),
    IMPLIES(OccupyNode(536),OccupyBox(313,44,323,54)),
    IMPLIES(OccupyNode(537),OccupyBox(304,40,314,50)),
    IMPLIES(OccupyNode(538),OccupyBox(295,37,105,47)),
    IMPLIES(OccupyNode(539),OccupyBox(286,33,296,43)),
    IMPLIES(OccupyNode(540),OccupyBox(277,29,287,39)),
    IMPLIES(OccupyNode(541),OccupyBox(268,26,278,36)),
    IMPLIES(OccupyNode(542),OccupyBox(259,22,269,32)),
    IMPLIES(OccupyNode(543),OccupyBox(254,20,264,30)),
    IMPLIES(OccupyNode(544),OccupyBox(250,18,260,28)),
    IMPLIES(OccupyNode(545),OccupyBox(250,18,260,28)),
    //END TRACK 18
                       
                       
                       
    //TRACK 19 ->  546 to 573  
    IMPLIES(OccupyNode(573),OccupyBox(11,100,21,110)),
    IMPLIES(OccupyNode(572),OccupyBox(11,100,21,110)) ,
    IMPLIES(OccupyNode(571),OccupyBox(20,94,30,104)) ,
    IMPLIES(OccupyNode(570),OccupyBox(29,88,39,98)) ,
    IMPLIES(OccupyNode(569),OccupyBox(38,82,48,92)) ,
    IMPLIES(OccupyNode(568),OccupyBox(47,76,57,86)) ,
    IMPLIES(OccupyNode(567),OccupyBox(56,70,66,80)) ,
    IMPLIES(OccupyNode(566),OccupyBox(65,64,75,74)),
    IMPLIES(OccupyNode(565),OccupyBox(74,58,84,68)),
    IMPLIES(OccupyNode(564),OccupyBox(83,52,93,62)),
    IMPLIES(OccupyNode(563),OccupyBox(92,46,102,56)),
    IMPLIES(OccupyNode(562),OccupyBox(101,40,111,50)),
    IMPLIES(OccupyNode(561),OccupyBox(110,34,120,44)),
    IMPLIES(OccupyNode(560),OccupyBox(119,28,129,38)),
    IMPLIES(OccupyNode(559),OccupyBox(128,22,138,32)),
    IMPLIES(OccupyNode(558),OccupyBox(137,16,147,26)),
    IMPLIES(OccupyNode(567),OccupyBox(140,10,150,20)),
    IMPLIES(OccupyNode(556),OccupyBox(150,10,160,20)),
    IMPLIES(OccupyNode(555),OccupyBox(160,10,170,20)),
    IMPLIES(OccupyNode(554),OccupyBox(170,10,180,20)),
    IMPLIES(OccupyNode(553),OccupyBox(180,10,190,20)),
    IMPLIES(OccupyNode(552),OccupyBox(190,10,200,20)),
    IMPLIES(OccupyNode(551),OccupyBox(200,10,210,20)),
    IMPLIES(OccupyNode(550),OccupyBox(210,10,220,20)),
    IMPLIES(OccupyNode(549),OccupyBox(220,10,230,20)),
    IMPLIES(OccupyNode(548),OccupyBox(230,10,240,20)),
    IMPLIES(OccupyNode(547),OccupyBox(240,10,250,20)),
    IMPLIES(OccupyNode(546),OccupyBox(250,10,260,20)),
    //END TRACK 19 
                       
                       
    //TRACK 20 ->  574 to 590     
    IMPLIES(OccupyNode(574),OccupyBox(10,110,20,120)) , 
    IMPLIES(OccupyNode(575),OccupyBox(10,120,20,130)),
    IMPLIES(OccupyNode(576),OccupyBox(10,130,20,140)),
    IMPLIES(OccupyNode(577),OccupyBox(10,140,20,150)),
    IMPLIES(OccupyNode(578),OccupyBox(10,150,20,160)),
    IMPLIES(OccupyNode(579),OccupyBox(10,160,20,170)),
    IMPLIES(OccupyNode(580),OccupyBox(10,170,20,180)),
    IMPLIES(OccupyNode(581),OccupyBox(10,180,20,190)),
    IMPLIES(OccupyNode(582),OccupyBox(10,190,20,200)),
    IMPLIES(OccupyNode(583),OccupyBox(10,200,20,210)),
    IMPLIES(OccupyNode(584),OccupyBox(10,210,20,220)),
    IMPLIES(OccupyNode(585),OccupyBox(10,220,20,230)),
    IMPLIES(OccupyNode(586),OccupyBox(10,230,20,240)),
    IMPLIES(OccupyNode(587),OccupyBox(10,240,20,250)),
    IMPLIES(OccupyNode(588),OccupyBox(10,250,20,260)),
    IMPLIES(OccupyNode(589),OccupyBox(10,260,20,270)),
    IMPLIES(OccupyNode(590),OccupyBox(10,260,20,270)),
    //END TRACK 20
                       
                       
    //TRACK 21 ->  591 to 609                   
    IMPLIES(OccupyNode(609),OccupyBox(19,265,29,275)),
    IMPLIES(OccupyNode(608),OccupyBox(19,265,29,275)),
    IMPLIES(OccupyNode(607),OccupyBox(23,258,33,268)),
    IMPLIES(OccupyNode(606),OccupyBox(25,249,35,259)),
    IMPLIES(OccupyNode(605),OccupyBox(30,240,40,250)),
    IMPLIES(OccupyNode(604),OccupyBox(35,231,45,241)),
    IMPLIES(OccupyNode(603),OccupyBox(40,222,50,232)),
    IMPLIES(OccupyNode(602),OccupyBox(45,213,55,223)),
    IMPLIES(OccupyNode(601),OccupyBox(50,204,60,214)),
    IMPLIES(OccupyNode(600),OccupyBox(55,195,65,205)),
    IMPLIES(OccupyNode(599),OccupyBox(60,186,70,196)),
    IMPLIES(OccupyNode(598),OccupyBox(56,182,66,192)),
    IMPLIES(OccupyNode(597),OccupyBox(51,174,61,184)),
    IMPLIES(OccupyNode(596),OccupyBox(46,163,56,173)),
    IMPLIES(OccupyNode(595),OccupyBox(41,152,51,162)),
    IMPLIES(OccupyNode(594),OccupyBox(36,141,46,151)),
    IMPLIES(OccupyNode(593),OccupyBox(31,130,41,140)),
    IMPLIES(OccupyNode(592),OccupyBox(26,119,36,129)),
    IMPLIES(OccupyNode(591),OccupyBox(21,108,31,118)),
    //END TRACK 21
                      
    //TRACK 22 -> 610 to 672
    IMPLIES(OccupyNode(610),OccupyBox(10,270,20,280)),
    IMPLIES(OccupyNode(611),OccupyBox(10,280,20,290)),
    IMPLIES(OccupyNode(612),OccupyBox(10,290,20,300)),
    IMPLIES(OccupyNode(613),OccupyBox(10,300,20,310)),
    IMPLIES(OccupyNode(614),OccupyBox(10,310,20,320)),
    IMPLIES(OccupyNode(615),OccupyBox(10,320,20,330)),
    IMPLIES(OccupyNode(616),OccupyBox(10,330,20,340)),
    IMPLIES(OccupyNode(617),OccupyBox(10,340,20,350)),
    IMPLIES(OccupyNode(618),OccupyBox(10,350,20,360)),
    IMPLIES(OccupyNode(619),OccupyBox(10,360,20,370)),
    IMPLIES(OccupyNode(620),OccupyBox(10,370,20,380)),
    IMPLIES(OccupyNode(621),OccupyBox(10,380,20,390)),
    IMPLIES(OccupyNode(622),OccupyBox(10,390,20,400)),
    IMPLIES(OccupyNode(623),OccupyBox(10,400,20,410)),
    IMPLIES(OccupyNode(624),OccupyBox(10,410,20,420)),
    IMPLIES(OccupyNode(625),OccupyBox(10,420,20,430)),
    IMPLIES(OccupyNode(626),OccupyBox(10,430,20,440)),
    IMPLIES(OccupyNode(627),OccupyBox(10,440,20,450)),
    IMPLIES(OccupyNode(628),OccupyBox(10,450,20,460)),
    IMPLIES(OccupyNode(629),OccupyBox(10,460,20,470)),
    IMPLIES(OccupyNode(630),OccupyBox(10,470,20,480)),
    IMPLIES(OccupyNode(631),OccupyBox(10,480,20,490)),
    IMPLIES(OccupyNode(632),OccupyBox(10,490,20,500)),
    IMPLIES(OccupyNode(633),OccupyBox(10,500,20,510)),
    IMPLIES(OccupyNode(634),OccupyBox(10,510,20,520)),
    IMPLIES(OccupyNode(635),OccupyBox(16,516,26,526)),
    IMPLIES(OccupyNode(636),OccupyBox(22,522,32,532)),
    IMPLIES(OccupyNode(637),OccupyBox(28,528,38,538)),
    IMPLIES(OccupyNode(638),OccupyBox(34,534,44,544)),
    IMPLIES(OccupyNode(639),OccupyBox(40,540,50,550)),
    IMPLIES(OccupyNode(640),OccupyBox(46,546,56,556)),
    IMPLIES(OccupyNode(641),OccupyBox(52,552,62,562)),
    IMPLIES(OccupyNode(642),OccupyBox(58,558,68,568)),
    IMPLIES(OccupyNode(643),OccupyBox(64,564,74,574)),
    IMPLIES(OccupyNode(644),OccupyBox(70,570,80,580)),
    IMPLIES(OccupyNode(645),OccupyBox(76,576,86,586)),
    IMPLIES(OccupyNode(646),OccupyBox(82,582,92,592)) ,                                           
    IMPLIES(OccupyNode(647),OccupyBox(88,588,98,598)),
    IMPLIES(OccupyNode(648),OccupyBox(94,594,104,604)),
    IMPLIES(OccupyNode(649),OccupyBox(97,597,107,607)),
    IMPLIES(OccupyNode(650),OccupyBox(100,600,110,610)),
    IMPLIES(OccupyNode(651),OccupyBox(107,595,117,605)),
    IMPLIES(OccupyNode(652),OccupyBox(114,590,124,600)),
    IMPLIES(OccupyNode(653),OccupyBox(121,585,131,595)),
    IMPLIES(OccupyNode(654),OccupyBox(128,580,138,590)),
    IMPLIES(OccupyNode(655),OccupyBox(135,575,145,585)),
    IMPLIES(OccupyNode(656),OccupyBox(142,570,152,580)),
    IMPLIES(OccupyNode(657),OccupyBox(149,565,159,575)),
    IMPLIES(OccupyNode(658),OccupyBox(156,560,166,570)),
    IMPLIES(OccupyNode(659),OccupyBox(163,555,173,565)),
    IMPLIES(OccupyNode(660),OccupyBox(170,550,180,560)),
    IMPLIES(OccupyNode(661),OccupyBox(177,545,187,555)),
    IMPLIES(OccupyNode(662),OccupyBox(184,540,194,550)),
    IMPLIES(OccupyNode(663),OccupyBox(191,535,201,545)),
    IMPLIES(OccupyNode(664),OccupyBox(198,530,208,540)),
    IMPLIES(OccupyNode(665),OccupyBox(205,525,215,535)),
    IMPLIES(OccupyNode(666),OccupyBox(210,520,220,530)),
    IMPLIES(OccupyNode(667),OccupyBox(210,510,220,520)),
    IMPLIES(OccupyNode(668),OccupyBox(210,500,220,510)),
    IMPLIES(OccupyNode(669),OccupyBox(210,490,220,500)),
    IMPLIES(OccupyNode(670),OccupyBox(210,480,220,490)),
    IMPLIES(OccupyNode(671),OccupyBox(210,470,220,480)),
    IMPLIES(OccupyNode(672),OccupyBox(210,470,220,480)),
    //END TRACK 22
    
    //TRACK 23 ->  673 to 705
    IMPLIES(OccupyNode(673),OccupyBox(1130,172,1140,182)), 
    IMPLIES(OccupyNode(674),OccupyBox(1120,168,1130,178)) ,
    IMPLIES(OccupyNode(675),OccupyBox(1110,164,1120,174)) ,
    IMPLIES(OccupyNode(676),OccupyBox(1100,159,1110,169)) ,
    IMPLIES(OccupyNode(677),OccupyBox(1090,153,1100,163)),
    IMPLIES(OccupyNode(678),OccupyBox(1080,148,1090,158)),
    IMPLIES(OccupyNode(679),OccupyBox(1070,144,1080,154)),
    IMPLIES(OccupyNode(680),OccupyBox(1060,138,1070,148)),
    IMPLIES(OccupyNode(681),OccupyBox(1050,135,1060,145)),
    IMPLIES(OccupyNode(682),OccupyBox(1040,132,1050,142)),
    IMPLIES(OccupyNode(683),OccupyBox(1030,129,1040,139)),
    IMPLIES(OccupyNode(684),OccupyBox(1020,126,1030,136)),
    IMPLIES(OccupyNode(685),OccupyBox(1010,123,1020,133)),
    IMPLIES(OccupyNode(686),OccupyBox(1000,120,1010,130)),
    IMPLIES(OccupyNode(687),OccupyBox(990,120,1000,130)),
    IMPLIES(OccupyNode(688),OccupyBox(980,120,990,130)),
    IMPLIES(OccupyNode(689),OccupyBox(970,120,980,130)),
    IMPLIES(OccupyNode(690),OccupyBox(960,120,970,130)),
    IMPLIES(OccupyNode(691),OccupyBox(950,120,960,130)),
    IMPLIES(OccupyNode(692),OccupyBox(940,120,950,130)),
    IMPLIES(OccupyNode(693),OccupyBox(930,120,940,130)),
    IMPLIES(OccupyNode(694),OccupyBox(920,120,930,130)),
    IMPLIES(OccupyNode(695),OccupyBox(910,120,920,130)),
    IMPLIES(OccupyNode(696),OccupyBox(900,120,910,130)),
    IMPLIES(OccupyNode(697),OccupyBox(890,120,900,130)),
    IMPLIES(OccupyNode(698),OccupyBox(880,120,890,130)),
    IMPLIES(OccupyNode(699),OccupyBox(870,120,880,130)),
    IMPLIES(OccupyNode(700),OccupyBox(860,120,870,130)),
    IMPLIES(OccupyNode(701),OccupyBox(850,120,860,130)),
    IMPLIES(OccupyNode(702),OccupyBox(840,120,850,130)),
    IMPLIES(OccupyNode(703),OccupyBox(830,120,840,130)),
    IMPLIES(OccupyNode(704),OccupyBox(820,120,830,130)),
    IMPLIES(OccupyNode(705),OccupyBox(820,120,830,130))
    )
    //END TRACK 23
  //}
      
      def getTimePoints(file : String): List[Invariant] = {
      var timepoints : List[Invariant] = Nil
           for (line <- Source.fromFile(file).getLines()) {
             val array = line.split(',')
            timepoints ::= IMPLIES(TimePoint(array(0).toLong), //JO: I did this conversion
                BIGAND(
                    OccupyNode(array(1).toInt):: //JO: I added this Int conversion
                    OccupyNode(array(2).toInt)::
                    OccupyNode(array(3).toInt)::
                    OccupyNode(array(4).toInt)::
                    OccupyNode(array(5).toInt)::
                    OccupyNode(array(6).toInt)::
                    OccupyNode(array(7).toInt)::
                    OccupyNode(array(8).toInt)::
                    OccupyNode(array(9).toInt)::
                    OccupyNode(array(10).toInt)
                    ::Nil))
          }
        return timepoints
      }
  
  // JO: I changed /added this variant
      def getTimeIntervals(file : String): List[Invariant] = {
      var arrayold : Array[String] = Array("-1","-1","-1","-1","-1","-1","-1","-1","-1","-1","-1") // new Array(11)
      var arrayoldold : Array[String] = Array("-1","-1","-1","-1","-1","-1","-1","-1","-1","-1","-1") //new Array(11)

      var timepoints : List[Invariant] = Nil
           for (line <- Source.fromFile(file).getLines()) {
             val array = line.split(',')
            timepoints ::= IMPLIES(TimeInterval(arrayoldold(0).toLong,array(0).toLong ), //JO: I did this conversion
                BIGAND(
                    OccupyNode(array(1).toInt):: //JO: I added this Int conversion
                    OccupyNode(array(2).toInt)::
                    OccupyNode(array(3).toInt)::
                    OccupyNode(array(4).toInt)::
                    OccupyNode(array(5).toInt)::
                    OccupyNode(array(6).toInt)::
                    OccupyNode(array(7).toInt)::
                    OccupyNode(array(8).toInt)::
                    OccupyNode(array(9).toInt)::
                    OccupyNode(array(10).toInt)::
                    OccupyNode(arrayold(1).toInt):: //JO: I added this Int conversion
                    OccupyNode(arrayold(2).toInt)::
                    OccupyNode(arrayold(3).toInt)::
                    OccupyNode(arrayold(4).toInt)::
                    OccupyNode(arrayold(5).toInt)::
                    OccupyNode(arrayold(6).toInt)::
                    OccupyNode(arrayold(7).toInt)::
                    OccupyNode(arrayold(8).toInt)::
                    OccupyNode(arrayold(9).toInt)::
                    OccupyNode(arrayold(10).toInt)::
                    OccupyNode(arrayoldold(1).toInt):: //JO: I added this Int conversion
                    OccupyNode(arrayoldold(2).toInt)::
                    OccupyNode(arrayoldold(3).toInt)::
                    OccupyNode(arrayoldold(4).toInt)::
                    OccupyNode(arrayoldold(5).toInt)::
                    OccupyNode(arrayoldold(6).toInt)::
                    OccupyNode(arrayoldold(7).toInt)::
                    OccupyNode(arrayoldold(8).toInt)::
                    OccupyNode(arrayoldold(9).toInt)::
                    OccupyNode(arrayoldold(10).toInt)
                    ::Nil))
              arrayoldold = arrayold
              arrayold = array
          }
        return timepoints
      }      

	def main(args: Array[String]) {
    val cd = new CoreDefinitions
    println (cd.getSubInvariantForTime(1429190483864L,BIGAND(List(IMPLIES(TimePoint(1429190483864L),BIGAND(List(OccupyNode(10),TRUE()))), IMPLIES(TimePoint(8),OccupyNode(20))))))
        println (cd.getSubInvariantForTime(8,BIGAND(List(IMPLIES(TimePoint(1429190483864L),BIGAND(List(OccupyNode(10),TRUE()))), IMPLIES(TimePoint(8),OccupyNode(20))))))

   // println( cd.fillTimePoints(0L,1L, BIGAND(getTimePoints("train1.txt").take(100))))
        println("-----")
    //println(cd.fillTimePoints(0L,1L, BIGAND(getTimePoints("train2.txt").take(100))))
        println("-----")
        println(BIGAND(getTimePoints("train1.txt").take(100)))
        println(BIGAND(getTimePoints("train1.txt").take(2)))
        println(1429190484062L);
        //TimePoint(1429190484062)
        println(cd.getSubInvariantForTime(1429190484062L, BIGAND (getTimePoints("train1.txt").take(1))))
        println(cd.getSubInvariantForTime(1429190484062L,BIGAND(List(IMPLIES(TimePoint(1429190484062L),BIGAND(List(OccupyNode(288), OccupyNode(289), OccupyNode(290), OccupyNode(291), OccupyNode(292), OccupyNode(293), OccupyNode(294), OccupyNode(295), OccupyNode(296), OccupyNode(297))))))))
        println(cd.encodeAsSatLarge (cd.getSubInvariantForTime(1429190484062L, BIGAND (getTimePoints("train1.txt").take(1))),Nil))
        println("-----")
        
    println(cd.collisionTestsBig(
        cd.getSubInvariantForTime(1429190484062L, BIGAND (getTimePoints("train1.txt").take(2)))::Nil, 
        cd.getSubInvariantForTime(1429190483864L, BIGAND (getTimePoints("train1.txt").take(2)))::Nil ))
    
        println("---List--")
        
    println(cd.collisionTestsBig(
        cd.getSubInvariantForTime(1, BIGAND (getTimePoints("train1.txt").take(2)))::Nil, 
        cd.getSubInvariantForTime(1429190483864L, BIGAND (getTimePoints("train1.txt").take(2)))::Nil ))
        
     //val l1 = getTimePoints("train1syn.txt")
     //val l2 = getTimePoints("train3syn.txt")
     //Collisions between Train1_2.txt and Train2_2.txt somewhere inbetween and between Train1_2.txt and Train3_2.txt and between Train3_2.txt and Train4_2.txt
     val time1 = System.currentTimeMillis()
     val l1 = getTimeIntervals("Train3_2.txt")
     val l2 = getTimeIntervals("Train4_2.txt")     
     var boolreturn : String = ""
     for ((e1,e2) <- (l1 zip l2)) {
       (e1,e2) match {
         case (IMPLIES(t1,inv1),IMPLIES(t2,inv2) ) => boolreturn += cd.collisionTestsBig(inv1::Nil, inv2::Nil)
       }
     }
     println("checking time " + (System.currentTimeMillis() - time1) + " ms")
     println (boolreturn)
        
      // println (mapNodesToGeometricPosition)
        
        
    //println(getTimePoints("train3.txt"))
    //  println("-----")
		//println("TrackGraph:")
		//println(invariant2Tuplelist(trackGraph))
	}
}