/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

package BeSpaceDFesto

import BeSpaceDCore._

package object model {  

  
  // ===================================================================================== Festo Device Identity
  
  // Devices (actuators and sensors) are defined by an enumeration type.
  
  trait FestoDevice { val name: String; override def toString = name }
  type DeviceComponent  = Component[FestoDevice]
  type DeviceOccupyNode = OccupyNode[FestoDevice]
  
//  class FestoDevice extends Enumeration
//  object FestoDevice extends Enumeration
//  {
//    type FestoDevice = Value    
//  }
//  
//  type DeviceComponent  = Component[FestoDevice.FestoDevice]
//  type DeviceOccupyNode = OccupyNode[FestoDevice.FestoDevice]
  
  
  // Parts
  
  
    // Station 1
    case class FestoPart(name: String) extends FestoDevice 
    val CapTube       = FestoPart("Cap Tube")
    val StackEjector  = FestoPart("Stack Ejector")
    val CapLoader     = FestoPart("Cap Loader")
    val VacuumGripper = FestoPart("Vacuum Gripper")
    
//    trait FestoPart extends FestoDevice 
//    case object CapTube       extends FestoPart { override def toString = "Cap Tube"      }
//    case object StackEjector  extends FestoPart { override def toString = "Stack Ejector" }
//    case object CapLoader     extends FestoPart { override def toString = "Cap Loader"    }
//    case object VacuumGripper extends FestoPart { override def toString = "Vacuum Gripper"}
    
    // TODO...
    // Station 2
    // Station 3
    // Station 4
    // Station 5
    // Station 6
    // Station 7
    // Station 8
    // Station 9
    // Station 10

  type PartComponent  = Component[FestoPart]
  type PartOccupyNode = OccupyNode[FestoPart]
  
  
//  object FestoPart extends FestoDevice
//  {
//    type FestoPart = Value
//
//    // Station 1
//    val CapTube       = Value(0, "Cap Tube")
//    val StackEjector  = Value(1, "Stack Ejector")
//    val CapLoader     = Value(2, "Cap Loader")
//    val VacuumGripper = Value(3, "Vacuum Gripper")
//    
//    // TODO...
//    // Station 2
//    // Station 3
//    // Station 4
//    // Station 5
//    // Station 6
//    // Station 7
//    // Station 8
//    // Station 9
//    // Station 10
//  }
//  
//  type PartComponent  = Component[FestoPart.FestoPart]
//  type PartOccupyNode = OccupyNode[FestoPart.FestoPart]
  
  
  // Actuators
  

    // Station 1
    case class FestoActuator(name: String) extends FestoDevice
    val StackEjectorExtend = FestoActuator("Stack Ejector Extend")
    val VacuumGrip         = FestoActuator("Vacuum Grip"         )
    val EjectAirPulse      = FestoActuator("Eject Air Pulse"     )
    val LoaderPickup       = FestoActuator("Loader Pickup"       )
    val LoaderDropoff      = FestoActuator("Loader Dropoff"      )
    
//    trait FestoActuator extends FestoDevice
//    case object StackEjectorExtend extends FestoActuator { override def toString = "Stack Ejector Extend"}
//    case object VacuumGrip         extends FestoActuator { override def toString = "Vacuum Grip"         }
//    case object EjectAirPulse      extends FestoActuator { override def toString = "Eject Air Pulse"     }
//    case object LoaderPickup       extends FestoActuator { override def toString = "Loader Pickup"       }
//    case object LoaderDropoff      extends FestoActuator { override def toString = "Loader Dropoff"      }
    
    // TODO...
    // Station 2
    // Station 3
    // Station 4
    // Station 5
    // Station 6
    // Station 7
    // Station 8
    // Station 9
    // Station 10

  
  type ActuatorComponent  = Component[FestoActuator]
  type ActuatorOccupyNode = OccupyNode[FestoActuator]
  
//  object FestoActuator extends FestoDevice
//  {
//    type FestoActuator = Value
//
//    // Station 1
//    val StackEjectorExtend = Value(0, "Stack Ejector Extend")
//    val VacuumGrip         = Value(1, "Vacuum Grip")
//    val EjectAirPulse      = Value(2, "Eject Air Pulse")
//    val LoaderPickup       = Value(3, "Loader Pickup")
//    val LoaderDropoff      = Value(4, "Loader Dropoff")
//    
//    // TODO...
//    // Station 2
//    // Station 3
//    // Station 4
//    // Station 5
//    // Station 6
//    // Station 7
//    // Station 8
//    // Station 9
//    // Station 10
//  }
//  
//  type ActuatorComponent  = Component[FestoActuator.FestoActuator]
//  type ActuatorOccupyNode = OccupyNode[FestoActuator.FestoActuator]
  
  
  // Sensors
  

    // Station 1
    case class FestoSensor(name:String) extends FestoDevice
    val StackEjectorExtended  = FestoSensor("Stack Ejector Extended" )
    val StackEjectorRetracted = FestoSensor("Stack Ejector Retracted")
    val WorkpieceGripped      = FestoSensor("Workpiece Gripped"      )
    val LoaderPickedUp        = FestoSensor("Loader Picked Up"       )
    val LoaderDroppedOff      = FestoSensor("Loader Dropped Off"     )
    val StackEmpty            = FestoSensor("Stack Empty"            )
    
//    trait FestoSensor extends FestoDevice
//    case object StackEjectorExtended  extends FestoSensor { override def toString = "Stack Ejector Extended" }
//    case object StackEjectorRetracted extends FestoSensor { override def toString = "Stack Ejector Retracted"}
//    case object WorkpieceGripped      extends FestoSensor { override def toString = "Workpiece Gripped"      }
//    case object LoaderPickedUp        extends FestoSensor { override def toString = "Loader Picked Up"       }
//    case object LoaderDroppedOff      extends FestoSensor { override def toString = "Loader Dropped Off"     }
//    case object StackEmpty            extends FestoSensor { override def toString = "Stack Empty"            }
    
    // TODO...
    // Station 2
    // Station 3
    // Station 4
    // Station 5
    // Station 6
    // Station 7
    // Station 8
    // Station 9
    // Station 10

                        
  type SensorComponent  = Component[FestoSensor]
  type SensorOccupyNode = OccupyNode[FestoSensor]
  
//  object FestoSensor extends FestoDevice
//  {
//    type FestoSensor = Value
//
//    // Station 1
//    val StackEjectorExtended  = Value(0, "Stack Ejector Extended")
//    val StackEjectorRetracted = Value(1, "Stack Ejector Retracted")
//    val WorkpieceGripped      = Value(2, "Workpiece Gripped")
//    val LoaderPickedUp        = Value(3, "Loader Picked Up")
//    val LoaderDroppedOff      = Value(4, "Loader Dropped Off")
//    val StackEmpty            = Value(5, "Stack Empty")
//    
//    // TODO...
//    // Station 2
//    // Station 3
//    // Station 4
//    // Station 5
//    // Station 6
//    // Station 7
//    // Station 8
//    // Station 9
//    // Station 10
//  }
//                        
//  type SensorComponent  = Component[FestoSensor.FestoSensor]
//  type SensorOccupyNode = OccupyNode[FestoSensor.FestoSensor]
  
  
  
  // ===================================================================================== Material flow model (Guillaume Prevost)
  
  
  
  
  // ===================================================================================== Abstract Action based model (James Harland)
  
  
  
  

}


