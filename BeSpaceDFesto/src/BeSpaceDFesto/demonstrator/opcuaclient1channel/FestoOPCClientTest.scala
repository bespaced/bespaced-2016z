package BeSpaceDFesto.demonstrator.opcuaclient1channel

import FestoOPCClient._
import BeSpaceDFesto.demonstrator.opcuaclient2channel.FestoOPCClient

object FestoOPCClientTest {
  
  def main(args: Array[String])
  {
    println("==================== TESTING FestoOPCClient...")
    
    importSignal("My Device A", 33.77F)
    importSignal("My Device B", 44.77F)
    importSignal("My Device C", 55.77F)
    
    println()
    println()
    println()
  }
}