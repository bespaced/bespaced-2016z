package BeSpaceDOPC.serviceport
/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

import java.lang.Exception
import java.io.File

import scala.io._

import BeSpaceDCore._
import BeSpaceDCore.Log._


/**
 * @author keith
 */

case class Datafile(val file: File) {  
  
  require(file.canRead(), "The file needs to be readable.")
  
  val source: BufferedSource = Source.fromFile(file)
  
  private var lines: Iterator[String] = source.getLines()
  
  private var columns: List[String] = Nil
  
  lazy val columnTypes: List[String] = columns map 
  {
    variableNameAndType =>
      // The variable name and its type are dot separated
      val typePosition = variableNameAndType.lastIndexOf(".") + 1
      val variableType = variableNameAndType.substring(typePosition)

      variableType
  }
  
  
  
  def ignoreLine: String  =
  {
    val line = if (lines.hasNext) lines.next else ""
    
    lines = lines drop 1
    
    line
  }
  
  
  
  def expectLine(terminal: String): String  =
  {
    val line = if (lines.hasNext) lines.next else ""
    
    if (line == terminal)
    {
      //lines = lines drop 1
      
      line
    }
    else
    {
      throw new ExpectException(s"Line '$terminal' ")
    }
    
  }
  
  
  
  def headerRow: List[String] =
  {
    val HeaderRowSyntax = "^((\\S+)\\t?)*$"
    
    if (lines.hasNext) 
    {
      val line = lines.next()
      //lines = lines drop 1
      
      columns = line.split("\t").toList
    }
    else
    {
      throw new ExpectException(s"Header Row")
    }
    
    columns
  }
  
  
  
  def genericRow[T](convert: (String => T)): List[T] =
  {
    if (lines.hasNext) 
    {
      val line = lines.next()
      lines = lines drop 1
      
      line.split("\t").toList map convert
    }
    else
    {
      throw new ExpectException(s"Data Row")
    }
  }
  
  
  
  def dataLoggerRowSimple[E](convertElement: (Int, String) => E): Option[List[E]] =
  {
    if (lines.hasNext) 
    {
      val line = lines.next()
      //lines = lines drop 1
      
      val indexedElements: List[(String, Int)] = line.split("\t").toList zipWithIndex
      
      val result: List[E] = for( (element, i) <- indexedElements) yield convertElement(i, element)
      
      testOff(s"DATA: $result")
      
      Some(result)
    }
    else
    {
      None
    }
  }
  
  
  
  def dataLoggerRow[R](convertRow: List[String] => R): Option[R] =
  {
    if (lines.hasNext) 
    {
      val line = lines.next()
      //lines = lines drop 1
      
      val elements: List[String] = line.split("\t").toList
      
      val result: R = convertRow(elements)
      
      testOff(s"DATA: $result")
      
      Some(result)
    }
    else
    {
      None
    }
  }
  
  
  
  case class ExpectException(message: String) extends Exception(s"Parsing error: $message expected.")
  
}


