package template

import BeSpaceDCore._

case class PathPlusParameters(path : List[OccupyPoint], graph : FirstGraph[OccupyPoint], start : OccupyPoint, target : OccupyPoint, obstacles : List[AbstractPolygon], maxPathLength : Int, maxRecursionSteps : Int, heuristic: (OccupyPoint, OccupyPoint) => Double)

class ThirdPathBacktracking extends CoreDefinitions {
  var path: List[OccupyPoint] = Nil
  def getPath(): List[OccupyPoint] = path

  var stepCounter: Int = 0

  def Backtracking(start: OccupyPoint, target: OccupyPoint, obstacles: List[AbstractPolygon], graph: FirstGraph[OccupyPoint], maxPathLength: Int, maxRecursionSteps: Int, heuristic: (OccupyPoint, OccupyPoint) => Double): PathPlusParameters = {
    backtrackingOnPaths(target, start :: Nil, obstacles, graph, maxPathLength, maxRecursionSteps, heuristic)
    return PathPlusParameters(path, graph, start, target, obstacles, maxPathLength, maxRecursionSteps, heuristic)
  }
  def Backtracking(): PathPlusParameters = {
    val graph: FirstGraph[OccupyPoint] = FirstGraphGenerator.graph
    val scene: (OccupyPoint, OccupyPoint, List[OccupyBox]) = new FirstSceneGenerator().Generator()
    val start: OccupyPoint = scene._1
    val target: OccupyPoint = scene._2
    val obstacles: List[AbstractPolygon] = scene._3.map { x => new InvariantObstacle(x) }
    val maxRecursionSteps: Int = Integer.MAX_VALUE
    val maxPathLength: Int = Integer.MAX_VALUE
    def heuristic(a: OccupyPoint, b: OccupyPoint): Double = new FirstAStar().distance(a, scene._2) - new FirstAStar().distance(b, scene._2)
    backtrackingOnPaths(target, start :: Nil, obstacles, graph, maxPathLength, maxRecursionSteps, heuristic)
    return PathPlusParameters(path, graph, start, target, obstacles, maxPathLength, maxRecursionSteps, heuristic)
  }
  def Backtracking(start: OccupyPoint, target: OccupyPoint, obstacles: List[AbstractPolygon], maxPathLength: Int, maxRecursionSteps: Int): PathPlusParameters = {
    val graph: FirstGraph[OccupyPoint] = FirstGraphGenerator.graph
    def heuristic(a: OccupyPoint, b: OccupyPoint): Double = new FirstAStar().distance(a, target) - new FirstAStar().distance(b, target)
    backtrackingOnPaths(target, start :: Nil, obstacles, graph, maxPathLength, maxRecursionSteps, heuristic)
    return PathPlusParameters(path, graph, start, target, obstacles, maxPathLength, maxRecursionSteps, heuristic)
  }
  def Backtracking(maxPathLength: Int, maxRecursionSteps: Int): PathPlusParameters = {
    val graph: FirstGraph[OccupyPoint] = FirstGraphGenerator.graph
    val scene: (OccupyPoint, OccupyPoint, List[OccupyBox]) = new FirstSceneGenerator().Generator()
    val start: OccupyPoint = scene._1
    val target: OccupyPoint = scene._2
    val obstacles: List[AbstractPolygon] = scene._3.map { x => new InvariantObstacle(x) }
    def heuristic(a: OccupyPoint, b: OccupyPoint): Double = new FirstAStar().distance(a, scene._2) - new FirstAStar().distance(b, scene._2)
    backtrackingOnPaths(target, start :: Nil, obstacles, graph, maxPathLength, maxRecursionSteps, heuristic)
    return PathPlusParameters(path, graph, start, target, obstacles, maxPathLength, maxRecursionSteps, heuristic)
  }
  //This method is supposed to simplify running of the backtracking, checking the result, modify the parameters, pass them back in again.
  def Backtracking(parameters : PathPlusParameters) : PathPlusParameters = {
    val successful : Boolean = backtrackingOnPaths(parameters.target, if(parameters.path == Nil) parameters.start::Nil else parameters.path, parameters.obstacles, parameters.graph, parameters.maxPathLength, parameters.maxRecursionSteps, parameters.heuristic)
    return PathPlusParameters(path, parameters.graph, parameters.start, parameters.target, parameters.obstacles, parameters.maxPathLength, parameters.maxRecursionSteps, parameters.heuristic)
  }

  def collision(point: OccupyPoint, obstacles: List[AbstractPolygon]): Boolean = {
    return !(obstacles.forall { x => !(x.collision(point)) })
  }

  def backtrackingOnPaths(target: OccupyPoint, decisions: List[OccupyPoint], obstacles: List[AbstractPolygon], graph: FirstGraph[OccupyPoint], maxPathLength: Int, maxRecursionSteps: Int, heuristic: ((OccupyPoint, OccupyPoint) => Double)): Boolean = {
    stepCounter += 1
    if (stepCounter > maxRecursionSteps || maxPathLength < 0) {
      return false
    }

    if (decisions.last == target) {
      path = decisions
      return true
    } else {
      val heuristicList: List[OccupyPoint] = graph.successors(decisions.last).sortWith(heuristic(_, _) < 0)
      var i: Int = 0
      while (i < heuristicList.size) {
        val nextChoice: OccupyPoint = heuristicList.apply(i)
        i = i + 1 //That's an important point...
        if (!(collision(nextChoice, obstacles)) && !(decisions.contains(nextChoice))) {
          if (backtrackingOnPaths(target, decisions ++ (nextChoice :: Nil), obstacles, graph, maxPathLength - 1, maxRecursionSteps, heuristic)) {
            return true
          }
        }
      }
      return false
    }
    return true
  }
}