package template

import BeSpaceDCore._

class ConnectivityClusteringSpec extends UnitSpec{
  "findConnectedObstacles" should "return all polygons in the same list if they have an overlap path" in
  {
//    val hull = new ConvexHull()
//    var a = OccupyPoint(170,120)
//    var b = OccupyPoint(100,250)
//    var c = OccupyPoint(20,190)
//    var result : String = hull.Turn(a,b,c)
//    
//    assertResult(expected = "right turn")(actual = result )
    val clustering: ConnectivityClustering = new ConnectivityClustering()
    val polygons: List[AbstractPolygon] = List(new InvariantObstacle(OccupyBox(3,3,8,8)), new Polygon(List(OccupyPoint(8,8), OccupyPoint(29,18), OccupyPoint(12,9))), new Polygon(List(OccupyPoint(27,15), OccupyPoint(34,16), OccupyPoint(30,35))))
    val result: List[List[AbstractPolygon]] = clustering.findConnectedObstacles(polygons)
    val howItShouldBe: List[List[AbstractPolygon]] = polygons :: Nil
    
    assertResult(expected = howItShouldBe)(actual = result)
  }
  it should "cluster the first 3 in OccupyBox(2,2,8,8), (8,2,12,8), (12,2,18,8), (19,2,25,8)" in {
    val clustering: ConnectivityClustering = new ConnectivityClustering()
    val polygons: List[AbstractPolygon] = List(new InvariantObstacle(OccupyBox(2,2,8,8)), new InvariantObstacle(OccupyBox(8,2,12,8)), new InvariantObstacle(OccupyBox(12,2,18,8)), new InvariantObstacle(OccupyBox(19,2,25,8)))
    val result: List[List[AbstractPolygon]] = clustering.findConnectedObstacles(polygons)
    val howItShouldBe: List[List[AbstractPolygon]] = List(polygons.take(3), polygons.last::Nil)
    
    assertResult(expected = howItShouldBe)(actual = result)
  }
  it should "cluster [0,2,3][1,4] in OccupyBox(2,2,8,8), (19,2,25,8), (8,2,12,8), (12,2,18,8), (20,6,46,9)" in {
    val clustering: ConnectivityClustering = new ConnectivityClustering()
    val polygons: List[AbstractPolygon] = List(new InvariantObstacle(OccupyBox(2,2,8,8)), new InvariantObstacle(OccupyBox(19,2,25,8)), new InvariantObstacle(OccupyBox(8,2,12,8)), new InvariantObstacle(OccupyBox(12,2,18,8)), new InvariantObstacle(OccupyBox(20,6,46,9)))
    val result: List[List[AbstractPolygon]] = clustering.findConnectedObstacles(polygons)
    val howItShouldBe: List[List[AbstractPolygon]] = List(polygons.apply(0)::polygons.apply(2)::polygons.apply(3)::Nil, polygons.apply(1)::polygons.apply(4)::Nil)
    
    assertResult(expected = howItShouldBe)(actual = result)
  }
  "findConnectedObstaclesRecursive" should "return a list with all polygons if they have an overlap path" in
  {
    val clustering: ConnectivityClustering = new ConnectivityClustering()
    val polygons: List[AbstractPolygon] = List(new InvariantObstacle(OccupyBox(3,3,8,8)), new Polygon(List(OccupyPoint(8,8), OccupyPoint(29,18), OccupyPoint(12,9))), new Polygon(List(OccupyPoint(27,15), OccupyPoint(34,16), OccupyPoint(30,35))))
    val result: List[AbstractPolygon] = clustering.findConnectedObstaclesRecursive(polygons.head::Nil, polygons.tail)
    val howItShouldBe: List[AbstractPolygon] = polygons
    
    assertResult(expected = howItShouldBe)(actual = result)
  }
}