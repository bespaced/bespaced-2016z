package template

import BeSpaceDCore._
import BeSpaceDData._
import scala.annotation.tailrec
import scalafx.scene.paint.Color
import scala.collection.mutable.HashMap
import scala.util._
import java.io._
import java.util.Scanner

object KinectScanDataTesterA extends PointVis {

  val scanMap: HashMap[String, List[Occupy3DPointDouble]] = new HashMap[String, List[Occupy3DPointDouble]]
  //  val core = standardDefinitions; import core._

  def main(args: Array[String]): Unit = {
    //    println(List(Occupy3DPoint(-1,1,2), Occupy3DPoint(-1,1,1), Occupy3DPoint(-2,2,3), Occupy3DPoint(-1,0,1), Occupy3DPoint(-2,2,4), Occupy3DPoint(0,1,2), Occupy3DPoint(0,1,3), Occupy3DPoint(1,4,7), Occupy3DPoint(0,0,1), Occupy3DPoint(0,0,0), Occupy3DPoint(2,4,7), Occupy3DPoint(2,3,5), Occupy3DPoint(1,1,2), Occupy3DPoint(1,1,3), Occupy3DPoint(-2,1,2), Occupy3DPoint(-1,1,3), Occupy3DPoint(2,1,2), Occupy3DPoint(-2,1,3), Occupy3DPoint(2,1,3), Occupy3DPoint(-2,1,4), Occupy3DPoint(0,1,4), Occupy3DPoint(-1,1,4), Occupy3DPoint(1,1,4), Occupy3DPoint(2,1,4), Occupy3DPoint(-1,0,2), Occupy3DPoint(-3,1,4), Occupy3DPoint(3,1,4), Occupy3DPoint(-2,0,2), Occupy3DPoint(-5,2,6), Occupy3DPoint(-3,1,5), Occupy3DPoint(-2,1,5), Occupy3DPoint(-1,1,5), Occupy3DPoint(-5,2,7), Occupy3DPoint(0,1,5), Occupy3DPoint(1,1,5), Occupy3DPoint(2,1,5), Occupy3DPoint(3,1,5), Occupy3DPoint(5,1,7), Occupy3DPoint(-2,0,3), Occupy3DPoint(-2,0,4), Occupy3DPoint(-2,1,6), Occupy3DPoint(-1,1,6), Occupy3DPoint(4,1,7), Occupy3DPoint(0,1,6), Occupy3DPoint(3,1,6), Occupy3DPoint(1,1,6), Occupy3DPoint(1,0,4), Occupy3DPoint(2,0,4), Occupy3DPoint(2,1,6), Occupy3DPoint(3,0,4), Occupy3DPoint(-2,0,5), Occupy3DPoint(-1,0,5), Occupy3DPoint(4,1,6), Occupy3DPoint(-1,0,4), Occupy3DPoint(0,0,5), Occupy3DPoint(-4,1,5), Occupy3DPoint(1,0,5), Occupy3DPoint(2,0,5), Occupy3DPoint(2,1,7), Occupy3DPoint(-1,0,6), Occupy3DPoint(0,0,6), Occupy3DPoint(3,0,6), Occupy3DPoint(4,0,6), Occupy3DPoint(2,0,7), Occupy3DPoint(2,0,6), Occupy3DPoint(4,0,7), Occupy3DPoint(5,0,7), Occupy3DPoint(1,0,3), Occupy3DPoint(1,0,2), Occupy3DPoint(0,0,2), Occupy3DPoint(0,0,3), Occupy3DPoint(5,-1,7), Occupy3DPoint(-1,-1,5), Occupy3DPoint(0,-1,5), Occupy3DPoint(1,0,1), Occupy3DPoint(-1,-1,3), Occupy3DPoint(1,-1,3), Occupy3DPoint(-1,-1,2), Occupy3DPoint(0,-1,2), Occupy3DPoint(1,-1,2), Occupy3DPoint(5,-3,7), Occupy3DPoint(4,-4,7), Occupy3DPoint(5,-4,7), Occupy3DPoint(0,-1,1), Occupy3DPoint(1,-1,1), Occupy3DPoint(-5,-4,7), Occupy3DPoint(-4,-4,7), Occupy3DPoint(-1,-4,7)).size)
    //test
    //    var myMap: HashMap[Int, Int] = new HashMap()
    //    myMap.put(1, 1)
    //    myMap.put(1, 1)

    //    obstacleMultiParameterTest(16)
    //    KinectScanRangeTest
    //    writePrecomputedPointCloud("aicause.kinect.scan.obstacles30")
//    println(Timer.allTimes)
//    Thread.sleep(1000)
//    println(Timer.allTimes)
    println(Math.round(Math.random() * 256))
    println(Math.round(Math.random() * 256))
    println(Math.round(Math.random() * 256))
    println(Math.round(Math.random() * 256))

  }
  def readTest = {
    println(java.util.Calendar.getInstance.getTime)
    val output: List[OccupyPoint] = readPrecomputedPointCloud("aicause.kinect.scan.obstacles30_precomputed")
    val pw: PrintWriter = new PrintWriter("" + output.hashCode() + "_" + System.currentTimeMillis() + ".txt")
    pw.write(output.toString())
    pw.close()
    println(java.util.Calendar.getInstance.getTime)
  }

  def KinectScanRangeTest = {
    val p = points("aicause.kinect.scan.obstacles30")
    val minX = p.minBy { x => x.x }
    val minY = p.minBy { x => x.y }
    val minZ = p.minBy { x => x.z }
    val maxX = p.maxBy { x => x.x }
    val maxY = p.maxBy { x => x.y }
    val maxZ = p.maxBy { x => x.z }
    println("minX = " + minX)
    println("minY = " + minY)
    println("minZ = " + minZ)
    println("maxX = " + maxX)
    println("maxY = " + maxY)
    println("maxZ = " + maxZ)
  }

  def someOldTest = {
    //    val ida: Int = (List.fill(16)("aicause.kinect.scan.obstacles30"), List.fill(16)("aicause.kinect.scan.obstacles30").map { y => points(y) }, 100, 1024, cutOff(-1.08, 1.48, -1.38, 1.18), 0.001, 0.5, 8, OccupyPoint(0, 0), OccupyPoint(1023, 1023), 2.3, 1, true, 20.0, true, heuristic1(1, 1)).hashCode()
    //    val idb: Int = (List.fill(16)("aicause.kinect.scan.obstacles30"), List.fill(16)("aicause.kinect.scan.obstacles30").map { y => points(y) }, 100, 1024, cutOff(-1.08, 1.48, -1.38, 1.18), 0.001, 0.5, 8, OccupyPoint(1023, 1023), OccupyPoint(0, 0), 2.3, 1, true, 20.0, true, heuristic1(1, 1)).hashCode()
    val ac: Configuration = Configuration(List.fill(16)("aicause.kinect.scan.obstacles30"), List.fill(16)("aicause.kinect.scan.obstacles30").map { y => points(y) }, arbitraryPlacementPoints, anglesForScanRotation, 100, 1024, cutOff(-1.08, 1.48, -1.38, 1.18), 0.001, 0.5, 8, OccupyPoint(0, 0), OccupyPoint(1023, 1023), 2.3, 1, true, 20.0, true, heuristic1(1, 1))
    val bc: Configuration = Configuration(List.fill(16)("aicause.kinect.scan.obstacles30"), List.fill(16)("aicause.kinect.scan.obstacles30").map { y => points(y) }, arbitraryPlacementPoints, anglesForScanRotation, 100, 1024, cutOff(-1.08, 1.48, -1.38, 1.18), 0.001, 0.5, 8, OccupyPoint(1023, 1023), OccupyPoint(0, 0), 2.3, 1, true, 20.0, true, heuristic1(1, 1))
    val ar: Results = obstacle3(ac)
    val br: Results = obstacle3(bc)
    println("First Path---------------------------------------------------------------")
    println(ar.Path)
    println("Second Path--------------------------------------------------------------")
    println(br.Path)
    println(ar.Path == br.Path)
  }

  def multipleTestInstantOutput = {

    ???
  }

  def arbitraryPlacementPoints: List[OccupyPoint] = {
    List(
      OccupyPoint(0, 0),
      OccupyPoint(279, 2),
      OccupyPoint(498, 0),
      OccupyPoint(716, 28),
      OccupyPoint(5, 254),
      OccupyPoint(236, 241),
      OccupyPoint(596, 309),
      OccupyPoint(821, 285),
      OccupyPoint(2, 514),
      OccupyPoint(272, 571),
      OccupyPoint(591, 510),
      OccupyPoint(785, 438),
      OccupyPoint(3, 758),
      OccupyPoint(253, 745),
      OccupyPoint(503, 792),
      OccupyPoint(761, 721))
    val xySeeds: List[Int] = (0 until 1024 by 256).toList
    val output: List[OccupyPoint] = for (x <- xySeeds; y <- xySeeds) yield OccupyPoint(x, y)
    return output
  }
  def anglesForScanRotation: List[Double] = {
    List(
      0.0,
      1.0,
      2.0,
      3.0,
      4.0,
      5.0,
      6.0,
      7.0,
      8.0,
      9.0,
      10.0,
      11.0,
      12.0,
      13.0,
      14.0,
      15.0)
    List.fill(16)(0.0)
  }
  def randomPlacementPoints(limits: Int): List[OccupyPoint] = {
    val output: List[OccupyPoint] = List.fill(15)(OccupyPoint((Math.random() * limits).toInt, (Math.random() * limits).toInt))
    val filePath = "./stored_output/"
    val pw: PrintWriter = new PrintWriter("" + filePath + "" + output.hashCode() + "_" + System.currentTimeMillis() + ".txt")
    pw.write(output.toString())
    pw.close()
    return output
  }

  /**
   * @param first
   * @param last
   * @param scan
   * @return
   */
  def parameters(first: Int, last: Int, scan: Option[Int]): List[Configuration] = {
    println("Single scan (s) or full matrix (f)?")
    val sc: Scanner = new Scanner(System.in)
    val small: Boolean = sc.next() match {
      case "s" => true
      case "f" => false
      case _   => false
    }
    val debug: Boolean = false
    val allScans: List[String] = List(
      "aicause.kinect.scan.obstacles17", //Good
      "aicause.kinect.scan.obstacles18", //Good
      //      "aicause.kinect.scan.obstacles19", //Corrupted
      "aicause.kinect.scan.obstacles20", //Good
      //      "aicause.kinect.scan.obstacles21", //Corrupted
      "aicause.kinect.scan.obstacles22", //Good
      //      "aicause.kinect.scan.obstacles23", //Corrupted
      //      "aicause.kinect.scan.obstacles24", //Corrupted
      //      "aicause.kinect.scan.obstacles25", //Corrupted
      //      "aicause.kinect.scan.obstacles26", //Corrupted
      //      "aicause.kinect.scan.obstacles27", //Corrupted
      //      "aicause.kinect.scan.obstacles28", //Corrupted
      //      "aicause.kinect.scan.obstacles29", //Corrupted
      "aicause.kinect.scan.obstacles30", //Good
      //      "aicause.kinect.scan.obstacles31", //Corrupted
      //      "aicause.kinect.scan.obstacles32" //Corrupted
      "aicause.kinect.scan.festo01",
      "aicause.kinect.scan.festo02",
      "aicause.kinect.scan.festo03",
      "aicause.kinect.scan.obstacles19a",
      "aicause.kinect.scan.obstacles21a",
      "aicause.kinect.scan.obstacles23a",
      "aicause.kinect.scan.obstacles24a",
      "aicause.kinect.scan.obstacles25a",
      "aicause.kinect.scan.obstacles26a",
      "aicause.kinect.scan.obstacles27a",
      "aicause.kinect.scan.obstacles28a",
      "aicause.kinect.scan.obstacles29a",
      "aicause.kinect.scan.obstacles31a",
      "aicause.kinect.scan.obstacles32a",
      "aicause.kinect.scan.obstacles33",
      "aicause.kinect.scan.obstacles34")
    val scans: List[String] = scan match {
      case None => debug match {
        case false => allScans
        case true  => List("aicause.kinect.scan.obstacles22")
      }
      case Some(d) => List(allScans.apply(d % allScans.length))
    }

    /*val resolution: Int = small match {
      case false => 100
      case true  => 400
    }*/
    val resolution: Int = 100
    val s_t: List[OccupyPoint] = List(
      OccupyPoint(4, 4),
      OccupyPoint(1019, 1019),
      OccupyPoint(37, 598),
      OccupyPoint(712, 972),
      OccupyPoint(1011, 76),
      OccupyPoint(875, 692),
      OccupyPoint(892, 209),
      OccupyPoint(119, 41),
      OccupyPoint(103, 908),
      OccupyPoint(390, 378),
      OccupyPoint(12, 1020),
      OccupyPoint(888, 960))

    def standardScanSet: List[String] = scans(0) :: scans(1) :: scans(2) :: scans(3) :: scans(4) :: scans(0) :: scans(1) :: scans(2) :: scans(3) :: scans(4) :: scans(0) :: scans(1) :: scans(2) :: scans(3) :: scans(4) :: scans(0) :: Nil //List.fill(16)(scans(4)) //for debug purposes//
    def standardScanSet4: List[String] = scans(0) :: scans(1) :: scans(2) :: scans(3) :: scans(4) :: scans(0) :: scans(1) :: scans(2) :: scans(3) :: scans(4) :: scans(0) :: scans(1) :: scans(2) :: scans(3) :: scans(4) :: scans(0) :: Nil
    def standardScanSet3: List[String] = scans(0) :: scans(1) :: scans(2) :: "empty" :: scans(3) :: scans(4) :: scans(0) :: "empty" :: scans(1) :: scans(2) :: scans(3) :: List.fill(5)("empty")
    def simpleScanSet4: List[String] = List.fill(16)(scans(4))
    def simpleScanSet5: List[String] = List.fill(16)(scans(18))
    //    def simpleScanSet3: List[String] = List.fill(3)(scans(4)) ++ "empty" :: Nil ++ List.fill(3)(scans(4)) ++ "empty" :: Nil ++ List.fill(3)(scans(4)) ++ "empty" :: Nil ++ List.fill(4)("empty")
    def simpleScanSet3: List[String] = scans(4) :: scans(4) :: scans(4) :: "empty" :: scans(4) :: scans(4) :: scans(4) :: "empty" :: scans(4) :: scans(4) :: scans(4) :: "empty" :: "empty" :: "empty" :: "empty" :: "empty" :: Nil
    def simpleScanSet2: List[String] = scans(4) :: scans(4) :: "empty" :: "empty" :: scans(4) :: scans(4) :: List.fill(10)("empty")
    def simpleScanSet1: List[String] = scans(4) :: List.fill(15)("empty")
    def scan1: List[String] = scans.take(16)
    def scan2: List[String] = scans.takeRight(16)
    def scan3: List[String] = scans(4) :: scans(3) :: scans(2) :: scans.takeRight(13)

    def setOfSets: List[List[String]] = small match {
      case false => (simpleScanSet5 :: simpleScanSet4 :: simpleScanSet3 :: simpleScanSet2 :: simpleScanSet1 :: standardScanSet4 :: standardScanSet3 :: scan1 :: scan2 :: scan3 :: Nil).take(1)
      case true  => scans.map { x => List(x) }
    }
    def standardTarget: OccupyPoint = OccupyPoint(1019, 1019)
    def standardStart: OccupyPoint = OccupyPoint(4, 4)
    val center: (Int, Int) = (500, 500)
    val magnificationFactor: Int = small match {
      case false => 1024
      case true  => 256
    } //Does not need to be a power of 2.
    println("Would you like to stepwise increase the size of the scan? Type true or false.")
    val increase: Boolean = sc.nextBoolean()
    val magnificationFactorList: List[Int] = increase match {
      case true => {
        println("Please enter a stepwidth.")
        val stepwidth: Int = sc.nextInt()
        if (stepwidth > 0) {
          (128 to magnificationFactor by stepwidth).toList
        } else if (stepwidth == 0) {
          List(magnificationFactor)
        } else {
          (128 to magnificationFactor by -stepwidth).toList.reverse
        }

      }
      case false => List(magnificationFactor)
    }
    val borders: List[cutOff] = List(cutOff(-1.08, 1.48, -1.38, 1.18), cutOff(-0.98, 1.58, -1.25, 1.31)).take(1) //cutOff(-1.6, 1.6, -1.6, 1.6)
    val empty: Double = 0
    val full: Double = 0.9
    val minSize: Int = 8
    val start = small match {
      case false => List(OccupyPoint(4, 4), OccupyPoint(888, 960), OccupyPoint(256, 4), OccupyPoint(4, 1019))
      case true  => List(OccupyPoint(4, 4), OccupyPoint(128, 4), OccupyPoint(251, 4), OccupyPoint(121,55), OccupyPoint(100,128), OccupyPoint(84,55))
    }
    val target = small match {
      case false => List(OccupyPoint(1019, 1019), OccupyPoint(602, 611), OccupyPoint(256, 1019), OccupyPoint(1019, 4))
      case true  => List(OccupyPoint(251, 251), OccupyPoint(128, 123), OccupyPoint(4, 251), OccupyPoint(73,84), OccupyPoint(227,188), OccupyPoint(114,198))
    }
    val zPlane: Double = 2.3
    val emptyValues: List[Double] = List(0) //List(0, 0.001, 0.002, 0.003, 0.004, 0.005, 0.006, 0.007, 0.008, 0.009, 0.01) //.map { x => x * 5 }
    val fullValues: List[Double] = List(0.9) //List(0.5, 0.6, 0.7, 0.8, 0.9, 0.95, 1) //.map { x => x + 4 }
    val minSizeValues: List[Int] = List(8)
    val densityRelationValues: List[Int] = List(1, 2, 4, 10, 20, 50) //Not in use yet.
    val heuristics: List[heuristicEnum] = List(
      heuristic1(1, 1),
      heuristic1(2, 1),
      heuristic1(1, 2),
      heuristic1(3, 1),
      heuristic1(1, 3),
      //      heuristic1(2, 3),
      //      heuristic1(3, 2),
      heuristic1(5, 1),
      heuristic1(1, 5),
      heuristic2(10000, 1000000, 1000, 1, 1),
      //      heuristic2(10000, 1000000, 1000, 2, 1),
      //      heuristic2(10000, 1000000, 1000, 1, 2),
      heuristic2(100, 1000000, 1000, 1, 1),
      heuristic2(100, 1000, 0, 1, 1),
      heuristic3(1, 1, 1),
      heuristic3(3, 1, 1),
      //      heuristic3(2, 1, 1),
      heuristic3(1, 3, 1),
      //      heuristic3(1, 2, 1),
      //      heuristic3(2, 2, 1),
      //      heuristic3(3, 3, 1),
      //      heuristic3(2, 3, 1),
      //      heuristic3(3, 2, 1),
      heuristic3(1, 1, 3),
      //      heuristic3(1, 1, 2),
      //      heuristic3(1, 2, 2),
      //      heuristic3(1, 3, 3),
      //      heuristic3(1, 3, 2),
      //      heuristic3(1, 2, 3),
      heuristic4(10000, 1, 1),
      heuristic5(),
      heuristic6(),
      heuristic7())

    val allSets = for (e <- emptyValues; f <- fullValues; m <- minSizeValues; s <- setOfSets; h <- heuristics; b <- borders; st <- start; ta <- target; mf <- magnificationFactorList) yield (e, f, m, s, h, b, st, ta, mf)
    println("All Sets are " + allSets.length)
    /*val realFirst: Int = first % allSets.length
    val preliminaryLast: Int = last % allSets.length
    val shift: Int = if(preliminaryLast < realFirst) allSets.length else 0
    val realLast: Int = preliminaryLast + shift*/
    val realFirst: Int = if (first >= last) 0 else first
    val setsShort = last / allSets.length match {
      case 0 => allSets.slice(realFirst, last)
      case d => {
        val indices = List.range(0, d + 1)
        val multiSets = indices.flatMap { x => allSets }
        multiSets.slice(realFirst, last)
        //        allSets.flatMap(f => List.fill(d + 1)(f))
      }
    }
    val configs = setsShort.map {
      f =>
        f match {
          case (e, f, m, s, h, b, st, ta, mf) => {
            increase match {
              case false => {
                //                val id: Int = (s, s.map { y => points(y) }, resolution, mf, b, e, f, m, st, ta, 2.3, /*if(small) 0 else*/ 1, true, 0.0, true, h).hashCode()
                Configuration(s, s.map { y => points(y) }, arbitraryPlacementPoints, anglesForScanRotation, resolution, mf, b, e, f, m, st, ta, 2.3, /*if(small) 0 else*/ 1, true, 0.0, true, h)
              }
              case true => {
                //                val id: Int = (s, s.map { y => points(y) }, resolution, mf, b, e, f, m, OccupyPoint(4, 4), OccupyPoint(110, 110 /*(mf * 93 / 100).toInt, (mf * 93 / 100).toInt*/ ), 2.3, /*if(small) 0 else*/ 1, true, 0.0, true, heuristic7()).hashCode()
                Configuration(s, s.map { y => points(y) }, arbitraryPlacementPoints, anglesForScanRotation, resolution, mf, b, e, f, m, OccupyPoint(4, 4), OccupyPoint(mf - 45, mf - 45 /*(mf * 93 / 100).toInt, (mf * 93 / 100).toInt*/ ), 2.3, /*if(small) 0 else*/ 1, true, 0.0, true, heuristic4(10000, 1, 1)/*4(10000, 1, 1)*/)
                /*
                 * Why 121 / 128? I tried to make sure the target point is not excluded from the search space due to rounding when subdividing a space of odd size. 
                 * Due to subdividing, the search space shrinks at most by log2(size). Assuming it has at least size 128, it shrinks at most by log2(128)*size, 
                 * since log2(x) increases slower than (log2(x')/x')*x for x larger than 4 or something (?). log2(128) <= 7.
                 */
              }
            }

          }
        }
    }
    //    Configuration(setsShort.apply(x)._4, setsShort.apply(x)._4.map { y => points(y) }, resolution, magnificationFactor, setsShort.apply(x)._6, setsShort.apply(x)._1, setsShort.apply(x)._2, setsShort.apply(x)._3, standardStart, standardTarget, 2.3, 1, true, 0.0, true, setsShort.apply(x)._5)
    println("Reverse? Type true or false.")
    val reverse: Boolean = sc.nextBoolean()
    return if(reverse) configs.reverse else configs
  }
  def sameParameters(n: Int): List[Configuration] = {
    val allScans: List[String] = List(
      //      "aicause.kinect.scan.obstacles17", //Good
      //      "aicause.kinect.scan.obstacles18", //Good
      //      "aicause.kinect.scan.obstacles19", //Corrupted
      "aicause.kinect.scan.obstacles20", //Good
      //      "aicause.kinect.scan.obstacles21", //Corrupted
      "aicause.kinect.scan.obstacles22", //Good
      //      "aicause.kinect.scan.obstacles23", //Corrupted
      //      "aicause.kinect.scan.obstacles24", //Corrupted
      //      "aicause.kinect.scan.obstacles25", //Corrupted
      //      "aicause.kinect.scan.obstacles26", //Corrupted
      //      "aicause.kinect.scan.obstacles27", //Corrupted
      //      "aicause.kinect.scan.obstacles28", //Corrupted
      //      "aicause.kinect.scan.obstacles29", //Corrupted
      "aicause.kinect.scan.obstacles30", //Good
      //      "aicause.kinect.scan.obstacles31", //Corrupted
      //      "aicause.kinect.scan.obstacles32" //Corrupted
      //      "aicause.kinect.scan.festo01",
      //      "aicause.kinect.scan.festo02",
      //      "aicause.kinect.scan.festo03",
      "aicause.kinect.scan.obstacles19a",
      "aicause.kinect.scan.obstacles21a",
      "aicause.kinect.scan.obstacles23a",
      "aicause.kinect.scan.obstacles24a",
      "aicause.kinect.scan.obstacles25a",
      "aicause.kinect.scan.obstacles26a",
      "aicause.kinect.scan.obstacles27a",
      "aicause.kinect.scan.obstacles28a",
      "aicause.kinect.scan.obstacles29a",
      "aicause.kinect.scan.obstacles31a",
      "aicause.kinect.scan.obstacles32a",
      "aicause.kinect.scan.obstacles33",
      "aicause.kinect.scan.obstacles34")

    val id: Int = (allScans, allScans.map { x => points(x) }, 100, 256, cutOff(-1.08, 1.48, -1.38, 1.18), 0.0, 0.9, 8, OccupyPoint(4, 4), OccupyPoint(251, 251), 2.3, 1, true, 0.0, true, heuristic7()).hashCode()
    val config: Configuration = Configuration(allScans, allScans.map { x => points(x) }, arbitraryPlacementPoints, anglesForScanRotation, 100, 256, cutOff(-1.08, 1.48, -1.38, 1.18), 0.0, 0.9, 8, OccupyPoint(4, 4), OccupyPoint(251, 251), 2.3, 1, true, 0.0, true, heuristic7())
    val output = List.fill(n)(config)
    return output
  }
  def parameterFilter(config: List[Configuration], emptyValues: List[Double], fullValues: List[Double], minSizeValues: List[Int], setOfSets: List[String], heuristics: List[heuristicEnum], borders: List[cutOff], start: List[OccupyPoint], target: List[OccupyPoint], magnificationFactorList: List[Int]): List[Configuration] = {
    val output = config.filter { x =>
      (emptyValues == Nil || emptyValues.contains(x.empty)) &&
        (fullValues == Nil || fullValues.contains(x.full)) &&
        (minSizeValues == Nil || minSizeValues.contains(x.minSize)) &&
        (setOfSets == Nil || setOfSets.contains(x.dataSetTitles)) &&
        (heuristics == Nil || heuristics.contains(x.heuristicForSubdivision)) &&
        (borders == Nil || borders.contains(x.borders)) &&
        (start == Nil || start.contains(x.start)) &&
        (target == Nil || target.contains(x.target)) &&
        (magnificationFactorList == Nil || magnificationFactorList.contains(x.magnificationFactor))
    }
    return output
  }
  def newSingleTesterAndWriter = {
    val id = (List("aicause.kinect.scan.obstacles30"), List(points("aicause.kinect.scan.obstacles30")), 100, 256, cutOff(-1.08, 1.48, -1.38, 1.18), 0, 0.9, 8, OccupyPoint(4, 4), OccupyPoint(251, 251), 2.3, 1, true, 0.0, true, heuristic2(100, 1000, 0, 1, 1)).hashCode()
    val config: Configuration = Configuration(List("aicause.kinect.scan.obstacles30"), List(points("aicause.kinect.scan.obstacles30")), arbitraryPlacementPoints, anglesForScanRotation, 100, 256, cutOff(-1.08, 1.48, -1.38, 1.18), 0, 0.9, 8, OccupyPoint(4, 4), OccupyPoint(251, 251), 2.3, 1, true, 0.0, true, heuristic2(100, 1000, 0, 1, 1))
    try {
      val result: Results = obstacle3(config)
      AutonomousVisualisation.writeToFileSingle((config, result))
    } catch {
      case e: java.lang.RuntimeException => {
        val pw = new PrintWriter(new File("" + AutonomousVisualisation.filePath + "" + config.id + ".csv"))
        pw.write("Parameters:" + '\n')
        pw.write(AutonomousVisualisation.parametersCSV(config) + '\n')
        e.printStackTrace(pw)
        pw.close
      }
    }
  }
  def multiTesterAndWriter(first: Int, last: Int, imageOutput: Boolean) = {
    val params = parameters(first, last, None)
    var counter = 0
    params.foreach { x =>
      println("" + counter + "_" + x.id)
      counter = counter + 1
      actualWriting(x, imageOutput)
      System.gc()
      Thread.sleep(1000)
    }
  }
  def actualWriting(x: Configuration, imageOutput: Boolean) = {
    try {
      var result: Results = obstacle3(x)
      //      val filePath: String = "E:/Dropbox/Uni/Praktikum/bespaced-2016z/CarlTemplate/stored_output/"
      imageOutput match {
        case true  => AutonomousVisualisation.writeToFileSingle((x, result))
        case false => OutputCSV.writeToFileSingle((x, result))
      }

      result = null

      //      Thread.sleep(1000)
      //      wait(3000)
    } catch {
      case e: java.lang.RuntimeException => {
        val filePath = imageOutput match {
          case true  => AutonomousVisualisation.filePath
          case false => OutputCSV.filePath
        }
        val parameters = imageOutput match {
          case true  => AutonomousVisualisation.parametersCSV(x)
          case false => OutputCSV.parametersCSV(x)
        }
        val pw = new PrintWriter(new File("" + filePath + "" + x.id + ".csv"))
        pw.write("Parameters:" + '\n')
        pw.write(parameters + '\n')
        e.printStackTrace(pw)
        pw.close
      }
    }
  }
  def multiTesterAndWriterWithLoop(first: Int, last: Int, set: Option[Int], imageOutput: Boolean) = {
    val sc = new Scanner(System.in)
    println("Same experiment? Type true or false.")
    val same: Boolean = sc.nextBoolean()
    val params = {
      if (same) {
        println("how many?")
        val howMany: Int = sc.nextInt()
        sameParameters(howMany)
      } else parameters(first, last, set)
    }
    /*val scans: List[String] = List(
      "aicause.kinect.scan.obstacles17", //Good
      "aicause.kinect.scan.obstacles18", //Good
      //      "aicause.kinect.scan.obstacles19", //Corrupted
      "aicause.kinect.scan.obstacles20", //Good
      //      "aicause.kinect.scan.obstacles21", //Corrupted
      "aicause.kinect.scan.obstacles22", //Good
      //      "aicause.kinect.scan.obstacles23", //Corrupted
      //      "aicause.kinect.scan.obstacles24", //Corrupted
      //      "aicause.kinect.scan.obstacles25", //Corrupted
      //      "aicause.kinect.scan.obstacles26", //Corrupted
      //      "aicause.kinect.scan.obstacles27", //Corrupted
      //      "aicause.kinect.scan.obstacles28", //Corrupted
      //      "aicause.kinect.scan.obstacles29", //Corrupted
      "aicause.kinect.scan.obstacles30", //Good
      //      "aicause.kinect.scan.obstacles31", //Corrupted
      //      "aicause.kinect.scan.obstacles32" //Corrupted
      "aicause.kinect.scan.festo01",
      "aicause.kinect.scan.festo02",
      "aicause.kinect.scan.festo03",
      "aicause.kinect.scan.obstacles19a",
      "aicause.kinect.scan.obstacles21a",
      "aicause.kinect.scan.obstacles23a",
      "aicause.kinect.scan.obstacles24a",
      "aicause.kinect.scan.obstacles25a",
      "aicause.kinect.scan.obstacles26a",
      "aicause.kinect.scan.obstacles27a",
      "aicause.kinect.scan.obstacles28a",
      "aicause.kinect.scan.obstacles29a",
      "aicause.kinect.scan.obstacles31a",
      "aicause.kinect.scan.obstacles32a",
      "aicause.kinect.scan.obstacles33",
      "aicause.kinect.scan.obstacles34")
    val filteredParams = parameterFilter(paramsRaw, Nil, Nil, Nil, scans(4) :: scans(3) :: scans(2) :: scans.takeRight(13), List(heuristic4(10000,1,1)), Nil, Nil, Nil, Nil)
    val params = filteredParams.toArray*/
    var counter = 0
    val max = params.length
    while (counter < max) {
      println("Next experiment starting at " + java.util.Calendar.getInstance.getTime)
      var config: Configuration = params.apply(counter)
      println("" + counter + "_" + config.id)
      counter = counter + 1
      actualWriting(config, imageOutput)
      config = null
      println("Current experiment finishing at " + java.util.Calendar.getInstance.getTime)
      //      System.gc()
      //      Thread.sleep(1000)
    }
  }

  //This method needs to be worked on before it is called.
  def setIterator = {
    var i: Int = 0
    while (i < 21) {
      scanMap.clear()
      System.gc()
      Thread.sleep(1000)
      multiTesterAndWriterWithLoop(0, 810, Some(i), false)
      i = i + 1
    }
  }

  def multiTesterWithReturnValue(first: Int, last: Int): List[(Configuration, Results)] = {
    val serialParams = parameters(first, last, None)
    val timeBeforeParallelization = System.currentTimeMillis()
    val params = serialParams.par
    val timeAfterParallelization = System.currentTimeMillis()
    val timeNeededToParallelize = timeAfterParallelization - timeBeforeParallelization
    println("Time needed to create parallel collection: " + timeNeededToParallelize)
    var counter = 0
    val configsAndResults = params.map { x =>
      println(counter)
      counter = counter + 1
      val result: Results = obstacle3(x);
      System.gc();
      //      wait(3000);
      (x, result)
    }.toList
    return configsAndResults
  }

  def singleTester: List[(Configuration, Results)] = {
    val filePath: String = "./"
    val scans: List[String] = List(
      "aicause.kinect.scan.obstacles17", //Good
      "aicause.kinect.scan.obstacles18", //Good
      //      "aicause.kinect.scan.obstacles19", //Corrupted
      "aicause.kinect.scan.obstacles20", //Good
      //      "aicause.kinect.scan.obstacles21", //Corrupted
      "aicause.kinect.scan.obstacles22", //Good
      //      "aicause.kinect.scan.obstacles23", //Corrupted
      //      "aicause.kinect.scan.obstacles24", //Corrupted
      //      "aicause.kinect.scan.obstacles25", //Corrupted
      //      "aicause.kinect.scan.obstacles26", //Corrupted
      //      "aicause.kinect.scan.obstacles27", //Corrupted
      //      "aicause.kinect.scan.obstacles28", //Corrupted
      //      "aicause.kinect.scan.obstacles29", //Corrupted
      "aicause.kinect.scan.obstacles30", //Good
      //      "aicause.kinect.scan.obstacles31", //Corrupted
      //      "aicause.kinect.scan.obstacles32" //Corrupted
      "aicause.kinect.scan.festo01",
      "aicause.kinect.scan.festo02",
      "aicause.kinect.scan.festo03",
      "aicause.kinect.scan.obstacles19a",
      "aicause.kinect.scan.obstacles21a",
      "aicause.kinect.scan.obstacles23a",
      "aicause.kinect.scan.obstacles24a",
      "aicause.kinect.scan.obstacles25a",
      "aicause.kinect.scan.obstacles26a",
      "aicause.kinect.scan.obstacles27a",
      "aicause.kinect.scan.obstacles28a",
      "aicause.kinect.scan.obstacles29a",
      "aicause.kinect.scan.obstacles31a",
      "aicause.kinect.scan.obstacles32a",
      "aicause.kinect.scan.obstacles33",
      "aicause.kinect.scan.obstacles34")
    def standardScanSet: List[String] = scans(0) :: scans(1) :: scans(2) :: scans(3) :: scans(4) :: scans(0) :: scans(1) :: scans(2) :: scans(3) :: scans(4) :: scans(0) :: scans(1) :: scans(2) :: scans(3) :: scans(4) :: scans(0) :: Nil //List.fill(16)(scans(4)) //for debug purposes//
    def standardScanSet4: List[String] = scans(0) :: scans(1) :: scans(2) :: scans(3) :: scans(4) :: scans(0) :: scans(1) :: scans(2) :: scans(3) :: scans(4) :: scans(0) :: scans(1) :: scans(2) :: scans(3) :: scans(4) :: scans(0) :: Nil
    def standardScanSet3: List[String] = scans(0) :: scans(1) :: scans(2) :: "empty" :: scans(3) :: scans(4) :: scans(0) :: "empty" :: scans(1) :: scans(2) :: scans(3) :: List.fill(5)("empty")
    def simpleScanSet4: List[String] = List.fill(16)(scans(4))
    //    def simpleScanSet3: List[String] = List.fill(3)(scans(4)) ++ "empty" :: Nil ++ List.fill(3)(scans(4)) ++ "empty" :: Nil ++ List.fill(3)(scans(4)) ++ "empty" :: Nil ++ List.fill(4)("empty")
    def simpleScanSet3: List[String] = scans(4) :: scans(4) :: scans(4) :: "empty" :: scans(4) :: scans(4) :: scans(4) :: "empty" :: scans(4) :: scans(4) :: scans(4) :: "empty" :: "empty" :: "empty" :: "empty" :: "empty" :: Nil
    def simpleSparse1: List[String] = scans(4) :: List.fill(14)("empty") ++ (scans(4) :: Nil)
    def simpleSparse2: List[String] = scans(4) :: scans(4) :: "empty" :: scans(4) :: scans(4) :: scans(4) :: "empty" :: scans(4) :: "empty" :: "empty" :: "empty" :: List.fill(5)(scans(4))
    val dataSetTitles = List.fill(16)(scans(4)) //scans.take(16)//List.fill(15)(scans(4)) ++ (scans(16)::Nil)//scans.take(16)
    val currentPoints = dataSetTitles.map { x => points(x) }
    val resolution = 100
    val magnificationFactor = 1024
    val borders = cutOff(-1.08, 1.48, -1.38, 1.18) //cutOff(-0.98, 1.58, -1.25, 1.31)
    val empty = 0.001
    val full = 0.6
    val minSize = 8
    val start = OccupyPoint(1019, 1019)
    val target = OccupyPoint(4, 4) //OccupyPoint(888, 960)
    val zPlane = 2.3
    val densityFilterLevel = 1
    val densityFilter = true
    val densityPostProcessingThreshold = 0
    val shorteningEnabled = true
    val heuristic = heuristic2(10000, 1000000, 1000, 1, 1)
    val id: Int = (dataSetTitles, currentPoints, resolution, magnificationFactor, borders, empty, full, minSize, start, target, zPlane, densityFilterLevel, densityFilter, densityPostProcessingThreshold, shorteningEnabled, heuristic).hashCode()
    val config: Configuration = Configuration(dataSetTitles, currentPoints, arbitraryPlacementPoints, anglesForScanRotation, resolution, magnificationFactor, borders, empty, full, minSize, start, target, zPlane, densityFilterLevel, densityFilter, densityPostProcessingThreshold, shorteningEnabled, heuristic)
    val result: Results = obstacle3(config)
    return List((config, result))
    val timeStamp: Long = System.currentTimeMillis()
    val pw = new PrintWriter(new File(filePath + timeStamp + ".csv"))
    pw.write("Measurements:" + '\n')
    pw.write(AutonomousVisualisation.measurementsCSV(result) + '\n')
    pw.write("Parameters:" + '\n')
    pw.write(AutonomousVisualisation.parametersCSV(config))
    pw.close
    val pwp = new PrintWriter(new File(filePath + timeStamp + "parameters" + ".csv"))
    pwp.write("Parameters:" + '\n')
    pwp.write(AutonomousVisualisation.parametersCSV(config))
    pwp.close
    val pwr = new PrintWriter(new File(filePath + timeStamp + "results" + ".csv"))
    pwr.write("Measurements:" + '\n')
    pwr.write(AutonomousVisualisation.measurementsCSV(result) + '\n')
    pwr.close
    val sideBySide = new PrintWriter(new File(filePath + timeStamp + "twoLines" + ".csv"))
    val c: String = AutonomousVisualisation.parametersCSV(config)
    val m: String = AutonomousVisualisation.measurementsCSV(result)
    val twoLines: String = AutonomousVisualisation.twoLinesCSV(c, m)
    sideBySide.write(twoLines)
    sideBySide.close
    val cells = result.Leaves.map { x =>
      if (x.density < x.empty) (KinectScanDataTesterA.cellBox(x), Color.rgb(0, 0, 255, 0.1))
      else if (x.density > x.full) (KinectScanDataTesterA.cellBox(x), Color.rgb(255, 0, 0, 0.1))
      else (KinectScanDataTesterA.cellBox(x), Color.rgb(0, 255, 0, 0.1))
    }
    val pointsOnPath = result.Path.map { x =>
      if (!(result.MinClearancePoint.isEmpty) && x == result.MinClearancePoint.get) (x :: Nil, Color.Red)
      else (x :: Nil, Color.Green)
    }
    var pointCloud: List[OccupyPoint] = Nil
    result.pointWorkingSet.foreach {
      x => pointCloud = OccupyPoint(x._1._1, x._1._2) :: pointCloud
    }
    AutonomousVisualisation.drawScene(magnificationFactor, magnificationFactor, cells, pointsOnPath ++ /*minClearancePointList ++ */ ((pointCloud, Color.Black) :: Nil), 1, timeStamp + "")
    return List((config, result))
  }

  def obstacleMultiParameterTest(n: Int): List[(Configuration, Results)] = {
    val filePath: String = "./"
    val scans: List[String] = List(
      "aicause.kinect.scan.obstacles17", //Good
      "aicause.kinect.scan.obstacles18", //Good
      //      "aicause.kinect.scan.obstacles19", //Corrupted
      "aicause.kinect.scan.obstacles20", //Good
      //      "aicause.kinect.scan.obstacles21", //Corrupted
      "aicause.kinect.scan.obstacles22", //Good
      //      "aicause.kinect.scan.obstacles23", //Corrupted
      //      "aicause.kinect.scan.obstacles24", //Corrupted
      //      "aicause.kinect.scan.obstacles25", //Corrupted
      //      "aicause.kinect.scan.obstacles26", //Corrupted
      //      "aicause.kinect.scan.obstacles27", //Corrupted
      //      "aicause.kinect.scan.obstacles28", //Corrupted
      //      "aicause.kinect.scan.obstacles29", //Corrupted
      "aicause.kinect.scan.obstacles30", //Good
      //      "aicause.kinect.scan.obstacles31", //Corrupted
      //      "aicause.kinect.scan.obstacles32" //Corrupted
      "aicause.kinect.scan.festo01",
      "aicause.kinect.scan.festo02",
      "aicause.kinect.scan.festo03",
      "aicause.kinect.scan.obstacles19a",
      "aicause.kinect.scan.obstacles21a",
      "aicause.kinect.scan.obstacles23a",
      "aicause.kinect.scan.obstacles24a",
      "aicause.kinect.scan.obstacles25a",
      "aicause.kinect.scan.obstacles26a",
      "aicause.kinect.scan.obstacles27a",
      "aicause.kinect.scan.obstacles28a",
      "aicause.kinect.scan.obstacles29a",
      "aicause.kinect.scan.obstacles31a",
      "aicause.kinect.scan.obstacles32a",
      "aicause.kinect.scan.obstacles33",
      "aicause.kinect.scan.obstacles34")
    val resolution: Int = 100
    val s_t: List[OccupyPoint] = List(
      OccupyPoint(4, 4),
      OccupyPoint(1019, 1019),
      OccupyPoint(37, 598),
      OccupyPoint(712, 972),
      OccupyPoint(1011, 76),
      OccupyPoint(875, 692),
      OccupyPoint(892, 209),
      OccupyPoint(119, 41),
      OccupyPoint(103, 908),
      OccupyPoint(390, 378),
      OccupyPoint(12, 1020),
      OccupyPoint(888, 960))
    val s_t_sample: List[OccupyPoint] = {
      val x: List[Int] = List.range(0, 1023, 255)
      val y: List[Int] = List.range(0, 1023, 255)
      for (a <- x; b <- y) yield OccupyPoint(a, b) //Is that allowed?
    }
    def corruptionChecker = {
      scans.foreach {
        s =>
          {
            try {
              loadOrThrow(s)
              println("" + s + " readable")
            } catch {
              case _ => println("" + s + " corrupted")
            }
          }
      }

    }
    //    corruptionChecker
    def standardScanSet: List[String] = scans(0) :: scans(1) :: scans(2) :: scans(3) :: scans(4) :: scans(0) :: scans(1) :: scans(2) :: scans(3) :: scans(4) :: scans(0) :: scans(1) :: scans(2) :: scans(3) :: scans(4) :: scans(0) :: Nil //List.fill(16)(scans(4)) //for debug purposes//
    def standardScanSet4: List[String] = scans(0) :: scans(1) :: scans(2) :: scans(3) :: scans(4) :: scans(0) :: scans(1) :: scans(2) :: scans(3) :: scans(4) :: scans(0) :: scans(1) :: scans(2) :: scans(3) :: scans(4) :: scans(0) :: Nil
    def standardScanSet3: List[String] = scans(0) :: scans(1) :: scans(2) :: "empty" :: scans(3) :: scans(4) :: scans(0) :: "empty" :: scans(1) :: scans(2) :: scans(3) :: List.fill(5)("empty")
    def simpleScanSet4: List[String] = List.fill(16)(scans(4))
    //    def simpleScanSet3: List[String] = List.fill(3)(scans(4)) ++ "empty" :: Nil ++ List.fill(3)(scans(4)) ++ "empty" :: Nil ++ List.fill(3)(scans(4)) ++ "empty" :: Nil ++ List.fill(4)("empty")
    def simpleScanSet3: List[String] = scans(4) :: scans(4) :: scans(4) :: "empty" :: scans(4) :: scans(4) :: scans(4) :: "empty" :: scans(4) :: scans(4) :: scans(4) :: "empty" :: "empty" :: "empty" :: "empty" :: "empty" :: Nil
    def scan1: List[String] = scans.take(16)
    def scan2: List[String] = scans.takeRight(16)
    def scan3: List[String] = scans(4) :: scans(3) :: scans(2) :: scans.takeRight(13)

    def setOfSets: List[List[String]] = (simpleScanSet4 :: simpleScanSet3 :: standardScanSet4 :: standardScanSet3 :: scan1 :: scan2 :: scan3 :: Nil)
    def standardTarget: OccupyPoint = OccupyPoint(1019, 1019)
    def standardStart: OccupyPoint = OccupyPoint(4, 4)
    val center: (Int, Int) = (500, 500)
    val magnificationFactor: Int = 1024 //Must be power of 2
    val borders: List[cutOff] = List(cutOff(-1.08, 1.48, -1.38, 1.18), cutOff(-0.98, 1.58, -1.25, 1.31)) //cutOff(-1.6, 1.6, -1.6, 1.6)
    val empty: Double = 0
    val full: Double = 0.9
    val minSize: Int = 8
    val start = OccupyPoint(1, 1)
    val target = OccupyPoint(1007, 1007)
    val zPlane: Double = 2.3
    val emptyValues: List[Double] = List(0) //List(0, 0.001, 0.002, 0.003, 0.004, 0.005, 0.006, 0.007, 0.008, 0.009, 0.01) //.map { x => x * 5 }
    val fullValues: List[Double] = List(0.9) //List(0.5, 0.6, 0.7, 0.8, 0.9, 0.95, 1) //.map { x => x + 4 }
    val minSizeValues: List[Int] = List(8)
    val densityRelationValues: List[Int] = List(1, 2, 4, 10, 20, 50) //Not in use yet.
    val heuristics: List[heuristicEnum] = List(
      heuristic1(1, 1),
      heuristic1(2, 1),
      heuristic1(1, 2),
      heuristic1(3, 1),
      heuristic1(1, 3),
      heuristic1(2, 3),
      heuristic1(3, 2),
      heuristic2(10000, 1000000, 1000, 1, 1),
      heuristic2(10000, 1000000, 1000, 2, 1),
      heuristic2(10000, 1000000, 1000, 1, 2),
      heuristic2(100, 1000000, 1000, 1, 1),
      heuristic2(100, 1000, 0, 1, 1),
      heuristic3(1, 1, 1),
      heuristic3(3, 1, 1),
      heuristic3(2, 1, 1),
      heuristic3(1, 3, 1),
      heuristic3(1, 2, 1),
      heuristic3(2, 2, 1),
      heuristic3(3, 3, 1),
      heuristic3(2, 3, 1),
      heuristic3(3, 2, 1),
      heuristic3(1, 1, 3),
      heuristic3(1, 1, 2),
      heuristic3(1, 2, 2),
      heuristic3(1, 3, 3),
      heuristic3(1, 3, 2),
      heuristic3(1, 2, 3),
      heuristic4(10000, 1, 1),
      heuristic5(),
      heuristic6()) //List(heuristic1(), heuristic2(), heuristic3(), heuristic4())
    def randomStart: OccupyPoint = OccupyPoint(Random.nextInt(1023), Random.nextInt(1023))
    def randomTarget = randomStart
    def zPlaneValues: List[Double] = 2.3 :: Nil //List.range(15, 31, 5).map { x => x.toDouble / 10.0 }
    def randomScans(n: Int): List[String] = {
      val indices: List[Int] = List.range(0, n)
      return indices.map { x => scans.apply(Random.nextInt(scans.size)) }
    }
    def randomScanPoints(n: Int): List[(String, List[Occupy3DPointDouble])] = {
      val scans = randomScans(n)

      val output = scans.map { x => if (!scanMap.contains(x)) scanMap.+=((x, points(x))); scanMap.apply(x) }
      return scans zip output
    }

    def scanSets(n: Int): List[List[String]] = {
      val indices: List[Int] = List.range(0, n)
      return indices.map { x => randomScans(9) }
    }
    def startTargetSets(n: Int): List[(OccupyPoint, OccupyPoint)] = {
      val indices: List[Int] = List.range(0, n)
      return indices.map { x => (randomStart, randomTarget) }
    }
    def configurations(nMax: Int): List[Configuration] = {
      println("Set generation started at: " + java.util.Calendar.getInstance.getTime)
      //      val st_sets: List[(OccupyPoint, OccupyPoint)] = startTargetSets(nMax)
      //      val scan: List[List[String]] = scanSets(nMax)

      //      val sets = for (scanList <- scan; e <- emptyValues; f <- fullValues; m <- minSizeValues; z <- zPlaneValues; start <- s_t_sample; target <- s_t_sample) yield (scanList, e, f, m, z, start, target)
      val setsShort = for (e <- emptyValues; f <- fullValues; m <- minSizeValues; s <- setOfSets; h <- heuristics; b <- borders) yield (e, f, m, s, h, b)
      println("Scan sets generated at: " + java.util.Calendar.getInstance.getTime)
      println(setsShort.length)
      /*val sets = setsA.map {
        f =>
          f match {
            case (scan0, scan1, scan2, scan3, scan4, scan5, scan6, scan7, scan8, e, f, m, z) => (List(scan0, scan1, scan2, scan3, scan4, scan5, scan6, scan7, scan8), e, f, m, z)
          }
      }*/
      val n = if (nMax > setsShort.size) setsShort.size else nMax
      val indices = List.range(0, n)
      println("Set generation finished at: " + java.util.Calendar.getInstance.getTime)
      val output: List[Configuration] = indices.map { x =>
        {
          println(s"Start reading set $x at: " + java.util.Calendar.getInstance.getTime)
          //          val p = sets.apply(x)._1.map { y => points(y) }
          println("Finished reading yet another set at: " + java.util.Calendar.getInstance.getTime)
          //          Configuration(sets.apply(x)._1, p, resolution, magnificationFactor, borders, sets.apply(x)._2, sets.apply(x)._3, sets.apply(x)._4, sets.apply(x)._6, sets.apply(x)._7, sets.apply(x)._5)
          //          val id: Int = (setsShort.apply(x)._4, setsShort.apply(x)._4.map { y => points(y) }, resolution, magnificationFactor, setsShort.apply(x)._6, setsShort.apply(x)._1, setsShort.apply(x)._2, setsShort.apply(x)._3, standardStart, standardTarget, 2.3, 1, true, 0.0, true, setsShort.apply(x)._5).hashCode()
          Configuration(setsShort.apply(x)._4, setsShort.apply(x)._4.map { y => points(y) }, arbitraryPlacementPoints, anglesForScanRotation, resolution, magnificationFactor, setsShort.apply(x)._6, setsShort.apply(x)._1, setsShort.apply(x)._2, setsShort.apply(x)._3, standardStart, standardTarget, 2.3, 1, true, 0.0, true, setsShort.apply(x)._5)
        }
      }
      return output
    }
    val configs = configurations(n)
    println("Start computing results at: " + java.util.Calendar.getInstance.getTime)
    val emptyResult: Results = Results(Nil, Nil, Nil, 0, 0, 0, 0, 0, 0, 0, None, new HashMap[(Int, Int), (Double, Int)], 0, 0, 0, Nil)
    var counter: Int = 0
    val results = configs.map { x =>
      try {
        //        println(s"Start computing set $x at: " + java.util.Calendar.getInstance.getTime)
        println(counter)
        counter += 1
        obstacle3(x)
      } catch {
        case e: java.lang.RuntimeException => {
          val pw = new PrintWriter(new File("" + filePath + "" + x.hashCode() + ".csv"))
          pw.write("Parameters:" + '\n')
          pw.write(AutonomousVisualisation.parametersCSV(x) + '\n')
          e.printStackTrace(pw)
          pw.close
          emptyResult
        }
      }
    }
    return configs zip results
  }
  def HACtest: Boolean = {
    var myList: List[AbstractPolygon] = Nil
    (0 until 508).foreach {
      x => myList = InvariantObstacle(OccupyPoint(Random.nextInt(1000), Random.nextInt(1000))) :: myList
    }
    val myHAC: HAC = new HAC(myList)
    println("Distance Init finished at: " + java.util.Calendar.getInstance().getTime())
    val clustering: List[List[AbstractPolygon]] = myHAC.hacClustering(5.0, Some(Single))
    println("Clustering finished at: " + java.util.Calendar.getInstance().getTime())
    println("Convex Hull computation started at: " + java.util.Calendar.getInstance().getTime())
    val hulls: List[Polygon] = clustering.map { x =>
      {
        val p = Polygon(x.flatMap { x => x.vis() }).convexHull();
        //          AbstractPolygonVis(p, Color.Beige);
        p
      }
    }
    hulls.foreach { x => AbstractPolygonVis(x, Color.BLUE) }
    Draw()
    true
  }
  def HACTimeTest: Unit = {
    (1 to 508).foreach {
      x =>
        {
          val currentTime = System.currentTimeMillis()
          var myList: List[AbstractPolygon] = Nil
          (1 to x).foreach {
            y => myList = InvariantObstacle(OccupyPoint(y, y + 5)) :: myList
          }
          println("Generated: " + x + " in " + (System.currentTimeMillis() - currentTime))
          val currentTime2 = System.currentTimeMillis()
          val myHAC: HAC = new HAC(myList)
          //          println("Distance Init finished at: " + java.util.Calendar.getInstance().getTime())
          val clustering: List[List[AbstractPolygon]] = myHAC.hacClustering(5.0, Some(Single))
          println("Clustered: " + x + " in " + (System.currentTimeMillis() - currentTime2))
        }
    }
  }
  def bottle() {
    val myData: Invariant = loadOrThrow("aicause.kinect.scan.obstacles16") //Scan.Kinect.bottle() //Robotics.Festo.MiniFactory.station1.scenario1()
    val filtered1: List[Invariant] = myData match {
      case IMPLIES(_, BIGAND(head :: _)) => head match {
        case Owner(myList: Product) =>
          println(myList.productElement(1)); myList match {
            case BIGAND(finalList) => finalList
            case _                 => Nil
          }
        case _ => ???
      }
    }
    val filtered2: List[Invariant] = filtered1.map { x =>
      x match {
        case IMPLIES(p, _) => p
        case _             => ???
      }
    }.distinct
    println("Length Original: " + filtered1.length)
    println("Duplicates removed: " + filtered2.length)
    println("filtered: " + filtered2)
  }

  //  def bottle3(){
  //    val OBSTACLES1_ID = "aicause.kinect.scan.obstacles1"
  //    
  //    val obstacles1_old = loadOrThrow(OBSTACLES1_ID)
  //    val obstacles1_fix = fixKinectScanDataSet(obstacles1_old)
  //    
  //    println(obstacles1_fix)
  //    
  //    save(obstacles1_fix, OBSTACLES1_ID)
  //  }

  import scala.compat.Platform.EOL
  def obstacle =
    {
      val KINECT_OBSTACLES17_ID = "aicause.kinect.scan.obstacles22"

      val obstacles17 = loadOrThrow(KINECT_OBSTACLES17_ID)

      //println(obstacles16)
      println("Read data started at: " + java.util.Calendar.getInstance().getTime())
      val points: List[Occupy3DPointDouble] = obstacles17 match {
        case IMPLIES(tp, BIGAND(IMPLIES(Owner(_), BIGAND(points)) :: IMPLIES(Owner(_), BIGAND(colors)) :: Nil)) => points.map {
          i =>
            i match {
              case IMPLIES(Occupy3DPointDouble(x, y, z), cs) => Occupy3DPointDouble(x, y, z)
            }
        }
        case _ => ???
      }
      //      val sortedPoints: List[Occupy3DPointDouble] = points.sortWith(_.z < _.z).sortWith(_.y < _.y).sortWith(_.x < _.x)

      @tailrec
      def diffFilter(points: List[Occupy3DPointDouble], currentList: List[Double]): List[Double] =
        points match {
          case Nil            => currentList
          case x :: Nil       => currentList
          case a :: b :: tail => diffFilter(b :: tail, b.x - a.x :: currentList)
        }

      //    println(s"Sorted points 10000-10005: ${sortedPoints slice (10000,10005)}$EOL")
      //      println(s"Sorted points first 3000: ${sortedPoints.take(3000)}$EOL")
      //      println(s"${diffFilter(sortedPoints.take(3000), Nil)}$EOL")
      println("Computing plane started at: " + java.util.Calendar.getInstance().getTime())
      val resolution: Int = 25
      val center: (Int, Int) = (125, 125)
      val borders: cutOff = cutOff(-2.5, 2.5, -2.5, 2.5)
      var testPlane: List[OccupyPoint] = Nil
      var z_value: Double = 0.0
      var i: Int = 0
      var red: Double = 0
      var green: Double = 0
      var blue: Double = 0
      var gray: Double = 0.1
      val colours: List[Color] = List(Color.Beige, Color.Yellow, Color.GreenYellow, Color.Khaki, Color.Green, Color.Cyan, Color.Blue, Color.DarkBlue, Color.BlueViolet, Color.Violet, Color.Red)
      while (z_value < 3.0) {

        val cutoffpoints: List[Occupy3DPointDouble] = fixedCutOff(points, borders)

        val plane: List[OccupyPoint] = cutoffpoints.filter { x => x.z < z_value }.map {
          x =>
            OccupyPoint(Math.round((x.x /* + sortedPoints.minBy { x => x.x }*/ ).toFloat * resolution),
              Math.round(x.y.toFloat * resolution))
        }

        val planeDistinct: List[OccupyPoint] = plane.distinct

        val fourbyfour: List[OccupyPoint] = appendBelow(appendRight(planeDistinct, planeDistinct), appendRight(planeDistinct, planeDistinct))
        //      val planeReduced: List[OccupyPoint] = planeDistinct.map { x => OccupyPoint(Math.round(x.x / 10).toInt, Math.round(x.y / 10).toInt) }.distinct
        val planeCentered: List[OccupyPoint] = fourbyfour.map { x => OccupyPoint(x.x + center._1, x.y + center._2) }
        //      //      val planeReduced: List[OccupyPoint] = points.map { x => OccupyPoint(Math.round((x.x - 500) / 10 + 500).toInt, Math.round((x.y - 500) / 10 + 500).toInt) }.distinct.map { x => OccupyPoint((x.x - 500) * 10 + 500, (x.y - 500) * 100 + 500) }
        if (z_value >= 2.1 && z_value <= 2.4) {
          testPlane = planeCentered
        }
        SetMagnification(1.0)
        val currentColour: Color = Color.gray(Math.max(0.0, 1.0 - gray)) //Color.rgb(red, green, blue)

        Vis(planeCentered, currentColour)
        i += 1
        println(z_value)
        z_value += 0.3
        gray *= 1.2
        //        red *= 1.5
        //        green += 25
        //        blue += 25
      }

      val backtracking: ForthPathBacktracking = new ForthPathBacktracking()
      val start = OccupyPoint(1, 1)
      val target = OccupyPoint(275, 275)
      def heuristic(a: OccupyPoint, b: OccupyPoint): Double = PointArithmetic.distance(a, target) - PointArithmetic.distance(b, target)
      println("Graph generation started at: " + java.util.Calendar.getInstance().getTime())
      val configuration: PathPlusParametersPseudoGraph = PathPlusParametersPseudoGraph(Nil, Some((0, resolution, 0, resolution)), start,
        target, testPlane.map { x => InvariantObstacle(x) }, 10000, 10000, heuristic)
      println("Backtracking started at: " + java.util.Calendar.getInstance().getTime())
      val outcome = backtracking.Backtracking(configuration)
      outcome.path.foreach(println(_))
      Vis(outcome.path, Color.Green)

      Draw()
      /*
      println("Distance init with " + planeCentered.length + " points started at: " + java.util.Calendar.getInstance().getTime())
      val myHAC: HAC = new HAC(planeCentered.map { x => InvariantObstacle(x) })
      println("Clustering started at: " + java.util.Calendar.getInstance().getTime())
      val clustering: List[List[AbstractPolygon]] = myHAC.hacClustering(5.0, Some(Single))
      println("Convex Hull computation started at: " + java.util.Calendar.getInstance().getTime())
      val hulls: List[Polygon] = clustering.map { x =>
        {
          val p = Polygon(x.flatMap { x => x.vis() }).convexHull();
          //          AbstractPolygonVis(p, Color.Beige);
          p
        }
      }

      //clustering.map { x => Polygon(new ConvexHull().convexHull(x.flatMap { x => x.vis() })) }
      val backtracking: ForthPathBacktracking = new ForthPathBacktracking()
      val start = OccupyPoint(1, 1)
      val target = OccupyPoint(99, 99)
      def heuristic(a: OccupyPoint, b: OccupyPoint): Double = PointArithmetic.distance(a, target) - PointArithmetic.distance(b, target)
      println("Graph generation started at: " + java.util.Calendar.getInstance().getTime())
      val configuration: PathPlusParametersPseudoGraph = PathPlusParametersPseudoGraph(Nil, Some((0, 100, 0, 100)), start,
        target, hulls, 10000, 10000, heuristic)
      println("Backtracking started at: " + java.util.Calendar.getInstance().getTime())
      val outcome = backtracking.Backtracking(configuration)
      outcome.path.foreach(println(_))
      Vis(outcome.path, Color.Green)*/

      //    val indexed : IndexedSeq[Occupy3DPointDouble] = sortedPoints.toIndexedSeq

      //      Draw()
      //    println(s"${diffFilter(sortedPoints).take(500)}$EOL")

      //    assert(obstacles16 match {
      //      case IMPLIES(tp, BIGAND(IMPLIES(Owner(_), BIGAND(points)) :: IMPLIES(Owner(_), BIGAND(colors)) :: Nil)) => {
      //
      //        println(s"#Points: ${points.length}")
      //        println(s"#Points: ${colors.length}$EOL")
      //        
      //        println(s"${points slice (10000,10005)}$EOL")
      //        
      //        println(colors take 5)
      //        true
      //      }
      //    })
    }
  def bottle3 {
    val old: Invariant = loadOrThrow("aicause.kinect.scan.obstacles16") //Scan.Kinect.bottle()
    val filtered1: List[Invariant] = old match {
      case IMPLIES(_, BIGAND(IMPLIES(_, BIGAND(myList)) :: _)) =>
        println(myList.size); myList.map { x =>
          x match {
            case IMPLIES(p, c) => p
            case _             => FALSE()
          }
        }.distinct.take(10000)

      case _ => Nil
    }
    println(filtered1)
  }
  def bottle2() {
    val old: Invariant = Scan.Kinect.bottle()
    val filtered2: Invariant = old match {
      case IMPLIES(tp, BIGAND(List(IMPLIES(Owner("Points"), points), IMPLIES(Owner("Colors"), colors)))) =>
        points
      case _ => FALSE()
    }
    val filtered3: List[Invariant] = filtered2 match {
      case BIGAND(list) => list.map { x =>
        x match {
          case IMPLIES(point, _) => point
          case _                 => ???
        }
      }
      case _ => ???
    }
    val filteredShort: List[Invariant] = filtered3.distinct
    println("Length Original: " + filtered3.length)
    println("Duplicates removed: " + filteredShort.length)
    println("filtered: " + filteredShort)

    //    val filtered1: Invariant = old match {
    //
    //      case IMPLIES(tp, BIGAND(List(Owner(pointsProduct: Product), Owner(colorsProduct: Product)))) =>
    //        {
    //          type POINTS = BIGAND[IMPLIES[Occupy3DPoint, ComponentState[(Int, Int)]]]
    //          type COLORS = BIGAND[ComponentState[(Int, Int, Int)]]
    //
    //          val points: POINTS = pointsProduct.productElement(1).asInstanceOf[POINTS]
    //          val colors: COLORS = colorsProduct.productElement(1).asInstanceOf[COLORS]
    //
    //          IMPLIES(tp, BIGAND(List(
    //            IMPLIES(Owner("Points"), points),
    //            IMPLIES(Owner("Colors"), colors))))
    //        }
    //    }
  }
  def obstacleOld() {
    val myData: Invariant = Scan.Kinect.obstacles() //Robotics.Festo.MiniFactory.station1.scenario1()
    val filtered1: List[Invariant] = myData match {
      case IMPLIES(_, BIGAND(head :: tail)) => head match {
        case IMPLIES(_, BIGAND(myList)) => myList.distinct.map { x =>
          x match {
            case IMPLIES(Occupy3DPoint(x, y, z), _) => Occupy3DPoint(x, y, z)
          }
        }.distinct
        case _ => Nil
      }
      case _ => Nil
    }

    val Original: List[Invariant] = myData match {
      case IMPLIES(_, BIGAND(head :: tail)) => head match {
        case IMPLIES(_, BIGAND(myList)) => myList
        case _                          => Nil
      }
      case _ => Nil
    }
    println("Length Original: " + Original.length)
    println("Duplicates removed: " + filtered1.length)
    println("filtered: " + filtered1)
  }

  def obstacle2 = {
    val KINECT_OBSTACLES17_ID = "aicause.kinect.scan.obstacles17"
    println("Read data started at: " + java.util.Calendar.getInstance().getTime())
    val obstacles17 = loadOrThrow(KINECT_OBSTACLES17_ID)

    //println(obstacles16)
    println("Read data finished at: " + java.util.Calendar.getInstance().getTime())
    val points: List[Occupy3DPointDouble] = obstacles17 match {
      case IMPLIES(tp, BIGAND(IMPLIES(Owner(_), BIGAND(points)) :: IMPLIES(Owner(_), BIGAND(colors)) :: Nil)) => points.map {
        i =>
          i match {
            case IMPLIES(Occupy3DPointDouble(x, y, z), cs) => Occupy3DPointDouble(x, y, z)
          }
      }
      case _ => ???
    }
    val resolution: Int = 100
    val center: (Int, Int) = (500, 500)
    val magnificationFactor: Int = 2048 //Must be power of 2
    val borders: cutOff = cutOff(-2.5, 2.5, -2.5, 2.5)
    val cut: List[Occupy3DPointDouble] = fixedCutOff(points, borders).filter { x => x.z < 2.3 }
    val cutShifted: List[Occupy3DPointDouble] = cut.map { x =>
      x match {
        case Occupy3DPointDouble(x, y, z) => Occupy3DPointDouble(x - borders.minX, y - borders.minY, z)
        case _                            => x
      }
    }
    val cutMagnified: List[Occupy3DPointDouble] = cutShifted.map { x =>
      x match {
        case Occupy3DPointDouble(x, y, z) => Occupy3DPointDouble(x * resolution, y * resolution, z)
        case _                            => x
      }
    }

    val empty: Double = 0.001
    val full: Double = 1.0
    val tree: FirstQuadTree = FirstQuadTree(cutMagnified.distinct, magnificationFactor, empty, full)
    println("Initialized QuadTree at: " + java.util.Calendar.getInstance.getTime)
    val leaves: List[Cell] = tree.leaves
    println("Computed Leaves at: " + java.util.Calendar.getInstance.getTime)
    println("Number of Leaves: " + leaves.size)
    println("First leaf: " + OccupyBox(leaves.head.xMin, leaves.head.yMin, leaves.head.xMin + leaves.head.sideLength, leaves.head.yMin + leaves.head.sideLength) + " with density: " + leaves.head.density)

    leaves.foreach { x =>
      if (x.density < empty) {
        println(cellBox(x) + " " + x.numPoints + " empty")
        Vis(cellBox(x), Color.rgb(0, 0, 255, 0.5))
      } else if (x.density > full) {
        println(cellBox(x) + " " + x.numPoints + " full")
        Vis(cellBox(x), Color.rgb(255, 0, 0, 0.5))
      } else {
        println(cellBox(x) + " " + x.numPoints + " undecided")
        Vis(cellBox(x), Color.rgb(0, 255, 0, 0.5))
      }
    }
    println("Initialized Visualisation at: " + java.util.Calendar.getInstance.getTime)
    val cutAsMap: HashMap[(Int, Int), (Double, Int)] = toMatrix(cutMagnified, 1)
    val FourByFour: HashMap[(Int, Int), (Double, Int)] = appendBelow(appendRight(cutAsMap, cutAsMap), appendRight(cutAsMap, cutAsMap))
    //    FourByFour.foreach(x => Vis(OccupyPoint(x._1._1, x._1._2), Color.gray(1.0/depth(x._1._1, x._1._2, FourByFour))))

    FourByFour.foreach(x => Vis(OccupyPoint(x._1._1, x._1._2) :: Nil, Color.Black))

    SetMagnification(1.0)
    //    println("Finished concatenating maps at " + java.util.Calendar.getInstance.getTime)
    Draw()
  }

  def precomputePointCloud(fileName: String): BIGAND[OccupyPoint] = {
    val pointCloud = loadOrThrow(fileName)
    val p: List[Occupy3DPointDouble] = pointCloud match {
      case IMPLIES(tp, BIGAND(IMPLIES(Owner(_), BIGAND(points)) :: IMPLIES(Owner(_), BIGAND(colors)) :: Nil)) => points.map {
        i =>
          i match {
            case IMPLIES(Occupy3DPointDouble(x, y, z), cs) => Occupy3DPointDouble(x, y, z)
          }
      }
      case _ => ???
    }
    val integerPositions: List[OccupyPoint] = p.map { a =>
      a match {
        case Occupy3DPointDouble(x, y, z) => OccupyPoint(x.toInt, y.toInt)
      }
    }
    val output: BIGAND[OccupyPoint] = BIGAND(integerPositions)
    return output
  }
  def writePrecomputedPointCloud(fileName: String): Unit = {
    val pointCloud = precomputePointCloud(fileName)
    BeSpaceDData.save(pointCloud, fileName + "_precomputed")
  }
  def readPrecomputedPointCloud(fileName: String): List[OccupyPoint] = {
    val pointCloud = loadOrThrow(fileName)
    val p: List[OccupyPoint] = pointCloud match {
      case BIGAND(x) => x.map { x =>
        x match {
          case OccupyPoint(x, y) => OccupyPoint(x, y)
        }
      }
    }
    return p
  }
  def points(fileName: String): List[Occupy3DPointDouble] = {
    println("Read " + fileName + " started at: " + java.util.Calendar.getInstance().getTime())
    if (fileName == "empty") {
      return Nil
    }
    if (scanMap.contains(fileName)) {
      println("Read " + fileName + " finished at: " + java.util.Calendar.getInstance().getTime())
      return scanMap.apply(fileName)
    }
    val obstacles17 = loadOrThrow(fileName)

    //println(obstacles16)
    println("Read " + fileName + " finished at: " + java.util.Calendar.getInstance().getTime())
    val p: List[Occupy3DPointDouble] = obstacles17 match {
      case IMPLIES(tp, BIGAND(IMPLIES(Owner(_), BIGAND(points)) :: IMPLIES(Owner(_), BIGAND(colors)) :: Nil)) => points.map {
        i =>
          i match {
            case IMPLIES(Occupy3DPointDouble(x, y, z), cs) => Occupy3DPointDouble(x, y, z)
          }
      }
      case _ => ???
    }
    scanMap.+=((fileName, p))
    return p
  }

  //Due to rounding, depth and density values of rotated scans cannot be guaranteed at this point.
  //Formula taken from: Holger Theisel, Grundlagen der Computergrafik, Sommersemester 2013, Otto-von-Guericke Universität Magdeburg, Lecture 7, slide 25
  def rotate(input: HashMap[(Int, Int), (Double, Int)], phi: Double): HashMap[(Int, Int), (Double, Int)] = {
    val output: HashMap[(Int, Int), (Double, Int)] = input.map {
      f =>
        f match {
          case ((x, y), (z, d)) => {
            val xn = (x * Math.cos(phi) - y * Math.sin(phi)).toInt
            val yn = (x * Math.sin(phi) - y * Math.cos(phi)).toInt
            ((xn, yn), (z, d))
          }
        }
    }
    return output
  }
  def rotate(input: List[Occupy3DPointDouble], phi: Double): List[Occupy3DPointDouble] = {
    val output: List[Occupy3DPointDouble] = input.map { p =>
      p match {
        case Occupy3DPointDouble(x, y, z) => {
          val xn = (x * Math.cos(phi) - y * Math.sin(phi))
          val yn = (x * Math.sin(phi) - y * Math.cos(phi))
          Occupy3DPointDouble(xn, yn, z)
        }
      }
    }
    return output
  }
  def obstacle3(config: Configuration): Results = config match {
    case Configuration(dataSetTitle, points, placement, rotation, resolution, magnificationFactor, borders, empty, full, minSize, start, target, zPlane, densityFilterLevel, densityFilter, densityPostProcessingThreshold, shorteningEnabled, heuristic) => obstacle3(dataSetTitle, points, placement, rotation, resolution, magnificationFactor, borders, empty, full, minSize, start, target, zPlane, densityFilterLevel, densityFilter, densityPostProcessingThreshold, shorteningEnabled, heuristic, config.id)
  }
  def obstacle3(dataSetTitle: List[String], points: List[List[Occupy3DPointDouble]], placement: List[OccupyPoint], rotation: List[Double], resolution: Int, magnificationFactor: Int, borders: cutOff, empty: Double, full: Double, minSize: Int, start: OccupyPoint, target: OccupyPoint, zPlane: Double, densityFilterLevel: Int, densityFilter: Boolean, densityPostProcessingThreshold: Double, shorteningEnabled: Boolean, heuristicForSubdivision: heuristicEnum, experimentID: Int): Results = {
    //    println(points.head.length)
    //    val KINECT_OBSTACLES17_ID = "aicause.kinect.scan.obstacles18"
    /*println("Read data started at: " + java.util.Calendar.getInstance().getTime())
    val obstacles17 = loadOrThrow(KINECT_OBSTACLES17_ID)

    //println(obstacles16)
    println("Read data finished at: " + java.util.Calendar.getInstance().getTime())
    val points: List[Occupy3DPointDouble] = obstacles17 match {
      case IMPLIES(tp, BIGAND(IMPLIES(Owner(_), BIGAND(points)) :: IMPLIES(Owner(_), BIGAND(colors)) :: Nil)) => points.map {
        i =>
          i match {
            case IMPLIES(Occupy3DPointDouble(x, y, z), cs) => Occupy3DPointDouble(x, y, z)
          }
      }
      case _ => ???
    }*/
    /*val resolution: Int = 100
    val center: (Int, Int) = (500, 500)
    val magnificationFactor: Int = 1024 //Must be power of 2
    val borders: cutOff = cutOff(-1.6, 1.6, -1.6, 1.6)
    val empty: Double = 0.005
    val full: Double = 0.6
    val minSize: Int = 8
    val start = OccupyPoint(1, 1)
    val target = OccupyPoint(1007, 1007)
    val zPlane: Double = 2.3*/
    var outputImages: List[Image] = Nil
    val grid: Boolean = false
    val rotatePoints: Boolean = false

    def matrix(p: List[List[Occupy3DPointDouble]]): List[HashMap[(Int, Int), (Double, Int)]] = p.map { x =>
      {
        val cut: List[Occupy3DPointDouble] = fixedCutOff(x, borders).filter { x => x.z < zPlane }
        val cutShifted: List[Occupy3DPointDouble] = cut.map { x =>
          x match {
            case Occupy3DPointDouble(x, y, z) => Occupy3DPointDouble(x - borders.minX, y - borders.minY, z)
            case _                            => x
          }
        }
        val cutAsMap: HashMap[(Int, Int), (Double, Int)] = toMatrix(cutShifted, resolution)
        cutAsMap
      }
    }
    def rotatedMatrix: List[HashMap[(Int, Int), (Double, Int)]] = {
      val pointsWithAngles: List[(List[Occupy3DPointDouble], Double)] = points zip anglesForScanRotation
      val rotatedPoints: List[List[Occupy3DPointDouble]] = pointsWithAngles.map {
        f =>
          f match {
            case (pointCloud, phi) => rotate(pointCloud, phi)
          }
      }
      return matrix(rotatedPoints)
    }
    //    val cut: List[Occupy3DPointDouble] = fixedCutOff(points, borders).filter { x => x.z < zPlane }

    def simpleMatrix: List[HashMap[(Int, Int), (Double, Int)]] = {
      matrix(points)
    }
    def mat: List[HashMap[(Int, Int), (Double, Int)]] = {
      rotatePoints match{
        case true => rotatedMatrix
        case false => simpleMatrix
      }
    }
    val filteredMatrix: List[HashMap[(Int, Int), (Double, Int)]] = mat.map {
      y =>
        y.filter {
          x => x._2._2 > densityFilterLevel || !densityFilter
        }
    }
    val FourByFour: HashMap[(Int, Int), (Double, Int)] = grid match {
      case false => cutMap(arbitraryPlacement(filteredMatrix, magnificationFactor, arbitraryPlacementPoints /*randomPlacementPoints(magnificationFactor - Math.min(magnificationFactor - 1, (borders.maxX - borders.minX).toInt))*/ ), magnificationFactor)
      case true => {
        filteredMatrix match {
          case a0 :: a1 :: a2 :: a3 :: a4 :: a5 :: a6 :: a7 :: a8 :: a9 :: a10 :: a11 :: a12 :: a13 :: a14 :: a15 :: _ => {
            val line0 = appendRight(appendRight(appendRight(a0, a1), a2), a3)
            val line1 = appendRight(appendRight(appendRight(a4, a5), a6), a7)
            val line2 = appendRight(appendRight(appendRight(a8, a9), a10), a11)
            val line3 = appendRight(appendRight(appendRight(a12, a13), a14), a15)
            appendBelow(appendBelow(appendBelow(line0, line1), line2), line3)
          }
          case a0 :: a1 :: a2 :: a3 :: a4 :: a5 :: a6 :: a7 :: a8 :: _ => {
            val line0 = appendRight(appendRight(a0, a1), a2)
            val line1 = appendRight(appendRight(a3, a4), a5)
            val line2 = appendRight(appendRight(a6, a7), a8)
            appendBelow(appendBelow(line0, line1), line2)
          }
          case a0 :: a1 :: a2 :: a3 :: _ => {
            val line0 = appendRight(a0, a1)
            val line1 = appendRight(a2, a3)
            appendBelow(line0, line1)
          }
          case a0 :: _ => {
            a0
          }
          case _ => ???
        }
      }
    }
    /*val FourByFour: HashMap[(Int, Int), (Double, Int)] = {
      filteredMatrix match {
        case a0 :: a1 :: a2 :: a3 :: a4 :: a5 :: a6 :: a7 :: a8 :: a9 :: a10 :: a11 :: a12 :: a13 :: a14 :: a15 :: _ => {
          val line0 = appendRight(appendRight(appendRight(a0, a1), a2), a3)
          val line1 = appendRight(appendRight(appendRight(a4, a5), a6), a7)
          val line2 = appendRight(appendRight(appendRight(a8, a9), a10), a11)
          val line3 = appendRight(appendRight(appendRight(a12, a13), a14), a15)
          appendBelow(appendBelow(appendBelow(line0, line1), line2), line3)
        }
        case a0 :: a1 :: a2 :: a3 :: a4 :: a5 :: a6 :: a7 :: a8 :: _ => {
          val line0 = appendRight(appendRight(a0, a1), a2)
          val line1 = appendRight(appendRight(a3, a4), a5)
          val line2 = appendRight(appendRight(a6, a7), a8)
          appendBelow(appendBelow(line0, line1), line2)
        }
        case a0 :: a1 :: a2 :: a3 :: _ => {
          val line0 = appendRight(a0, a1)
          val line1 = appendRight(a2, a3)
          appendBelow(line0, line1)
        }
        case a0 :: _ => {
          a0
        }
        case _ => ???
      }
    }.filter {
      x => x._2._2 > densityFilterLevel || !densityFilter
    }*/
    //    val line: HashMap[(Int, Int), (Double, Int)] = appendRight(matrix.apply(0), appendRight(matrix.apply(1), matrix.apply(2)))
    //    val TwoByTwo: HashMap[(Int, Int), (Double, Int)] = appendBelow(line, appendBelow(line, line)) //Is actually 3*3 by now.
    //    val TwoByTwo: HashMap[(Int, Int), (Double, Int)] = appendBelow(appendRight(cutAsMap, cutAsMap), appendRight(cutAsMap, cutAsMap))

    val unionFind: DisjointSet[SecondQuadTree] = new DisjointSet[SecondQuadTree]
    //    def heuristicForSubdivision(a: SecondQuadTree, b: SecondQuadTree): Double = PointArithmetic.distance(a.center, target) - PointArithmetic.distance(b.center, target)
    val parentMap: HashMap[SecondQuadTree, SecondQuadTree] = new HashMap[SecondQuadTree, SecondQuadTree]
    val tree: TreeController = TreeController(FourByFour, 0, 0, magnificationFactor, empty, full, unionFind, minSize, start, target, heuristicForSubdivision, densityPostProcessingThreshold)
    //    unionFind.add(tree)

    println("Build tree started at " + java.util.Calendar.getInstance.getTime)
    val time0 = Timer.allTimes
    val allLeaves: (List[SecondQuadTree], Option[SecondQuadTree], Option[SecondQuadTree], List[Image]) = tree.freeSpace
    outputImages = allLeaves._4 ++ outputImages
    val time1 = Timer.allTimes
    val leaves: List[SecondQuadTree] = allLeaves._1
    val free: List[SecondQuadTree] = allLeaves._2 match {
      case Some(d) => allLeaves._3 match {
        case Some(e) => tree.channel(leaves, d, e)
        case None    => Nil
      }
      case None => Nil
    }

    val time2 = Timer.allTimes
    val startOrTargetInObstacle: Boolean = allLeaves._2.isEmpty || allLeaves._3.isEmpty
    println("Build tree finished at: " + java.util.Calendar.getInstance.getTime)
    println("Number of Leaves: " + leaves.size)
    println("Number of Free Leaves: " + free.size)

    //    val nonFree: List[SecondQuadTree] = leaves.filterNot { x => free.contains(x) }
    /*leaves.foreach { x => Vis(x.center, Color.Red) }

    leaves.foreach { x =>
      if (x.density < empty) {
        println(cellBox(x) + " " + x.numPoints + " empty")
        Vis(cellBox(x), Color.rgb(0, 0, 255, 0.1))
      } else if (x.density > full) {
        println(cellBox(x) + " " + x.numPoints + " full")
        Vis(cellBox(x), Color.rgb(255, 0, 0, 0.1))
      } else {
        println(cellBox(x) + " " + x.numPoints + " undecided")
        Vis(cellBox(x), Color.rgb(0, 255, 0, 0.1))
      }
    }

    TwoByTwo.foreach(x => Vis(OccupyPoint(x._1._1, x._1._2) :: Nil, Color.Black))

    SetMagnification(1.0)
    
    Draw()*/

    assert(free.length < Int.MaxValue)

    //    val pairs: List[(SecondQuadTree, SecondQuadTree)] = for (i <- free; j <- free) yield (i, j)
    //    val adjacentPairs: List[(SecondQuadTree, SecondQuadTree)] = pairs.filter(p => p._1.isAdjacentTo(p._2))
    //    val adjacentPairsCenters: List[(OccupyPoint, OccupyPoint)] = adjacentPairs.map(f => (f._1.center, f._2.center))
    def showStep(points: HashMap[(Int, Int), (Double, Int)], cells: List[SecondQuadTree]) = {
      val p = points.map {
        f => OccupyPoint(f._1._1, f._1._2)
      }.toList
      val c = cells.map { x =>
        if (free.contains(x)) (cellBox(x), Color.rgb(255, 0, 255, 0.1))
        else if (x.density <= x.empty) (cellBox(x), Color.rgb(0, 0, 255, 0.1))
        else if (x.density <= full) (cellBox(x), Color.rgb(0, 255, 0, 0.1))
        else (cellBox(x), Color.rgb(255, 0, 0, 0.1))
      }
      val timeStamp: Long = System.currentTimeMillis()
      /*AutonomousVisualisation.drawScene*/ outputImages = Image(magnificationFactor, magnificationFactor, c, (p, Color.Black) :: Nil, 1, "" + timeStamp, "") :: outputImages
    }

    val graph: FirstGraph[OccupyPoint] = new FirstGraph[OccupyPoint]
    showStep(FourByFour, leaves)
    val timeBeforeEdgeInsertion = Timer.allTimes
    free.foreach { x =>
      {
        val up = tree.upNeighbour(x)
        val low = tree.lowNeighbour(x)
        val left = tree.leftNeighbour(x)
        val right = tree.rightNeighbour(x)
        low match {
          case None =>
          case Some(d) => if (d.density <= d.empty) {
            val clearanceControlPoint: OccupyPoint = OccupyPoint(x.lowCenter.x, x.lowCenter.y - minSize / 2)
            graph.addEdge(clearanceControlPoint, x.lowCenter, false, PointArithmetic.distance(clearanceControlPoint, x.lowCenter), "" + clearanceControlPoint + " to " + x.lowCenter)
            graph.addEdge(clearanceControlPoint, d.center, false, PointArithmetic.distance(clearanceControlPoint, d.center), "" + clearanceControlPoint + " to " + d.center)
          }
        }
        up match {
          case None =>
          case Some(d) => if (d.density <= d.empty) {
            val clearanceControlPoint: OccupyPoint = OccupyPoint(x.upCenter.x, x.upCenter.y + minSize / 2)
            graph.addEdge(clearanceControlPoint, x.upCenter, false, PointArithmetic.distance(clearanceControlPoint, x.upCenter), "" + clearanceControlPoint + " to " + x.upCenter)
            graph.addEdge(clearanceControlPoint, d.center, false, PointArithmetic.distance(clearanceControlPoint, d.center), "" + clearanceControlPoint + " to " + d.center)
          }
        }
        right match {
          case None =>
          case Some(d) => if (d.density <= d.empty) {
            val clearanceControlPoint: OccupyPoint = OccupyPoint(x.rightCenter.x + minSize / 2, x.rightCenter.y)
            graph.addEdge(clearanceControlPoint, x.rightCenter, false, PointArithmetic.distance(clearanceControlPoint, x.rightCenter), "" + clearanceControlPoint + " to " + x.rightCenter)
            graph.addEdge(clearanceControlPoint, d.center, false, PointArithmetic.distance(clearanceControlPoint, d.center), "" + clearanceControlPoint + " to " + d.center)
          }
        }
        left match {
          case None =>
          case Some(d) => if (d.density <= d.empty) {
            val clearanceControlPoint: OccupyPoint = OccupyPoint(x.leftCenter.x - minSize / 2, x.leftCenter.y)
            graph.addEdge(clearanceControlPoint, x.leftCenter, false, PointArithmetic.distance(clearanceControlPoint, x.leftCenter), "" + clearanceControlPoint + " to " + x.leftCenter)
            graph.addEdge(clearanceControlPoint, d.center, false, PointArithmetic.distance(clearanceControlPoint, d.center), "" + clearanceControlPoint + " to " + d.center)
          }
        }
        graph.addEdge(x.center, x.lowCenter, false, PointArithmetic.distance(x.center, x.lowCenter), x.center + " to " + x.lowCenter)
        graph.addEdge(x.center, x.upCenter, false, PointArithmetic.distance(x.center, x.upCenter), x.center + " to " + x.upCenter)
        graph.addEdge(x.center, x.leftCenter, false, PointArithmetic.distance(x.center, x.leftCenter), x.center + " to " + x.leftCenter)
        graph.addEdge(x.center, x.rightCenter, false, PointArithmetic.distance(x.center, x.rightCenter), x.center + " to " + x.rightCenter)
        /*val list = up :: left :: low :: right :: Nil
        list.foreach { y =>
          y match {
            case None    =>
            case Some(d) => if (d.density < d.empty) graph.addEdge(x.center, d.center, false, PointArithmetic.distance(x.center, d.center), x.center + " to " + d.center)
          }
        }*/
      }
    }
    //    adjacentPairsCenters.foreach(f => graph.addEdge(f._1, f._2, false, PointArithmetic.distance(f._1, f._2), f._1 + " to " + f._2)) //PointArithmetic.distance(f._1, f._2)
    val timeAfterEdgeInsertion = Timer.allTimes
    val startBox: Option[SecondQuadTree] = free.find { x => x.containsLocation(start) }
    val targetBox: Option[SecondQuadTree] = free.find { x => x.containsLocation(target) }
    startBox match {
      case Some(b) => graph.addEdge(start, b.center, false, PointArithmetic.distance(start, b.center), start + " to " + b.center)
      case None    => println("Start node not in range")
    }
    targetBox match {
      case Some(b) => graph.addEdge(target, b.center, false, PointArithmetic.distance(target, b.center), target + " to " + b.center)
      case None    => println("Target node not in range")
    }
    //    ThirdAStar.graphVisPoint(graph)
    println("Graph generation finished at: " + java.util.Calendar.getInstance().getTime())
    val timeBeforeAStar = Timer.allTimes
    val pathRaw: (List[OccupyPoint], Double) = ForthAStar.Search(graph, start, target)
    val timeAfterAStar = Timer.allTimes
    val graphImage: Image = graphVisPointPath(graph, pathRaw._1, magnificationFactor)
    outputImages = graphImage :: outputImages
    Thread.sleep(1) //To make sure this image is not overwritten by the Shortening image.

    def leaf(pathPoint: OccupyPoint): Option[SecondQuadTree] = {
      return leaves.find { x => x.containsLocation(pathPoint) }
    }
    val nonFreeLeaves: List[SecondQuadTree] = leaves.filterNot { x => free.contains(x) }
    //Path Postprocessing
    def shortening(p: List[OccupyPoint]): List[OccupyPoint] = {
      return shorteningC(p, 0)
    }
    def lightShortening(p: List[OccupyPoint]): List[OccupyPoint] = {
      p match {
        case a :: b :: c :: tail => {
          if (allInSameLeaf(a :: b :: c :: Nil)) {
            return lightShortening(a :: c :: tail)
          } else {
            return a :: lightShortening(b :: c :: tail)
          }
        }
        case x => return x
      }
    }
    def shorteningC(p: List[OccupyPoint], clearance: Int): List[OccupyPoint] = {
      p match {
        case a :: b :: c :: tail =>
          /*println(a :: b :: c :: Nil); */ collisionTestAllC(a, c, nonFreeLeaves, clearance) match {
            case true  => return shorteningC(a :: c :: tail, clearance)
            case false => return a :: shorteningC(b :: c :: tail, clearance)
          }
        case x => return x
      }
    }

    def allInSameLeaf(p: List[OccupyPoint]): Boolean = {
      p match {
        case a :: b :: tail => return leaf(a) == leaf(b) && allInSameLeaf(b :: tail)
        case _              => return true
      }
    }
    //This method return true iff the visibility line between a and b collides with none of the cells.
    def collisionTestAll(a: OccupyPoint, b: OccupyPoint, cells: List[SecondQuadTree]): Boolean = {
      return collisionTestAllC(a, b, cells, 0) //cells.forall { x => !(x.lineSegmentCollision(a, b)) }
    }
    def collisionTestAllC(a: OccupyPoint, b: OccupyPoint, cells: List[SecondQuadTree], clearance: Int): Boolean = {
      return cells.forall { x => !(x.lineSegmentCollision(a, b, clearance)) }
    }

    //This method merges two lists to a new one with at most the euclidian length of the shorter one, while retaining common points.
    def listMergeShortening(a: List[OccupyPoint], b: List[OccupyPoint]): List[OccupyPoint] = {
      if (a == Nil) return b
      if (b == Nil) return a
      val commons: Option[OccupyPoint] = a.tail.find { x => b.tail.contains(x) } //That should be symmetric, but might not be efficient.

      val aSubList: List[OccupyPoint] = commons match {
        case None    => a
        case Some(d) => a.takeWhile { x => x != d }
      }
      val bSubList: List[OccupyPoint] = commons match {
        case None    => b
        case Some(d) => b.takeWhile { x => x != d }
      }
      val aSubListLength: Double = PointArithmetic.pathLength(aSubList)
      val bSubListLength: Double = PointArithmetic.pathLength(bSubList)
      val shorterSubList: List[OccupyPoint] = if (aSubListLength > bSubListLength) bSubList else aSubList

      val aDropped: List[OccupyPoint] = a.drop(aSubList.length)
      val bDropped: List[OccupyPoint] = b.drop(bSubList.length)
      return shorterSubList ++ listMergeShortening(aDropped, bDropped)
    }
    //This method organises the complete shortening
    def completeShortening(p: List[OccupyPoint], clearance: Int): List[OccupyPoint] = {
      val a: List[OccupyPoint] = shorteningC(p, clearance)
      val b: List[OccupyPoint] = shorteningC(p.reverse, clearance).reverse
      return listMergeShortening(a, b)
    }
    def AStarShortening(p: List[OccupyPoint], clearance: Int): List[OccupyPoint] = {
      val a: List[OccupyPoint] = shorteningC(p, clearance)
      val b: List[OccupyPoint] = shorteningC(p.reverse, clearance).reverse

      assert((a.isEmpty && b.isEmpty) || a.head == b.head && a.last == b.last)
      val shorteningGraph: FirstGraph[OccupyPoint] = new FirstGraph[OccupyPoint]
      def addAll(g: FirstGraph[OccupyPoint], list: List[OccupyPoint]): Unit = {
        list match {
          case h1 :: h2 :: tail => {
            g.addEdge(h1, h2, false, PointArithmetic.distance(h1, h2), "" + h1 + " to " + h2)
            addAll(g, h2 :: tail)
          }
          case _ =>
        }
      }
      addAll(shorteningGraph, a)
      addAll(shorteningGraph, b)
      val output: (List[OccupyPoint], Double) = if (a.isEmpty || b.isEmpty) (Nil, 0.0) else ForthAStar.Search(shorteningGraph, a.head, a.last)
      outputImages = graphVisPointPath(shorteningGraph, output._1, magnificationFactor) :: outputImages
      return output._1
    }
    def StrongShortening(p: List[OccupyPoint], clearance: Int): List[OccupyPoint] = {
      val pairs = for (a <- p; b <- p) yield Set(a, b)

      /*val filteredPairs = pairs.filterNot {
        e => e.size == 1 //e._1 == e._2 || e._1.x < e._2.x || (e._1.x == e._2.x && e._1.y < e._2.y)
      }*/
      val visiblePairs = pairs.filter { x =>
        x.toList match {
          case a :: b :: _ => collisionTestAllC(a, b, nonFreeLeaves, clearance)
          case _           => false
        }
      }
      val graph: FirstGraph[OccupyPoint] = new FirstGraph[OccupyPoint]
      visiblePairs.foreach { x =>
        x.toList match {
          case a :: b :: _ => graph.addEdge(a, b, false, PointArithmetic.distance(a, b), "" + a + " to " + b)
          case _           =>
        }
      }
      val path = ForthAStar.Search(graph, start, target)
      outputImages = graphVisPointPath(graph, path._1, magnificationFactor) :: outputImages
      return path._1
    }
    //    val shorteningEnabled: Boolean = false
    val timeBeforeShortening = Timer.allTimes
    //    val path: (List[OccupyPoint], Double) = (if (shorteningEnabled) shorteningC(shorteningC(pathRaw._1 /*.reverse*/ , minSize / 2).reverse, minSize / 2).reverse else /*lightShortening*/ (pathRaw._1.reverse), pathRaw._2)
    //    val path: (List[OccupyPoint], Double) = (if (shorteningEnabled) completeShortening(pathRaw._1.reverse, minSize / 2) else /*lightShortening*/ (pathRaw._1.reverse), pathRaw._2)
    val path: (List[OccupyPoint], Double) = (if (shorteningEnabled) /*StrongShortening*/ AStarShortening(pathRaw._1.reverse, minSize / 2) else /*lightShortening*/ (pathRaw._1.reverse), pathRaw._2)
    val timeAfterShortening = Timer.allTimes
    println("Shortening completed in: " + (timeAfterShortening.cpu - timeBeforeShortening.cpu) + " cpu time")
    println("Shortening completed in: " + (timeAfterShortening.user - timeBeforeShortening.user) + " user time")
    /*val nonFree: List[SecondQuadTree] = leaves.filterNot { x => x.density < empty }
    //    val nonFreeUnion: DisjointSet[SecondQuadTree] = new DisjointSet[SecondQuadTree]
    val obstacles: List[AbstractPolygon] = nonFree.map { x => InvariantObstacle(x.center) }

    println("Obstacle identification finished at: " + java.util.Calendar.getInstance().getTime())
    //    graph.addEdge(n1, n2, directed, weight, label)
*/
    val allCenters: List[OccupyPoint] = leaves.map { x => x.center }
    /*
    println("Backtracking initialization started at: " + java.util.Calendar.getInstance().getTime())

    val backtracking: ThirdPathBacktracking = new ThirdPathBacktracking()

    
    def heuristic(a: OccupyPoint, b: OccupyPoint): Double = PointArithmetic.distance(a, target) - PointArithmetic.distance(b, target)
    val configuration: PathPlusParameters = PathPlusParameters(Nil, graph, start, target, obstacles, 10000, 10000, heuristic)
    //    val configuration: PathPlusParametersPseudoGraph = PathPlusParametersPseudoGraph(Nil, Some((0, resolution, 0, resolution)), start,
    //      target, testPlane.map { x => InvariantObstacle(x) }, 10000, 10000, heuristic)
    println("Backtracking started at: " + java.util.Calendar.getInstance().getTime())
    val time3 = System.currentTimeMillis()
    val outcome = backtracking.Backtracking(configuration)
    val time4 = System.currentTimeMillis()
    outcome.path.foreach(println(_))
    val pointsOnPath: List[OccupyPoint] = PointArithmetic.borderPointsRecursive(outcome.path)*/

    val pointsOnPath: List[OccupyPoint] = PointArithmetic.borderPointsRecursive(path._1)

    //AStar specific code
    val pathLength: Double = if(startOrTargetInObstacle) -1.0 else PointArithmetic.pathLength(path._1) //path._2 is not updated after shortening.
    val pathCorners: Int = path._1.size

    def adjacentNonFreeCell(cell: SecondQuadTree): Option[SecondQuadTree] = {
      return nonFreeLeaves.find { x => cell.isAdjacentTo(x) }
    }
    def minClearancePoint: Option[OccupyPoint] = {
      if (pointsOnPath.isEmpty) return None
      val output: OccupyPoint = pointsOnPath.minBy { x =>
        distanceToAdjacentNonFreeCell(x) match {
          case None    => Double.MaxValue
          case Some(d) => d
        }
      }
      return Some(output)
    }
    def minClearance: Option[Double] = {
      minClearancePoint match {
        case None => return None
        case Some(d) => distanceToAdjacentNonFreeCell(d) match {
          case None =>
            {
              println(d)
              val currentLeaf = leaf(d)
              println(currentLeaf match {
                case None    => None
                case Some(l) => s"xMin: $l.xMin yMin: $l.yMin" + '\n' + adjacentNonFreeCell(l)
              })
              //              assert(false);
              return None //This can happen if there is only one leaf. Somehow...
            }
          case Some(e) => return Some(e)
        }
      }
    }

    def distanceToAdjacentNonFreeCell(pathPoint: OccupyPoint): Option[Double] = {
      return leaf(pathPoint) match {
        case None => None
        case Some(d) => adjacentNonFreeCell(d) match {
          case None    => None
          case Some(e) => d.distanceToBorder(pathPoint, e)
        }
      }
    }

    /*
    val pathLength: Double = PointArithmetic.pathLength(outcome.path)
    val pathCorners: Int = outcome.path.size

    //The following code is supposed to measure the minimum safe clearance between the path and any non-free cell.
    //Not exactly sure if looking at the nearest pathCorner-vertex is sufficient.
    def hasOccupiedNeighbour(y: SecondQuadTree): Boolean = {
      val adjacents: List[SecondQuadTree] = OccupiedNeighbours(y)
//      println(adjacents.length)
      val isIsolated: Boolean = adjacents.isEmpty
      return !isIsolated
      //        return !(leaves.filter { x => x.isAdjacentTo(y) }.isEmpty)
    }
    def OccupiedNeighbours(y: SecondQuadTree): List[SecondQuadTree] = {
      leaves.filter { x => x.isAdjacentTo(y) && x.density > empty }
    }
    val AllBorderVertices: List[OccupyPoint] = {

      leaves.filter { x => hasOccupiedNeighbour(x) }.map { x => x.center }
      //      allCenters.filter { x =>
      //        //      !(graph.successors(x).filter { y => obstacles.contains(y) }.isEmpty)
      //        var check: Boolean = false
      //        adjacentPairs.foreach(f => f match {
      //          case (a, b) => if (a == leaf(x) && b.density > empty) println("matched" + x); check = true
      //        })
      //        check
      //      }
    }
    def nearestPathCorner(pathPoint: OccupyPoint): Option[OccupyPoint] = if (!(pointsOnPath.contains(pathPoint))) None else {
      val position: Int = pointsOnPath.indexOf(pathPoint)
      val nearest: OccupyPoint = outcome.path.minBy { x => (pointsOnPath.indexOf(x) - position) * (pointsOnPath.indexOf(x) - position) }
      return Some(nearest)
    }
    def nearestBorderPathCorner(pathPoint: OccupyPoint): Option[OccupyPoint] = if (!(pointsOnPath.contains(pathPoint))) None else {
      val position: Int = pointsOnPath.indexOf(pathPoint)
      val nearest: OccupyPoint = BorderVerticesOnPath.minBy { x => (pointsOnPath.indexOf(x) - position) * (pointsOnPath.indexOf(x) - position) }
      return Some(nearest)
    }

    def BorderVerticesOnPath: List[OccupyPoint] = outcome.path.filter { x =>
      BorderVertex(x) match {
        case None    => println("This should not happen"); false;
        case Some(d) => d
      }
    }
    def BorderVertex(cornerPoint: OccupyPoint): Option[Boolean] = if (!(outcome.path.contains(cornerPoint))) None else {
      return Some(AllBorderVertices.contains(cornerPoint))
      //      return Some(!(graph.successors(cornerPoint).filter { x => obstacles.contains(x) }.isEmpty))
    }
    def closeToNonFreeArea(pathPoint: OccupyPoint): Boolean = {
      //      return nearestPathCorner(pathPoint) == nearestBorderPathCorner(pathPoint)
      val currentTree: SecondQuadTree = leaf(pathPoint) match {
        case None    => return false //Is this allowed?
        case Some(d) => d
      }
      val output: Boolean = BorderVertex(currentTree.center) match {
        case None    => false
        case Some(d) => d
      }
      val result = output || obstacles.contains(currentTree.center)
      //      println(pathPoint + "" + result)
      return result
    }
    def leaf(pathPoint: OccupyPoint): Option[SecondQuadTree] = {
      return leaves.find { x => x.containsLocation(pathPoint) }
    }
    def clearance(pathPoint: OccupyPoint): Option[Double] = if (!closeToNonFreeArea(pathPoint)) return None else {
      var nonFreeCell: Boolean = false
      return leaf(pathPoint) match {
        case None => None
        case Some(d) =>
          if (d.density > empty) nonFreeCell = true;
          d.distanceToBorder(pathPoint) match {
            case None    => None
            case Some(e) => Some(if (nonFreeCell) -e else e)
          }
      }
    }
    def clearanceSpecific(pathPoint: OccupyPoint, neighbour: SecondQuadTree): Option[Double] = if (!closeToNonFreeArea(pathPoint)) return None else {
      var nonFreeCell: Boolean = false
      return leaf(pathPoint) match {
        case None => None
        case Some(d) =>
          if (d.density > empty) nonFreeCell = true;
          d.distanceToBorder(pathPoint, neighbour) match {
            case None    => None
            case Some(e) => Some(if (nonFreeCell) -e else e)
          }
      }
    }
    def minClearanceAll(pathPoint: OccupyPoint): Option[Double] = {
      val output: List[Option[Double]] = nonFree.map { x => clearanceSpecific(pathPoint, x) }
      val relevant: List[Option[Double]] = output.filter { x => x match {
        case None => false
        case Some(d) => true}
      }
      if(relevant.isEmpty){
        return None
      }
      val relevantDoubles: List[Double] = relevant.map { x => x match{
        case None => ???
        case Some(d) => d}
      }
      return Some(relevantDoubles.min)
    }
    def minClearancePointOld: OccupyPoint = pointsOnPath.minBy { x =>
      clearance(x) match {
        case None    => Double.MaxValue
        case Some(d) => d
      }
    }
    def minClearancePoint: OccupyPoint = pointsOnPath.minBy { x =>
      minClearanceAll(x) match {
        case None    => Double.MaxValue
        case Some(d) => d
      }
    }
    def minClearance: Double = minClearanceAll(minClearancePoint) match{
      case None => Double.MaxValue
      case Some(d) => d
    }
    def minClearanceOld: Double = clearance(minClearancePoint) match {
      case None => {
        println(minClearancePoint)
        println("closeToNonFreeArea: " + closeToNonFreeArea(minClearancePoint))
        println(leaf(minClearancePoint) match {
          case None => "Not in any leaf";
          case Some(d) => d.distanceToBorder(minClearancePoint) match {
            case None    => "Not in this leaf: " + d
            case Some(d) => "In this leaf: " + d
          }
        })
        Double.MaxValue
      }
      case Some(d) => d
    }
    
*/
    if (!path._1.isEmpty) {
      minClearancePoint match {
        case Some(d) => Vis(d, Color.Yellow)
        case None    =>
      }
      //      Vis(minClearancePoint, Color.Yellow) //This is not visible at this point in the code.
      println("Minimum clearance at: " + minClearancePoint)
      val minDistanceToObstacle: Double = minClearance match {
        case Some(d) => d
        case None    => -1.0
      }
      println(minDistanceToObstacle)
      //      val debugNegativeClearance: Option[Double] = clearance(minClearancePoint)
    }

    Vis(pointsOnPath, Color.Green)

    //    allCenters.foreach { x => Vis(x, Color.Red) }

    leaves.foreach { x => Vis(x.center, Color.Red) }

    //    val Cells: List[OccupyBox] = leaves.map { x => cellBox(x) }

    leaves.foreach { x =>
      if (x.density < empty) {
        //        println(cellBox(x) + " " + x.numPoints + " empty")
        Vis(cellBox(x), Color.rgb(0, 0, 255, 0.1))
      } else if (x.density > full) {
        //        println(cellBox(x) + " " + x.numPoints + " full")
        Vis(cellBox(x), Color.rgb(255, 0, 0, 0.1))
      } else {
        //        println(cellBox(x) + " " + x.numPoints + " undecided")
        Vis(cellBox(x), Color.rgb(0, 255, 0, 0.1))
      }
    }

    FourByFour.foreach(x => Vis(OccupyPoint(x._1._1, x._1._2) :: Nil, Color.Black))

    SetMagnification(1.0)

    val minClearanceForOutput = if (!path._1.isEmpty) minClearance match {
      case None    => 0.0
      case Some(d) => d
    }
    else 0.0
    //The following code is supposed to aggregate the measurements and parameters and write them to a file.
    val measurements: String = "QuadTree operations performed in " + (time1.wallclock - time0.wallclock) + '\n' + "Extracted free space in " +
      (time2.wallclock - time1.wallclock) + '\n' + "Graph generated in " + (timeAfterEdgeInsertion.wallclock - timeBeforeEdgeInsertion.wallclock) + '\n' +
      "A* performed in " + (timeAfterAStar.wallclock - timeBeforeAStar.wallclock) + '\n' + "euclidian length of the path: " + pathLength + '\n' +
      "number of corner points of the path: " + pathCorners + '\n' + "minimum clearance: " + minClearanceForOutput + " at " + minClearancePoint
    //    println("QuadTree operations performed in " + (time1 - time0))
    //    println("Backtracking performed in " + (timeBeforeAStar - timeAfterAStar))
    //    println("minimum clearance: " + minClearanceForOutput)
    println(measurements)
    val parameters: String = "dataSetTitle: " + dataSetTitle + '\n' + "z plane: " + zPlane + '\n' +
      "resolution: " + resolution + '\n' + "magnification factor: " + magnificationFactor + '\n' +
      "minimum cell size: " + minSize + '\n' + "start: " + start + '\n' + "target: " + target + '\n' +
      "empty: " + empty + '\n' + "full: " + full + '\n' + "cutOff: " + borders
    /*val pw = new PrintWriter(new File("17.txt"))
    pw.write("Measurements:" + '\n')
    pw.write(measurements + '\n')
    pw.write("Parameters:" + '\n')
    pw.write(parameters)
    pw.close*/
    //    println(BorderVertices)
    println("Finished writing data to file at " + java.util.Calendar.getInstance.getTime)
    //    val allBV: List[OccupyPoint] = AllBorderVertices
    //    println("All border vertices: " + allBV.size)
    println("All leaves: " + leaves.size)
    //    println(allBV)

    println("Ready to draw")
    //    println("Finished concatenating maps at " + java.util.Calendar.getInstance.getTime)
    //    AutonomousDraw()

    outputImages = outputImages.map { x =>
      x match {
        case Image(x, y, cells, points, magnificationFactor, fileName, label) => Image(x, y, cells, points, magnificationFactor, "" + experimentID + "_" + fileName, "" + heuristicForSubdivision + "\t" + label)
      }
    }
    return Results(path._1, pointsOnPath, leaves, (time1.cpu - time0.cpu), (time2.cpu - time1.cpu), (timeAfterEdgeInsertion.cpu - timeBeforeEdgeInsertion.cpu), (timeAfterAStar.cpu - timeBeforeAStar.cpu), pathLength, pathCorners, minClearanceForOutput, minClearancePoint, FourByFour, free.size, leaves.size, (timeAfterShortening.cpu - timeBeforeShortening.cpu), outputImages)
  }

  def cellBox(x: Cell): OccupyBox = OccupyBox(x.xMin, x.yMin, x.xMin + x.sideLength, x.yMin + x.sideLength)

  def cellBox(x: SecondQuadTree): OccupyBox = OccupyBox(x.xMin, x.yMin, x.xMin + x.size, x.yMin + x.size)

  def fixedCutOff(points: List[Occupy3DPointDouble], borders: cutOff): List[Occupy3DPointDouble] = {
    return points.filter { x =>
      x match {
        case Occupy3DPointDouble(x, y, z) => borders match {
          case cutOff(xMin, xMax, yMin, yMax) => xMin <= x && xMax >= x && yMin <= y && yMax >= y
          case _                              => false
        }
        case _ => false
      }
    }
  }
  def randomPlacement(filteredMatrix: List[HashMap[(Int, Int), (Double, Int)]], size: Int): HashMap[(Int, Int), (Double, Int)] = {
    val stepWidth: Int = (Math.sqrt(filteredMatrix.length) * size).toInt
    val randomFactor: Int = 10
    val randomizedStep: Int = stepWidth + (Math.random() * randomFactor).toInt - randomFactor / 2
    ???
  }
  def arbitraryPlacement(filteredMatrix: List[HashMap[(Int, Int), (Double, Int)]], size: Int, seedPoints: List[OccupyPoint]): HashMap[(Int, Int), (Double, Int)] = {
    val mapsPoints: List[(HashMap[(Int, Int), (Double, Int)], OccupyPoint)] = filteredMatrix zip seedPoints
    val shiftedMaps: List[HashMap[(Int, Int), (Double, Int)]] = mapsPoints.map {
      x => moveScan(x._1, x._2)
    }
    /*    val completeMap: HashMap[(Int, Int), (Double, Int)] = shiftedMaps.reduceLeft{
      addScan(_, _)
    }*/
    var completeMap = new HashMap[(Int, Int), (Double, Int)]
    shiftedMaps.foreach {
      x => completeMap = addScan(completeMap, x)
    }
    return completeMap
  }
  def moveScan(a: HashMap[(Int, Int), (Double, Int)], start: OccupyPoint): HashMap[(Int, Int), (Double, Int)] = {
    val aShifted = a.map {
      x => ((x._1._1 + start.x, x._1._2 + start.y), (x._2._1, x._2._2))
    }
    return aShifted
  }
  def addScan(a: HashMap[(Int, Int), (Double, Int)], rest: HashMap[(Int, Int), (Double, Int)]): HashMap[(Int, Int), (Double, Int)] = {
    val output: HashMap[(Int, Int), (Double, Int)] = new HashMap[(Int, Int), (Double, Int)]
    rest.foreach(f => output.+=(f))
    a.foreach {
      x =>
        if (output.contains(x._1)) {
          val oldPair: ((Int, Int), (Double, Int)) = (x._1, output.get(x._1).get)
          val newDensity: Int = oldPair._2._2 + x._2._2
          val newDepth: Double = (oldPair._2._1 * oldPair._2._2 + x._2._1 * x._2._2) / newDensity
          val newPair: ((Int, Int), (Double, Int)) = (x._1, (newDepth, newDensity))
          output.+=(newPair)
        } else {
          output.+=(x)
        }
    }
    return output
  }
  def addScan2(a: HashMap[(Int, Int), (Double, Int)], rest: HashMap[(Int, Int), (Double, Int)]): HashMap[(Int, Int), (Double, Int)] = {
    val output: HashMap[(Int, Int), (Double, Int)] = rest
    a.foreach {
      x =>
        if (output.contains(x._1)) {
          val oldPair: ((Int, Int), (Double, Int)) = (x._1, output.get(x._1).get)
          val newDensity: Int = oldPair._2._2 + x._2._2
          val newDepth: Double = (oldPair._2._1 * oldPair._2._2 + x._2._1 * x._2._2) / newDensity
          val newPair: ((Int, Int), (Double, Int)) = (x._1, (newDepth, newDensity))
          output.+=(newPair)
        } else {
          output.+=(x)
        }
    }
    return output
  }
  def cutMap(a: HashMap[(Int, Int), (Double, Int)], size: Int): HashMap[(Int, Int), (Double, Int)] = {
    val output: HashMap[(Int, Int), (Double, Int)] = a.filter {
      z =>
        z match {
          case ((x, y), (_, _)) => x >= 0 && x < size && y >= 0 && y < size
        }
    }
    return output
  }

  def appendRight(a: HashMap[(Int, Int), (Double, Int)], b: HashMap[(Int, Int), (Double, Int)]): HashMap[(Int, Int), (Double, Int)] = {
    if (a.isEmpty) return b else if (b.isEmpty) return a
    val xMax: Int = a.maxBy(_._1._1)._1._1
    val bShifted: HashMap[(Int, Int), (Double, Int)] = b.map(x => ((x._1._1 + xMax, x._1._2), (x._2._1, x._2._2)))
    return a ++ bShifted
  }
  def appendRight(a: List[OccupyPoint], b: List[OccupyPoint]): List[OccupyPoint] = {
    if (a.isEmpty) return b else if (b.isEmpty) return a
    val xMax: Int = a.maxBy { x => x.x }.x
    val bShifted: List[OccupyPoint] = b.map { x => OccupyPoint(x.x + xMax, x.y) }
    return a ++ bShifted
  }
  def appendBelow(a: HashMap[(Int, Int), (Double, Int)], b: HashMap[(Int, Int), (Double, Int)]): HashMap[(Int, Int), (Double, Int)] = {
    if (a.isEmpty) return b else if (b.isEmpty) return a
    val yMax: Int = a.maxBy(_._1._2)._1._2
    val bShifted: HashMap[(Int, Int), (Double, Int)] = b.map(x => ((x._1._1, x._1._2 + yMax), (x._2._1, x._2._2)))
    return a ++ bShifted
  }
  def appendBelow(a: List[OccupyPoint], b: List[OccupyPoint]): List[OccupyPoint] = {
    if (a.isEmpty) return b else if (b.isEmpty) return a
    val yMax: Int = a.maxBy { x => x.y }.y
    val bShifted: List[OccupyPoint] = b.map { x => OccupyPoint(x.x, x.y + yMax) }
    return a ++ bShifted
  }
  /*def cutOff(points: List[Occupy3DPointDouble], resolution: Int, densityThreshold: Int): cutOff = {
    val minX: Int = Math.round(points.minBy { x => x.x }.x * 100).toInt
    val minY: Int = Math.round(points.minBy { x => x.y }.y * 100).toInt
    val maxX: Int = Math.round(points.maxBy { x => x.x }.x * 100).toInt
    val maxY: Int = Math.round(points.minBy { x => x.y }.y * 100).toInt
    val pairs: IndexedSeq[(Int, Int)] = for (x <- minX to maxX; y <- minY to maxY) yield (x, y)
    val sortedX = pairs.sortWith(_._1 < _._1)
    val sortedY = pairs.sortWith(_._2 < _._2)

    ???
  }*/

  def depth(x: Int, y: Int, points: List[Occupy3DPointDouble], resolution: Int): Double = {
    val pointData: (Double, Int) = toMatrix(points, resolution).apply((x, y))
    return pointData._1 / pointData._2
  }
  def density(x: Int, y: Int, points: List[Occupy3DPointDouble], resolution: Int): Double = {
    val pointData: (Double, Int) = toMatrix(points, resolution).apply((x, y))
    return pointData._2
  }
  def depth(x: Int, y: Int, points: HashMap[(Int, Int), (Double, Int)]): Double = {
    val pointData: (Double, Int) = points.apply((x, y))
    if (pointData._2 != 0) {
      return pointData._1 / pointData._2
    }
    return 0.0
  }
  def density(x: Int, y: Int, points: HashMap[(Int, Int), (Double, Int)]): Double = {
    val pointData: (Double, Int) = points.apply((x, y))
    return pointData._2
  }
  def toMatrix(points: List[Occupy3DPointDouble], resolution: Int): HashMap[(Int, Int), (Double, Int)] = {
    val myMap: HashMap[(Int, Int), (Double, Int)] = new HashMap()
    points.foreach { x =>
      val coordinates: (Int, Int) = (Math.round(x.x * resolution).toInt, Math.round(x.y * resolution).toInt)
      if (!(myMap.contains(coordinates))) {
        myMap.put(coordinates, (x.z, 1))
      } else {
        myMap.put(coordinates, (myMap.apply(coordinates)._1 + x.z, myMap.apply(coordinates)._2 + 1))
      }
    }
    //    Array.ofDim(n1, n2)
    return myMap
  }
  def toMatrix(points: List[OccupyPoint]): HashMap[(Int, Int), (Double, Int)] = {
    val myMap: HashMap[(Int, Int), (Double, Int)] = new HashMap[(Int, Int), (Double, Int)]
    points.foreach { p =>
      p match {
        case OccupyPoint(x, y) => myMap.get((x, y)) match {
          case None    => myMap.put((x, y), (0, 1))
          case Some(d) => myMap.put((x, y), (0, 1 + d._2))
        }
      }
    }
    return myMap
  }
  def depthEdgeFilter(points: List[Occupy3DPointDouble], resolution: Int, edgeStrength: Double): HashMap[(Int, Int), (Double, Int)] = {
    ???
  }
  def graphVisPoint(g: FirstGraph[OccupyPoint], magnificationFactor: Int): Image = {
    val edges = g.getEdges()
    val edgePoints = edges.map { x =>
      val color = if (x.directed) Color.Purple else if (x.weight == 0) Color.Green else Color.Red //Color.rgb(Math.min(255,(255 * x.weight).toInt), Math.min(255,(255 * x.weight).toInt), Math.min(255,(255 * x.weight).toInt))
      val points = Bresenham.BresenhamAlgorithm(x.n1, x.n2)
      (points, color)
    }
    return Image(magnificationFactor, magnificationFactor, Nil, edgePoints, 1, "" + System.currentTimeMillis() + "Graph", "")
  }
  def graphVisPointPath(g: FirstGraph[OccupyPoint], p: List[OccupyPoint], magnificationFactor: Int): Image = {
    val edges = g.getEdges()
    val edgePoints = edges.map { x =>
      val color = if (x.directed) Color.Purple else if (x.weight == 0) Color.Green else Color.Red //Color.rgb(Math.min(255,(255 * x.weight).toInt), Math.min(255,(255 * x.weight).toInt), Math.min(255,(255 * x.weight).toInt))
      val points = Bresenham.BresenhamAlgorithm(x.n1, x.n2)
      (points, color)
    }
    val pathPoints = (PointArithmetic.borderPointsRecursive(p), Color.Blue)
    return Image(magnificationFactor, magnificationFactor, Nil, edgePoints ++ (pathPoints :: Nil), 1, "" + System.currentTimeMillis() + "Graph", "")
  }
}
case class cutOff(minX: Double, maxX: Double, minY: Double, maxY: Double)
case class Results(PathCornerPoints: List[OccupyPoint], Path: List[OccupyPoint], Leaves: List[SecondQuadTree], QuadTreeTime: Long, FreeSpaceTime: Long, GraphGenerationTime: Long, AStarTime: Long, PathLength: Double, PathCorners: Int, MinClearance: Double, MinClearancePoint: Option[OccupyPoint], pointWorkingSet: HashMap[(Int, Int), (Double, Int)], numEmptyCells: Int, numAllCells: Int, PathShorteningTime: Long, images: List[Image])
case class Configuration(dataSetTitles: List[String], points: List[List[Occupy3DPointDouble]], placement: List[OccupyPoint], rotation: List[Double], resolution: Int, magnificationFactor: Int, borders: cutOff, empty: Double, full: Double, minSize: Int, start: OccupyPoint, target: OccupyPoint, zPlane: Double, densityFilterLevel: Int, densityFilter: Boolean, densityPostProcessingThreshold: Double, shorteningEnabled: Boolean, heuristicForSubdivision: heuristicEnum) {
  assert(dataSetTitles.size == points.size)
  def dataSetTitle = {
    var output = ""
    dataSetTitles.foreach { x => output = output + x }
  }
  def id: Int = this.hashCode()
}
/*"QuadTree operations performed in " + (time1 - time0) + '\n' + "Extracted free space in " +
      (time2 - time1) + '\n' + "Graph generated in " + (timeAfterEdgeInsertion - timeBeforeEdgeInsertion) + '\n' +
      "A* performed in " + (timeAfterAStar - timeBeforeAStar) + '\n' + "euclidian length of the path: " + pathLength + '\n' +
      "number of corner points of the path: " + pathCorners + '\n' + "minimum clearance: " + minClearanceForOutput + " at " + minClearancePoint*/