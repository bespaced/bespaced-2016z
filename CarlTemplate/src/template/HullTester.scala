package template

import BeSpaceDCore._

object HullTester extends CoreDefinitions with PointVis{
  def main(args: Array[String]): Unit = {
    val obstacles : List[AbstractPolygon] = List(OccupyBox(17,45,77,49), OccupyBox(33,38,88,44), OccupyBox(76,46,78,61), OccupyBox(49,42,73,43), OccupyBox(1,29,61,70), OccupyBox(86,49,88,67), OccupyBox(26,24,44,29), OccupyBox(84,54,94,71), OccupyBox(42,44,42,57), OccupyBox(6,23,32,47), OccupyBox(77,75,80,76), OccupyBox(59,52,69,71), OccupyBox(52,42,68,53), OccupyBox(68,53,89,64), OccupyBox(81,67,99,77), OccupyBox(4,71,90,76), OccupyBox(6,48,91,59), OccupyBox(32,26,69,79), OccupyBox(25,62,57,77), OccupyBox(50,40,80,75), OccupyBox(98,42,99,42), OccupyBox(17,44,31,71), OccupyBox(51,50,59,73), OccupyBox(14,71,40,76), OccupyBox(1,68,99,71), OccupyBox(57,58,94,61), OccupyBox(58,58,83,70), OccupyBox(5,69,38,76), OccupyBox(16,76,83,79), OccupyBox(95,46,96,67), OccupyBox(28,65,34,79), OccupyBox(44,39,82,54), OccupyBox(83,33,97,34), OccupyBox(89,27,89,59), OccupyBox(99,23,99,25), OccupyBox(65,45,84,59), OccupyBox(24,72,48,73), OccupyBox(23,51,81,74), OccupyBox(66,62,82,63), OccupyBox(28,75,49,78)).map { x => new InvariantObstacle(x) }
    val hullObject : ConvexHull = new ConvexHull()
    val points : List[OccupyPoint] = hullObject.convexHull(obstacles.flatMap { x => x.vis() })
    println(points)
    
  }
}