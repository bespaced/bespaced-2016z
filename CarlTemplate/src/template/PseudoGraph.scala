package template

import BeSpaceDCore._

/**
 * @author Carl
 * Usable and probably faster than FirstGraph[OccupyPoint] where graphs with 4-adjacency and a node for each 
 * Integer position are used.
 *
 */
object PseudoGraph {
  def successors(currentPosition: OccupyPoint): List[OccupyPoint] = {
    currentPosition match {
      case OccupyPoint(x, y) => return OccupyPoint(x, y + 1) :: OccupyPoint(x + 1, y) :: OccupyPoint(x, y - 1) :: OccupyPoint(x - 1, y) :: Nil
      case _                 => Nil
    }
  }
  def successors(currentPosition: OccupyPoint, limitXLow: Int, limitXHigh: Int, limitYLow: Int, limitYHigh: Int): List[OccupyPoint] = {
    return successors(currentPosition).filter { x =>
      x match {
        case OccupyPoint(x, y) => x >= limitXLow && x <= limitXHigh && y >= limitYLow && y <= limitYHigh
        case _                 => false
      }
    }
  }
}