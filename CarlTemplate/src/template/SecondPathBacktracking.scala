package template

import BeSpaceDCore._

class SecondPathBacktracking extends CoreDefinitions {
  var path: List[OccupyPoint] = Nil
  val graph: FirstGraph[OccupyPoint] = FirstGraphGenerator.graph
  val scene: (OccupyPoint, OccupyPoint, List[OccupyBox]) = new FirstSceneGenerator().Generator()
  val start: OccupyPoint = scene._1
  val target: OccupyPoint = scene._2
  val obstacles: List[Invariant] = scene._3
  val maxRecursionSteps : Int = Integer.MAX_VALUE
  val maxPathLength : Int = Integer.MAX_VALUE
  def heuristic(a: OccupyPoint, b: OccupyPoint) : Double = new FirstAStar().distance(a, scene._2) - new FirstAStar().distance(b, scene._2)
  var stepCounter : Int = 0
  
  def getPath() : List[OccupyPoint] = path

  //Various combinations of input parameters. More combinations are possible. It is always the same function, calling either with parameters or with the predefined values.
  def Backtracking(start: OccupyPoint, target: OccupyPoint, obstacles: List[Invariant], graph: FirstGraph[OccupyPoint], maxPathLength : Int, maxRecursionSteps : Int, heuristic : (OccupyPoint, OccupyPoint) => Double, collision : (OccupyPoint, List[Invariant]) => Boolean): List[OccupyPoint] = {
    backtrackingOnPaths(target, start::Nil, obstacles, graph, maxPathLength, maxRecursionSteps, heuristic, collision)
    return path
  }
//  def Backtracking(start: OccupyPoint, target: OccupyPoint, obstacles: List[Invariant], graph: FirstGraph[OccupyPoint], maxPathLength : Int, maxRecursionSteps : Int, heuristic : (OccupyPoint, OccupyPoint) => Double): List[OccupyPoint] = {
//    backtrackingOnPaths(target, start::Nil, obstacles, graph, maxPathLength, maxRecursionSteps, heuristic, collision)
//    return path
//  }
  def Backtracking(start: OccupyPoint, target: OccupyPoint, obstacles: List[Invariant], graph: FirstGraph[OccupyPoint], maxPathLength : Int, maxRecursionSteps : Int, collision : (OccupyPoint, List[Invariant]) => Boolean): List[OccupyPoint] = {
    backtrackingOnPaths(target, start::Nil, obstacles, graph, maxPathLength, maxRecursionSteps, heuristic, collision)
    return path
  }
  def Backtracking(start: OccupyPoint, target: OccupyPoint, obstacles: List[Invariant], graph: FirstGraph[OccupyPoint], maxPathLength : Int, collision : (OccupyPoint, List[Invariant]) => Boolean): List[OccupyPoint] = {
    backtrackingOnPaths(target, start::Nil, obstacles, graph, maxPathLength, maxRecursionSteps, heuristic, collision)
    return path
  }
  def Backtracking(start: OccupyPoint, target: OccupyPoint, obstacles: List[Invariant], graph: FirstGraph[OccupyPoint], collision : (OccupyPoint, List[Invariant]) => Boolean): List[OccupyPoint] = {
    backtrackingOnPaths(target, start::Nil, obstacles, graph, maxPathLength, maxRecursionSteps, heuristic, collision)
    return path
  }
  def Backtracking(start: OccupyPoint, target: OccupyPoint, obstacles: List[Invariant], collision : (OccupyPoint, List[Invariant]) => Boolean): List[OccupyPoint] = {
    backtrackingOnPaths(target, start::Nil, obstacles, graph, maxPathLength, maxRecursionSteps, heuristic, collision)
    return path
  }
  def Backtracking(maxPathLength : Int, maxRecursionSteps : Int, heuristic : (OccupyPoint, OccupyPoint) => Double): List[OccupyPoint] = {
    backtrackingOnPaths(target, start::Nil, obstacles, graph, maxPathLength, maxRecursionSteps, heuristic, collision)
    return path
  }
  def Backtracking(maxPathLength : Int, maxRecursionSteps : Int): List[OccupyPoint] = {
    backtrackingOnPaths(target, start::Nil, obstacles, graph, maxPathLength, maxRecursionSteps, heuristic, collision)
    return path
  }
  def Backtracking(heuristic : (OccupyPoint, OccupyPoint) => Double): List[OccupyPoint] = {
    backtrackingOnPaths(target, start::Nil, obstacles, graph, maxPathLength, maxRecursionSteps, heuristic, collision)
    return path
  }
  def Backtracking(): List[OccupyPoint] = {
    backtrackingOnPaths(target, start::Nil, obstacles, graph, maxPathLength, maxRecursionSteps, heuristic, collision)
    return path
  }
  
  //Helper method for recursive backtracking.
  def collision(point: OccupyPoint, obstacles: List[Invariant]): Boolean = {
    return collisionTestsBig(obstacles.map(x => unfoldInvariant(x)), unfoldInvariant(point) :: Nil)
  }
  def collision[ObstacleType](point : OccupyPoint, obstacles : ObstacleType) : Boolean = {
    if(obstacles.isInstanceOf[List[OccupyBox]]){
      
    }
    ???
  }

  //The actual recursive backtracking method.
  def backtrackingOnPaths(target: OccupyPoint, decisions: List[OccupyPoint], obstacles: List[Invariant], graph : FirstGraph[OccupyPoint], maxPathLength : Int, maxRecursionSteps : Int, heuristic : ((OccupyPoint, OccupyPoint) => Double), collision : (OccupyPoint, List[Invariant]) => Boolean): Boolean = {
    stepCounter += 1
    if(stepCounter > maxRecursionSteps || maxPathLength < 0){
      return false
    }
    
    if (decisions.last == target) {
      path = decisions
      return true
    } else {
      val heuristicList: List[OccupyPoint] = graph.successors(decisions.last).sortWith(heuristic(_,_) < 0)
      var i: Int = 0
      while (i < heuristicList.size) {
        val nextChoice: OccupyPoint = heuristicList.apply(i)
        i = i + 1 //That's an important point...
        if (!(collision(nextChoice, obstacles)) && !(decisions.contains(nextChoice))) {
          if (backtrackingOnPaths(target, decisions ++ (nextChoice :: Nil), obstacles, graph, maxPathLength - 1, maxRecursionSteps, heuristic, collision)) {
            return true
          }
        }
      }
      return false
    }
    return true
  }
}