package template

import BeSpaceDCore._
import scala.math._
import Bresenham._

/**
 * @author Carl
 * 	Note about turns and angles: clockwise and counter-clockwise turns refer to a coordinate system 
 * where the first argument is the value on the horizontal axis and the second one on the vertical 
 * axis. The horizontal axis goes from left to right, the vertical axis from bottom to top.
 * Angles are positive if they go CCW according to above definition, negative if CW, 0 otherwise.
 * 
 * Most of these functions are based on
 * FEITO, F.; TORRES, Juan Carlos; URENA, A. Orientation, simplicity, and inclusion test for planar polygons. Computers & Graphics, 1995, 19. Jg., Nr. 4, S. 595-600.
 * 
 * The polygon representation in there has the following advantages:
 * It works robustly for polygons, even if they are not convex.
 * It avoids floating-point errors.
 * It provides the most important methods - orientation, inclusion, simplicity-check.
 * 
 * It has the following disadvantages:
 * There are points on the border which are regarded inside and others regarded outside. => Not robust at borders.
 * It doesn't regard polygons with collinear edges as simple. See example below [1]
 * The computation of area for a polygon is highly questionable - in fact, it returned 4 instead of 2 for (0,0),(2,0),(1,2).
 * There seem to be a few other flaws in the paper.
 * I think the simplicity-check is computationally more expensive than O(n) claimed in the paper.
 * 
 * [1] This is, when two edges overlap each other, so the width of the polygon reduces to 1 at some point, there is an edge intersection between one of the edges and a neighbouring edge to the other one.
 * For example: The polygon OccupyPoint(1,6),OccupyPoint(3,7),OccupyPoint(4,7),OccupyPoint(6,5),OccupyPoint(6,7),OccupyPoint(1,7) is regarded non-simple, because the edge (6,7) to (1,7) intersects with the edge (4,7) to (6,5).
 * However, this example is actually a legal polygon, it should have an area and orientation, there should also be inclusion tests possible. Since we don't expect to see such cases in the real data, we don't deal with them now.
 */
object PointArithmetic extends CoreDefinitions {
  def enforceCCW(points: List[OccupyPoint]) : List[OccupyPoint] = {
    if(polygonSelfIntersection(points)){
      //Not a simple polygon, has self-intersection, ccw not defined. Actually, in some cases it could be defined, but this method forbids it. TODO: Create more verbose exception.
      throw new Exception()
      return Nil
    }
    if(orientation(points) < 0){
      return points.reverse
    }
    return points
  }
  
  //returns -1 for cw, 1 for ccw, 0 for collinear polygons
  def orientation(points: List[OccupyPoint]): Int = {
    return signum(orientationTestSimplePolygon(points))
  }
  
  //polygonOrientation can't handle polygons with a certain combination of collinear points. At u-turns, the algorithm doesn't know which direction to turn. It returns 0 due to collinearity.
  def polygonOrientation(points: List[OccupyPoint]): Double = {
    val extendedPoints: List[OccupyPoint] = points.apply(points.length - 2) :: points.last :: points
    return polygonOrientationRecursive(extendedPoints)
  }
  def polygonOrientationRecursive(points: List[OccupyPoint]): Double = {
    points match {
      case head1 :: head2 :: head3 :: tail =>  return orientationSensitiveOuterAngle(head1, head2, head3) + polygonOrientationRecursive(head2 :: head3 :: tail)
      case _                               => return 0.0
    }
  }

  // 2D cross product of OA and OB vectors, i.e. z-component of their 3D cross product.
  // Returns a positive value, if OAB makes a counter-clockwise turn,
  // negative for clockwise turn, and zero if the points are collinear.
  def cross(o: (Int, Int), a: (Int, Int), b: (Int, Int)): Int = {
    (a._1 - o._1) * (b._2 - o._2) - (a._2 - o._2) * (b._1 - o._1)
  }
  def cross(o: OccupyPoint, a: OccupyPoint, b: OccupyPoint): Int = {
    return cross((o.x, o.y), (a.x, a.y), (b.x, b.y))
  }

  //Angle A, i.e. OAB
  //Returns degrees
  def angle(o: OccupyPoint, a: OccupyPoint, b: OccupyPoint): Double = {
    val r = radiant(o, a, b)
    val output = r.toDegrees
    return output
  }
  def orientationSensitiveAngle(o: OccupyPoint, a: OccupyPoint, b: OccupyPoint): Double = {
    return angle(o, a, b) * signum(cross(o, a, b)) 
  }
  def outerAngle(o: OccupyPoint, a: OccupyPoint, b: OccupyPoint): Double = {
    return outerRadiant(o, a, b).toDegrees
  }
  def orientationSensitiveOuterAngle(o: OccupyPoint, a: OccupyPoint, b: OccupyPoint): Double = {
    return outerAngle(o, a, b) * signum(cross(o, a, b)) 
  }
  //Angle in A, i.e. OAB
  //Returns radiants
  def radiant(o: OccupyPoint, a: OccupyPoint, b: OccupyPoint): Double = {
    val n = normDotP(o, a, b)
    val output = acos(n)
    return output
  }
  def orientationSentitiveRadiant(o: OccupyPoint, a: OccupyPoint, b: OccupyPoint): Double = {
    return radiant(o, a, b) * signum(cross(o, a, b))
  }
  def outerRadiant(o: OccupyPoint, a: OccupyPoint, b: OccupyPoint): Double = {
    return math.Pi - acos(normDotP(o, a, b))
  }
  def orientationSentitiveOuterRadiant(o: OccupyPoint, a: OccupyPoint, b: OccupyPoint): Double = {
    return outerRadiant(o, a, b) * signum(cross(o, a, b))
  }
  def normDotP(o: OccupyPoint, a: OccupyPoint, b: OccupyPoint): Double = {
    val dot = dotP(o,a,b)
    val dab = distance(a, b)
    val doa = distance(o, a)
    val normed = dot / (dab * doa)
    val output = bound(normed, -1, 1)
    return output
  }
  def distance(a: OccupyPoint, b: OccupyPoint): Double = {
    return math.sqrt((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y))
  }
  def dotP(o: OccupyPoint, a: OccupyPoint, b: OccupyPoint): Double = {
    //return (a.x - o.x) * (b.x - o.x) + (a.y - o.y) * (b.y - o.y) //This would have been AOB.
    return (o.x - a.x) * (b.x - a.x) + (o.y - a.y) * (b.y - a.y)
  }
  //This formula is taken from FEITO, F.; TORRES, Juan Carlos; URENA, A. Orientation, simplicity, and inclusion test for planar polygons. Computers & Graphics, 1995, 19. Jg., Nr. 4, S. 595-600.
  def orientationTestSimplePolygon(points: List[OccupyPoint]): Int = {
    return orientationTestSimplePolygonRecursive(points.last :: points)
  }
  def orientationTestSimplePolygonRecursive(points: List[OccupyPoint]): Int = {
    points match{
      case a :: b :: tail => return (a.x * b.y - b.x * a.y) + orientationTestSimplePolygonRecursive(b :: tail)
      case _ => return 0
    }
  }
  //Also taken from FEITO, F.; TORRES, Juan Carlos; URENA, A. Orientation, simplicity, and inclusion test for planar polygons. Computers & Graphics, 1995, 19. Jg., Nr. 4, S. 595-600.
  //and makes much more intuitively sense than the other formula above
  def polygonSelfIntersection(points: List[OccupyPoint]): Boolean = {
    return polygonSelfIntersectionRecursive(points.last::points)
  }
  def polygonSelfIntersectionRecursive(points: List[OccupyPoint]) : Boolean = {
    points match{
      case a::b::tail => return polygonSelfIntersectionHelper(b::tail, a, b) || polygonSelfIntersectionRecursive(b::tail)
      case _ => return false
    }
  }
  def polygonSelfIntersectionHelper(points: List[OccupyPoint], a: OccupyPoint, b: OccupyPoint): Boolean = {
    points match{
      case c::d::tail => return edgeIntersection(a, b, c, d) || polygonSelfIntersectionHelper(d::tail, a, b)
      case _ => return false
    }
  }
  def edgeIntersection(i: OccupyPoint, j: OccupyPoint, k: OccupyPoint, l: OccupyPoint) : Boolean = {
    if(i == k || i == l || j == k || j == l){
      return false
    }
    return (orientation(List(k,i,j)) != orientation(List(l,i,j))) && (orientation(List(i,k,l)) != orientation(List(j,k,l)))
  }
  def inclusion(polygon: List[OccupyPoint], point: OccupyPoint): Boolean = {
    val jValues : List[Int] = inclusionA(polygon++(polygon.head::Nil), point)
    val kValues : List[Int] = inclusionB(jValues ++ (jValues.head::Nil))
    val included : Boolean = inclusionC(kValues)
    return included || onBorder(polygon, point)
    //return inclusionC(inclusionB(inclusionA(polygon++(polygon.head::Nil), point)++(inclusionA((polygon++(polygon.head::Nil)), point).head)::Nil)) //Chaos
  }
  def inclusionA(polygon: List[OccupyPoint], point: OccupyPoint): List[Int] = {
    polygon match{
      case a::b::tail => return if(inclusionTriangle(List(OccupyPoint(0,0),a,b), point) >= 0) orientation(List(OccupyPoint(0,0),a,b))::inclusionA(b::tail, point) else return 0::inclusionA(b::tail, point)
      case _ => return Nil
    }
  }
  def inclusionB(inclusionData: List[Int]): List[Int] = {
    inclusionData match{
      case a::b::tail => if(a == b) return 0::inclusionB(b::tail) else return a::inclusionB(b::tail)
      case _ => return Nil
    }
  }
  def inclusionC(differenceData: List[Int]): Boolean = {
    return differenceData.sum > 0
  }
  def inclusionTriangle(triangle: List[OccupyPoint], testPoint: OccupyPoint): Int = {
    val positiveTriangle: List[OccupyPoint] = enforceCCW(triangle.take(3))
    return inclusionTriangle(positiveTriangle.apply(0), positiveTriangle.apply(1), positiveTriangle.apply(2), testPoint)
  }
  def inclusionTriangle(triangleA: OccupyPoint, triangleB: OccupyPoint, triangleC: OccupyPoint, testPoint: OccupyPoint): Int = {
    //return cross(triangleA, triangleB, testPoint) >= 0 && cross(triangleB, triangleC, testPoint) >= 0 && cross(triangleC, triangleA, testPoint) >= 0
    val ab : Int = cross(triangleA, triangleB, testPoint)
    val bc : Int = cross(triangleB, triangleC, testPoint)
    val ca : Int = cross(triangleC, triangleA, testPoint)
    if(ab < 0 || bc < 0 || ca < 0) return -1 else if(ab == 0 || bc == 0 || ca == 0) return 0 else return 1
  }
  
  //This is hard to test, not tested yet.
  def onBorder(points: List[OccupyPoint], testPoint: OccupyPoint): Boolean = {
    return borderPoints(points).contains(testPoint)
  }
  def borderPoints(points: List[OccupyPoint]) : List[OccupyPoint] = {
    return borderPointsRecursive(points ++ (points.head::Nil))
  }
  //This seems to work. Tested in KinectScanDataTesterA.scala
  def borderPointsRecursive(points: List[OccupyPoint]) : List[OccupyPoint] = {
    points match{
      case a::b::tail => return Bresenham.BresenhamAlgorithm(a, b) ++ borderPointsRecursive(b::tail)
      case _ => return Nil
    }
  }
  
  def pathLength(points: List[OccupyPoint]): Double = {
    points match{
      case a::b::tail => return distance(a,b) + pathLength(b::tail)
      case _ => return 0.0
    }
  }
  
  def polygonOverlap(a: List[OccupyPoint], b: List[OccupyPoint]): Boolean = {
    val aInB: Boolean = !a.forall { x => !inclusion(b, x) }
    val bInA: Boolean = !b.forall { x => !inclusion(a, x) }
    return aInB || bInA
  }
  
  //Got inspiration from http://stackoverflow.com/questions/17999409/scala-equivalent-of-javas-number
  def bound[T](i: T, lowerBound: T, upperBound: T)(implicit n: Numeric[T]): T = {
    import n._
    val upper: T = if(i > upperBound) upperBound else i
    val lower: T = if(upper < lowerBound) lowerBound else upper
    return lower
  }
//  def bound[T: Numeric](i: T, lowerBound: T, upperBound: T): T = {
//    val upper: T = if(i > upperBound) upperBound else i
//    return Math.max(Math.min(i, upperBound), lowerBound)
//  }
//  def bound(i: Int, lowerBound: Int, upperBound: Int): Int = {
//    return Math.max(Math.min(i, upperBound), lowerBound)
//  }
//  def bound(i: Double, lowerBound: Double, upperBound: Double): Double = {
//    return Math.max(Math.min(i, upperBound), lowerBound)
//  }
}