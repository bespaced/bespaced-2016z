package template

import BeSpaceDCore._

/*
 * Generates a graph where every geometrical position is a node and it is connected in 4 directions.
 */

object FirstGraphGenerator extends CoreDefinitions {
  val limits : (Int, Int) = (100,100)
  val graph : FirstGraph[OccupyPoint] = generation(limits)
  
  def generation(limits: (Int, Int)): FirstGraph[OccupyPoint] = {
    var tempGraph: FirstGraph[OccupyPoint] = new FirstGraph[OccupyPoint]
    (1 until limits._1).foreach{
    x => (1 until limits._2).foreach{
      y =>
        tempGraph.addEdge(OccupyPoint(x-1,y), OccupyPoint(x,y), false, 1, (x-1) + "," + y + " to " + x + "," + y)
//        graph.addEdge(OccupyPoint(x+1,y), OccupyPoint(x,y), false, 1, (x+1) + "," + y + " to " + x + "," + y)
        tempGraph.addEdge(OccupyPoint(x,y-1), OccupyPoint(x,y), false, 1, x + "," + (y-1) + " to " + x + "," + y)
//        graph.addEdge(OccupyPoint(x,y+1), OccupyPoint(x,y), false, 1, x + "," + (y+1) + " to " + x + "," + y)
    }
  }
    return tempGraph
  }
  
}