package template

import BeSpaceDCore._
import scala.collection.mutable.PriorityQueue

//The main method is a test method, creating its own parameters, with output to console and visualisation.
//To call from another class, the method Backtracking should be used.

object FirstPathBacktracking extends CoreDefinitions with PointVis{
  var path: List[OccupyPoint] = Nil
  var graph: FirstGraph[OccupyPoint] = new FirstGraph()
  var scene : (OccupyPoint, OccupyPoint, List[OccupyBox]) = null//(OccupyPoint(1,3), OccupyPoint(5,3), OccupyBox(2,2,4,4)::Nil)
  var obstacles: List[Invariant] = null//scene._3
  def main(args: Array[String]): Unit = {
    println("Started graph generation at " + java.util.Calendar.getInstance().getTime())
    graph = FirstGraphGenerator.graph
    println("Graph generated at " + java.util.Calendar.getInstance().getTime())
    scene = new FirstSceneGenerator().Generator()
    println("Scene generated at " + java.util.Calendar.getInstance().getTime())
    obstacles = scene._3
//    obstacles = scene._3
    println("Obstacles: " + obstacles.toString())
    println("Start: " + scene._1.toString())
    println("Target: " + scene._2.toString())
    println(backtrackingOnPaths(scene._2, scene._1 :: Nil).toString())
    println("Finished backtracking at " + java.util.Calendar.getInstance().getTime())
    path.foreach { x => println(x.toString()) }
    
    Vis(scene._1::Nil, scalafx.scene.paint.Color.Red)
    Vis(scene._2::Nil, scalafx.scene.paint.Color.Red)
    Vis(path, scalafx.scene.paint.Color.Green)
    
    obstacles.foreach { x => Vis(x, scalafx.scene.paint.Color.Blue) }
    Draw()
    println("Finished drawing at " + java.util.Calendar.getInstance().getTime())
  }
  
  def Backtracking(start : OccupyPoint, target : OccupyPoint, obstacles : List[OccupyBox]) : List[OccupyPoint] = {
    graph = FirstGraphGenerator.graph
    path = Nil
    backtrackingOnPaths(target, start::Nil)
    return path
  }

  def collision(point: OccupyPoint, obstacles: List[Invariant]): Boolean = {
//    return !(obstacles.forall { x => x match{
//      case OccupyBox(x1,y1,x2,y2) => !(x1 <= point.x && x2 >= point.x && y1 <= point.y && y2 >= point.y)
//      case _ => false
//    }
//    })
//    obstacles.head match{
//      case OccupyBox(x1,y1,x2,y2) => return (x1 <= point.x && x2 >= point.x && y1 <= point.y && y2 >= point.y)
//      case _ => return true
//    }
    return collisionTestsBig(obstacles.map(x => unfoldInvariant(x)), unfoldInvariant(point) :: Nil)
  }

  def backtrackingOnPaths(target: OccupyPoint, decisions: List[OccupyPoint]): Boolean = {
    if (decisions.last == target) {
      path = decisions
      return true
    } else {
      //      case class Point(point : OccupyPoint) extends Ordered[Point]{
      //        def compare(that: Point) = new FirstAStar().distance(this.point, target) compare new FirstAStar().distance(that.point, target)
      //      }
      val heuristicList: List[OccupyPoint] = graph.successors(decisions.last).sortWith(new FirstAStar().distance(_, target) < new FirstAStar().distance(_, target))
      var i: Int = 0
      while (i < heuristicList.size) {
        val nextChoice: OccupyPoint = heuristicList.apply(i)
        i = i + 1//That's an important point...
        if (!(collision(nextChoice, obstacles)) && !(decisions.contains(nextChoice))) {
          if (backtrackingOnPaths(target, decisions ++ (nextChoice::Nil))) {
            return true
          }
        }
      }
      return false
    }
    return true
  }
}