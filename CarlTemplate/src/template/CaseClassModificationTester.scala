package template

import scala.collection.mutable.HashMap
import scala.collection.mutable.LinkedList
import javax.activation.DataSource
import BeSpaceDCore._

object CaseClassModificationTester {
  def main(args: Array[String]): Unit = {
    val myMap: HashMap[test, test] = new HashMap[test, test]
    val myStorage: dataStorageTest = new dataStorageTest
    val myTest: test2 = test2(1,2,myStorage)
    val myTest2: test2 = test2(9,8,myStorage)
    println(myTest.data.getML)
    myStorage.setML(List(myTest, myTest2))
    println(myTest.data.getML)
    val testCase: test = test(1,2,myMap)
    println(testCase.map)
    val testCase2: test = test(3,4,myMap)
    myMap.+=(testCase -> testCase2)
    println("Before trying to retrieve map")
//    println(testCase.map)
    println("After retrieving map")
//    myMap.+=(testCase2 -> testCase)
    println("After second insertion")
    
//    println(testCase.map)
    val newMap: HashMap[(Int, Int), (Double, Int)] = new HashMap[(Int, Int), (Double, Int)]
    newMap.+=((55, 104) -> (1.0, 1))
    newMap.+=((56, 105) -> (1.0, 1))
    newMap.+=((57, 106) -> (1.0, 1))
    newMap.+=((55, 107) -> (1.0, 1))
    val unionFind: DisjointSet[SecondQuadTree] = new DisjointSet[SecondQuadTree]
    val upLink: parentStorage = new parentStorage
    val downLink: branchesStorage = new branchesStorage
    def heuristicForSubdivision(a: SecondQuadTree, b: SecondQuadTree): Double = PointArithmetic.distance(a.center, OccupyPoint(1007, 1007)) - PointArithmetic.distance(b.center, OccupyPoint(1007, 1007))
//    val parent: SecondQuadTree = SecondQuadTree(newMap, 0, 64, 64, 0.001, 0.6, upLink, unionFind, 8, OccupyPoint(0, 0), OccupyPoint(1007, 1007), heuristicForSubdivision, downLink)
//    val children: List[SecondQuadTree] = parent.subdivision.branches match {
//      case None                        => ??? //This should not happen, since the node has just been subdivided.
//      case Some(treeLayer(a, b, c, d)) =>
//        println(treeLayer(a, b, c, d)); List(a, b, c, d)
//      case _                           => ??? //This should not happen either.
//    }
//    children.foreach { x => if (x.density <= x.empty) { println(x); unionFind.add(x); x.union } }
  }
}

case class test(a: Int, b: Int, map: HashMap[test, test]){
  private var modifiableList: List[Int] = Nil
  def setML(in: List[Int]) = {
    modifiableList = in
  }
  def getML = modifiableList
}
case class test2(a: Int, b: Int, data: dataStorageTest)

class dataStorageTest {
  private var modifiableList: List[test2] = Nil
  def setML(in: List[test2]) = {
    modifiableList = in
  }
  def getML = modifiableList
}