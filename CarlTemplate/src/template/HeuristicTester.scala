package template

import BeSpaceDCore._
import scala.collection.mutable.HashMap
import java.io._
import scala.collection.mutable.PriorityQueue

case class heuristicTest(a: Int) extends Ordered[heuristicTest] {
  def compare(that: heuristicTest): Int = a - that.a
}
case class heuristicTest2(a: Int, ordering: (heuristicTest2, heuristicTest2) => Int) extends Ordered[heuristicTest2] {
  def compare(that: heuristicTest2): Int = ordering(this, that)
  def ==(that: heuristicTest2): Boolean = {
    return this.a == that.a
  }
}

object HeuristicTester {
  var globalPriority = 1
  def testOrdering(a: heuristicTest2, b: heuristicTest2): Int = {
    val a1: Int = a.a * globalPriority
    val b1: Int = b.a * globalPriority
    val result: Int = a1 - b1
    return -result
  }
  def testOrdering2(a: SecondQuadTree, b: SecondQuadTree): Int = {
    val a1: Int = a.size * globalPriority
    val b1: Int = b.size * globalPriority
    val result: Int = a1 - b1
    return -result
  }
  def main(args: Array[String]): Unit = {
    val pq2: BinaryMinHeap[heuristicTest2] = new BinaryMinHeap[heuristicTest2](testOrdering)
    pq2.+=(heuristicTest2(1, testOrdering))
    pq2.+=(heuristicTest2(2, testOrdering))
    pq2.+=(heuristicTest2(3, testOrdering))
    //    pq2.+=(heuristicTest2(4, testOrdering))
    pq2.+=(heuristicTest2(4, testOrdering))
    pq2.+=(heuristicTest2(0, testOrdering))
    println(heuristicTest2(4, testOrdering) == heuristicTest2(4, testOrdering))
    println(heuristicTest2(4, testOrdering).ordering == heuristicTest2(4, testOrdering).ordering)
    val test = heuristicTest2(4, testOrdering)
    println(test.ordering == test.ordering)
    println(testOrdering _ == testOrdering _)
    
    //    println(pq2)
    //    globalPriority = -1
    //    println(pq2)
    //    println(pq2.dequeue() + "" + pq2)
    val pq3: BinaryMinHeap[heuristicTest2] = new BinaryMinHeap[heuristicTest2](testOrdering)
    while (!pq2.isEmpty) {
      val head: heuristicTest2 = pq2.dequeue
      pq3.+=(head)
      println(head)
    }
    println("---")
    globalPriority = -1
    println(pq3.peak)
    println(pq3.find(heuristicTest2(4, testOrdering)))
    pq3.reevaluate(1)
    println(pq3.peak)
    println("---")
    //    val head = pq3.dequeue()
    //    println(head)
    //    pq3.+=(head)
    val timeBeforeResorting = System.currentTimeMillis()
    //    val pq4 = pq3.dequeueAll
    val pq5: BinaryMinHeap[heuristicTest2] = new BinaryMinHeap[heuristicTest2](testOrdering)
    val timeAfterResorting = System.currentTimeMillis()
    //    println(pq4)
    //    pq4.foreach { x => pq5.+=(x) }
    while (!pq3.isEmpty) {
      println(pq3.dequeue)
    }
    println(timeAfterResorting - timeBeforeResorting)
    while (!pq5.isEmpty) {
      println(pq5.dequeue)
    }

    //Test with SecondQuadTree
    println("Test with SecondQuadTree")
    globalPriority = 1
    val pqS1 = new BinaryMinHeap[SecondQuadTree](testOrdering2)
    val emptyMap = new HashMap[(Int, Int), (Double, Int)]
    val a = SecondQuadTree(emptyMap, 0, 0, 9, 0, 1, testOrdering2)
    val b = SecondQuadTree(emptyMap, 20, 20, 10, 0, 1, testOrdering2)
    val c = SecondQuadTree(emptyMap, 40, 40, 11, 0, 1, testOrdering2)
    val d = SecondQuadTree(emptyMap, 60, 60, 12, 0, 1, testOrdering2)
    val e = SecondQuadTree(emptyMap, 80, 80, 13, 0, 1, testOrdering2)
    println(a == a)
    println(a.ordering == a.ordering)
    
    pqS1.+=(a)
    pqS1.+=(b)
    pqS1.+=(c)
    pqS1.+=(d)
    pqS1.+=(e)
    val pqS2 = new BinaryMinHeap[SecondQuadTree](testOrdering2)
    while(!pqS1.isEmpty){
      val cache = pqS1.dequeue
      println(cache)
      pqS2.+=(cache)
    }
    globalPriority = -1
    println(pqS2.find(e))
    pqS2.reevaluateElement(e)
//    pqS2.refresh
    while(!pqS2.isEmpty){
      println(pqS2.dequeue)
    }

    
    /*val pq: PriorityQueue[heuristicTest] = new PriorityQueue[heuristicTest]
    pq.+=(heuristicTest(1))
    pq.+=(heuristicTest(4))
    pq.+=(heuristicTest(3))
    val sorted = pq.toList.sorted
    println(sorted)
    while (!pq.isEmpty) {
      println(pq.dequeue())
    }
    val pw = new PrintWriter(new File(test + ".csv"))
    pw.write("Measurements:" + '\n')
    pw.write(0.002 + "")
//    pw.write("Parameters:" + '\n')
//    pw.write(parametersCSV(Configuration(dataSetTitle, points, resolution, magnificationFactor, borders, empty, full, minSize, start, target, zPlane)))
    pw.close*/
    //    pq.foreach { x => println(x) } //That's not the same as above!
  }

}