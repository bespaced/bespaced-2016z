package template
import scala.collection.mutable.PriorityQueue
import BeSpaceDCore._
import scala.collection.mutable.HashMap
import scala.math._
//import CoreDefinitions._

case class TreePoint(prio : Double, payload : SecondQuadTree) extends Ordered[TreePoint]{
  def compare(that: TreePoint)= (that.prio compare this.prio)
}

class SecondAStar {
  def Search(graph : FirstGraph[SecondQuadTree], start : SecondQuadTree, target : SecondQuadTree) : (List[SecondQuadTree], Double) = AStar(graph, start, target)
  var heuristic : HashMap[SecondQuadTree, Double] = new HashMap[SecondQuadTree, Double]()
  var openList : PriorityQueue[TreePoint] = new PriorityQueue[TreePoint]()
  var closedList : List[SecondQuadTree] = Nil
  var predecessor : HashMap[SecondQuadTree, SecondQuadTree] = new HashMap[SecondQuadTree, SecondQuadTree]()
  var pathValue : HashMap[SecondQuadTree, Double] = new HashMap[SecondQuadTree, Double]()
//  var expectedValue : HashMap[SecondQuadTree, Double] = new HashMap[SecondQuadTree, Double]()
  
  def AStar(g : FirstGraph[SecondQuadTree], start : SecondQuadTree, end : SecondQuadTree) : (List[SecondQuadTree], Double) = {
    //Fill heuristic and initialize pathValue and expectedValue with positive (pseudo) infinity
    g.getNodes().foreach { x => heuristic.+=( (x -> distance(x, end))); pathValue.+=((x -> Int.MaxValue)) }
    //The following line was missing until 5.10.2016, 18:05 - inserted independently in FirstAStar and SecondAStar
    pathValue.+=(start -> 0)
    
    openList.+=(TreePoint(0.0, start))
    
    while(!(openList.isEmpty)){
      val currentNode : TreePoint = openList.dequeue()
      if(currentNode.payload == end){
        val myPath : List[SecondQuadTree] = path(end)
        val length : Double = pathLength(g, myPath)
        return (myPath, length)
      }
      closedList = currentNode.payload :: closedList
      
      //Expand Node
      val successors = g.successors(currentNode.payload)
      successors.foreach { x => if(!(closedList.contains(x))){
        val currentValue = pathValue.get(currentNode.payload)
        var tentative_g : Double = 0.0
        currentValue match{
          case Some(d) => tentative_g = d + g.weight(currentNode.payload, x)
          case None => throw new NoSuchElementException
        }
//        val tentative_g : Double = pathValue.get(currentNode.payload) + g.weight(currentNode.payload, x)
        val oldValue = pathValue.get(x)
        var old_g : Double = 0.0
        oldValue match{
          case Some(d) => old_g = d
          case None => throw new NoSuchElementException
        }
        if(!(openListContains(openList, x) && tentative_g >= old_g)){
          predecessor.+=((x -> currentNode.payload))
          pathValue.+=((x -> tentative_g))
          
          val heuristicValue = heuristic.get(x)
          var expectation : Double = 0.0
          heuristicValue match{
            case Some(d) => expectation = tentative_g + d
            case None => throw new NoSuchElementException
          }
          if(openListContains(openList, x)){
            openList.map { elem => if(elem.payload == x) TreePoint(expectation, x) else elem }//Test this!!!
//            val index = openList.findIndexOf { elem => elem.payload == x }
//            openList.update(index, Point(expectation, x))
          }
          else{
            openList.enqueue(TreePoint(expectation, x))
          }
        }
      }
      }
    }
    (Nil, 0.0)//No Path Found
  }
  def openListContains(l : PriorityQueue[TreePoint], o : SecondQuadTree) : Boolean = {
    if(l.isEmpty) return false
    l.head match {
//      case TreePoint(d,SecondQuadTree(o.x, o.y)) => return true//Does this catch all cases? It shouldn't.
      case TreePoint(d,p) => return if(p == o) true else openListContains(l.tail, o)//Is this still unreachable? Does l.tail work as l without the head element?
    }
  }
  def distance(point1 : SecondQuadTree, point2 : SecondQuadTree):Double = {
    val distanceX = abs(point1.center.x - point2.center.x)
    val distanceY = abs(point1.center.y - point2.center.y)
    return sqrt(pow(distanceX,2) + pow(abs(point1.center.y - point2.center.y),2)).toDouble
  }
//  def path(end : SecondQuadTree, length : Double) : (List[SecondQuadTree], Double) = {
//    predecessor.get(end) match{
//      case Some(d) => return (end :: path(d, length + distance(end, d))._1, path(d, length + distance(end, d))._2)
//      case None => return (end :: Nil, length)
//    }
//  }
  def path(end : SecondQuadTree) : List[SecondQuadTree] = {
	  predecessor.get(end) match{
	  case Some(d) => return end :: path(d)
	  case None => return end :: Nil
	  }
  }
  def pathLength (g : FirstGraph[SecondQuadTree], l : List[SecondQuadTree]) : Double = {
	  l match{
	  case Nil => return 0.0
	  case head :: Nil => return 0.0
	  case head1 :: head2 :: tail => return g.weight(head1, head2) + pathLength(g, head2 :: tail)
	  }
  }  
}