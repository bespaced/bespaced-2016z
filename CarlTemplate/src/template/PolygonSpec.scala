package template

import BeSpaceDCore._

class PolygonSpec extends UnitSpec{
  "vis" should "return a list of all points in the polygon" in{
    val test : Polygon = new Polygon(List(OccupyPoint(2,1), OccupyPoint(2,4), OccupyPoint(5,4), OccupyPoint(5,1)))
    def lt(a : OccupyPoint, b : OccupyPoint) : Boolean = if(a.x == b.x) return a.y - b.y < 0 else return a.x - b.x < 0
    val howItShouldBe : List[OccupyPoint] = List(OccupyPoint(2,1), OccupyPoint(3,1), OccupyPoint(4,1), OccupyPoint(5,1), OccupyPoint(2,2), OccupyPoint(3,2), OccupyPoint(4,2), OccupyPoint(5,2), OccupyPoint(2,3), OccupyPoint(3,3), OccupyPoint(4,3), OccupyPoint(5,3), OccupyPoint(2,4), OccupyPoint(3,4), OccupyPoint(4,4), OccupyPoint(5,4)).sortWith(lt)
//    val obstaclePoints: List[OccupyPoint] = simplifyInvariant(unfoldInvariant(BIGAND(myScene._3))) match {
//      case BIGAND(x: List[OccupyPoint]) => x
//      case _                            => Nil
//    }
    val result : List[OccupyPoint] = test.vis().sortWith(lt)
    assertResult(expected = howItShouldBe)(actual = result)
  }
  "collision" should "return true for points on all edges of the polygon" in{
    val test : Polygon = new Polygon(List(OccupyPoint(2,1), OccupyPoint(2,4), OccupyPoint(5,4), OccupyPoint(5,1)))
    val result : Boolean = test.collision(OccupyPoint(5,1))
    assertResult(expected = true)(actual = result)
  }
  "centroid" should "return 0,0 for -2,-2,2,2" in {
    val poly : AbstractPolygon = new Polygon(List(OccupyPoint(-2,-2), OccupyPoint(2,-2), OccupyPoint(2,2), OccupyPoint(-2,2)))
    val result : OccupyPoint = poly.centroid()
    assertResult(expected = OccupyPoint(0,0))(actual = result)
  }
  it should "return 0,0 for a some non-regular polygon with center 0,0" in{
    val poly : AbstractPolygon = new Polygon(List(OccupyPoint(-2,-2), OccupyPoint(0,-3), OccupyPoint(2,-2), OccupyPoint(2,2), OccupyPoint(0,3), OccupyPoint(-2,2)))
    val result : OccupyPoint = poly.centroid()
    assertResult(expected = OccupyPoint(0,0))(actual = result)
  }
  it should "not return 0,0 for some non-regular polygon with center somewhere else" in{
    val poly : AbstractPolygon = new Polygon(List(OccupyPoint(-2,-2), OccupyPoint(1,-3), OccupyPoint(2,-2), OccupyPoint(2,2), OccupyPoint(0,3), OccupyPoint(-2,2)))
    val result : OccupyPoint = poly.centroid()
    assertResult(expected = OccupyPoint(0,0))(actual = result)
  }
}