package template
import scala.math._
import BeSpaceDCore._
import scala.collection._

object Template {
  def main (args: Array[String]):Unit = {
    val scene : MyScene = new RandomSceneGenerator()
    var paths : List[Path] = Nil
    var clustering : Clustering = new FirstClustering()
    clustering.Generator(scene)
    var numPaths : Int = 5
    while(numPaths > 0){
      val abstraction : Abstraction = new FirstAbstraction()
      val simplified = abstraction.abstractClusters(clustering)
      val graph = abstraction.GenerateGraph(simplified)
      val search = new FirstAStar()
      val path = new FirstPath()
      val result = search.Search(graph, scene.start, scene.target)
      path.read(result._1, result._2)
      paths = path :: paths
      numPaths -= 1
      clustering = clustering.Refine(clustering)
    }
    val minPath : Path = paths.sortWith(_.lengthAbs() < _.lengthAbs()).head
    println(minPath.toString())
  }
  def recursiveRefinement (scene: MyScene, clustering : Clustering) : Clustering = {
    ???
  }
}
trait MyScene {
  //Generates a scene, i.e. robot position, target, obstacles
  val (start, target, obstacles) = Generator()//Is that ok? How often is this thing called?
  
  def Generator ():(OccupyPoint, OccupyPoint, List[Invariant])
}
trait Abstraction {
  def abstractClusters(clustering : Clustering) : Clustering
  def abstractCluster (cluster : oldCluster) : oldCluster
  def GenerateGraph(clustering : Clustering) : FirstGraph[OccupyPoint]
  
}
trait Clustering {
  def Generator (scene : MyScene) : Clustering
  def Refine (clustering : Clustering) : Clustering
  var data : List[oldCluster]
  def getData() : List[oldCluster] = {
    return data
  }
  def setData(clusters : List[oldCluster]) = {
    data = clusters
  }
}
class oldCluster {
  var data : List[Invariant] = null
  def setData (l : List[Invariant]) : Unit = {
    data = l
  }
  def getData () : List[Invariant] = {
    return data
  }
  def addInvariant(inv : Invariant) : List[Invariant] = {
    data = inv :: data
    return data
  }
}
abstract class Graph {
  
}
abstract class Search {
  def Search(graph : FirstGraph[OccupyPoint], start : OccupyPoint, target : OccupyPoint) : (List[OccupyPoint], Double)
}
trait Path {
  def collisionTest(inv : Invariant) : OccupyPoint
  def pred(inv : Invariant) : Option[Invariant]
  def succ(inv : Invariant) : Option[Invariant]
  def at(index : Int) : Option[Invariant]
  def lengthNodes() : Int
  def lengthAbs() : Double
  def read(path : List[Invariant], length : Double) : Unit
}