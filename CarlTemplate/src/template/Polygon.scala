package template

import BeSpaceDCore._

/**
 * @author Carl
 *
 * ConvexPolygon should inherit from this, but I don't know how I can enforce that.
 */
case class Polygon(hull : List[OccupyPoint]) extends CoreDefinitions with AbstractPolygon{
  
  def getCornerPoints(): List[OccupyPoint] = PointArithmetic.enforceCCW(hull)
  
  def convexHull(): Polygon = {
    return new Polygon(new ConvexHull().convexHull(getCornerPoints()))
  }
  
  def collision(point : OccupyPoint) : Boolean = {
    return PointArithmetic.inclusion(getCornerPoints(), point)
  }
  
  def vis() : List[OccupyPoint] = {
    val maxX : Int = getCornerPoints().maxBy { x => x.x }.x
    val minX : Int = getCornerPoints().minBy { x => x.x }.x
    val maxY : Int = getCornerPoints().maxBy { x => x.y }.y
    val minY : Int = getCornerPoints().minBy { x => x.y }.y
    var list : List[OccupyPoint] = Nil
    (minX to maxX).foreach{
      x => (minY to maxY).foreach{
        y => list = if(collision(OccupyPoint(x,y))) OccupyPoint(x,y) :: list else list
      }
    }
    return list
  }
  
  override def toString() : String = {
    getCornerPoints().toString()
  }
  
  //How to make sure this function is applied when calling == ?
  def equivalencePoly(that: Polygon): Boolean = {
//    that match{
//      case x: Polygon => return (this.getCornerPoints() ++ this.getCornerPoints()).containsSlice(x.getCornerPoints()) && (x.getCornerPoints() ++ x.getCornerPoints()).containsSlice(this.getCornerPoints())
//      case _ => return this.vis() == that.vis() //This might actually work for both cases, but the above one is probably faster. Or it might be equal due to compiler optimisation. Anyway.
//    }
    println("Uses my implementation")
    return (this.getCornerPoints() ++ this.getCornerPoints()).containsSlice(that.getCornerPoints()) && (that.getCornerPoints() ++ that.getCornerPoints()).containsSlice(this.getCornerPoints())
  }
  def equivalenceInv(that: InvariantObstacle): Boolean = {
    return this.vis() == that.vis()
  }
//  //How to make sure this function is applied when calling == ?
//  def equals(that: ConvexPolygon): Boolean = {
//    return (this.getCornerPoints() ++ this.getCornerPoints()).containsSlice(that.getCornerPoints()) && (that.getCornerPoints() ++ that.getCornerPoints()).containsSlice(this.getCornerPoints())
//  }
  
  def overlapInv(that: InvariantObstacle): Boolean = {
    return collisionTestsBig(unfoldInvariant(that.parameter) :: Nil, unfoldInvariant(BIGAND(getCornerPoints())) :: Nil)
  }
  def overlapPoly(that: Polygon): Boolean = {
    return PointArithmetic.polygonOverlap(this.getCornerPoints(), that.getCornerPoints())
  }
  def getBorder: List[OccupyPoint] = PointArithmetic.borderPoints(getCornerPoints())
}