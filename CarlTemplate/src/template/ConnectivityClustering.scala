package template

import BeSpaceDCore._
import PointArithmetic._

/**
 * @author Carl
 *
 */
class ConnectivityClustering {
  def findConnectedObstacles(polygons: List[AbstractPolygon]): List[List[AbstractPolygon]] = {
    if (polygons == Nil) {
      return Nil
    } else {
//      val current: List[AbstractPolygon] = polygons.filter { x => x.overlap(polygons.head) }// ++ (polygons.head::Nil)
//      val other: List[AbstractPolygon] = polygons.filterNot { x => current.contains(x) }
      val current: List[AbstractPolygon] = polygons.head::Nil
      val other: List[AbstractPolygon] = polygons.tail
      val currentCluster: List[AbstractPolygon] = findConnectedObstaclesRecursive(current, other)
      val rest: List[AbstractPolygon] = polygons.filterNot { x => currentCluster.contains(x) }
      return (currentCluster :: findConnectedObstacles(rest)).distinct
    }
  }
  def findConnectedObstaclesRecursive(current: List[AbstractPolygon], other: List[AbstractPolygon]): List[AbstractPolygon] = {
    current match{
      case head::tail => return (current ++ other.filter { x => head.overlap(x) } ++ findConnectedObstaclesRecursive(tail ++ other.filter { x => head.overlap(x) }, other.filterNot { x => head.overlap(x) })).distinct
      case _ => return Nil
    }
    //Using distinct might not be the most computationally efficient way of achieving it, but it works.
  }
}