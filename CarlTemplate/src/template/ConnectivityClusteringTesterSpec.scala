package template

import BeSpaceDCore._

class ConnectivityClusteringTesterSpec extends UnitSpec{
  "convexHull" should "return something for List(OccupyBox(29,69,30,70), OccupyBox(19,68,34,77), OccupyBox(29,75,30,75))" in{
    val cluster: List[AbstractPolygon] = List(OccupyBox(29,69,30,70), OccupyBox(19,68,34,77), OccupyBox(29,75,30,75)).map { x => new InvariantObstacle(x) }
    val result: AbstractPolygon = ConnectivityClusteringTester.convexHull(cluster)
    val howItShouldBe: AbstractPolygon = new Polygon(List(OccupyPoint(19,68),OccupyPoint(34,68),OccupyPoint(34,77),OccupyPoint(19,77)))
    
    assertResult(expected = true)(actual = result.equivalence(howItShouldBe))
  }
  it should "return something else for List(OccupyBox(29,69,30,70), OccupyBox(19,68,34,77), OccupyBox(29,75,30,75))" in{
    val cluster: List[AbstractPolygon] = List(OccupyBox(29,69,30,70), OccupyBox(19,68,34,77), OccupyBox(29,75,30,75)).map { x => new InvariantObstacle(x) }
    val result: AbstractPolygon = ConnectivityClusteringTester.convexHull(cluster)
    val howItShouldBe: AbstractPolygon = new Polygon(List(OccupyPoint(19,68),OccupyPoint(34,68),OccupyPoint(34,77),OccupyPoint(19,77), OccupyPoint(20,73)))
    
    assertResult(expected = false)(actual = result.equivalence(howItShouldBe))
  }
  it should "return something for List(OccupyBox(71,79,76,79))" in {
    val cluster: List[AbstractPolygon] = List(OccupyBox(71,79,76,79)).map { x => new InvariantObstacle(x) }
    val result: AbstractPolygon = ConnectivityClusteringTester.convexHull(cluster)
    val howItShouldBe: AbstractPolygon = new Polygon(List(OccupyPoint(71,79), OccupyPoint(76,79)))
    println(result.vis())
    assertResult(expected = true)(actual = result.equivalence(howItShouldBe))
  }
  it should "return something for List(OccupyBox(53,60,53,61))" in {
    val cluster: List[AbstractPolygon] = List(OccupyBox(53,60,53,61)).map { x => new InvariantObstacle(x) }
    val result: AbstractPolygon = ConnectivityClusteringTester.convexHull(cluster)
    val howItShouldBe: AbstractPolygon = new Polygon(List(OccupyPoint(53,60), OccupyPoint(53,61)))
    println("how it should be: " + howItShouldBe.vis())
//    println("actual: " + result.vis())
    assertResult(expected = true)(actual = result.equivalence(howItShouldBe))
  }
}