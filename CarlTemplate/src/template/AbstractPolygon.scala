package template

import BeSpaceDCore._

//This trait represents an abstract supertype of things that can be used as obstacles in the pathfinding algorithms. Therefore, it has a collision detection method.
//It should also support visualisation by returning the points that should represent it when visualised.

trait AbstractPolygon extends CoreDefinitions{
  
  def convexHull(): Polygon
  
  def collision(point : OccupyPoint) : Boolean
  
  def vis() : List[OccupyPoint]
  
  def centroid() : OccupyPoint = {
    return OccupyPoint(vis().map { x => x.x }.sum / vis().length,vis().map { x => x.y }.sum/vis().length)
  }
  
  def equivalence(that: AbstractPolygon): Boolean = {
    that match{
      case p: Polygon => return equivalencePoly(p)
      case i: InvariantObstacle => return equivalenceInv(i)
    }
  }
  
  def equivalenceInv(that: InvariantObstacle): Boolean
  
  def equivalencePoly(that: Polygon): Boolean
  
  def overlap(that: AbstractPolygon): Boolean = {
    that match{
      case p: Polygon => return overlapPoly(p)
      case i: InvariantObstacle => return overlapInv(i)
    }
  }
  
  def overlapInv(that: InvariantObstacle): Boolean
  
  def overlapPoly(that: Polygon): Boolean
  
  def getBorder: List[OccupyPoint]
}