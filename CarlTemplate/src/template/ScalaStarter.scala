package template

import java.util.Scanner

object ScalaStarter {
  def run(): Unit = {
    val timerOK: Boolean = Timer.timerOK
    println("Timer CPU time support " + timerOK)
    println("Do you want to output images? Type true or false.")
    val sc: Scanner = new Scanner(System.in)
    val imageOutput: Boolean = sc.nextBoolean()
    imageOutput match{
      case true => AutonomousVisualisation.main(null)
      case false => OutputCSV
    }
  }
}