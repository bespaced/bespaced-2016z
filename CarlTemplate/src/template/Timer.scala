package template

import java.lang.management.ManagementFactory

case class times(wallclock: Long, cpu: Long, user: Long, system: Long)

object Timer {
  def getCpuTime: Long = {
    val bean = ManagementFactory.getThreadMXBean
    val supported: Boolean = bean.isCurrentThreadCpuTimeSupported()
    supported match{
      case true => return bean.getCurrentThreadCpuTime / 1000000
      case false => return 0//System.currentTimeMillis()
    }
  }
  def getUserTime: Long = {
    val bean = ManagementFactory.getThreadMXBean
    val supported: Boolean = bean.isCurrentThreadCpuTimeSupported()
    supported match{
      case true => return bean.getCurrentThreadUserTime / 1000000
      case false => return 0//System.currentTimeMillis()
    }
  }
  def getSystemTime: Long = {
    val bean = ManagementFactory.getThreadMXBean
    val supported: Boolean = bean.isCurrentThreadCpuTimeSupported()
    supported match{
      case true => return bean.getCurrentThreadCpuTime - bean.getCurrentThreadUserTime / 1000000
      case false => return 0//System.currentTimeMillis()
    }
  }
  def timerOK: Boolean = {
    val bean = ManagementFactory.getThreadMXBean
    val supported: Boolean = bean.isCurrentThreadCpuTimeSupported()
    return supported
  }
  def allTimes: times = {
    val bean = ManagementFactory.getThreadMXBean
    val supported: Boolean = bean.isCurrentThreadCpuTimeSupported()
    supported match{
      case true => {
        val wallclock = System.currentTimeMillis()
        val cpu = bean.getCurrentThreadCpuTime / 1000000
        val user = bean.getCurrentThreadUserTime / 1000000
        val system = cpu - user
        return times(wallclock, cpu, user, system)
      }
      case false => return times(0, 0, 0, 0)//System.currentTimeMillis()
    }
  }
}