package template
import BeSpaceDCore._
import scala.util._

class FirstPath extends CoreDefinitions with Path {
  var path : List[Invariant] = Nil
  var length : Double = 0.0
  def at(index: Int): Option[BeSpaceDCore.Invariant] = {
//    val default = None
//    path.applyOrElse(Some(path.apply(index)), None)
    val l : Int = path.size
    if(index < 0 || index >= l){
      return None
    }
    return Some(path.apply(index))
  }
  def collisionTest(inv: BeSpaceDCore.Invariant): BeSpaceDCore.OccupyPoint = ???
  def lengthAbs(): Double = length
  def lengthNodes(): Int = path.size
  def pred(inv: BeSpaceDCore.Invariant): Option[BeSpaceDCore.Invariant] = {
    if(!(path.contains(inv)) || path.head == inv){
      return None
    }
    return Some(path.apply(path.indexOf(inv) - 1))
  }
  def read(path: List[BeSpaceDCore.Invariant],length: Double): Unit = {
    this.path = path
    this.length = length
  }
  def succ(inv: BeSpaceDCore.Invariant): Option[BeSpaceDCore.Invariant] = {
    if(!(path.contains(inv)) || path.last == inv){
      return None
    }
    return Some(path.apply(path.indexOf(inv) + 1))
  }

}