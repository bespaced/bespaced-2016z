package template

import BeSpaceDCore._
import java.io.PrintWriter
import BeSpaceDData._

object BacktrackingOnPathsTesterNew extends CoreDefinitions with PointVis{
  def main(args: Array[String]): Unit = {
    println("Start initializing at " + java.util.Calendar.getInstance().getTime())
    val backtracking = new ThirdPathBacktracking()
    val testStart: OccupyPoint = OccupyPoint(4,13)
    val testTarget: OccupyPoint = OccupyPoint(81,99)
    val testObstacles: List[OccupyBox] = List(OccupyBox(29,69,30,70), OccupyBox(36,63,37,70), 
        OccupyBox(79,43,80,44), OccupyBox(2,42,3,47), OccupyBox(20,54,21,55), OccupyBox(98,29,99,30), 
        OccupyBox(75,36,76,37), OccupyBox(13,39,14,40), OccupyBox(44,51,59,52), OccupyBox(33,26,34,30), 
        OccupyBox(12,34,13,35), OccupyBox(70,53,71,54), OccupyBox(19,68,34,77), OccupyBox(57,50,63,51), 
        OccupyBox(78,23,83,24), OccupyBox(81,33,85,40), OccupyBox(20,50,21,51), OccupyBox(50,65,51,66), 
        OccupyBox(46,70,52,71), OccupyBox(11,44,14,65), OccupyBox(20,36,21,36), OccupyBox(23,58,24,65), 
        OccupyBox(50,45,51,54), OccupyBox(10,31,11,32), OccupyBox(83,47,90,54), OccupyBox(19,20,20,27), 
        OccupyBox(57,50,63,51), OccupyBox(66,70,67,71), OccupyBox(46,70,47,71), OccupyBox(35,57,36,58), 
        OccupyBox(30,32,31,38), OccupyBox(61,48,66,49), OccupyBox(67,78,68,79), OccupyBox(25,39,26,40), 
        OccupyBox(22,56,29,57), OccupyBox(54,55,55,56), OccupyBox(39,40,40,41), OccupyBox(38,24,39,29), 
        OccupyBox(11,45,12,58), OccupyBox(32,63,33,64), OccupyBox(46,69,47,70), OccupyBox(57,46,58,47), 
        OccupyBox(56,21,60,22), OccupyBox(91,31,93,32), OccupyBox(44,60,45,71), OccupyBox(5,52,18,53), 
        OccupyBox(15,64,16,64), OccupyBox(12,51,15,52), OccupyBox(29,75,30,75), OccupyBox(28,54,29,55), 
        OccupyBox(99,21,99,41), OccupyBox(22,23,23,24), OccupyBox(77,63,78,64), OccupyBox(50,46,51,47), 
        OccupyBox(2,33,4,34), OccupyBox(53,60,53,61), OccupyBox(69,29,70,40), OccupyBox(49,23,50,30), 
        OccupyBox(18,48,19,51), OccupyBox(57,69,58,71), OccupyBox(50,69,51,70), OccupyBox(55,30,56,35), 
        OccupyBox(50,30,53,31), OccupyBox(6,26,7,26), OccupyBox(27,52,28,61), OccupyBox(50,68,57,69), 
        OccupyBox(31,42,37,43), OccupyBox(71,79,76,79), OccupyBox(31,25,32,33), OccupyBox(82,50,83,52), 
        OccupyBox(86,49,87,50), OccupyBox(15,28,16,51), OccupyBox(6,60,7,61), OccupyBox(58,27,75,40), 
        OccupyBox(39,54,40,55))
//    val clusters: List[List[AbstractPolygon]] = new ConnectivityClustering().findConnectedObstacles(testObstacles.map { x => new InvariantObstacle(x) })
//    val testObstacles: List[OccupyPoint] = testObstacles2.flatMap { x => simplifyInvariant(unfoldInvariant(x)) match {
//      case BIGAND(x : List[OccupyPoint]) => x
//      case AND(OccupyPoint(a,b),OccupyPoint(c,d)) => List(OccupyPoint(a,b),OccupyPoint(c,d))
//      case OccupyPoint(a,b) => List(OccupyPoint(a,b))
//      case _ => Nil }}
    //val testObstacles: List[OccupyPoint] = testObstacles2.flatMap { x => new InvariantObstacle(x).vis() }.distinct
    val myHAC = new HAC(testObstacles.map { x => new InvariantObstacle(x) })
    println("Start distanceInit at " + java.util.Calendar.getInstance.getTime)
    myHAC.distanceInitAllPoints(testObstacles.map { x => new InvariantObstacle(x) })
    println("Finished distanceInit at " + java.util.Calendar.getInstance.getTime)
    println(testObstacles.length)
    println(myHAC.distances.size)
    val clusters: List[List[AbstractPolygon]] = myHAC.hacClustering(5.0, Some(Single))
    println("Finished clustering at " + java.util.Calendar.getInstance.getTime)
    val hulls: List[AbstractPolygon] = clusters.map { x => new Polygon(new ConvexHull().convexHull(x.flatMap { x => x.vis() })) }
    var parameters : PathPlusParameters = backtracking.Backtracking(testStart, testTarget, hulls ,10000, 1000)
//    var parameters : PathPlusParameters = backtracking.Backtracking(10000,1000)
    println("Finished backtracking at " + java.util.Calendar.getInstance().getTime())
    
    println("Start:")
    println(parameters.start)
    println("Target:")
    println(parameters.target)
    println("Obstacles:")
    println(parameters.obstacles)
    var path : List[OccupyPoint] = parameters.path
    
    if(path == Nil) {
      println(false)
      println("Trying convex hull at " + java.util.Calendar.getInstance().getTime())
      var points : List[OccupyPoint] = parameters.obstacles.flatMap { x => x.vis() }
//      parameters.obstacles.foreach { x => points = x.vis() ++ points }
      //This can definitely be made more efficient.
      val hullObject : ConvexHull = new ConvexHull()
      val hull : AbstractPolygon = new Polygon(hullObject.convexHull(points))
//      val hull : AbstractPolygon = new Polygon(List(OccupyPoint(3,79), OccupyPoint(95,79), OccupyPoint(98,73), OccupyPoint(99,69), OccupyPoint(99,30), OccupyPoint(96,25), OccupyPoint(65,24), OccupyPoint(41,24), OccupyPoint(21,37), OccupyPoint(3,74)))
      Vis(hull.vis(), scalafx.scene.paint.Color.Blue)
      println("New obstacle:")
      println(hull.toString())
//      println("New obstacle points:")
//      println(hull.vis())
      parameters = backtracking.Backtracking(PathPlusParameters(Nil, parameters.graph, parameters.start, parameters.target, hull::Nil, parameters.maxPathLength, 200000, parameters.heuristic))
      println(hull == parameters.obstacles)
      path = parameters.path
    }
    
    println("Path:")
    path.foreach { x => println(x) }
    
    Vis(parameters.start::Nil, scalafx.scene.paint.Color.Red)//Start
    Vis(parameters.target::Nil, scalafx.scene.paint.Color.Red)//Target
    Vis(path, scalafx.scene.paint.Color.Green)//Path
    parameters.obstacles.foreach { x => Vis(x.vis(), scalafx.scene.paint.Color.Blue) }//Obstacles
    Draw()
  }
}