package template

import BeSpaceDCore._
import com.sun.org.apache.xalan.internal.xsltc.cmdline.getopt.IllegalArgumentException

object BacktrackingOnPathsTester extends CoreDefinitions with PointVis {
  def myCollisionHull(a: OccupyPoint, b: List[OccupyPoint]): Boolean = {
    return new ConvexHull().insideOrBorder(a, b)
  } //This actually makes a lot of sense for convex hulls.
  def myCollisionBoxes(a: OccupyPoint, b: List[OccupyBox]): Boolean = {
    return collisionTestsBig(b.map(x => unfoldInvariant(x)), unfoldInvariant(a) :: Nil)
  } //This is for checking boxes.

  def myCollision(a: OccupyPoint, b: List[Invariant]): Boolean = {
    b match{
      case point: List[OccupyPoint] => return myCollisionHull(a, point)
      case box: List[OccupyBox] => return myCollisionBoxes(a, box)
      case _ => throw new Exception
    }
  }
  def myCollision2(a: OccupyPoint, b: List[Invariant]):Boolean = {
    return collisionTestsBig(b.map(x => unfoldInvariant(x)), unfoldInvariant(a) :: Nil)
  }
//  def myCollision(a: OccupyPoint, b: List[OccupyPoint]): Boolean = {
//    myCollisionHull(a, b)
//  }
//  def myCollision(a: OccupyPoint, b: List[OccupyBox]): Boolean = {
//    myCollisionBoxes(a, b)
//  }

  def main(args: Array[String]): Unit = {
    println("Start initializing at " + java.util.Calendar.getInstance().getTime())
    val backtracking: SecondPathBacktracking = new SecondPathBacktracking()
    println("Finished initializing, start backtracking at " + java.util.Calendar.getInstance().getTime())
    def myComparator(a: OccupyPoint, b: OccupyPoint): Double = b.x - a.x //Just testing :)    
    //    def myCollision(a: OccupyPoint, b: List[Invariant]): Boolean = {
    //      b match {
    //        case b: List[OccupyPoint] => return new ConvexHull().insideOrBorder(a, b)
    //        case c                    => return collisionTestsBig(c.map(x => unfoldInvariant(x)), unfoldInvariant(a) :: Nil)
    //      }
    //    } //This actually makes a lot of sense for convex hulls. But doesn't work.
    val myScene = new FirstSceneGenerator().Generator()
    def standardComparator(a: OccupyPoint, b: OccupyPoint): Double = new FirstAStar().distance(a, myScene._2) - new FirstAStar().distance(b, myScene._2)
/*    val obstaclePoints: List[OccupyPoint] = simplifyInvariant(unfoldInvariant(BIGAND(myScene._3))) match {
      case BIGAND(x: List[OccupyPoint]) => x
      case _                            => Nil
    }*/
    backtracking.Backtracking(myScene._1, myScene._2, myScene._3, FirstGraphGenerator.graph, 20000, 20000, standardComparator, myCollision2).toString()
    println("Finished backtracking, start drawing at " + java.util.Calendar.getInstance().getTime())
    println(myScene._1)
    println(myScene._2)
    println(myScene._3)
    backtracking.getPath.foreach { x => println(x.toString()) }
    if (backtracking.getPath == Nil) println(false)
    println(backtracking.stepCounter)
    println(backtracking.getPath.size)
    Vis(myScene._1 :: Nil, scalafx.scene.paint.Color.Red)
    Vis(myScene._2 :: Nil, scalafx.scene.paint.Color.Red)
    Vis(backtracking.getPath, scalafx.scene.paint.Color.Green)

    myScene._3.foreach { x => Vis(x, scalafx.scene.paint.Color.Blue) }
    Draw()
    println("Finished drawing at " + java.util.Calendar.getInstance().getTime())
  }
}