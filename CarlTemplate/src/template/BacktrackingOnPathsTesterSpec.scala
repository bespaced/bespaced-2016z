package template

import BeSpaceDCore._
import BacktrackingOnPathsTester._

class BacktrackingOnPathsTesterSpec extends UnitSpec{
  "myCollisionBoxes" should "find a collision between OccupyPoint(5,7) and List(OccupyBox(3,2,5,6), OccupyBox(4,5,7,7))" in{
    val obstacles : List[OccupyBox] = List(OccupyBox(3,2,5,6), OccupyBox(4,5,7,7))
    val point : OccupyPoint = OccupyPoint(5,7)
    
    val result = myCollisionBoxes(point, obstacles)

    assertResult(expected = true)(actual = result )
  }
  it should "not find a collision between OccupyPoint(8,7) and List(OccupyBox(3,2,5,6), OccupyBox(4,5,7,7))" in{
    val obstacles : List[OccupyBox] = List(OccupyBox(3,2,5,6), OccupyBox(4,5,7,7))
    val point : OccupyPoint = OccupyPoint(8,7)
    
    val result = myCollisionBoxes(point, obstacles)

    assertResult(expected = false)(actual = result )
  }
  it should "find a collision between OccupyPoint(5,2) and List(OccupyBox(3,2,5,6), OccupyBox(4,5,7,7))" in{
    val obstacles : List[OccupyBox] = List(OccupyBox(3,2,5,6), OccupyBox(4,5,7,7))
    val point : OccupyPoint = OccupyPoint(5,2)
    
    val result = myCollisionBoxes(point, obstacles)

    assertResult(expected = true)(actual = result )
  }
  it should "find a collision between OccupyPoint(6,5) and List(OccupyBox(3,2,5,6), OccupyBox(4,5,7,7))" in{
    val obstacles : List[OccupyBox] = List(OccupyBox(3,2,5,6), OccupyBox(4,5,7,7))
    val point : OccupyPoint = OccupyPoint(6,5)
    
    val result = myCollisionBoxes(point, obstacles)

    assertResult(expected = true)(actual = result )
  }
  "myCollisionHull" should "not find a collision between OccupyPoint(3,2) and List(OccupyPoint(2,4), OccupyPoint(3,8), OccupyPoint(5,9), OccupyPoint(7,5), OccupyPoint(5,1), OccupyPoint(4,1))" in{
    val obstacles : List[OccupyPoint] = List(OccupyPoint(2,4), OccupyPoint(3,8), OccupyPoint(5,9), OccupyPoint(7,5), OccupyPoint(5,1), OccupyPoint(4,1))
    val point : OccupyPoint = OccupyPoint(3,2)
    
    val result = myCollisionHull(point, obstacles)

    assertResult(expected = false)(actual = result )
  }
  it should "find a collision between OccupyPoint(6,4) and List(OccupyPoint(2,4), OccupyPoint(3,8), OccupyPoint(5,9), OccupyPoint(7,5), OccupyPoint(5,1), OccupyPoint(4,1))" in{
    val obstacles : List[OccupyPoint] = List(OccupyPoint(2,4), OccupyPoint(3,8), OccupyPoint(5,9), OccupyPoint(7,5), OccupyPoint(5,1), OccupyPoint(4,1))
    val point : OccupyPoint = OccupyPoint(6,4)
    
    val result = myCollisionHull(point, obstacles)

    assertResult(expected = true)(actual = result )
  }
  it should "find a collision between OccupyPoint(2,4) and List(OccupyPoint(2,4), OccupyPoint(3,8), OccupyPoint(5,9), OccupyPoint(7,5), OccupyPoint(5,1), OccupyPoint(4,1))" in{
    val obstacles : List[OccupyPoint] = List(OccupyPoint(2,4), OccupyPoint(3,8), OccupyPoint(5,9), OccupyPoint(7,5), OccupyPoint(5,1), OccupyPoint(4,1))
    val point : OccupyPoint = OccupyPoint(2,4)
    
    val result = myCollisionHull(point, obstacles)

    assertResult(expected = true)(actual = result )
  }
  it should "find a collision between OccupyPoint(2,4) and List(OccupyPoint(2,4), OccupyPoint(3,7), OccupyPoint(5,9), OccupyPoint(7,5), OccupyPoint(5,1), OccupyPoint(4,1))" in{
    val obstacles : List[OccupyPoint] = List(OccupyPoint(2,4), OccupyPoint(3,7), OccupyPoint(5,9), OccupyPoint(7,5), OccupyPoint(5,1), OccupyPoint(4,1))
    val point : OccupyPoint = OccupyPoint(4,8)
    
    val result = myCollisionHull(point, obstacles)

    assertResult(expected = true)(actual = result )
  }
}