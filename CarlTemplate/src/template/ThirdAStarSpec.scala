package template

import BeSpaceDCore._
import scala.collection.mutable.HashMap

class ThirdAStarSpec extends UnitSpec {
  "Search" should "return the shortest path" in {
    val graph = new FirstGraph[SecondQuadTree]
    val emptyMap = new HashMap[(Int, Int), (Double, Int)]
    def densityHeuristic(a: SecondQuadTree, b: SecondQuadTree): Double = {
      return b.density - a.density
    }
    val a = SecondQuadTree(emptyMap, 0, 0, 10, 0, 1, densityHeuristic)
    val b = SecondQuadTree(emptyMap, 20, 20, 10, 0, 1, densityHeuristic)
    val c = SecondQuadTree(emptyMap, 40, 40, 10, 0, 1, densityHeuristic)
    println(a == a)
    println(a.ordering == a.ordering)
    println(densityHeuristic _ == densityHeuristic _)
    graph.addEdge(a, b, false, 1, "")
    graph.addEdge(b, c, false, 1, "")
    val result = ThirdAStar.Search(graph, a, c)
    assertResult(expected = (List(a,b,c), 2.0))(actual = result)
  }
}