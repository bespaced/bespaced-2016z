package template

import BeSpaceDCore._
import scalafx.scene.paint.{ Stops, LinearGradient, Color }

class FirstGraph[NodeType] extends Graph{
  //case class Node(payload : NodeType)
  case class Edge(n1 : NodeType, n2 : NodeType, directed : Boolean, weight : Double, label : String)
  var nodes : List[NodeType] = Nil
  var edges : List[Edge] = Nil
  def getNodes() : List[NodeType] = nodes
  def getEdges() : List[Edge] = edges
  def addNode(data : NodeType) = {
    if(!(nodes.contains(data))){
      nodes = data :: nodes
    }
    
    
//    val testNode : Node[OccupyBox] = Node[OccupyBox](OccupyBox(2,4,3,6))
//    val testNode2: Node[OccupyBox] = Node[OccupyBox](OccupyBox(4,8,6,12))
//    edges = Edge((testNode,testNode2,true,1,"testEdge"))  ::  edges
  }
  def addEdge(data : Edge){
    if(!(nodes.contains(data.n1))){
      addNode(data.n1)
    }
    if(!(nodes.contains(data.n2))){
      addNode(data.n2)
    }
    edges = data :: edges
  }
  def addEdge(n1 : NodeType, n2: NodeType, directed : Boolean, weight : Double, label : String){
    val newEdge : Edge = Edge(n1, n2, directed, weight, label)
    addEdge(newEdge)
  }
//  def addEdge(payload : (Any, Any, Boolean, Double, String)){
//    addNode(payload._1)
//    addNode(payload._2)
//    val newPayload : (Node[Any], Node[Any], Boolean, Double, String) = (Node[Any](payload._1, None), Node[Any](payload._2, None), payload._3, payload._4, payload._5)
//    addEdge(newPayload)
//  }
  def getOutEdges(data : NodeType) : List[Edge] = {
    if(!(nodes.contains(data))){
      return Nil
    }
//    var output : List[Edge] = Nil
    //edges.map { x => if(x.payload._1 == data) {output = x :: output} }
    //edges.foreach {x => if(x.payload._1 == data) {output = x :: output}}
    val output : List[Edge] = edges.filter { e : Edge => e.n1 == data || (e.n2 == data && !e.directed) }.distinct
    //TODO: Replace by a while loop.
//    for(edge : Edge <- edges){
//      if(edge.payload._1 == data){
//        output = edge :: output
//      }
//    }
    return output
  }
  def successors(data : NodeType) : List[NodeType] = {
    getOutEdges(data).map { x => x match{
//      case Edge(data, data, _, _, _) => data //Actually an important point, but throws an error: ambiguous reference to overloaded definition, both value data of type NodeType and value data of type NodeType match expected type ?
      case Edge(_, n2, true, _, _) => n2
      case Edge(n1, n2, false, _, _) => if(n1 == data) n2 else if(n2 == data) n1 else throw new Exception()
      //For-Comprehension:
//      for(c <- "hello"; if(c > 'h')) yield {c => s"char $c"}
//      case Edge(n1, data, false, _, _) => n1 //Why is this unreachable?
      }
    }.distinct //Added distinct on 10.10.2016 when I saw that the Dijkstra has trouble with too many edges.
  }
  def getInEdges(data : NodeType) : List[Edge] = {
    if(!(nodes.contains(data))){
      return Nil
    }
    val output : List[Edge] = edges.filter { e : Edge => e.n2 == data || (e.n1== data && !e.directed) }.distinct
    return output
  }
  def getEdge(a : NodeType, b : NodeType) : List[Edge] = {
    edges.filter { x : Edge => x.n1 == a && x.n2 == b || (x.n1 == b && x.n2 == a && !x.directed)}
  }
  def isConnected(a: NodeType, b: NodeType): Boolean = getEdge(a,b) == Nil && getEdge(b,a) == Nil
  def deleteEdge(a : NodeType, b : NodeType) : List[Edge] = {
    edges = edges.filterNot { x : Edge => x.n1 == a && x.n2 == b || (x.n1 == b && x.n2 == a && !x.directed)}
    return edges
  }
  def deleteEdge(e : Edge) : List[Edge] = {
    return deleteEdge(e.n1, e.n2)
  }
  def updateEdge(n1 : NodeType, n2 : NodeType, directed : Boolean, weight : Double, label : String) = {
    deleteEdge(n1, n2)
    addEdge(n1, n2, directed, weight, label)
  }
  def deleteNode(data : NodeType) : List[NodeType] = {
    
    getOutEdges(data).foreach { x : Edge => deleteEdge(x) }
    getInEdges(data).foreach { x : Edge => deleteEdge(x) }
    nodes = nodes.filterNot { x : NodeType =>  x == data}
    return nodes
  }
  
  //Finds the weight only for the first edge between the two nodes. Should be extended to find the min weight edge's weight.
  def weight (n1 : NodeType, n2 : NodeType) : Double = {
    getEdge(n1, n2).head.weight
  }
}