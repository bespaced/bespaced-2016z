package template

import BeSpaceDCore._
import java.io._
import java.util.Scanner

object OutputCSV {
  val filePath: String = "./stored_output/"
  println("Please enter index of first and last experiment.")
  val sc = new Scanner(System.in)
  val firstExperiment: Int = sc.nextInt()
  val lastExperiment: Int = sc.nextInt()
  println("Do you want to run the experiments in parallel and get the results later (p) or serial and get them immediately (s)?")
  sc.next() match{
    case "s" => serialRun(firstExperiment, lastExperiment)
    case "p" => parallelRun(firstExperiment, lastExperiment)
    case "t" => KinectScanDataTesterA.newSingleTesterAndWriter
  }
  def parallelRun(firstExperiment: Int, lastExperiment: Int): Unit = {
    val configsAndResults: List[(Configuration, Results)] = KinectScanDataTesterA.multiTesterWithReturnValue(firstExperiment, lastExperiment) //KinectScanDataTesterA.singleTester//obstacleMultiParameterTest(28) //obstacleMultiParameterTest(32)
    writeToFile(configsAndResults)
  }
  def serialRun(firstExperiment: Int, lastExperiment: Int): Unit = {
//    KinectScanDataTesterA.newSingleTesterAndWriter
    println("Which scan do you want to run experiments on? Enter a number. Type -1 if you want to run all experiments.")
    val numExperiment: Int = sc.nextInt()
    KinectScanDataTesterA.multiTesterAndWriterWithLoop(firstExperiment, lastExperiment, if(numExperiment == -1) None else Some(numExperiment), false)
  }
  def minClearancePointForOutput(mp: Option[OccupyPoint]): OccupyPoint = mp match {
    case None    => OccupyPoint(-1, -1)
    case Some(d) => d
  }
  def twoLinesCSV(a: String, b: String): String = {
    val a1: Array[String] = a.split("\n").map { x => x.trim() }
    val b1: Array[String] = b.split("\n").map { x => x.trim() }
    val z: Array[(String, String)] = a1 zip b1
    var output: String = ""
    z.foreach {
      f => output += f._1 + f._2 + "\n"
    }
    return output
  }
  def parametersCSV(p: Configuration): String = {
    p match {
      case Configuration(dataSetTitle, points, placement, rotation, resolution, magnificationFactor, borders, empty, full, minSize, start, target, zPlane, densityFilterLevel, densityFilter, densityPostProcessingThreshold, shorteningEnabled, heuristic) => {
        var dstTitle: String = ""
        (0 until (dataSetTitle.length)).foreach { x => dstTitle = dstTitle + "dataSetTitle" + x + ":," }
        var dstValue: String = ""
        (0 until (dataSetTitle.length)).foreach { x => dstValue = dstValue + dataSetTitle.apply(x) + "," }
        var placementPointTitles: String = ""
        (0 until (placement.length)).foreach { x => placementPointTitles = placementPointTitles + "Placement Point " + x + ":," }
        var placementPoints: String = ""
        (0 until (placement.length)).foreach { x => placementPoints = placementPoints + placement.apply(x).x + "_" + placement.apply(x).y + "," }
        var rotationTitles: String = ""
        (0 until (rotation.length)).foreach { x => rotationTitles = rotationTitles + "Rotation of Set " + x + ":," }
        var rotationValues: String = ""
        (0 until (rotation.length)).foreach { x => rotationValues = rotationValues + rotation.apply(x) + "," }
        "id:," + dstTitle + /*dataSetTitle0:,dataSetTitle1:,dataSetTitle2:,dataSetTitle3:,dataSetTitle4:,dataSetTitle5:,dataSetTitle6:,dataSetTitle7:,dataSetTitle8:,dataSetTitle9:,dataSetTitle10:,dataSetTitle11:,dataSetTitle12:,dataSetTitle13:,dataSetTitle14:,dataSetTitle15:,*/"z plane:,resolution:,magnification factor:,size:,minimum cell size:,startX:,startY:,targetX:,targetY:,empty:,full:,cutoffMinX:,cutoffMaxX:,cutoffMinY:,cutoffMaxY:,densityFilterLevel:,filtered by density:,post-processing by density:,shortening enabled:,heuristic:," +
          '\n' + "" + p.id + "," + dstValue/*dataSetTitle.apply(0) + "," + dataSetTitle.apply(1) + "," + dataSetTitle.apply(2) + "," + dataSetTitle.apply(3) + "," + dataSetTitle.apply(4) + "," + dataSetTitle.apply(5) + "," + dataSetTitle.apply(6) + "," + dataSetTitle.apply(7) + "," + dataSetTitle.apply(8) + "," + dataSetTitle.apply(9) + "," + dataSetTitle.apply(10) + "," + dataSetTitle.apply(11) + "," + dataSetTitle.apply(12) + "," + dataSetTitle.apply(13) + "," + dataSetTitle.apply(14) + "," + dataSetTitle.apply(15) + ","*/ + zPlane + "," + resolution + "," + magnificationFactor + "," + (magnificationFactor * magnificationFactor) + ","+ minSize + "," + start.x + "," + start.y + "," + target.x + "," + target.y + "," + empty + "," + full + "," + borders.minX + "," + borders.maxX + "," + borders.minY + "," + borders.maxY + "," + densityFilterLevel + "," + densityFilter + "," + densityPostProcessingThreshold + "," + shorteningEnabled + "," + heuristic + ","
      }
    }
  }
  def measurementsCSV(m: Results): String = {
    m match {
      case Results(pathCorners, path, leaves, qtime, ftime, gtime, atime, length, corners, minClearance, minClearancePoint, pointWorkingSet, numFreeCells, numAllCells, stime, images) => {
        "QuadTree operations performed in:,Extracted free space in:,Graph generated in:,A* performed in:,Total Time:,Euclidian length of the path:,Number of corner points of the path:,Minimum Clearance:,Minimum Clearance at X:,Minimum Clearance at Y:,Number of free cells:,Number of all cells:,Time for shortening the path:," +
          '\n' + qtime + "," + ftime + "," + gtime + "," + atime + "," + (qtime + ftime + gtime + atime) + "," + length + "," + corners + "," + minClearance + "," + minClearancePointForOutput(minClearancePoint).x + "," + minClearancePointForOutput(minClearancePoint).y + "," + numFreeCells + "," + numAllCells + "," + stime + ","
      }
    }
  }
  def writeToFileSingle(configsAndResults: (Configuration, Results)) = {
    writeToFile(List(configsAndResults))
  }
  def writeToFile(configsAndResults: List[(Configuration, Results)]) = {
    val num = 0 to configsAndResults.length
    (num zip configsAndResults).foreach {
      f =>
        f match {
          case (n, (Configuration(dataSetTitle, points, placement, rotation, resolution, magnificationFactor, borders, empty, full, minSize, start, target, zPlane, densityFilterLevel, densityFilter, densityPostProcessingThreshold, shorteningEnabled, heuristic), Results(pathCorners, path, leaves, qtime, ftime, gtime, atime, length, corners, minClearance, minClearancePoint, pointWorkingSet, numFreeCells, numAllCells, stime, images))) => {
            /*val measurements: String = "QuadTree operations performed in " + qtime + '\n' + "Extracted free space in " +
            ftime + '\n' + "Graph generated in " + gtime + '\n' + "A* performed in " + atime + '\n' +
            "euclidian length of the path: " + length + '\n' +
            "number of corner points of the path: " + corners + '\n' +
            "minimum clearance: " + minClearance + " at " + minClearancePoint + '\n' +
            "number of free cells: " + numFreeCells + '\n' + "number of all cells: " + numAllCells

          val measurementsCSV: String = "QuadTree operations performed in: , Extracted free space in: , Graph generated in: , A* performed in: , Euclidian length of the path: , Number of corner points of the path: , Minimum Clearance: , Minimum Clearance at X: , Minimum Clearance at Y: , Number of free cells: , Number of all cells: " +
            '\n' + qtime + "," + ftime + "," + gtime + "," + atime + "," + length + "," + corners + "," + minClearance + "," + minClearancePointForOutput.x + "," + minClearancePointForOutput.y + "," + numFreeCells + "," + numAllCells
          val parameters: String = "dataSetTitle: " + dataSetTitle + '\n' +
            "z plane: " + zPlane + '\n' +
            "resolution: " + resolution + '\n' + "magnification factor: " + magnificationFactor + '\n' +
            "minimum cell size: " + minSize + '\n' + "start: " + start + '\n' + "target: " + target + '\n' +
            "empty: " + empty + '\n' + "full: " + full + '\n' + "cutOff: " + borders
          val parametersCSV: String = "dataSetTitle: , z plane: , resolution: , magnification factor: , minimum cell size: , startX: , startY: , targetX: , targetY: , empty: , full: , cutoffMinX: , cutoffMaxX: , cutoffMinY: , cutoffMaxY: " +
            '\n' + zPlane + "," + resolution + "," + magnificationFactor + "," + minSize + "," + start.x + "," + start.y + "," + target.x + "," + target.y + "," + empty + "," + full + "," + borders.minX + "," + borders.maxX + "," + borders.minY + "," + borders.maxY*/

            val timeStamp: Long = System.currentTimeMillis()
            val pw = new PrintWriter(new File(filePath + "" + f._2._1.id + "_" + timeStamp + ".csv"))
            pw.write("Measurements:" + '\n')
            pw.write(measurementsCSV(Results(pathCorners, path, leaves, qtime, ftime, gtime, atime, length, corners, minClearance, minClearancePoint, pointWorkingSet, numFreeCells, numAllCells, stime, images)) + '\n')
            pw.write("Parameters:" + '\n')
            pw.write(parametersCSV(Configuration(dataSetTitle, points, placement, rotation, resolution, magnificationFactor, borders, empty, full, minSize, start, target, zPlane, densityFilterLevel, densityFilter, densityPostProcessingThreshold, shorteningEnabled, heuristic)))
            pw.close
            //            val pwp = new PrintWriter(new File(filePath + "" + id + "_" + timeStamp + "parameters" + ".csv"))
            //            pwp.write("Parameters:" + '\n')
            //            pwp.write(parametersCSV(Configuration(dataSetTitle, points, resolution, magnificationFactor, borders, empty, full, minSize, start, target, zPlane, densityFilterLevel, densityFilter, densityPostProcessingThreshold, shorteningEnabled, heuristic, id)))
            //            pwp.close
            //            val pwr = new PrintWriter(new File(filePath + "" + id + "_" + timeStamp + "results" + ".csv"))
            //            pwr.write("Measurements:" + '\n')
            //            pwr.write(measurementsCSV(Results(pathCorners, path, leaves, qtime, ftime, gtime, atime, length, corners, minClearance, minClearancePoint, pointWorkingSet, numFreeCells, numAllCells, stime, images)) + '\n')
            //            pwr.close
            val sideBySide = new PrintWriter(new File(filePath + "" + f._2._1.id + "_" + timeStamp + "twoLines" + ".csv"))
            val c: String = parametersCSV(Configuration(dataSetTitle, points, placement, rotation, resolution, magnificationFactor, borders, empty, full, minSize, start, target, zPlane, densityFilterLevel, densityFilter, densityPostProcessingThreshold, shorteningEnabled, heuristic))
            val m: String = measurementsCSV(Results(pathCorners, path, leaves, qtime, ftime, gtime, atime, length, corners, minClearance, minClearancePoint, pointWorkingSet, numFreeCells, numAllCells, stime, images))
            val twoLines: String = twoLinesCSV(c, m)
            sideBySide.write(twoLines)
            sideBySide.close  
          }

        }
    }
  }
}