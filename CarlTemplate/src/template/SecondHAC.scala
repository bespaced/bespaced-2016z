package template

import scala.collection.mutable.HashMap
import BeSpaceDCore._

/**
 * @author Carl
 * Hierarchical Agglomerative Clustering
 * Computes distances between clusters by distance between its components, and this is again computed by distance between its points.
 *
 */
class SecondHAC(obstacles: List[AbstractPolygon]) extends CoreDefinitions {
  val startClusters: List[Cluster] = ClusterInit()
  var distances: HashMap[(Set[Cluster], LinkageMode), Double] = new HashMap()
  distanceInit(startClusters)
  //  var clusterDistances: HashMap[(Set[Cluster], LinkageMode), Double] = distances.map(x => (x._1._1.map { x => ??? })
  val SingleCluster: List[Cluster] = Nil
  var ClusterMap: HashMap[LinkageMode, Cluster] = new HashMap()
  def hacClustering(distanceThreshold: Double, linkageMode: Option[LinkageMode]): List[List[AbstractPolygon]] = {
    val linkage: LinkageMode = linkageMode match {
      case Some(d) => d
      case None    => Single
    }
    //    distanceInit(obstacles)
    val cluster: Cluster = if (ClusterMap.contains(linkage)) ClusterMap.apply(linkage) else clustering(linkage)
    val clusters: List[Cluster] = hacClusteringRecursive(cluster, distanceThreshold)
    val output: List[List[AbstractPolygon]] = clusters.map { x => x.data }
    return output
  }
  def hacClusteringRecursive(cluster: Cluster, distanceThreshold: Double): List[Cluster] = {
    if (cluster.distance >= distanceThreshold) {
      cluster match {
        case Cluster(Some(l), Some(r), _, _) => return hacClusteringRecursive(l, distanceThreshold) ++ hacClusteringRecursive(r, distanceThreshold)
        case Cluster(Some(l), None, _, _)    => return hacClusteringRecursive(l, distanceThreshold)
        case Cluster(None, Some(r), _, _)    => return hacClusteringRecursive(r, distanceThreshold)
        case Cluster(_, _, _, Some(p))       => return cluster :: Nil
        case Cluster(None, None, _, None)    => throw new IllegalArgumentException
      }
    }
    return cluster :: Nil
  }
  def clustering(linkage: LinkageMode): Cluster = hac(startClusters, linkage)

  //Idea: Precompute the distances between individual obstacles once, then just recall them accordingly for computing distances between clusters

  def distanceInit(obstacles: List[Cluster]): Unit = {
    for (m: Cluster <- obstacles; n: Cluster <- obstacles) {
      //Assuming this is called at the beginning of the algorithm --> each cluster has just one AbstractPolygon.
      assert(m.data.length == 1 && n.data.length == 1)
      val i: AbstractPolygon = m.data.head
      val j: AbstractPolygon = n.data.head
      val pairs: List[(OccupyPoint, OccupyPoint)] = for (k: OccupyPoint <- i.getBorder; l: OccupyPoint <- j.getBorder) yield (k, l)
      val min = pairs.minBy(x => PointArithmetic.distance(x._1, x._2))
      val max = pairs.maxBy(x => PointArithmetic.distance(x._1, x._2))
      val avg = PointArithmetic.distance(i.centroid(), j.centroid())
      val minValue = PointArithmetic.distance(min._1, min._2)
      val maxValue = PointArithmetic.distance(max._1, max._2)
      val set: Set[Cluster] = Set(m, n)
      distances.put((set, Complete), maxValue)
      distances.put((set, Single), minValue)
      distances.put((set, Centroid), avg)
      //      distances.+=((set, Complete) -> maxValue)
      //      distances.+=((set, Single) -> minValue)
      //      distances.+=((set, Centroid) -> avg)
    }
  }
  def distanceInit(): Unit = distanceInit(startClusters)
  def distanceInitAllPoints(obstacles: List[Cluster]): Unit = {
    for (m: Cluster <- obstacles; n: Cluster <- obstacles) {
      //Assuming this is called at the beginning of the algorithm --> each cluster has just one AbstractPolygon.
      assert(m.data.length == 1 && n.data.length == 1)
      val i: AbstractPolygon = m.data.head
      val j: AbstractPolygon = n.data.head
      val pairs: List[(OccupyPoint, OccupyPoint)] = for (k: OccupyPoint <- i.vis; l: OccupyPoint <- j.vis) yield (k, l)
      val min = pairs.minBy(x => PointArithmetic.distance(x._1, x._2))
      val max = pairs.maxBy(x => PointArithmetic.distance(x._1, x._2))
      val avg = PointArithmetic.distance(i.centroid(), j.centroid())
      val minValue = PointArithmetic.distance(min._1, min._2)
      val maxValue = PointArithmetic.distance(max._1, max._2)
      val set: Set[Cluster] = Set(m, n)
      distances.put((set, Complete), maxValue)
      distances.put((set, Single), minValue)
      distances.put((set, Centroid), avg)
      //      distances.+=((set, Complete) -> maxValue)
      //      distances.+=((set, Single) -> minValue)
      //      distances.+=((set, Centroid) -> avg)
    }
  }
  /*def distance(first: Cluster, second: Cluster, linkage: LinkageMode): Double = {
    val pairs: List[(AbstractPolygon, AbstractPolygon)] = for (i: AbstractPolygon <- first.data; j: AbstractPolygon <- second.data) yield (i, j)
//    println(distances)
    val min = pairs.minBy(x => distances.apply((Set(x._1, x._2), Single)))
    val max = pairs.maxBy(x => distances.apply((Set(x._1, x._2), Complete)))
    val avg = pairs.minBy(x => distances.apply((Set(x._1, x._2), Centroid))) //?
    val minValue = distances.apply((Set(min._1, min._2), Single))
    val maxValue = distances.apply((Set(max._1, max._2), Complete))
    val avgValue = distances.apply((Set(min._1, min._2), Centroid))
    linkage match {
      case Complete => return maxValue
      case Single   => return minValue
      case Centroid => return avgValue
    }
  }*/
  def ClusterInit(): List[Cluster] = {
    return ClusterInit(obstacles)
  }
  def ClusterInit(obstacles: List[AbstractPolygon]): List[Cluster] = {
    return obstacles.map { x => Cluster(None, None, 0.0, Some(List(x))) }
  }
  def hac(clusters: List[Cluster], linkage: LinkageMode): Cluster = {
    if (clusters.length <= 1) {
      return clusters.head
    }
    val allPairs: List[(Cluster, Cluster)] = for (i: Cluster <- clusters; j: Cluster <- clusters) yield (i, j)
    val pairs: List[(Cluster, Cluster)] = allPairs.filterNot(x => x._1 == x._2)
    val minPair = pairs.minBy(x => distances.apply((Set(x._1, x._2), linkage)))
    val newCluster = Cluster(Some(minPair._1), Some(minPair._2), distances.apply(Set(minPair._1, minPair._2), linkage), None)
    for ((key, value) <- distances) {
      if (distances.contains(key)) {
        val these: List[Cluster] = key._1.filter { x => x == minPair._1 || x == minPair._2 }.toList
        val those: List[Cluster] = key._1.filterNot { x => x == minPair._1 || x == minPair._2 }.toList
        if (!(those.isEmpty || these.isEmpty)) {
          //Assuming those and these have size 1
          assert(those.size == 1 && these.size == 1)
          val that = those.head
          val minDistance: Double = distances.apply(key)
          distances.remove(key)
          distances.put((Set(newCluster, that), linkage), minDistance)
        }
      }

    }
    return hac(clusters.filterNot { x => x == minPair._1 || x == minPair._2 } ++ List(newCluster), linkage)
  }
  //Methods for convenience
  def listClusters(start: Cluster): List[Cluster] = {
    start match {
      case Cluster(Some(l), Some(r), _, _) => return start :: listClusters(l) ++ listClusters(r)
      case Cluster(Some(l), None, _, _)    => return start :: listClusters(l)
      case Cluster(None, Some(r), _, _)    => return start :: listClusters(r)
      case Cluster(None, None, _, _)       => return start :: Nil
    }
  }
  def initClusters(clusters: List[List[AbstractPolygon]]): List[Cluster] = {
    distanceInit(ClusterInit(clusters.flatten))
    return clusters.map { x => Cluster(None, None, 0.0, Some(x)) }
  }
  //Methods for consistency testing
  def moreDifferentThanChildren(start: Cluster): Boolean = {
    return listClusters(start).forall { x =>
      x match {
        case Cluster(Some(l), Some(r), d, _) => d >= l.distance && d >= r.distance
        case Cluster(Some(l), None, d, _)    => d >= l.distance
        case Cluster(None, Some(r), d, _)    => d >= r.distance
        case _                               => true
      }
    }
  }
  def includesChildren(start: Cluster): Boolean = {
    return listClusters(start).forall { x =>
      x match {
        case Cluster(Some(l), Some(r), _, _) => x.data == l.data ++ r.data //x.data.containsSlice(l.data) && x.data.containsSlice(r.data)
        case Cluster(Some(l), None, _, _)    => x.data == l.data
        case Cluster(None, Some(r), _, _)    => x.data == r.data
        case _                               => true
      }
    }
  }
  def consistencyCheck(start: Cluster): Boolean = moreDifferentThanChildren(start) && includesChildren(start)
}
//case class Cluster(left: Option[Cluster], right: Option[Cluster], distance: Double, payload: Option[List[AbstractPolygon]]) {
//  def data: List[AbstractPolygon] = {
//    payload match {
//      case Some(d) => return d
//      case None => {
//        this match {
//          case Cluster(Some(l), Some(r), _, _) => return l.data ++ r.data
//          case Cluster(None, Some(r), _, _)    => return r.data
//          case Cluster(Some(l), None, _, _)    => return l.data
//        }
//      }
//    }
//  }
//}
//trait LinkageMode
//case object Complete extends LinkageMode
//case object Single extends LinkageMode
//case object Centroid extends LinkageMode
//
//object debug{
//  type Key = (AbstractPolygon, AbstractPolygon, LinkageMode)
//  var getKey: Key = (new InvariantObstacle(OccupyBox(0,0,0,0)), new InvariantObstacle(OccupyBox(0,0,0,0)), Single)
//  var putKey: Key = (new InvariantObstacle(OccupyBox(0,0,0,0)), new InvariantObstacle(OccupyBox(0,0,0,0)), Single)
//}