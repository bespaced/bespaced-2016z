package template

import scala.collection.mutable.PriorityQueue
import scala.annotation.tailrec
import scala.collection.mutable.HashMap
import BeSpaceDCore._
import scalafx.scene.paint.{ Stops, LinearGradient, Color }

object ThirdAStar {
  //This doesn't return the shortest path, because the heuristic overestimates the distance.
  def Search(graph: FirstGraph[SecondQuadTree], start: SecondQuadTree, target: SecondQuadTree): (List[SecondQuadTree], Double) = AStar(graph, start, target)
  def AStar(graph: FirstGraph[SecondQuadTree], start: SecondQuadTree, target: SecondQuadTree): (List[SecondQuadTree], Double) = {
    val showGraph: Boolean = false
    if(showGraph)graphVisTree(graph)
    val allNodes: List[SecondQuadTree] = graph.getNodes()
    val distanceToStart: HashMap[SecondQuadTree, Double] = new HashMap[SecondQuadTree, Double]
    allNodes.foreach { x => distanceToStart.+=((x, Double.MaxValue)) }
    val distanceToTarget: HashMap[SecondQuadTree, Double] = new HashMap[SecondQuadTree, Double]
    allNodes.foreach { x => distanceToTarget.+=((x, PointArithmetic.distance(x.center, target.center))) }
    val priority: HashMap[SecondQuadTree, Double] = new HashMap[SecondQuadTree, Double]
    allNodes.foreach { x => priority.+=((x, Double.MaxValue)) }
    val parentNode: HashMap[SecondQuadTree, SecondQuadTree] = new HashMap[SecondQuadTree, SecondQuadTree]
    case class helper(payload: SecondQuadTree) extends Ordered[helper] {
      def compare(that: helper): Int = {
        -(priority.get(this.payload).get - priority.get(that.payload).get).signum
      }
    }
    var open: PriorityQueue[helper] = new PriorityQueue[helper]
    open.+=(helper(start))
    distanceToStart.+=((start, 0))
    priority.+=((start, distanceToTarget.get(start).get))
    while (!open.isEmpty) {
      val currentNode: SecondQuadTree = open.dequeue().payload
      if(currentNode == target){
        val pathOutput: List[SecondQuadTree] = path(target, Nil)
        val pathLengthOutput: Double = pathLength(graph, pathOutput)
        return (pathOutput, pathLengthOutput)
      }
      val successors: List[SecondQuadTree] = graph.successors(currentNode)
      successors.foreach { x => 
        val dt: Double = distanceToStart.get(currentNode).get + graph.weight(currentNode, x)
        val pr: Double = dt + distanceToTarget.get(x).get
        if(!parentNode.contains(x) && !(x == start)){
          priority.+=((x, pr))
          distanceToStart.+=((x, dt))
          parentNode.+=((x, currentNode))
          open.enqueue(helper(x))
        } else if(open.exists { y => y.payload == x } && dt < distanceToStart.get(x).get){
          priority.+=((x, pr))
          distanceToStart.+=((x, dt))
          parentNode.+=((x, currentNode))
//          val cache = open.dequeueAll
//          cache.foreach { z => open.enqueue(z) }
          //Here I update the priority of the current node x in the priority queue. With a better priority queue, this might be accelerated.
          open = open.filterNot { z => z.payload == x }
          open.enqueue(helper(x))
        }
      }
    }
    @tailrec
    def path(end: SecondQuadTree, currentPath: List[SecondQuadTree]): List[SecondQuadTree] = {
      val predecessor: Option[SecondQuadTree] = parentNode.get(end)
      predecessor match {
        case None    => return end :: currentPath
        case Some(d) => return path(d, end :: currentPath)
      }
    }
    def pathLength(g: FirstGraph[SecondQuadTree], l: List[SecondQuadTree]): Double = {
      l match {
        case Nil                    => return 0.0
        case head :: Nil            => return 0.0
        case head1 :: head2 :: tail => return g.weight(head1, head2) + pathLength(g, head2 :: tail)
      }
    }
    return (Nil, 0)
  }
  def Search(graph: FirstGraph[OccupyPoint], start: OccupyPoint, target: OccupyPoint): (List[OccupyPoint], Double) = AStar(graph, start, target)
  def AStar(graph: FirstGraph[OccupyPoint], start: OccupyPoint, target: OccupyPoint): (List[OccupyPoint], Double) = {
    val showGraph: Boolean = true
    graphVisPoint(graph)
    val allNodes: List[OccupyPoint] = graph.getNodes()
    val distanceToStart: HashMap[OccupyPoint, Double] = new HashMap[OccupyPoint, Double]
    allNodes.foreach { x => distanceToStart.+=((x, Double.MaxValue)) }
    val distanceToTarget: HashMap[OccupyPoint, Double] = new HashMap[OccupyPoint, Double]
    allNodes.foreach { x => distanceToTarget.+=((x, PointArithmetic.distance(x, target))) }
    val priority: HashMap[OccupyPoint, Double] = new HashMap[OccupyPoint, Double]
    allNodes.foreach { x => priority.+=((x, Double.MaxValue)) }
    val parentNode: HashMap[OccupyPoint, OccupyPoint] = new HashMap[OccupyPoint, OccupyPoint]
    case class helper(payload: OccupyPoint) extends Ordered[helper] {
      def compare(that: helper): Int = {
        -(priority.get(this.payload).get - priority.get(that.payload).get).signum
      }
    }
    var open: PriorityQueue[helper] = new PriorityQueue[helper]
    open.+=(helper(start))
    distanceToStart.+=((start, 0))
    priority.+=((start, distanceToTarget.get(start).get))
    while (!open.isEmpty) {
      val currentNode: OccupyPoint = open.dequeue().payload
      if(currentNode == target){
        val pathOutput: List[OccupyPoint] = path(target, Nil)
        val pathLengthOutput: Double = pathLength(graph, pathOutput)
        return (pathOutput, pathLengthOutput)
      }
      val successors: List[OccupyPoint] = graph.successors(currentNode)
      successors.foreach { x => 
        val dt: Double = distanceToStart.get(currentNode).get + graph.weight(currentNode, x)
        val pr: Double = dt + distanceToTarget.get(x).get
        if(!parentNode.contains(x) && !(x == start)){
          priority.+=((x, pr))
          distanceToStart.+=((x, dt))
          parentNode.+=((x, currentNode))
          open.enqueue(helper(x))
        } else if(open.exists { y => y.payload == x } && dt < distanceToStart.get(x).get){
          priority.+=((x, pr))
          distanceToStart.+=((x, dt))
          parentNode.+=((x, currentNode))
//          val cache = open.dequeueAll
//          cache.foreach { z => open.enqueue(z) }
          //Here I update the priority of the current node x in the priority queue. With a better priority queue, this might be accelerated.
          open = open.filterNot { z => z.payload == x }
          open.enqueue(helper(x))
        }
      }
    }
    @tailrec
    def path(end: OccupyPoint, currentPath: List[OccupyPoint]): List[OccupyPoint] = {
      val predecessor: Option[OccupyPoint] = parentNode.get(end)
      predecessor match {
        case None    => return end :: currentPath
        case Some(d) => return path(d, end :: currentPath)
      }
    }
    def pathLength(g: FirstGraph[OccupyPoint], l: List[OccupyPoint]): Double = {
      l match {
        case Nil                    => return 0.0
        case head :: Nil            => return 0.0
        case head1 :: head2 :: tail => return g.weight(head1, head2) + pathLength(g, head2 :: tail)
      }
    }
    return (Nil, 0)
  }
  def graphVisTree(g: FirstGraph[SecondQuadTree]): Unit = {
    val edges = g.getEdges()
    val edgePoints = edges.map { x => 
      val color = if(x.directed) Color.Purple else if(x.weight == 0) Color.Green else Color.rgb(Math.min(255,(255 * x.weight).toInt), Math.min(255,(255 * x.weight).toInt), Math.min(255,(255 * x.weight).toInt))
      val points = Bresenham.BresenhamAlgorithm(x.n1.center, x.n2.center)
      (points, color)
    }
    AutonomousVisualisation.drawScene(1024, 1024, Nil, edgePoints, 1, "" + System.currentTimeMillis() + "Graph")
  }
  def graphVisPoint(g: FirstGraph[OccupyPoint]): Unit = {
    val edges = g.getEdges()
    val edgePoints = edges.map { x => 
      val color = if(x.directed) Color.Purple else if(x.weight == 0) Color.Green else Color.Red//Color.rgb(Math.min(255,(255 * x.weight).toInt), Math.min(255,(255 * x.weight).toInt), Math.min(255,(255 * x.weight).toInt))
      val points = Bresenham.BresenhamAlgorithm(x.n1, x.n2)
      (points, color)
    }
    AutonomousVisualisation.drawScene(1024, 1024, Nil, edgePoints, 1, "" + System.currentTimeMillis() + "Graph")
  }
}