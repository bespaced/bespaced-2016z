package template

import BeSpaceDCore._

case class PathPlusParametersPseudoGraph(path : List[OccupyPoint], limits : Option[(Int, Int, Int, Int)], start : OccupyPoint, target : OccupyPoint, obstacles : List[AbstractPolygon], maxPathLength : Int, maxRecursionSteps : Int, heuristic: (OccupyPoint, OccupyPoint) => Double)

class ForthPathBacktracking extends CoreDefinitions {
  var path: List[OccupyPoint] = Nil
  def getPath(): List[OccupyPoint] = path

  var stepCounter: Int = 0

  def Backtracking(start: OccupyPoint, target: OccupyPoint, obstacles: List[AbstractPolygon], limits: Option[(Int, Int, Int, Int)], maxPathLength: Int, maxRecursionSteps: Int, heuristic: (OccupyPoint, OccupyPoint) => Double): PathPlusParametersPseudoGraph = {
    backtrackingOnPaths(target, start :: Nil, obstacles, limits, maxPathLength, maxRecursionSteps, heuristic)
    return PathPlusParametersPseudoGraph(path, limits, start, target, obstacles, maxPathLength, maxRecursionSteps, heuristic)
  }
  def Backtracking(): PathPlusParametersPseudoGraph = {
    val limits: Option[(Int, Int, Int, Int)] = Some((0, 1000, 0, 1000))
    val scene: (OccupyPoint, OccupyPoint, List[OccupyBox]) = new FirstSceneGenerator().Generator()
    val start: OccupyPoint = scene._1
    val target: OccupyPoint = scene._2
    val obstacles: List[AbstractPolygon] = scene._3.map { x => new InvariantObstacle(x) }
    val maxRecursionSteps: Int = Integer.MAX_VALUE
    val maxPathLength: Int = Integer.MAX_VALUE
    def heuristic(a: OccupyPoint, b: OccupyPoint): Double = PointArithmetic.distance(a, scene._2) - PointArithmetic.distance(b, scene._2)
    backtrackingOnPaths(target, start :: Nil, obstacles, limits, maxPathLength, maxRecursionSteps, heuristic)
    return PathPlusParametersPseudoGraph(path, limits, start, target, obstacles, maxPathLength, maxRecursionSteps, heuristic)
  }
  def Backtracking(start: OccupyPoint, target: OccupyPoint, obstacles: List[AbstractPolygon], maxPathLength: Int, maxRecursionSteps: Int): PathPlusParametersPseudoGraph = {
    val limits: Option[(Int, Int, Int, Int)] = Some((0, 1000, 0, 1000))
    def heuristic(a: OccupyPoint, b: OccupyPoint): Double = PointArithmetic.distance(a, target) - PointArithmetic.distance(b, target)
    backtrackingOnPaths(target, start :: Nil, obstacles, limits, maxPathLength, maxRecursionSteps, heuristic)
    return PathPlusParametersPseudoGraph(path, limits, start, target, obstacles, maxPathLength, maxRecursionSteps, heuristic)
  }
  def Backtracking(maxPathLength: Int, maxRecursionSteps: Int): PathPlusParametersPseudoGraph = {
    val limits: Option[(Int, Int, Int, Int)] = Some((0, 1000, 0, 1000))
    val scene: (OccupyPoint, OccupyPoint, List[OccupyBox]) = new FirstSceneGenerator().Generator()
    val start: OccupyPoint = scene._1
    val target: OccupyPoint = scene._2
    val obstacles: List[AbstractPolygon] = scene._3.map { x => new InvariantObstacle(x) }
    def heuristic(a: OccupyPoint, b: OccupyPoint): Double = new FirstAStar().distance(a, scene._2) - new FirstAStar().distance(b, scene._2)
    backtrackingOnPaths(target, start :: Nil, obstacles, limits, maxPathLength, maxRecursionSteps, heuristic)
    return PathPlusParametersPseudoGraph(path, limits, start, target, obstacles, maxPathLength, maxRecursionSteps, heuristic)
  }
  //This method is supposed to simplify running of the backtracking, checking the result, modify the parameters, pass them back in again.
  def Backtracking(parameters : PathPlusParametersPseudoGraph) : PathPlusParametersPseudoGraph = {
    val successful : Boolean = backtrackingOnPaths(parameters.target, if(parameters.path == Nil) parameters.start::Nil else parameters.path, parameters.obstacles, parameters.limits, parameters.maxPathLength, parameters.maxRecursionSteps, parameters.heuristic)
    return PathPlusParametersPseudoGraph(path, parameters.limits, parameters.start, parameters.target, parameters.obstacles, parameters.maxPathLength, parameters.maxRecursionSteps, parameters.heuristic)
  }

  def collision(point: OccupyPoint, obstacles: List[AbstractPolygon]): Boolean = {
    return !(obstacles.forall { x => !(x.collision(point)) })
  }

  def backtrackingOnPaths(target: OccupyPoint, decisions: List[OccupyPoint], obstacles: List[AbstractPolygon], limits: Option[(Int, Int, Int, Int)], maxPathLength: Int, maxRecursionSteps: Int, heuristic: ((OccupyPoint, OccupyPoint) => Double)): Boolean = {
    stepCounter += 1
    if (stepCounter > maxRecursionSteps || maxPathLength < 0) {
      println("current recursion steps: " + stepCounter + " current remaining path length: " + maxPathLength)
      return false
    }

    if (decisions.last == target) {
      path = decisions
      return true
    } else {
      val succ: List[OccupyPoint] = limits match{
        case None => PseudoGraph.successors(decisions.last).sortWith(heuristic(_, _) < 0)
        case Some(l) => PseudoGraph.successors(decisions.last, l._1, l._2, l._3, l._4).sortWith(heuristic(_, _) < 0)
      }
      val heuristicList: List[OccupyPoint] = succ.sortWith(heuristic(_, _) < 0)
      var i: Int = 0
      while (i < heuristicList.size) {
        val nextChoice: OccupyPoint = heuristicList.apply(i)
        i = i + 1 //That's an important point...
        if (!(collision(nextChoice, obstacles)) && !(decisions.contains(nextChoice))) {
          if (backtrackingOnPaths(target, decisions ++ (nextChoice :: Nil), obstacles, limits, maxPathLength - 1, maxRecursionSteps, heuristic)) {
            return true
          }
        }
      }
      return false
    }
    return true
  }
}