package template

import BeSpaceDCore._
import scalafx.scene.paint.Color

trait PointVis {
  val core = standardDefinitions; import core._
  def Vis(inv: Invariant, colour: Color): Unit = {
    val something = simplifyInvariant(unfoldInvariant(inv))
//    println(something)
    val list : List[OccupyPoint] = something match {
      case BIGAND(x : List[OccupyPoint]) => x
      case AND(OccupyPoint(a,b),OccupyPoint(c,d)) => List(OccupyPoint(a,b),OccupyPoint(c,d))
      case OccupyPoint(a,b) => List(OccupyPoint(a,b))
      case _ => Nil
    }
    Vis(list, colour)
  }
  def Vis(list: List[OccupyPoint], colour: Color):Unit = {
    VisParameters.list = (list,colour)::VisParameters.list
  }
  def SetMagnification(factor : Double) : Unit = {
    VisParameters.magnifying = factor
  }
  def Draw(){
    Visualisation.main(Array(""))
  }
  def AutonomousDraw(){
    AutonomousVisualisation.main(Array(""))
  }
  def AbstractPolygonVis(polygon : AbstractPolygon, colour : Color) : Unit = {
    Vis(polygon.vis(), colour)
  }
  def Vis(box: OccupyBox, colour: Color): Unit = {
    VisParameters.listBoxes = (box, colour)::VisParameters.listBoxes
  }
}