package template

import BeSpaceDCore._
import scalafx.scene.paint.Color

object VisTester extends CoreDefinitions with PointVis{
  def main(args: Array[String]): Unit = {
    
    val something = simplifyInvariant(unfoldInvariant(OccupyBox(23, 32, 68, 47)))
    println(something)
    val list : List[OccupyPoint] = something match {
      case BIGAND(x : List[OccupyPoint]) => x
      case _ => Nil
    }
    println(MyParameters.colour)
    MyParameters.points = list::MyParameters.points
    MyParameters.colour = Color.Brown::MyParameters.colour
    Visualisation.main(args)
    
    val otherthing = simplifyInvariant(unfoldInvariant(OccupyBox(8,5,98,28)))
    println(otherthing)
    val list2 : List[OccupyPoint] = otherthing match{
      case BIGAND(x : List[OccupyPoint]) => x
      case _ => Nil
    }
    MyParameters.points = list2::MyParameters.points
    MyParameters.colour = Color.Azure::MyParameters.colour
    Visualisation.main(args)// java.lang.IllegalStateException: Application launch must not be called more than once
//    val vis : Visualisation = new Visualisation()
//    val vis: Visualisation = new Visualisation()
//    vis.OccupyPointVis(list, Color.Green)
//    println(vis)
  }
  
//  vis.OccupyPointVis(points, colour)
}

object MyParameters{
  var list : List[(List[OccupyPoint],Color)] = Nil //It must be a list so more than one set of points and more than one colour can be drawn.
  var colour : List[Color] = scalafx.scene.paint.Color.Green::Nil
  var points : List[List[OccupyPoint]] = (OccupyPoint(2, 4)::OccupyPoint(3,4) :: OccupyPoint(4,4) :: Nil)::Nil
}