package template

import BeSpaceDCore._
import scala.util._

/*
 * This class generates a start point and a target point plus a list of boxes in between the two.
 * The collision tests are probably no longer necessary.
 */

class FirstSceneGenerator extends RandomSceneGenerator {
  def limits() : (Int,Int,Int,Int) = (100,100,20,80)//(x-limit, y-limit, y-maxStart, y-minTarget)
  override def Generator() : (OccupyPoint, OccupyPoint, List[OccupyBox]) = {
    val numObstacles : Int = 75
    println("numObstacles: " + numObstacles)
    val obstacles : List[OccupyBox] = GenerateObstacles(numObstacles)
    
    //println(obstacles)
    return(GenerateStart(obstacles), GenerateTarget(obstacles), obstacles)
  }
  override def GenerateObstacles(n : Int) : List[OccupyBox] = {
    //return OccupyBox(2,2,4,4)::Nil
    val x1 = Random.nextInt(limits()._1)
    val x2 = Math.min(Random.nextInt(limits()._1 - x1) + x1, Math.max(x1 + (10 * Random.nextGaussian()).toInt, x1 + 1))
    val y1 = limits()._3 + Random.nextInt(limits()._4 - limits()._3)
    val y2 = Math.min(y1 + Random.nextInt(limits()._4 - y1), Math.max(y1 + 1, y1 + (10 * Random.nextGaussian()).toInt))
    if(n < 1) return Nil else return OccupyBox(x1, y1, x2, y2)::GenerateObstacles(n - 1)
  }
  def GenerateStart(obstacles : List[OccupyBox]) : OccupyPoint = {
    //return OccupyPoint(1,3)
    val point : OccupyPoint = OccupyPoint(Random.nextInt(limits()._1), Random.nextInt(limits()._3))
    if(collisionTestsBig(obstacles.map(x => unfoldInvariant(x)), unfoldInvariant(point)::Nil)) return GeneratePoints(obstacles) else return point
  }
  def GenerateTarget(obstacles : List[OccupyBox]) : OccupyPoint = {
    //return OccupyPoint(5,3)
    val point : OccupyPoint = OccupyPoint(Random.nextInt(limits()._1), Random.nextInt(limits()._2 - limits()._4) + limits()._4)
    if(collisionTestsBig(obstacles.map(x => unfoldInvariant(x)), unfoldInvariant(point)::Nil)) return GeneratePoints(obstacles) else return point
  }
}