package template
import scala.collection.mutable.PriorityQueue
import BeSpaceDCore._
import scala.collection.mutable.HashMap
import scala.math._
//import CoreDefinitions._

case class Point(prio : Double, payload : OccupyPoint) extends Ordered[Point]{
  def compare(that: Point)= (that.prio compare this.prio)
}

class FirstAStar extends Search {
  def Search(graph : FirstGraph[OccupyPoint], start : OccupyPoint, target : OccupyPoint) : (List[OccupyPoint], Double) = AStar(graph, start, target)
  var heuristic : HashMap[OccupyPoint, Double] = new HashMap[OccupyPoint, Double]()
  var openList : PriorityQueue[Point] = new PriorityQueue[Point]()
  var closedList : List[OccupyPoint] = Nil
  var predecessor : HashMap[OccupyPoint, OccupyPoint] = new HashMap[OccupyPoint, OccupyPoint]()
  var pathValue : HashMap[OccupyPoint, Double] = new HashMap[OccupyPoint, Double]()
//  var expectedValue : HashMap[OccupyPoint, Double] = new HashMap[OccupyPoint, Double]()
  
  def AStar(g : FirstGraph[OccupyPoint], start : OccupyPoint, end : OccupyPoint) : (List[OccupyPoint], Double) = {
    //Fill heuristic and initialize pathValue and expectedValue with positive (pseudo) infinity
    g.getNodes().foreach { x => heuristic.+=( (x -> distance(x, end))); pathValue.+=((x -> Int.MaxValue)) }
    //The following line was missing until 5.10.2016, 18:05 - inserted independently in FirstAStar and SecondAStar
    pathValue.+=(start -> 0)
    
    openList.+=(Point(0.0, start))
    
    while(!(openList.isEmpty)){
      val currentNode : Point = openList.dequeue()
      if(currentNode.payload == end){
        val myPath : List[OccupyPoint] = path(end)
        val length : Double = pathLength(g, myPath)
        return (myPath, length)
      }
      closedList = currentNode.payload :: closedList
      
      //Expand Node
      val successors = g.successors(currentNode.payload)
      successors.foreach { x => if(!(closedList.contains(x))){
        val currentValue = pathValue.get(currentNode.payload)
        var tentative_g : Double = 0.0
        currentValue match{
          case Some(d) => tentative_g = d + g.weight(currentNode.payload, x)
          case None => throw new NoSuchElementException
        }
//        val tentative_g : Double = pathValue.get(currentNode.payload) + g.weight(currentNode.payload, x)
        val oldValue = pathValue.get(x)
        var old_g : Double = 0.0
        oldValue match{
          case Some(d) => old_g = d
          case None => throw new NoSuchElementException
        }
        if(!(openListContains(openList, x) && tentative_g >= old_g)){
          predecessor.+=((x -> currentNode.payload))
          pathValue.+=((x -> tentative_g))
          
          val heuristicValue = heuristic.get(x)
          var expectation : Double = 0.0
          heuristicValue match{
            case Some(d) => expectation = tentative_g + d
            case None => throw new NoSuchElementException
          }
          if(openListContains(openList, x)){
            openList.map { elem => if(elem.payload == x) Point(expectation, x) else elem }//Test this!!!
//            val index = openList.findIndexOf { elem => elem.payload == x }
//            openList.update(index, Point(expectation, x))
          }
          else{
            openList.enqueue(Point(expectation, x))
          }
        }
      }
      }
    }
    (Nil, 0.0)//No Path Found
  }
  def openListContains(l : PriorityQueue[Point], o : OccupyPoint) : Boolean = {
    if(l.isEmpty) return false
    l.head match {
      case Point(d,OccupyPoint(o.x, o.y)) => return true//Does this catch all cases? It shouldn't.
      case Point(d,p) => return openListContains(l.tail, o)//Is this still unreachable? Does l.tail work as l without the head element?
    }
  }
  def distance(point1 : OccupyPoint, point2 : OccupyPoint):Double = {
    val distanceX = abs(point1.x - point2.x)
    val distanceY = abs(point1.y - point2.y)
    return sqrt(pow(distanceX,2) + pow(abs(point1.y - point2.y),2)).toDouble
  }
//  def path(end : OccupyPoint, length : Double) : (List[OccupyPoint], Double) = {
//    predecessor.get(end) match{
//      case Some(d) => return (end :: path(d, length + distance(end, d))._1, path(d, length + distance(end, d))._2)
//      case None => return (end :: Nil, length)
//    }
//  }
  def path(end : OccupyPoint) : List[OccupyPoint] = {
	  predecessor.get(end) match{
	  case Some(d) => return end :: path(d)
	  case None => return end :: Nil
	  }
  }
  def pathLength (g : FirstGraph[OccupyPoint], l : List[OccupyPoint]) : Double = {
	  l match{
	  case Nil => return 0.0
	  case head :: Nil => return 0.0
	  case head1 :: head2 :: tail => return g.weight(head1, head2) + pathLength(g, head2 :: tail)
	  }
  }  
}